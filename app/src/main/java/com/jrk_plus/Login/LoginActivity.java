package com.jrk_plus.Login;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.app.jrk_plus.R;
import com.jrk_plus.NewDashboard.ConnectActivity;
import com.jrk_plus.Tunnel.ConfigurationConstants;
import com.jrk_plus.Tunnel.CredentialResults;
import com.jrk_plus.Tunnel.Provisioning;
import com.jrk_plus.fragments.SelectedServer;
import com.jrk_plus.utilities.AppSignature;
import com.jrk_plus.utilities.CredentialCheck;
import com.jrk_plus.utilities.ExperimentPermissionClass;
import com.jrk_plus.utilities.MoreProtectedEncryption;
import com.jrk_plus.utilities.PrefManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

/**
 * A login screen that offers login via email/password.
 */

public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor>, CredentialResults,
        Provisioning.ReceiveProvInfoInterface, ExperimentPermissionClass.PermissionCallBacks,
        CompoundButton.OnCheckedChangeListener {

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private CredentialCheck credentialCheck;
    public String identifier = "";
    AsyncTask<Object, Integer, Integer> workerAsyncTask = null;
    private PrefManager prefManager;
    private String userIdToStore, passwordToStore;
    private ExperimentPermissionClass experimentPermissionClass = null;
    private Switch rememberMeSwitch;
    String username = "abcdefghijkl@google.com";
    String salt = "salty";
    private int count = 0;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public static void hideKeyboard(AppCompatActivity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById(R.id.parent).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(LoginActivity.this);
            }
        });

//        getWindow().setBackgroundDrawableResource(R.drawable.login_drawable);
//        encryptKey();
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
//        populateAutoComplete();
        prefManager = new PrefManager(this);
        experimentPermissionClass = new ExperimentPermissionClass(this, this);

        mPasswordView = (EditText) findViewById(R.id.password);
       /* mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    if(!experimentPermissionClass.checkPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE)){
                        experimentPermissionClass.requestPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE);
                    }else {
                        getImeiNo();
                        attemptLogin();
//                        if (identifier == null) {
//                            getImeiNo();
//                        } else {
//                            attemptLogin();
//                        }
                    }

                    return true;
                }
                return false;
            }
        });*/

        if (prefManager.isLogOutDone()) {
            mEmailView.setText(prefManager.getUserName());
            mPasswordView.setText(prefManager.getPassword());
        }


        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (!experimentPermissionClass.checkPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE)) {
                    experimentPermissionClass.requestPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE);
                } else {*/
                getImeiNo();
                attemptLogin();
//                    if (identifier == null) {
//                        getImeiNo();
//                    } else {
//                        attemptLogin();
//                    }
//                }
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        rememberMeSwitch = findViewById(R.id.rememberMeSwitch);
        /*if(prefManager.isRemeberMeOn()){
            rememberMeSwitch.setChecked(true);
        }else{
            rememberMeSwitch.setChecked(false);
        }*/
        rememberMeSwitch.setOnCheckedChangeListener(this);

        if (prefManager.isLoggedIn()) {
//            launchLoginScreen();
//            launchHomeFragment(false, "", "");
            launchHomeFragment();
        } else {
            mLoginFormView.setVisibility(View.VISIBLE);
        }
    }

    private void launchHomeFragment() {

        prefManager.setFirstTimeLogin(false);
//        prefManager.setMobileNumber(number);
//        prefManager.setKey(key);
        startActivity(new Intent(LoginActivity.this, ConnectActivity.class));
        LoginActivity.this.finish();
    }

//    private void populateAutoComplete() {
//        if (!mayRequestContacts()) {
//            return;
//        }
//
//        getLoaderManager().initLoader(0, null, this);
//    }

//    private boolean mayRequestContacts() {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            return true;
//        }
//        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
//            return true;
//        }
//        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
//            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
//                    .setAction(android.R.string.ok, new View.OnClickListener() {
//                        @Override
//                        @TargetApi(Build.VERSION_CODES.M)
//                        public void onClick(View v) {
//                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
//                        }
//                    });
//        } else {
//            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
//        }
//        return false;
//    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
//        if (requestCode == REQUEST_READ_CONTACTS) {
//            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                populateAutoComplete();
//            }
//        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            doLoginWork(email, email);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        String st = email.trim();
        if (st.length() > 0)
            return true;
        else
            return false;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 1;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
       /* mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });*/

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
       /* mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });*/
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    @Override
    public void getErrorCode(int errorCode) {
        Log.d("LoginActivity", "Got Error in provisining request ===" + errorCode);
        count++;
        gotErrorProvisiningResponse(errorCode);
    }

    @Override
    public void onTaskCompletion(int errorCode, SelectedServer selectedServer, Context ctx, CredentialCheck.RequestType requestType) {
        Log.d("LoginActivity", "On task complition === " + errorCode);
        if (errorCode == 0) {
            showProgress(false);
            createWorkerAsyncTask(errorCode, selectedServer, ctx, requestType);
        }
    }

    private void createWorkerAsyncTask(final int errorCode, final SelectedServer selectedServer, final Context ctx, final CredentialCheck.RequestType requestType) {
        if (workerAsyncTask == null) {
            workerAsyncTask = new AsyncTask<Object, Integer, Integer>() {
                @Override
                protected Integer doInBackground(Object[] objects) {

                    executeTask(errorCode, selectedServer, ctx, requestType);
                    return null;
                }

            };
            workerAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            workerAsyncTask.cancel(true);

            workerAsyncTask = null;
            createWorkerAsyncTask(errorCode, selectedServer, ctx, requestType);
        }
    }


    public void executeTask(int errorCode, SelectedServer selectedServer, Context ctx,
                            CredentialCheck.RequestType requestType) {
        prefManager.setIsProvisiningDone(true);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                waitingExpiryDate.setVisibility(View.GONE);
                prefManager.setIsProvisiningDone(true);
                prefManager.setUserName(userIdToStore);
                prefManager.setPassword(passwordToStore);
//                if (!prefManager.isValidityExpired()) {
//                    removeInitialisingFragment();
//                    validityText.setText(prefManager.getDeactivationDate());
//                } else {
//                    validityText.setText("Expired");
//                }
            }
        });

        if (requestType == CredentialCheck.RequestType.PROV) {
            prefManager.setFirstTimeLogin(false);
            prefManager.setIsLogin(true);
            prefManager.setIsProvisiningDone(true);
            Intent intent = new Intent(LoginActivity.this, ConnectActivity.class);
            startActivity(intent);
            LoginActivity.this.finish();
//            if(!prefManager.isValidityExpired()) {
//                gotProvisiongResponse();
//            }else{
//                gotValidityExpiredFromProvisining();
//            }
            return;
        } else if (requestType == CredentialCheck.RequestType.SELECTED_SERVER) {
//            Config config = new Config();
//            config.setLocalIp("0.0.0.0");
//            config.setLocalPort(1195);
//
//            config.setClientChildCount(1);
//            config.setClientMppc(20000);
//            config.setRemoteIp(selectedServer.ip);
//            config.setRemotePort(selectedServer.port);
//            config.setSni((String) selectedServer.sniList.get(0));
//
//            String tunnelName = "testVpn";
//            String keyFile = "";
//            String keyPass = "";
//            if (tp != null) {
//                logger.info("[TUNNEL] :: Stopping previous tunnel...");
//                tp.stop();
//            }
//
//            logger.info("[TUNNEL] :: About to start tunnel...");
//            logger.info("[TUNNEL] : " + config.getRemoteIp());
//            logger.info("[TUNNEL] : " + config.getRemotePort());
//            logger.info("[TUNNEL] : " + config.getSni());
//            tp = new TcpProxy(tunnelName, config.getLocalPort(), config.getRemoteIp(),
//                    config.getRemotePort(), keyFile, keyPass, config.getSni(), ctx);
//            try {
//                tp.serve();
//            } catch (Exception e) {
//
//            }

//            configureAndStartVpn(config, ctx);

        } else if (requestType == CredentialCheck.RequestType.EASY_VPN) {
//            fragment.prepareVpn();
        }
    }

    @Override
    public void responseReceived(boolean isError) {
        Log.d("LoginActivity", "Provisining response received == " + isError);
    }

    @Override
    public void onPermissionAllowed(int requestCode) {
        if (requestCode == ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE) {
            getImeiNo();
        }
    }

    @Override
    public void onPermissionDeny(int requestCode) {
        if (requestCode == ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE) {
            getImeiNo();
        }
    }

    @Override
    public void onDialogYesButtonIsClicked() {

    }

    @Override
    public void onDialogNoButtonIsClicked() {

    }

    @SuppressLint("HardwareIds")
    public void getImeiNo() {

        identifier = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

//        System.out.println("IMEI::" + identifier);

        /*if (!experimentPermissionClass.checkPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE)) {
            experimentPermissionClass.requestPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            identifier = telephonyManager.getDeviceId();
//            System.out.println("IMEI::" + identifier + "," + telephonyManager.getDeviceId());
        }*/
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            prefManager.setRememberMe(true);
        } else {
            prefManager.setRememberMe(false);
            prefManager.setUserName("");
            prefManager.setPassword("");
        }
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    public void doLoginWork(String email, String password) {

//        TelephonyManager tm = (TelephonyManager) LoginActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
//        if (tm != null)
//            identifier = tm.getDeviceId();
//        if (identifier == null || identifier.length() == 0)
//            identifier = Settings.Secure.getString(LoginActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);

        getImeiNo();
//        System.out.println("login:-"+email+","+password+","+identifier);
        credentialCheck = new CredentialCheck(LoginActivity.this, email, password, identifier, CredentialCheck.RequestType.PROV, null);
        this.userIdToStore = email;
        this.passwordToStore = password;
//            credentialCheck.credentialResults = HomeActivity.this;
        credentialCheck.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


//        Intent intent = new Intent(LoginActivity.this, ConnectActivity.class);
//        showProgress(false);
//        startActivity(intent);
    }

    private void gotErrorProvisiningResponse(int errorCode) {
//        System.out.println("errorCode:-" + errorCode);
//        if (count > 1)
        showProgress(false);

//        if (!parseErrorCode(errorCode).equalsIgnoreCase("Sorry! Something went wrong."))
        if (errorCode != 0)
            Toast.makeText(LoginActivity.this, parseErrorCode(errorCode), Toast.LENGTH_LONG).show();

    }

    String parseErrorCode(int errorCode) {
        switch (errorCode) {
            case ConfigurationConstants.USER_NOT_FOUND:
                return "Invalid Credentials !!";
            case ConfigurationConstants.INVALID_PASSWORD:
                return "Invalid Credentials !!";
            case ConfigurationConstants.INVALID_MESSGAE_FORMAT:
                return "Sorry! Something went wrong !!";
            case ConfigurationConstants.MESSAGE_PARSING_FAILED:
                return "Sorry! Something went wrong !!";
            case ConfigurationConstants.USERNAME_EXPIRED:
                return "User Account Expired !!";
            case ConfigurationConstants.IMEI_VALIDATION_FALIED:
                return "Account linked to another device !!";
            case ConfigurationConstants.UNSUPPORTED_VESRION_UPDATE_APP:
                return "Unsupported Version. Please update app !!";
            case ConfigurationConstants.PASSWORD_INVALID:
                return "Invalid Credentials";
            case ConfigurationConstants.MAX_ALLOWED_DEVICES_NEGATIVE:
                return "Maximum allowed devices exceeded !!";
            case ConfigurationConstants.USERID_NOT_FOUND:
                return "UserID Not Found !!";
            case ConfigurationConstants.DUPLICATE_REGISTRATION:
                return "Multiple Login Not Allowed !!";
            case ConfigurationConstants.ACTIVATION_LIMIT_REACHED:
                return "Login limit exceeded,Please contact support!!";
            case ConfigurationConstants.MISSING_APP_VERSION:
                return "Internal server error!!";
            case ConfigurationConstants.MISSING_OS_VERSION_:
                return "Internal server error!!";
            case ConfigurationConstants.MISSING_PLATFORM_TYPE_:
                return "Internal server error!!";
            case ConfigurationConstants.INVALID_VOUCHER:
                return "Invalid Voucher-Id.";
            default:
                return "Sorry! Something went wrong.";
        }
    }

    private void encryptKey() {

        Log.d("LoginActivity", "Doing Incription");
        String filename = "logcat_madeena_key.txt";
        String logFileAbsolutePath = "";
        File outputFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/logcat/");
        if (outputFile.mkdirs() || outputFile.isDirectory()) {
            logFileAbsolutePath = outputFile.getAbsolutePath() + "/" + filename;
        }
        File prevLogFile = new File(logFileAbsolutePath);
        byte[] stringTodecrypt = readFileContentFromAssetsFolder("Keys");

        try {
            MoreProtectedEncryption.saveFile(new String(stringTodecrypt), prevLogFile, MoreProtectedEncryption.generateKey(AppSignature.getAppSignature(this), username, salt));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    private byte[] readFileContentFromAssetsFolder(String fileName) {

        InputStream in = null;
        try {
            in = this.getAssets().open(fileName);
            byte[] arra = inputStreamTobyteArray(in);
            return arra;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] inputStreamTobyteArray(InputStream in) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int theByte;

        try {
            while ((theByte = in.read()) != -1) {
                buffer.write(theByte);
            }
            in.close();
            buffer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] result = buffer.toByteArray();
        return result;
    }


}

