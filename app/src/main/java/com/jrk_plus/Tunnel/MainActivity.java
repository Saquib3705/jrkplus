package com.jrk_plus.Tunnel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.jrk_plus.R;
import com.crashlytics.android.Crashlytics;
import com.jrk_plus.Login.LoginActivity;
import com.jrk_plus.TransportLayer.HttpRequestClass;
import com.jrk_plus.TransportLayer.OkktpHttpRequest;
import com.jrk_plus.TransportLayer.SimpleHttpServerConnection;
import com.jrk_plus.utilities.AppSignature;
import com.jrk_plus.utilities.ExperimentPermissionClass;
import com.jrk_plus.utilities.MessageConstatns;
import com.jrk_plus.utilities.MessageParser;
import com.jrk_plus.utilities.MoreProtectedEncryption;
import com.jrk_plus.utilities.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.fabric.sdk.android.Fabric;
import opt.packet.Packet;
import opt.utils.CustomBase64;

import static com.jrk_plus.Provisioning.Information.createRegistrarInfo;
import static com.jrk_plus.TransportLayer.OkktpHttpRequest.setSNIHost;
import static opt.log.OmLogger.logger;

public class MainActivity extends AppCompatActivity implements HttpRequestClass.HttpResponseEvents, OkktpHttpRequest.OkktpHttpResponseEvents ,ExperimentPermissionClass.PermissionCallBacks{

    public static int APP_REQUEST_CODE = 99;
    public ViewPager viewPager;
    private LinearLayout dotsLayout;
    private int[] layouts;
    private Button buttonSkip, buttonNext;
    private PrefManager prefManager;
    ProgressDialog progressDialog;
    private String token = "";
    private String noTosave = "";
    private StringBuffer provGet = new StringBuffer("AndroidAppCheckSum");
    private String imeiNo = "xoxoxoxoxooxoxo";
    private CountDownTimer counterTimer = null;
    private ExperimentPermissionClass experimentPermissionClass = null;
    String username = "abcdefghijkl@google.com";
    String salt = "salty";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);
        Fabric.with(this, new Crashlytics());
//        encryptKey();
        prefManager = new PrefManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");

        experimentPermissionClass = new ExperimentPermissionClass(this,this);

        if(!experimentPermissionClass.checkPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE)){
            experimentPermissionClass.requestPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE);
        }

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_main);
        initializeViews();

        logger.debug("MainActivity", "Is First time lounch ====" + prefManager.isFirstTimeLogin());
        if (prefManager.isLoggedIn()) {
//            launchLoginScreen();
//            launchHomeFragment(false, "", "");
            launchHomeFragment();
        }
        // }

        // layouts of all welcome sliders
//        layouts = new int[]{
//                R.layout.welcome_screen1,
//                R.layout.welcome_screen2,
//                R.layout.welcome_screen3};
//        // adding bottom dots
//        addBottomDots(0);
        // making notification bar transparent
        changeStatusBarColor();
//        WelcomeSlideAdapter myViewPagerAdapter = new WelcomeSlideAdapter(MainActivity.this, layouts);
//        viewPager.setAdapter(myViewPagerAdapter);
//        viewPager.addOnPageChangeListener(onPageChangeListener);

        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!experimentPermissionClass.checkPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE)){
                    experimentPermissionClass.requestPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE);
                }else {
                    if (imeiNo.equals("xoxoxoxoxooxoxo")) {
                        getImeiNo();
                    } else {
//                        verifyNumber();
                        openLoginActivity();
                    }
                }
            }
        });
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
//                if (current < layouts.length) {
//                    // move to next screen
//                    viewPager.setCurrentItem(current);
//                } else {
                    if(!experimentPermissionClass.checkPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE)){
                        experimentPermissionClass.requestPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE);
                    }else {
//                        if (imeiNo.equals("xoxoxoxoxooxoxo")) {
//                            getImeiNo();
//                        } else {
//                            verifyNumber();
                            openLoginActivity();
//                        }
                    }
//                }
            }
        });
    }


    private void encryptKey(){

        String filename = "logcat_madeena_key.txt";
        String logFileAbsolutePath = "";
        File outputFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/logcat/");
        if (outputFile.mkdirs() || outputFile.isDirectory()) {
            logFileAbsolutePath = outputFile.getAbsolutePath() + "/" + filename;
        }
        File prevLogFile = new File(logFileAbsolutePath);
        byte[] stringTodecrypt = readFileContentFromAssetsFolder("Keys");

        try {
            MoreProtectedEncryption.saveFile(new String(stringTodecrypt),prevLogFile,MoreProtectedEncryption.generateKey(AppSignature.getAppSignature(this),username,salt));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    private byte[] readFileContentFromAssetsFolder(String fileName){

        InputStream in = null;
        try {
            in = this.getAssets().open(fileName);
            byte[] arra = inputStreamTobyteArray(in);
            return arra;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] inputStreamTobyteArray(InputStream in){
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int theByte;

        try {
            while((theByte = in.read()) != -1)
            {
                buffer.write(theByte);
            }
            in.close();
            buffer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] result = buffer.toByteArray();
        return result;
    }

    private void getImeiNo() {
        if(!experimentPermissionClass.checkPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE)){
            experimentPermissionClass.requestPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE);
        }else{
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
         /*  logger.info("IMEI::" + telephonyManager.getDeviceId());
            System.out.println("IMEI::" + telephonyManager.getDeviceId());*/
            imeiNo = telephonyManager.getDeviceId();
        }
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[layouts.length];
        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private void launchLoginScreen() {
//        prefManager.setFirstTimeLaunch(false);
//        startActivity(new Intent(MainActivity.this, LoginSplash.class));
//        finish();
        verifyNumber();

//        sendMessageToserver("","");
    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                buttonNext.setText(getString(R.string.start));
                buttonSkip.setVisibility(View.GONE);
            } else {
                // still pages are left
                buttonNext.setText(getString(R.string.next));
                buttonSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void initializeViews() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        buttonSkip = (Button) findViewById(R.id.btn_skip);
        buttonNext = (Button) findViewById(R.id.btn_next);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        for (int i = 0, len = permissions.length; i < len; i++) {
            experimentPermissionClass.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


//        if (requestCode == 123) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                 logger.info("PLAYGROUND", "Permission has been granted");
////                sendMessageToserver(noTosave,token);
//                getImeiNo();
//
//            } else {
//                 logger.info("PLAYGROUND", "Permission has been denied or request cancelled");
////                button.setEnabled(false);
//                if (!shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)) {
//                    showMessageOKCancel("You need to grant this permission unless it will not work!",
//                            new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                  /*  requestPermissions(new String[]{Manifest.permission.SEND_SMS},
//                                            GetPermission.PERMISSIONS_REQUEST_READ_CONTACTS);*/
//                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
//                                    intent.setData(uri);
//                                    startActivityForResult(intent, PERMISSIONS_REQUEST_READ_PHONE_STATE);
//                                }
//                            });
//                }
//            }
//        }else{
//
//        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void openLoginActivity(){

        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void verifyNumber() {
//        final Intent intent = new Intent(MainActivity.this, AccountKitActivity.class);
//        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
//                new AccountKitConfiguration.AccountKitConfigurationBuilder(
//                        LoginType.PHONE,
//                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
//        // ... perform additional configuration ...
////        PhoneNumber phoneNumber = null;
////        if (contactNumber != null) {
////            String[] split = contactNumber.getText().toString().split(" ");
////            if (split.length > 1) {
////                phoneNumber = new PhoneNumber("+" + split[0], split[1], CountryCode.getCountryIso(split[0]));
////            }
////        }
////        if (phoneNumber != null)
////            configurationBuilder.setInitialPhoneNumber(phoneNumber);
//        intent.putExtra(
//                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
//                configurationBuilder.build());
//        startActivityForResult(intent, APP_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
//            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
//            String toastMessage;
//            if (loginResult.getError() != null) {
//                toastMessage = loginResult.getError().getErrorType().getMessage();
//                Toast.makeText(MainActivity.this, "Error in Authentication", Toast.LENGTH_SHORT).show();
//            } else if (loginResult.wasCancelled()) {
//                toastMessage = "Login Cancelled";
//                Toast.makeText(MainActivity.this, toastMessage, Toast.LENGTH_LONG).show();
//            } else {
//                if (loginResult.getAccessToken() != null) {
//                    getAccountDetails(loginResult.getAccessToken().getToken());
//                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
//
//                } else {
//                    toastMessage = String.format(
//                            "Success:%s...",
//                            loginResult.getAuthorizationCode().substring(0, 10));
//                }
//                // If you have an authorization code, retrieve it from
//                // loginResult.getAuthorizationCode()
//                // and pass it to your server and exchange it for an access token.
//                // Success! Start your next activity...
//                // goToMyLoggedInActivity();
//            }
//            // Surface the result to your user in an appropriate way.
////            Toast.makeText(
////                    this,
////                    toastMessage,
////                    Toast.LENGTH_LONG)
////                    .show();
        }
    }

    private void getAccountDetails(final String accessToken) {
//        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
//            @Override
//            public void onSuccess(final Account account) {
//                String accountKitId = account.getId();
//                PhoneNumber phoneNumber = account.getPhoneNumber();
//                String phoneNumberString = phoneNumber.toString();
//                noTosave = phoneNumber.getCountryCode().concat(phoneNumber.getPhoneNumber());
//                token = accessToken;
//                sendMessageToserver(noTosave, accessToken);
//            }
//
//            @Override
//            public void onError(final AccountKitError error) {
//                // Handle Error
//                logger.debug("FacebookKit", "On Error");
//
//            }
//        });
    }

    private void sendMessageToserver(final String number, final String token) {

        logger.debug("MainActivity", "Token to verify ====" + token);
        if (progressDialog != null)
            progressDialog.show();
        startCountDown();
        PackageInfo pInfo = null;
        String version = "1.0.0";
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (pInfo != null) {
            version = pInfo.versionName;
        }
        String buildVersion = Build.FINGERPRINT;
        if(buildVersion.length() > 10){
            buildVersion = buildVersion.substring(0,10);
        }
        logger.debug("Register", "Version = = " + version);
        logger.debug("Register", "Build Number == " + buildVersion);
        logger.debug("Register", "CHECKSUM == " + AppSignature.getCheckSum(MainActivity.this));

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(MessageConstatns.IMEI, imeiNo);
            jsonObject.put(MessageConstatns.BUILD_NUMBER, buildVersion);
            jsonObject.put(MessageConstatns.PLATFORM, "android");
            jsonObject.put(MessageConstatns.CHECKSUM, AppSignature.getCheckSum(MainActivity.this));
            jsonObject.put(MessageConstatns.VERSION, version);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String finalVersion = version;
        Thread domainFrontingThread = new Thread() {
            @Override
            public void run() {
                try {
//                    String validation_message = "POST /verifyNumber/" + number + "/" + token + " HTTP/1.1\r\n" +
//                            "User-Agent: androidVPN\r\n" +
//                            "Accept-Language: en-US,en;q=0.5\r\n" +
//                            "Content-Type: application/json; charset=utf-8\r\n" +
//                            "Content-Length: " +
//                            jsonObject.toString().length() +
//                            "\r\n" +
//                            "Host: social-188310.appspot.com\r\n" +
//                            "Connection: Keep-Alive\r\n" +
////                            "Accept-Encoding: gzip\r\n" +
//                            "\r\n" +
//                            jsonObject.toString();
                    CustomBase64 base64 = new CustomBase64("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_");
                    Packet packet = new Packet(jsonObject.toString().getBytes(), 0, jsonObject.toString().length());
                    Packet provPacket = new Packet(2048);
                    base64.encode(packet, 0, packet.length(), provPacket);

//                    String message = jsonObject.toString();
//                    String encodedMessage = Base64.encodeToString(message.getBytes(), Base64.DEFAULT);

                    String validation_message = "GET /verifyNumber/" + number + "/" + token + "/" +
                            provPacket.toString() +
                            " HTTP/1.1\r\n" +
                            "User-Agent: androidVPN\r\n" +
                            "Accept-Language: en-US,en;q=0.5\r\n" +
                            "Host: social-188310.appspot.com\r\n" +
                            "Connection: Keep-Alive\r\n" +
                            "Accept-Encoding: gzip\r\n" +
                            "\r\n";

                    new OkktpHttpRequest().sendDomainFrontingRequest(MainActivity.this, validation_message.getBytes(),
                            "www.facebook.com", MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        domainFrontingThread.start();

        Thread ptlsThread = new Thread() {
            @Override
            public void run() {
                try {
                    String validation_message = "POST /verifyNumber/" + number + "/" + token + " HTTP/1.1\r\n" +
                            "User-Agent: androidVPN\r\n" +
                            "Accept-Language: en-US,en;q=0.5\r\n" +
                            "Content-Type: application/json; charset=utf-8\r\n" +
                            "Content-Length: " +
                            jsonObject.toString().length() +
                            "\r\n" +
                            "Host: dashboard.madeenavpn.net\r\n" +
                            "Connection: Keep-Alive\r\n" +
                            "Accept-Encoding: gzip\r\n" +
                            "\r\n" +
                            jsonObject.toString();

                    byte[] info = createRegistrarInfo(ConfigurationConstants.MessageType.REGISTRAR, validation_message);

                    info = com.jrk_plus.Provisioning.Information.addHeader(info, "0.0.0.0",
                            0, ConfigurationConstants.PROTO_TCP, ConfigurationConstants.ACTION_FORWARD_REGISTRAR_PROXY);

                    new OkktpHttpRequest().sendPtlsRequest(MainActivity.this, info,
                            "www.facebook.com", MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

//        ptlsThread.start();

        Thread sslThread = new Thread() {
            @Override
            public void run() {
                try {
                    TrustManager[] trustAllCerts = new TrustManager[]{
                            new X509TrustManager() {
                                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                    return null;
                                }

                                public void checkClientTrusted(
                                        java.security.cert.X509Certificate[] certs, String authType) {
                                }

                                public void checkServerTrusted(
                                        java.security.cert.X509Certificate[] certs, String authType) {
                                }
                            }
                    };
                    SSLSocket st = null;
                    try {
                        SSLContext ssl_context = SSLContext.getInstance("TLS");
                        ssl_context.init(null, trustAllCerts, null);
                        final SSLSocketFactory sf = ssl_context.getSocketFactory();
                        st = (SSLSocket) sf.createSocket("dashboard.madeenavpn.net", 443);
                        setSNIHost(sf, (SSLSocket) st, "www.facebook.com");
                        ((SSLSocket) st).startHandshake();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    InputStream inputStream = null;
                    OutputStream outputStream = null;
                    try {
                        outputStream = st.getOutputStream();
                        inputStream = st.getInputStream();
                    } catch (Exception e) {

                    }

                    String validation_message = "POST /verifyNumber/" + number + "/" + token + " HTTP/1.1\r\n" +
                            "User-Agent: androidVPN\r\n" +
                            "Accept-Language: en-US,en;q=0.5\r\n" +
                            "Content-Type: application/json\r\n" +
                            "Content-Length: " +
                            jsonObject.toString().length() +
                            "\r\n" +
                            "Host: dashboard.madeenavpn.net\r\n" +
                            "Connection: Keep-Alive\r\n" +
                            "Accept-Encoding: gzip\r\n" +
                            "\r\n" +
                            jsonObject.toString();

                    try {
                        logger.info("[SSL] :: " + validation_message);
                        outputStream.write(validation_message.getBytes());
                        outputStream.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    SimpleHttpServerConnection simpleHttpServerConnection = null;
                    try {
                        simpleHttpServerConnection = new SimpleHttpServerConnection(inputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String response = simpleHttpServerConnection.readResponse();
                    logger.info("[SSL] :: " + response);

                    (MainActivity.this).okktpHttpResponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        sslThread.start();

    }

    @Override
    public void httpResponseForLogin(String response) {
        logger.debug("MainActivity", "Response == " + response);
        if (counterTimer != null)
            counterTimer.cancel();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        MessageParser messageParser = new MessageParser(response);
        if (messageParser.isJssonPresent) {
            if (!messageParser.error) {
                //Do login work
                if (messageParser.result.equals("OK")) {
                    doLoginWork(messageParser.mobileNumber, messageParser.key);
                }
            } else {
                logger.debug("MainActivity", "Error in response");
                Toast.makeText(MainActivity.this, "Error Response", Toast.LENGTH_SHORT).show();
            }
        } else {
            logger.debug("MainActivity", "Invalid Response");
        }
    }

    @Override
    public void couponAction(int responseCode ,String response) {

    }

    @Override
    public void httpErrorResponse() {
        if (counterTimer != null)
            counterTimer.cancel();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Do you want to retry ?")
                        .setTitle("Error Response");
                builder.setIcon(R.drawable.small_logo);
                // Add the buttons
                builder.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        sendMessageToserver(noTosave, token);
                    }
                });
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });
                builder.create();
                builder.show();
                
            }
        });

    }

    private void doLoginWork(String number, String key) {
        launchHomeFragment();
    }

    private void launchHomeFragment() {

        prefManager.setFirstTimeLogin(false);
//        prefManager.setMobileNumber(number);
//        prefManager.setKey(key);
        if (counterTimer != null)
            counterTimer.cancel();
//        startActivity(new Intent(MainActivity.this, ConnectActivity.class));
//        finish();
    }

    @Override
    public void okktpHttpResponse(String response) {
        if (counterTimer != null)
            counterTimer.cancel();
        if (progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        response = response.replace("\\\"", "'");
        final MessageParser messageParser = new MessageParser(response.substring(1, response.length() - 1));
        if (messageParser.isJssonPresent) {
            if (!messageParser.error) {
                //Do login work
                if (messageParser.result.equals("OK")) {
                    doLoginWork(messageParser.mobileNumber, messageParser.key);
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        logger.debug("MainActivity", "Error in response of registration");

                        if(messageParser.errorCode == 2010){
                            Toast.makeText(MainActivity.this, "INVALID IMEI NUMBER", Toast.LENGTH_SHORT).show();
                        }else if(messageParser.errorCode == 2011){
                            Toast.makeText(MainActivity.this, "INVALID MOBILE NUMBER", Toast.LENGTH_SHORT).show();
                        }else if(messageParser.errorCode == 2012){
                            Toast.makeText(MainActivity.this, "Duplicate Register,Please try after sometime.", Toast.LENGTH_SHORT).show();
                        } else if(messageParser.errorCode == 2014){
                            Toast.makeText(MainActivity.this, "CHECKSUM_VALIDATION_FAILED", Toast.LENGTH_SHORT).show();
                        }else if(messageParser.errorCode == 2015){
                            Toast.makeText(MainActivity.this, "USER_DEACTIVATED", Toast.LENGTH_SHORT).show();
                        }else if(messageParser.errorCode == 2016){
                            Toast.makeText(MainActivity.this, "USER_EXPIRED", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(MainActivity.this, "Error Response", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } else {
            logger.debug("MainActivity", "Invalid Response of registeration");
        }
    }

    @Override
    public void okktpHttpErrorResponse() {
        if (counterTimer != null)
            counterTimer.cancel();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Do you want to retry ?")
                        .setTitle("Error Response");
                builder.setIcon(R.drawable.small_logo);
                // Add the buttons
                builder.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        sendMessageToserver(noTosave, token);
                    }
                });
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });
                builder.create();
                builder.show();
            }
        });
    }


    public void startCountDown() {

        int countDownInterval = 1000;
        int finishTime = 20;
        if (counterTimer == null) {
            counterTimer = new CountDownTimer(finishTime * 1000, countDownInterval) {
                public void onFinish() {
                    //finish your activity here
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Server taking too much time  for responding..", Toast.LENGTH_LONG).show();
                }

                public void onTick(long millisUntilFinished) {

                }
            };
            counterTimer.start();
        } else {
            counterTimer.cancel();
            counterTimer = new CountDownTimer(finishTime * 1000, countDownInterval) {
                public void onFinish() {
                    //finish your activity here
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Server taking too much time  for responding..", Toast.LENGTH_LONG).show();
                }

                public void onTick(long millisUntilFinished) {
                    //called every 1 sec coz countDownInterval = 1000 (1 sec)
                }
            };
            counterTimer.start();
        }
    }

    @Override
    public void onPermissionAllowed(int requestCode) {
        if(requestCode == ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE){
            getImeiNo();
        }
//        else if(requestCode == ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_SMS ){
//            getImeiNo();
//        }
    }

    @Override
    public void onPermissionDeny(int requestCode) {
        if(requestCode == ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE){
            getImeiNo();
        }
//        else if(requestCode == ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_SMS){
//            Toast.makeText(MainActivity.this,"Auto read otp will not work",Toast.LENGTH_LONG).show();
//            getImeiNo();
////            if(!experimentPermissionClass.checkPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_SMS)){
////                experimentPermissionClass.requestPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_SMS);
////            }
//        }
    }

    @Override
    public void onDialogYesButtonIsClicked() {

    }

    @Override
    public void onDialogNoButtonIsClicked() {

    }

}
