package com.jrk_plus.Tunnel;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

import opt.packet.ReceivedPacket;
import opt.packet.SendPacket;
import opt.selector.OmInterface;

import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 4/24/2017.
 */

public class SslInterface extends OmInterface {
    public static AtomicLong connectedSocket = new AtomicLong(0);
    public static AtomicLong receivedCounter = new AtomicLong(0);
    private int nRecv = 0;
    private boolean connectionStatus = false;
    public static Config config;
    public static OutputStream streamToClient;

    public SslInterface(boolean isSecured, int special) {
        super(isSecured, special);
    }

    public static void setDetails(Config conf, OutputStream clientOutputStream) {
        config = conf;
        streamToClient = clientOutputStream;
    }

    @Override
    public void processData(ReceivedPacket rp) {
        byte[] reply;
        logger.info("---------Received Data of length [" + rp.receivedLength + "] from [" + rp.remoteAddress + "]------");
        receivedCounter.incrementAndGet();
        nRecv++;
        logger.info("******** Stats ..Connected Count [" + MultiHttpInterface.connectedSocket.get() + "] and receivedCounter [" + MultiHttpInterface.receivedCounter.get() + "]");

        logger.info(new String(rp.buffer));
        reply = Arrays.copyOfRange(rp.buffer, rp.bufferOffset, rp.bufferOffset + rp.length());

        try {
            streamToClient.write(reply, 0, reply.length);
            streamToClient.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (nRecv == config.getClientMppc())
            this.close();
    }

    @Override
    public void sendData(byte[] bytes, int i, int i1) {
        logger.info("SID [" + getId() + "] Sending Data Length [" + i1 + "]");
        SendPacket sendPacket = new SendPacket(8192);
        sendPacket.sendToIp = connectIp;
        sendPacket.sendToPort = connectPort;
        sendPacket.appendInEnd(bytes, i, i1);
        addDataForSending(sendPacket);
    }

    @Override
    public void sendData(SendPacket sp) {
        logger.info("SID [" + getId() + "] Sending Data Length [" + sp.length() + "]");
        addDataForSending(sp);
    }

    @Override
    public void receiveTimeout() {

    }

    @Override
    public void socketConnected() {
        logger.info("Connection estabished");
        this.connectionStatus = true;
    }

    @Override
    public void close() {
        super.close();
    }

    public boolean isConnected() {
        return this.connectionStatus;
    }
}
