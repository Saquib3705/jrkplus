package com.jrk_plus.utilities;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.jrk_plus.R;

import static com.jrk_plus.utilities.ExperimentPermissionClass.PERMISSIONS_REQUEST_CAMERA;
import static com.jrk_plus.utilities.ExperimentPermissionClass.PERMISSIONS_REQUEST_FINE_LOCATION;
import static com.jrk_plus.utilities.ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_CONTACT;
import static com.jrk_plus.utilities.ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE;
import static com.jrk_plus.utilities.ExperimentPermissionClass.PERMISSIONS_REQUEST_RECORD_AUDIO;
import static com.jrk_plus.utilities.ExperimentPermissionClass.PERMISSIONS_REQUEST_WRITE_CONTACT;
import static com.jrk_plus.utilities.ExperimentPermissionClass.PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;
import static com.jrk_plus.utilities.ExperimentPermissionClass.PERMISSION_REQUEST_INTERNET;


/**
 * Created by bot on 12-09-2017.
 */

public class PermissionDialog extends Dialog {

    private static final String TAG = "DialogYesNo";
    LinearLayout buttonYes;
    LinearLayout buttonNo;
    private Context context;

    public PermissionDialog(Context context, String msg, int permission_number) {
        super(context, R.style.DialogSlideAnim);
        this.context = context;
        setContentView(R.layout.permission_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setGravity(Gravity.BOTTOM);
        TextView titleText = (TextView) findViewById(R.id.titleContent);
        TextView messageDiscription = (TextView) findViewById(R.id.messageDiscription);
        buttonNo = (LinearLayout) findViewById(R.id.noButton);
        buttonYes = (LinearLayout) findViewById(R.id.yesButton);

        setCancelable(false);
        titleText.setText(msg);
        String discriptionMessage = "This permission is essentioal for app";
        switch (permission_number) {
            case PERMISSIONS_REQUEST_READ_PHONE_STATE:
                discriptionMessage = context.getString(R.string.discription_msg_allow_status_permission);
                break;
            case PERMISSIONS_REQUEST_READ_CONTACT:
                discriptionMessage = context.getString(R.string.discription_msg_allow_read_contact_permission);
                break;
            case PERMISSIONS_REQUEST_WRITE_CONTACT:
                discriptionMessage = context.getString(R.string.discription_msg_allow_status_permission);
                break;
            case PERMISSIONS_REQUEST_FINE_LOCATION:
                discriptionMessage = context.getString(R.string.discription_msg_allow_location_permission);
                break;
//            case PERMISSIONS_REQUEST_READ_SMS:
//                discriptionMessage = context.getString(R.string.discription_msg_allow_read_sms_permission);
//                break;
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                discriptionMessage = context.getString(R.string.discription_msg_allow_write_external_storage_permission);
                break;
            case PERMISSIONS_REQUEST_RECORD_AUDIO:
                discriptionMessage = context.getString(R.string.discription_msg_allow_record_audio_permission);
                break;
            case PERMISSIONS_REQUEST_CAMERA:
                discriptionMessage = context.getString(R.string.discription_msg_allow_camera_permission);
                break;
            case PERMISSION_REQUEST_INTERNET:
                discriptionMessage = context.getString(R.string.discription_msg_allow_internet_permission);
                break;
        }
        messageDiscription.setText(discriptionMessage);
    }

    public void setYesClickListener(View.OnClickListener yesClickListener) {
        buttonYes.setOnClickListener(yesClickListener);
    }

    public void setNoClickListener(View.OnClickListener noClickListener) {
        buttonNo.setOnClickListener(noClickListener);
    }
}
