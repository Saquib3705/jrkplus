package com.jrk_plus.Validity;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.jrk_plus.R;

import java.util.ArrayList;

/**
 * Created by bot on 02-12-2016.
 */

public class CouponRowAdapter extends RecyclerView.Adapter<CouponRowAdapter.MyViewHolder> {

    private Context mContext;

    private AdapterCallback mAdapterCallback;
    private ArrayList<CoupanInfo> list = new ArrayList<>();

    public CouponRowAdapter(Context mContext , ArrayList<CoupanInfo> list) {
        this.mContext = mContext;
        this.list = list;
        try {
            this.mAdapterCallback = ((AdapterCallback) mContext);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    @Override
    public CouponRowAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.extend_valididty_row_layout, parent, false);

        return new CouponRowAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CouponRowAdapter.MyViewHolder holder, int position) {
        CoupanInfo coupanInfo = list.get(position);
        configureRow(holder ,coupanInfo.getDay(),coupanInfo.getMoney());
    }

    @Override
    public int getItemCount() {
        return (list == null) ? 0 : list.size();
    }

    public  interface AdapterCallback {
        void onMethodCallback(String validityFee);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView validity_day,validity_fee;
        private Button extend_button;

        public MyViewHolder(View view) {
            super(view);

            validity_day = (TextView)view.findViewById(R.id.validity_day);
            validity_fee = (TextView)view.findViewById(R.id.validity_fee);
            extend_button = (Button)view.findViewById(R.id.extend_button);
            extend_button.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view == extend_button){
                mAdapterCallback.onMethodCallback(validity_fee.getText().toString());
            }
        }

        public TextView getValidity_day() {
            return validity_day;
        }

        public TextView getValidity_fee() {
            return validity_fee;
        }
    }

    private void configureRow(CouponRowAdapter.MyViewHolder holder , String days , String money){
        //set values in row
        holder.getValidity_day().setText(days);
        holder.getValidity_fee().setText(money);
    }

}
