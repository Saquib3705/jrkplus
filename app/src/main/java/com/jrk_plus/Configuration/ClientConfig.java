package com.jrk_plus.Configuration;

import android.content.Context;
import android.util.Log;

import com.app.jrk_plus.BuildConfig;
import com.jrk_plus.NewDashboard.ConnectActivity;
import com.jrk_plus.utilities.MoreProtectedEncryption;
import com.jrk_plus.utilities.Studio;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by ABHISHEK MAHATO on 5/8/2017.
 */

public class ClientConfig {
    private String ip;
    private String clientName;
    private String protocol;
    private int localPort;
    private String excludedRoutes;
    private String cipherMethod;
    private boolean compression;
    private boolean replay;
    private int verbLevel;
    private String subnet;


    public String getSubnet() {
        return subnet;
    }

    public void setSubnet(String subnet) {
        this.subnet = subnet;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public void setExcludedRoutes(String excludedRoutes) {
        this.excludedRoutes = excludedRoutes;
    }

    public void setCipherMethod(String cipherMethod) {
        this.cipherMethod = cipherMethod;
    }

    public int getLocalPort() {
        return localPort;
    }
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }

    public String getClientName() {
        return clientName;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getExcludedRoutes() {
        return excludedRoutes;
    }

    public String getCipherMethod() {
        return cipherMethod;
    }

    public boolean getCompression() {
        return compression;
    }

    public void setCompression(boolean compression) {
        this.compression = compression;
    }

    public boolean getReplay() {
        return replay;
    }

    public void setReplay(boolean replay) {
        this.replay = replay;
    }

    public int getVerbLevel() {
        return verbLevel;
    }

    public void setVerbLevel(int verbLevel) {
        this.verbLevel = verbLevel;
    }

    public ClientConfig(){
        /*this.setClientName("client");
        this.setProtocol("tcp");
        this.setLocalPort(1195);
        this.setExcludedRoutes("88.150.226.228");
        this.setCipherMethod("AES-256-CBC");
        this.setCompression(true);
        this.setReplay(true);
        this.setVerbLevel(3);*/
    }

    public String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public String getStringFromFile(InputStream fin) throws Exception {
//        File fl = new File(filePath);
//        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    public StringBuilder createConfig(Context context,String cert) {
        StringBuilder config = new StringBuilder();

        if(cert != null && cert.length() > 10){
            Log.e("ClientConfig","Using Key associated with server "+cert);
            config.append(cert);
        }else{
            Log.d("ClientConfig","Using Saved Key");

            config.append(this.getClientName() + "\n");
            config.append("dev tun" + "\n");
            config.append("proto " + this.getProtocol() + "\n");
            config.append("sndbuf 0" + "\n");
            config.append("rcvbuf 0" + "\n");
//            config.append("socks-proxy 213.136.92.174 1080");
//            config.append("remote " + this.getIp() + " " +"1194"  + "\n");
            config.append("remote " + this.getIp() + " " + this.getLocalPort() + "\n");
            config.append("route " + this.getExcludedRoutes() + " " + this.getSubnet() + " net_gateway" + "\n");
            Log.d("TESTINGG ", "createConfig: " + this.subnet);
            config.append("resolv-retry infinite" + "\n");
            config.append("nobind" + "\n");
            config.append("socket-flags TCP_NODELAY" + "\n");
            config.append("persist-key" + "\n");
            config.append("persist-tun" + "\n");
            config.append("remote-cert-tls server" + "\n");
            config.append("cipher " + this.getCipherMethod() + "\n");
            config.append("ncp-disable\n");

            if(this.getCompression())
                config.append("comp-lzo" + "\n");
            if(!this.getReplay())
                config.append("no-replay" + "\n");

            config.append("setenv opt block-outside-dns" + "\n");
            config.append("key-direction 1" + "\n");
            config.append("verb " + this.getVerbLevel() + "\n");

            String keys = "";
            String toBeDecrypt = "";
            InputStream in = null;
            byte[] stringTodecrypt;
            if (BuildConfig.DEBUG) {
                stringTodecrypt = readFileContentFromAssetsFolder(context, "logcat_madeena_test_key.txt");
                Studio.log("TESTINGG", "inDebugKey");
            } else {
                stringTodecrypt = readFileContentFromAssetsFolder(context, "logcat_madeena_key.txt");
                Studio.log("TESTINGG", "inReleaseKey");
            }

            try {
                byte[] decodedData = MoreProtectedEncryption.decodeFile(ConnectActivity.ourKey, stringTodecrypt);
                keys = new String(decodedData);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("MainActivity", "Not able to decode");
            }

            config.append(keys);
        }

        return config;
    }


    private byte[] readFileContentFromAssetsFolder(Context context, String fileName){

        InputStream in = null;
        try {
            in = context.getAssets().open(fileName);
            byte[] arra = inputStreamTobyteArray(in);
            return arra;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] inputStreamTobyteArray(InputStream in){
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int theByte;

        try {
            while((theByte = in.read()) != -1)
            {
                buffer.write(theByte);
            }
            in.close();
            buffer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] result = buffer.toByteArray();
        return result;
    }
}
