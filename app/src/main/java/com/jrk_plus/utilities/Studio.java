package com.jrk_plus.utilities;

import android.util.Log;

import com.app.jrk_plus.BuildConfig;


public class Studio {

    public static void log(String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, message);
        }
    }
}
