package com.jrk_plus.utilities;

/**
 * Created by bot on 08/01/2018.
 */

public class MessageConstatns {

    //Message
    public static final int DIRECT_UDP = 5;
    public static final int DIRECT_TCP = 6;
    public static final String CALLER_INFO = "CallerInfo";
    public static final String MOBILE_NUMBER = "MobileNumber";
    public static final String KEY = "key";
    public static final String MSG_TYPE = "MsgType";
    public static final String MTYPE = "MType";
    public static final String VERIFYPHONE = "VerifyPhone";
    public static final String CONF_REQ = "ConfigurationRequest";
    public static final String SER_REQ = "ServerListRequest";
    public static final String AUTH_REQ = "AuthenticationRequest";
    public static final String IS_MODE_ENABLED = "isModeEnabled";
    public static final String CONF_RES = "ConfigurationResponse";
    public static final String SER_RES = "ServerListResponse";
    public static final String AUTH_RES = "AuthenticationResponse";
    public static final String SUCCESS_OK = "OK";
    public static final String SUCCESS_FAILURE = "FAILURE";
    public static final String ERROR = "Error";
    public static final String RESULT = "Result";
    public static final String ID = "_id";
    public static final String MSG_CONTENT = "MsgContent";
    public static final boolean FAIL_RESPONSE = false;
    public static final boolean SUCCESS_RESPONSE = true;
    public static final String IMEI = "imei";
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
    public static final String COUNTRY = "country";
    public static final String APP_VERSION = "app_version";
    public static final String OS_VERSION = "os_version";
    public static final String PLATFORM_TYPE = "PlatFormType";
    public static final String SERVER_VERSION = "serverVersion";
    public static final String CUSTOMIZATION_DETAILS = "idCustomizationDetails";
    public static final String VERSION = "version";
    public static final String ISP = "isp";
    public static final String BUILD_NUMBER = "buildNumber";
    public static final String INTERNAL_VERSION = "internalversion";
    public static final String PLATFORM = "platform";
    public static final String CHECKSUM = "checksum";
    public static final String TID = "TID";
    public static final String PING_REQUEST_TYPE = "pingRequest";
    public static final String PING_RESPONSE_TYPE = "pingResponse";
    public static final String KEEP_ALIVE_TIMER = "KeepAliveTimer";
    public static final String MAX_RETRY_ATTEMPTS = "MaxRetryAttempts";
    public static final String USER_ACTIVE_DURATION = "UserActiveDuration";
    public static final String DEMO_ACT_SERVER_PROFILE = "DemoActivationServerProfile";
    public static final String DEMO_ACTIVATION_DAYS = "DemoActivationDays";
    public static final String DEMO_ACT_DAYS = "DemoActivationDays";
    public static final String PROXY_COUNT = "ProxyCount";
    public static final String SERVER_PORT = "serverport";
    public static final String SERVER_PROTOCOL = "serverprotocol";
    public static final String SERVER_IP = "serverip";
    public static final String SERVER_PRIMARY_IP = "serverprimaryip";
    public static final String SERVER_NAME = "servername";
    public static final String SNI_LIST = "snilist";
    public static final String SERVER_CERTIFICATE = "servercertificate";
    public static final String SERVER_DETAILS = "serverdetails";
    public static final String DEACTIVATION_DATE = "DeactivationDate";
    public static final String DOMAIN_NAME = "backupdomainnames";
    public static final String VALIDITY_INFO = "ValidityInfo";
    public static final String ERROR_CODE = "ErrorCode";
    public static final String CUSTOMIZATIONFLAG = "CustomizationFlag";

    //Message Id

    public static final int SERVERVERSION = 1;
    public static final int WRONG_VOUCHER_APPLIED_CONTINUOUSLY = 5 ;
    public static final int REGISTER_RESPONSE = 20000 ;
    public static final int CONFIGURATION_REQUEST = 20001;
    public static final int CONFIGURATION_REPSONSE = 20002;
    public static final int AUTHENTICATION_REQUEST = 20003;
    public static final int AUTHENTICATION_RESPONSE = 20004;
    public static final int SERVERLIST_REQUEST = 20005;
    public static final int SERVERLIST_RESPONSE = 20006;
    public static final int PING_REQUEST = 20006;
    public static final int PING_RESPONSE = 20020;


}
