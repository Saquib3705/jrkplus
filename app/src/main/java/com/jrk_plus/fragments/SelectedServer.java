package com.jrk_plus.fragments;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by bot on 22/02/2018.
 */

public class SelectedServer implements Parcelable {

    public static final Creator<SelectedServer> CREATOR = new Creator<SelectedServer>() {
        @Override
        public SelectedServer createFromParcel(Parcel in) {
            return new SelectedServer(in);
        }

        @Override
        public SelectedServer[] newArray(int size) {
            return new SelectedServer[size];
        }
    };

    public String ip = "";
    public int port ;
    public int protocol;
    public ArrayList sniList;
    public String cert = "";

    public SelectedServer(String ip,int port,int protocol,ArrayList<String> sniList,String cert) {
        this.ip = ip;
        this.port = port;
        this.sniList = sniList;
        this.protocol = protocol;
        this.cert = cert;

    }

    protected SelectedServer(Parcel in) {
        ip = in.readString();
        port = in.readInt();
        protocol = in.readInt();
        sniList = in.readArrayList(String.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(ip);
        parcel.writeInt(port);
        parcel.writeInt(protocol);
        parcel.writeList(sniList);

    }
}
