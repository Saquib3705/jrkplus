package com.jrk_plus.Tunnel;

import android.util.Log;

import com.jrk_plus.DnsClass.DNSHeader;
import com.jrk_plus.DnsClass.DNSRecord;
import com.jrk_plus.DnsClass.DnsClientJava;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import static com.jrk_plus.DnsClass.DnsClientJava.T_AAAA;
import static com.jrk_plus.DnsClass.Ipv4ToIpv6Wrapping.unwrapIpv6ToIpv4;
import static com.jrk_plus.Tunnel.ProxySSL.printHexDump;
import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 3/22/2017.
 */

public class DnsResolverAPI {
    private static final String CHAR_LIST = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    private byte[] info;
    public static int dnsApiIndex = 1;
    public static int dnsNewDomainIndex = 1;
    String ip;

    public int getRandomNumber() {
        int randomInt = 0;
        try {
            Random randomGenerator = new Random();
            randomInt = randomGenerator.nextInt(CHAR_LIST.length());
            if (randomInt == 0) {
                return randomInt;
            } else {
                return randomInt - 1;
            }
        } catch (Exception e) {
            return 1;
        }
    }

    public String getRandomString(int numOfCharactors) {
        try {
            StringBuffer randStr = new StringBuffer();
            for (int i = 0; i < numOfCharactors; i++) {
                int number = getRandomNumber();
                char ch = CHAR_LIST.charAt(number);
                randStr.append(ch);
            }
            return randStr.toString();
        } catch (Exception e) {
            return "12345";
        }

    }

    public static String print_content(HttpsURLConnection con) {
        StringBuilder response = new StringBuilder();
        if (con != null) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String input;
                while ((input = br.readLine()) != null) {
                    response.append(input);
                }
                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return response.toString();
    }

    public static Set<String> resolveAddress(String domain, byte recordType) {
        try {
//            StringBuilder ipList = new StringBuilder();
            Set<String> ipSet = new HashSet<String>();
            Class<?> SystemProperties = Class.forName("android.os.SystemProperties");
            Method method = SystemProperties.getMethod("get", new Class[]{String.class});
            ArrayList<String> servers = new ArrayList<String>();
            for (String name : new String[]{"net.dns1", "net.dns2", "net.dns3", "net.dns4",}) {
                String value = (String) method.invoke(null, name);
                if (value != null && !"".equals(value) && !servers.contains(value)) {
                    servers.add(value);
                }
            }
            List<String> nameservers = servers;
            nameservers.add("8.8.8.8");
            nameservers.add("8.8.4.4");
            String ipv4;
             logger.info("ng", "Number of servers : " + nameservers.size());
            for (String ns : nameservers) {
                DnsClientJava dnsReq = new DnsClientJava();
                DNSHeader result = dnsReq.DnsQuery(domain, recordType, ns);

                if (result == null) {
                     logger.info("ng", "Result is null.");
                    continue;
                }
                for (DNSRecord record : result.ANRecord) {
                    System.out.println("ipv6 address:  " + printHexDump(record.Data, record.Data.length));
                    if (recordType == T_AAAA) {
                        String ipv6 = Inet6Address.getByAddress(record.Data).getHostAddress();
                        ipv4 = unwrapIpv6ToIpv4(ipv6);
                    } else {
                        ipv4 = InetAddress.getByAddress(record.Data).getHostAddress();
                    }
                    if (ipv4 != null) {
                         logger.info("ng", "IPV4 address resolved from " + domain + " : " + ipv4);
//                        return ipv4;
                        ipSet.add(ipv4);
                    }
                }
            }
            return ipSet;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public DnsResolverAPI(String ip) {
        this.ip = ip;
    }

    public String getVpnProxyIp() {
        JSONArray answers = null;
        try {
                URL url = new URL("https://dns.google.com/resolve?name=" + this.ip + "&type=A&dnssec=false");
                HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
                con.setConnectTimeout(10000);
                con.setReadTimeout(10000);

                String response = print_content(con);
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(response);
                JSONObject jsonObject = (JSONObject) obj;

                answers = (JSONArray) jsonObject.get("Answer");
        } catch (Exception e) {
             logger.info("ng", Log.getStackTraceString(e));
            return null;
        }

        if (answers != null) {
             logger.info("ng", "Answer Section is not null");
            String ip = null;

            Iterator<JSONObject> iterator = answers.iterator();
            Long type;
            while (iterator.hasNext()) {
                JSONObject ans = iterator.next();
                type = (Long) ans.get("type");
                if (type != 1)
                    continue;
                ip = (String) ans.get("data");
                 logger.info("ng", "Resolved IP : " + ip);
                return ip;
            }
        }
        return null;
    }


    public static String getVpnProxyIp(String ipAddress) {
        JSONArray answers = null;
        try {
            URL url = new URL("https://dns.google.com/resolve?name=" + ipAddress + "&type=A&dnssec=false");
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setConnectTimeout(10000);
            con.setReadTimeout(10000);

            String response = print_content(con);
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(response);
            JSONObject jsonObject = (JSONObject) obj;

            answers = (JSONArray) jsonObject.get("Answer");
        } catch (Exception e) {
            logger.info("ng", Log.getStackTraceString(e));
            return null;
        }

        if (answers != null) {
            logger.info("ng", "Answer Section is not null");
            String ip = null;

            Iterator<JSONObject> iterator = answers.iterator();
            Long type;
            while (iterator.hasNext()) {
                JSONObject ans = iterator.next();
                type = (Long) ans.get("type");
                if (type != 1)
                    continue;
                ip = (String) ans.get("data");
                logger.info("ng", "Resolved IP : " + ip);
                return ip;
            }
        }
        return null;
    }

}
