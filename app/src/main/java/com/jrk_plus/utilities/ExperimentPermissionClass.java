package com.jrk_plus.utilities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.app.jrk_plus.R;


/**
 * Created by bot on 14-09-2017.
 */

public class ExperimentPermissionClass {
    public static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;
    public static final int PERMISSIONS_REQUEST_READ_CONTACT = 2;
    public static final int PERMISSIONS_REQUEST_WRITE_CONTACT = 3;
    public static final int PERMISSIONS_REQUEST_FINE_LOCATION = 4;
//    public static final int PERMISSIONS_REQUEST_READ_SMS = 5;
    public static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 6;
    public static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 7;
    public static final int PERMISSIONS_REQUEST_CAMERA = 8;
    public static final int PERMISSION_REQUEST_INTERNET = 9;
    private static final String TAG = ExperimentPermissionClass.class.getName();
    private static final String PERMISSION_PREFERENCES = "permission_preferences";
    private static final String PREFERENCES_PERMISSION_PHONE_STATUS = "permission_phone_status";
    private static final String PREFERENCES_PERMISSION_PHONE_CONTACT = "permission_phone_contact";
    private static final String PREFERENCES_PERMISSION_FINE_LOCATION = "permission_phone_location";
//    private static final String PREFERENCES_PERMISSION_READ_SMS = "permission_phone_read_sms";
//    private static final String PREFERENCES_PERMISSION_WRITE_SMS = "permission_phone_write_sms";
    private static final String PREFERENCES_PERMISSION_WRITE_EXTERNAL_STORAGE = "permission_phone_write_external_storage";
    private static final String PREFERENCES_PERMISSION_RECORD_AUDIO = "permission_phone_write_record_audio";
    private static final String PREFERENCES_PERMISSION_CAMERA = "permission_phone_camera";
    private static final String PREFERENCES_PERMISSION_INTERNET = "permission_phone_camera";
    //Fragment fragment;
    AppCompatActivity activity;
    PermissionCallBacks permissionCallBacks;
    String permission;
    PermissionDialog dialog = null;
    private int permissionType;

    public ExperimentPermissionClass(AppCompatActivity activity, PermissionCallBacks permissionCallBacks) {

        this.activity = activity;
        this.permissionCallBacks = permissionCallBacks;
    }

    public boolean checkPermission(int requestPermission) {
        switch (requestPermission) {
            case PERMISSIONS_REQUEST_READ_PHONE_STATE:
                permission = Manifest.permission.READ_PHONE_STATE;
                break;
            case PERMISSIONS_REQUEST_READ_CONTACT:
                permission = Manifest.permission.READ_CONTACTS;
                break;
            case PERMISSIONS_REQUEST_WRITE_CONTACT:
                permission = Manifest.permission.WRITE_CONTACTS;
                break;
            case PERMISSIONS_REQUEST_FINE_LOCATION:
                permission = Manifest.permission.ACCESS_FINE_LOCATION;
                break;
//            case PERMISSIONS_REQUEST_READ_SMS:
//                permission = Manifest.permission.READ_SMS;
//                break;
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                break;
            case PERMISSIONS_REQUEST_RECORD_AUDIO:
                permission = Manifest.permission.RECORD_AUDIO;
                break;
            case PERMISSIONS_REQUEST_CAMERA:
                permission = Manifest.permission.CAMERA;
                break;
            case PERMISSION_REQUEST_INTERNET:
                permission = Manifest.permission.INTERNET;
                break;
        }

        return ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission(int permissionRequest) {
        // Should we show an explanation?
        this.permissionType = permissionRequest;
        switch (permissionType) {
            case PERMISSIONS_REQUEST_READ_PHONE_STATE:
                permission = Manifest.permission.READ_PHONE_STATE;
                break;
            case PERMISSIONS_REQUEST_READ_CONTACT:
                permission = Manifest.permission.READ_CONTACTS;
                break;
            case PERMISSIONS_REQUEST_WRITE_CONTACT:
                permission = Manifest.permission.WRITE_CONTACTS;
                break;
            case PERMISSIONS_REQUEST_FINE_LOCATION:
                permission = Manifest.permission.ACCESS_FINE_LOCATION;
                break;
//            case PERMISSIONS_REQUEST_READ_SMS:
//                permission = Manifest.permission.READ_SMS;
//                break;
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                break;
            case PERMISSIONS_REQUEST_RECORD_AUDIO:
                permission = Manifest.permission.RECORD_AUDIO;
                break;
            case PERMISSIONS_REQUEST_CAMERA:
                permission = Manifest.permission.CAMERA;
                break;
            case PERMISSION_REQUEST_INTERNET:
                permission = Manifest.permission.INTERNET;
                break;
        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            ActivityCompat.requestPermissions(activity, new String[]{permission},
                    permissionType);
        } else if (isPermissionAsked()) {
            showAppSettingsDialog();
        } else {
//            savePermissionAsked(true);
            ActivityCompat.requestPermissions(activity, new String[]{permission},
                    permissionType);
        }
    }

    private void savePermissionAsked(boolean value) {
        SharedPreferences sharedPref = activity.getSharedPreferences(
                PERMISSION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        switch (permissionType) {
            case PERMISSIONS_REQUEST_READ_PHONE_STATE:
                editor.putBoolean(PREFERENCES_PERMISSION_PHONE_STATUS, value);
                break;
            case PERMISSIONS_REQUEST_READ_CONTACT:
                editor.putBoolean(PREFERENCES_PERMISSION_PHONE_CONTACT, value);
                break;
            case PERMISSIONS_REQUEST_WRITE_CONTACT:
                editor.putBoolean(PREFERENCES_PERMISSION_PHONE_CONTACT, value);
                break;
            case PERMISSIONS_REQUEST_FINE_LOCATION:
                editor.putBoolean(PREFERENCES_PERMISSION_FINE_LOCATION, value);
                break;
//            case PERMISSIONS_REQUEST_READ_SMS:
//                editor.putBoolean(PREFERENCES_PERMISSION_READ_SMS, value);
//                break;
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                editor.putBoolean(PREFERENCES_PERMISSION_WRITE_EXTERNAL_STORAGE, value);
                break;
            case PERMISSIONS_REQUEST_RECORD_AUDIO:
                editor.putBoolean(PREFERENCES_PERMISSION_RECORD_AUDIO, value);
                break;
            case PERMISSIONS_REQUEST_CAMERA:
                editor.putBoolean(PREFERENCES_PERMISSION_CAMERA, value);
                break;
            case PERMISSION_REQUEST_INTERNET:
                editor.putBoolean(PREFERENCES_PERMISSION_INTERNET, value);
                break;
        }
        editor.apply();
        editor.commit();
    }

//    public void showPermisionReason() {
//        //dismissing previuos dialog
//        if (dialog != null && dialog.isShowing()) {
//            dialog.dismiss();
//        }
//        int permission_number = 0;
//        String msg = null;
//        switch (permissionType) {
//            case PERMISSIONS_REQUEST_READ_PHONE_STATE:
//                permission_number = PERMISSIONS_REQUEST_READ_PHONE_STATE;
//                msg = activity.getString(R.string.msg_allow_status_permission);
//                break;
//            case PERMISSIONS_REQUEST_READ_CONTACT:
//                permission_number = PERMISSIONS_REQUEST_READ_CONTACT;
//                msg = activity.getString(R.string.msg_allow_read_contact_permission);
//                break;
//            case PERMISSIONS_REQUEST_WRITE_CONTACT:
//                permission_number = PERMISSIONS_REQUEST_WRITE_CONTACT;
//                msg = activity.getString(R.string.msg_allow_write_contact_permission);
//                break;
//            case PERMISSIONS_REQUEST_FINE_LOCATION:
//                permission_number = PERMISSIONS_REQUEST_FINE_LOCATION;
//                msg = activity.getString(R.string.msg_allow_location_permission);
//                break;
//            case PERMISSIONS_REQUEST_READ_SMS:
//                permission_number = PERMISSIONS_REQUEST_READ_SMS;
//                msg = activity.getString(R.string.msg_allow_read_sms_permission);
//                break;
//            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
//                permission_number = PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;
//                msg = activity.getString(R.string.msg_allow_write_external_storage_permission);
//                break;
//            case PERMISSIONS_REQUEST_RECORD_AUDIO:
//                permission_number = PERMISSIONS_REQUEST_RECORD_AUDIO;
//                msg = activity.getString(R.string.msg_allow_record_audio_permission);
//                break;
//            case PERMISSIONS_REQUEST_CAMERA :
//                permission_number = PERMISSIONS_REQUEST_CAMERA;
//                msg = activity.getString(R.string.msg_allow_camera_permission);
//                break;
//
//            case PERMISSION_REQUEST_INTERNET :
//                permission_number = PERMISSION_REQUEST_INTERNET;
//                msg = activity.getString(R.string.msg_allow_internet_permission);
//                break;
//        }
//
//        final int requestCode = permission_number;
//        dialog = new PermissionDialog(activity,msg,permission_number);
//        dialog.setYesClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                if(!isPermissionAsked()) {
//                    ActivityCompat.requestPermissions(activity, new String[]{permission},
//                            permissionType);
//                }else{
//                    showAppSettingsDialog();
//                }
//            }
//        });
//        dialog.setNoClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                if (permissionCallBacks != null) {
//                    permissionCallBacks.onPermissionDeny(requestCode);
//                }
//            }
//        });
//        dialog.show();
//    }

    public void showAppSettingsDialog() {
        //dismissing previuos dialog
        closeDialogIfOpen();
        int permission_number = 0;
        String msg = null;
        switch (permissionType) {
            case PERMISSIONS_REQUEST_READ_PHONE_STATE:
                permission_number = PERMISSIONS_REQUEST_READ_PHONE_STATE;
                msg = activity.getString(R.string.msg_allow_status_permission);
                // msg = activity.getString(R.string.msg_allow_status_permission) + "\n\n" + activity.getString(R.string.open_app_settings_msg_for_status);
                break;
            case PERMISSIONS_REQUEST_READ_CONTACT:
                permission_number = PERMISSIONS_REQUEST_READ_CONTACT;
                msg = activity.getString(R.string.msg_allow_read_contact_permission);
                //msg = activity.getString(R.string.msg_allow_contact_permission) + "\n\n" + activity.getString(R.string.open_app_settings_msg_for_contact);
                break;
            case PERMISSIONS_REQUEST_WRITE_CONTACT:
                permission_number = PERMISSIONS_REQUEST_WRITE_CONTACT;
                msg = activity.getString(R.string.msg_allow_write_contact_permission);
                // msg = activity.getString(R.string.msg_allow_contact_permission) + "\n\n" + activity.getString(R.string.open_app_settings_msg_for_contact);
                break;
            case PERMISSIONS_REQUEST_FINE_LOCATION:
                permission_number = PERMISSIONS_REQUEST_FINE_LOCATION;
                msg = activity.getString(R.string.msg_allow_location_permission);
                //msg = activity.getString(R.string.msg_allow_location_permission) + "\n\n" + activity.getString(R.string.open_app_settings_msg_for_location);
                break;
//            case PERMISSIONS_REQUEST_READ_SMS:
//                permission_number = PERMISSIONS_REQUEST_READ_SMS;
//                msg = activity.getString(R.string.msg_allow_read_sms_permission);
                // msg = activity.getString(R.string.msg_allow_read_sms_permission) + "\n\n" + activity.getString(R.string.open_app_settings_msg_for_read_sms);
//                break;
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                permission_number = PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;
                msg = activity.getString(R.string.msg_allow_write_external_storage_permission);
                //msg = activity.getString(R.string.msg_allow_write_external_storage_permission) + "\n\n" + activity.getString(R.string.open_app_settings_msg_for_write_external_storage);
                break;
            case PERMISSIONS_REQUEST_RECORD_AUDIO:
                permission_number = PERMISSIONS_REQUEST_RECORD_AUDIO;
                msg = activity.getString(R.string.msg_allow_record_audio_permission);
                // msg = activity.getString(R.string.msg_allow_record_audio_permission) + "\n\n" + activity.getString(R.string.open_app_settings_msg_for_record_audio);
                break;
            case PERMISSIONS_REQUEST_CAMERA:
                permission_number = PERMISSIONS_REQUEST_CAMERA;
                msg = activity.getString(R.string.msg_allow_camera_permission);
                //msg = activity.getString(R.string.msg_allow_camera_permission);
                break;
            case PERMISSION_REQUEST_INTERNET:
                permission_number = PERMISSION_REQUEST_INTERNET;
                msg = activity.getString(R.string.msg_allow_internet_permission);
                break;
        }
        final int requestCode = permission_number;
        dialog = new PermissionDialog(activity, msg, permission_number);
        dialog.setYesClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permissionCallBacks != null) {
                    permissionCallBacks.onDialogYesButtonIsClicked();
                }
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setData(Uri.parse("package:" + activity.getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                activity.startActivity(intent);
                if(!activity.isFinishing())
                dialog.dismiss();
            }
        });
        dialog.setNoClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!activity.isFinishing())
                dialog.dismiss();
                if (permissionCallBacks != null) {
                    permissionCallBacks.onPermissionDeny(requestCode);
                }
            }
        });

        dialog.show();
    }

    private boolean isPermissionAsked() {
        SharedPreferences sharedPref = activity.getSharedPreferences(
                PERMISSION_PREFERENCES, Context.MODE_PRIVATE);
        switch (permissionType) {
            case PERMISSIONS_REQUEST_READ_PHONE_STATE:
                return sharedPref.getBoolean(PREFERENCES_PERMISSION_PHONE_STATUS, false);
            case PERMISSIONS_REQUEST_READ_CONTACT:
                return sharedPref.getBoolean(PREFERENCES_PERMISSION_PHONE_CONTACT, false);
            case PERMISSIONS_REQUEST_WRITE_CONTACT:
                return sharedPref.getBoolean(PREFERENCES_PERMISSION_PHONE_CONTACT, false);
            case PERMISSIONS_REQUEST_FINE_LOCATION:
                return sharedPref.getBoolean(PREFERENCES_PERMISSION_FINE_LOCATION, false);
//            case PERMISSIONS_REQUEST_READ_SMS:
//                return sharedPref.getBoolean(PREFERENCES_PERMISSION_READ_SMS, false);
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                return sharedPref.getBoolean(PREFERENCES_PERMISSION_WRITE_EXTERNAL_STORAGE, false);
            case PERMISSIONS_REQUEST_RECORD_AUDIO:
                return sharedPref.getBoolean(PREFERENCES_PERMISSION_RECORD_AUDIO, false);
            case PERMISSIONS_REQUEST_CAMERA:
                return sharedPref.getBoolean(PREFERENCES_PERMISSION_CAMERA, false);
            case PERMISSION_REQUEST_INTERNET:
                return sharedPref.getBoolean(PREFERENCES_PERMISSION_INTERNET, false);
        }
        return false;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission was granted, yay! Do the
            // contacts-related task you need to do.
            savePermissionAsked(true);
            if (permissionCallBacks != null) {
                permissionCallBacks.onPermissionAllowed(requestCode);
            }
        } else if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED){
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
            savePermissionAsked(true);
            boolean showRationale = activity.shouldShowRequestPermissionRationale(permission);
            if (!showRationale) {
                //It means never ask again checkbox is checked so we have to take appropriate steps
                showAppSettingsDialog();
            } else {
                if (permissionCallBacks != null) {
                    permissionCallBacks.onPermissionDeny(requestCode);
                }
            }
        }
        return;
    }

    public void closeDialogIfOpen() {
        if (!activity.isFinishing() && dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public interface PermissionCallBacks {
        void onPermissionAllowed(int requestCode);

        void onPermissionDeny(int requestCode);

        void onDialogYesButtonIsClicked();

        void onDialogNoButtonIsClicked();
    }
}
