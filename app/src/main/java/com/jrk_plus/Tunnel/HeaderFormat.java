package com.jrk_plus.Tunnel;

import opt.packet.Packet;
import opt.utils.Utils;

/**
 * Created by ABHISHEK MAHATO on 4/24/2017.
 */

public class HeaderFormat {

    public static final int HEADER_FORMAT_SIZE = 8;

    public enum ActionType {
        ACTION_NONE(0),
        ACTION_ECHO_INSTANT(1),
        ACTION_DROP(2),
        ACTION_FORWARD(3),
        ACTION_ECHO_AFTER_TIME(4),
        ACTION_FORWARD_PROVISIONING_PROXY(5),
        ACTION_FORWARD_BALANCE_PROXY(6),
        ACTION_FORWARD_SIGNALING_PROXY(7);
        public final int action;

        private ActionType(int ac) {
            action = ac;
        }

        public static ActionType getActionType(byte ty) {
            for (ActionType actionType : ActionType.values()) {
                if (ty == actionType.action) {
                    return actionType;
                }
            }
            return ACTION_NONE;
        }
    }

    public enum ProtocolType {
        PROTOCOL_NONE(0),
        PROTOCOL_UDP_SOCKET(1),
        PROTOCOL_TCP_SOCKET(2),
        PROTOCOL_SSL_SOCKET(3),
        PROTOCOL_HTTP_SOCKET(4),
        PROTOCOL_HTTPS_SOCKET(5),
        PROTOCOL_PSEUDO_SSL_SOCKET(6);
        public final int protocol;

        private ProtocolType(int p) {
            protocol = p;
        }

        public static ProtocolType getProtocolType(byte ty) {
            for (ProtocolType protocolType : ProtocolType.values()) {
                if (ty == protocolType.protocol) {
                    return protocolType;
                }
            }
            return PROTOCOL_NONE;
        }

    }
    private static final int IP_INDEX = 0;
    private static final int PORT_INDEX = 4;
    private static final int PROTOCOL_INDEX = 6;
    private static final int ACTION_INDEX = 7;

    /**
     * 4 byte -> IP 2 byte -> Port 1 byte -> Protocol 1 byte -> Action
     */
    public static String parseIp(byte[] data) {
        if (data.length < PORT_INDEX) {
            return null;
        }
        return Utils.fourByteToIp(data, 0);
    }

    public static String parseIp(Packet packet) {
        if (packet.length() < PORT_INDEX) {
            return null;
        }
        return Utils.fourByteToIp(packet.buffer, packet.bufferOffset + IP_INDEX);
    }

    public static int parsePort(byte[] data) {
        if (data.length <= PROTOCOL_INDEX) {
            return -1;
        }
        return Utils.twoByteToInt(data, 4);
    }

    public static int parsePort(Packet packet) {
        if (packet.length() < PROTOCOL_INDEX) {
            return -1;
        }
        return Utils.twoByteToInt(packet.buffer, packet.bufferOffset + PORT_INDEX);
    }

    public static ProtocolType parseProtocol(byte[] data) {
        if (data.length < PROTOCOL_INDEX) {
            return ProtocolType.PROTOCOL_NONE;
        }
        return ProtocolType.getProtocolType(data[PROTOCOL_INDEX]);
    }

    public static ProtocolType parseProtocol(Packet packet) {
        if (packet.length() < PROTOCOL_INDEX) {
            return ProtocolType.PROTOCOL_NONE;
        }
        return ProtocolType.getProtocolType(packet.buffer[packet.bufferOffset + PROTOCOL_INDEX]);
    }

    public static ActionType parseAction(byte[] data) {
        if (data.length < ACTION_INDEX) {
            return ActionType.ACTION_NONE;
        }
        return ActionType.getActionType(data[ACTION_INDEX]);
    }

    public static ActionType parseAction(Packet packet) {
        if (packet.length() < ACTION_INDEX) {
            return ActionType.ACTION_NONE;
        }
        return ActionType.getActionType(packet.buffer[packet.bufferOffset + ACTION_INDEX]);
    }
}