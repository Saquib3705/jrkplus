package com.jrk_plus.Tunnel;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 5/30/2017.
 */

public class CrashReportGenerator implements Runnable, LoggerQueue {
    File file;
    OutputStream outToFile = null;

    public CrashReportGenerator() {
        String filePath =  Environment.getExternalStorageDirectory() + File.separator + "vpn-log.txt";
         logger.info("TEST", "FILE PATH : " + filePath);
        file = new File(filePath);
        try {
            file.createNewFile();
            outToFile = new FileOutputStream(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (!queue.isEmpty()){
                    if(file.exists()) {
                        if(outToFile != null)
                            outToFile.write(queue.take().toString().getBytes());
                        outToFile.write("\n".getBytes());
                    }
                }
                     logger.info("TEST", queue.take().toString());
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
