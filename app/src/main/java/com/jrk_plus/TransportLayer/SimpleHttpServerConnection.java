package com.jrk_plus.TransportLayer;
/**
 * Created by ABHISHEK MAHATO on 2/9/2018.
 */

/*
 * $Header: /home/data/cvs/rt/org.eclipse.ecf/tests/bundles/org.eclipse.ecf.tests.apache.httpclient.server/src/org/apache/commons/httpclient/server/SimpleHttpServerConnection.java,v 1.1 2009/02/13 18:07:51 slewis Exp $
 * $Revision: 1.1 $
 * $Date: 2009/02/13 18:07:51 $
 *
 * ====================================================================
 *
 *  Copyright 1999-2004 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

import static opt.log.OmLogger.logger;

/**
 * A connection to the SimpleHttpServer.
 *
 * @author Christian Kohlschuetter
 * @author Oleg Kalnichevski
 */
public class SimpleHttpServerConnection {

    private static final String HTTP_ELEMENT_CHARSET = "US-ASCII";

    private Socket socket = null;
    private InputStream in = null;
    private OutputStream out = null;
    private boolean keepAlive = false;
    private int responseCode = 404;

    public int getResponseCode() {
        return responseCode;
    }

    public SimpleHttpServerConnection(InputStream inputStream)
            throws IOException {
        this.in = inputStream;
    }

    public synchronized void close() {
        try {
            if (socket != null) {
                in.close();
                out.close();
                socket.close();
                socket = null;
            }
        } catch (IOException e) {
        }
    }

    public synchronized boolean isOpen() {
        return this.socket != null;
    }

    public void setKeepAlive(boolean b) {
        this.keepAlive = b;
    }

    public boolean isKeepAlive() {
        return this.keepAlive;
    }

    public InputStream getInputStream() {
        return this.in;
    }

    public OutputStream getOutputStream() {
        return this.out;
    }

    /**
     * Returns the ResponseWriter used to write the output to the socket.
     *
     * @return This connection's ResponseWriter
     */
//    public ResponseWriter getWriter() throws UnsupportedEncodingException {
//        return new ResponseWriter(out);
//    }
//
//    public SimpleRequest readRequest() throws IOException {
//        try {
//            String line = null;
//            do {
//                line = HttpParser.readLine(in, HTTP_ELEMENT_CHARSET);
//            } while (line != null && line.length() == 0);
//
//            if (line == null) {
//                setKeepAlive(false);
//                return null;
//            }
//            SimpleRequest request = new SimpleRequest(
//                    RequestLine.parseLine(line),
//                    HttpParser.parseHeaders(this.in, HTTP_ELEMENT_CHARSET),
//                    this.in);
//            return request;
//        } catch (IOException e) {
//            close();
//            throw e;
//        }
//    }
    public Map<String, String> parseHTTPHeaders()
            throws IOException {
        int charRead;
        StringBuffer sb = new StringBuffer();
        while (true) {
            sb.append((char) (charRead = in.read()));
            if ((char) charRead == '\r') {            // if we've got a '\r'
                sb.append((char) in.read()); // then write '\n'
                charRead = in.read();        // read the next char;
                if (charRead == '\r') {                  // if it's another '\r'
                    sb.append((char) in.read());// write the '\n'
                    break;
                } else {
                    sb.append((char) charRead);
                }
            }
        }

        String[] headersArray = sb.toString().split("\r\n");
        Map<String, String> headers = new HashMap<>();
        for (int i = 1; i < headersArray.length - 1; i++) {
            headers.put(headersArray[i].split(": ")[0],
                    headersArray[i].split(": ")[1]);
        }

        this.responseCode = Integer.parseInt(headersArray[0].split(" ")[1]);
        logger.info("[HEADER] : " + sb.toString());
        return headers;
    }

    public String parseResponse(int content_length) {
        char charRead;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < content_length; i++) {
            try {
                charRead = (char) in.read();
                sb.append(charRead);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public String readResponse() {
        try {
            String response = null;
            Map<String, String> headers = parseHTTPHeaders();
            String chunked = headers.get("Transfer-Encoding");
            if (chunked == null) {
                int content_length = Integer.parseInt(headers.get("Content-Length"));
                logger.info("[HEADER] : Content-Length : " + content_length);
                logger.info("[HEADER] : Not Chunked");
                response = parseResponse1(content_length);
            } else {
                logger.info("[HEADER] : Chunked");
                response = parseChunkedResponse1();
            }
            logger.info("[HEADER] : " + response);
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String parseResponse1(int content_length) {
        int bytesRead = 0;
        StringBuilder sb = new StringBuilder(content_length + 50);
        byte[] partialBuffer = new byte[4096];
        int len;

        while (bytesRead < content_length) {
            try {
                len = in.read(partialBuffer);
                if (len > 0) {
                    sb.append(new String(partialBuffer, 0, len));
                    bytesRead += len;
                    logger.info("[CSV] : " + sb.toString().length());
                } else
                    logger.info("[CSV] : Socket closed...");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public String parseChunkedResponse() {
        StringBuilder sb = new StringBuilder();
        char charRead;
        try {
//            charRead = (char) in.read(); // for '\r'
//            charRead = (char) in.read(); // for '\n'
//            charRead = (char) in.read(); // for '\r'
//            charRead = (char) in.read(); // for '\n'

            int len = 1;
            while (len > 0) {
                byte[] partialLen = new byte[2];
                partialLen[0] = (byte) in.read();
                partialLen[1] = (byte) in.read();
                logger.info("[HEADER] : PARTIAL LEN - " + partialLen[0] + " : " + partialLen[1]);
                String lenHexVal = new String(partialLen, "UTF-8");
                logger.info("[HEADER] : HEXVAL - " + lenHexVal);
//                len = twoByteToInt(partialLen, 0);
                len = Integer.parseInt(lenHexVal, 16);
                logger.info("[HEADER] : PARTIAL LEN - " + len);
                charRead = (char) in.read(); // for '\r'
                charRead = (char) in.read(); // for '\n'

                for (int i = 0; i < len; i++) {
                    charRead = (char) in.read();
                    sb.append(charRead);
                }
                charRead = (char) in.read(); // for '\r'
                charRead = (char) in.read(); // for '\n'
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    public String parseChunkedResponse1() throws IOException {
        int charRead;
        StringBuffer sb = new StringBuffer();
        while (true) {
            sb.append((char) (charRead = in.read()));
            if ((char) charRead == '\r') {            // if we've got a '\r'
                sb.append((char) in.read()); // then write '\n'
                charRead = in.read();        // read the next char;
                if (charRead == '\r') {                  // if it's another '\r'
                    sb.append((char) in.read());// write the '\n'
                    break;
                } else {
                    sb.append((char) charRead);
                }
            }
        }

        String[] dataArray = sb.toString().split("\r\n");
        StringBuffer dataBuf = new StringBuffer();

        for (int i = 1; i < dataArray.length; i += 2) {
            dataBuf.append(dataArray[i]);
        }
        return dataBuf.toString();
    }


//    public void writeRequest(final SimpleRequest request) throws IOException {
//        if (request == null) {
//            return;
//        }
//        ResponseWriter writer = new ResponseWriter(this.out, HTTP_ELEMENT_CHARSET);
//        writer.println(request.getRequestLine().toString());
//        Iterator item = request.getHeaderIterator();
//        while (item.hasNext()) {
//            Header header = (Header) item.next();
//            writer.print(header.toExternalForm());
//        }
//        writer.println();
//        writer.flush();
//
//        OutputStream outsream = this.out;
//        InputStream content = request.getBody();
//        if (content != null) {
//
//            Header transferenc = request.getFirstHeader("Transfer-Encoding");
//            if (transferenc != null) {
//                request.removeHeaders("Content-Length");
//                if (transferenc.getValue().indexOf("chunked") != -1) {
//                    outsream = new ChunkedOutputStream(outsream);
//                }
//            }
//            byte[] tmp = new byte[4096];
//            int i = 0;
//            while ((i = content.read(tmp)) >= 0) {
//                outsream.write(tmp, 0, i);
//            }
//            if (outsream instanceof ChunkedOutputStream) {
//                ((ChunkedOutputStream) outsream).finish();
//            }
//        }
//        outsream.flush();
//    }
//
//    public void writeResponse(final SimpleResponse response) throws IOException {
//        if (response == null) {
//            return;
//        }
//        ResponseWriter writer = new ResponseWriter(this.out, HTTP_ELEMENT_CHARSET);
//        writer.println(response.getStatusLine());
//        Iterator item = response.getHeaderIterator();
//        while (item.hasNext()) {
//            Header header = (Header) item.next();
//            writer.print(header.toExternalForm());
//        }
//        writer.println();
//        writer.flush();
//
//        OutputStream outsream = this.out;
//        InputStream content = response.getBody();
//        if (content != null) {
//
//            Header transferenc = response.getFirstHeader("Transfer-Encoding");
//            if (transferenc != null) {
//                response.removeHeaders("Content-Length");
//                if (transferenc.getValue().indexOf("chunked") != -1) {
//                    outsream = new ChunkedOutputStream(outsream);
//                }
//            }
//
//            byte[] tmp = new byte[1024];
//            int i = 0;
//            while ((i = content.read(tmp)) >= 0) {
//                outsream.write(tmp, 0, i);
//            }
//            if (outsream instanceof ChunkedOutputStream) {
//                ((ChunkedOutputStream) outsream).finish();
//            }
//        }
//        outsream.flush();
//    }

    public int getSocketTimeout() throws SocketException {
        return this.socket.getSoTimeout();
    }

    public void setSocketTimeout(int timeout) throws SocketException {
        this.socket.setSoTimeout(timeout);
    }

}