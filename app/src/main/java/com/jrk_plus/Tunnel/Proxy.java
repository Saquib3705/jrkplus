package com.jrk_plus.Tunnel;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import opt.packet.Packet;
import pseudoSsl.PseudoSslWrapUnWrap;

import static opt.log.OmLogger.logger;

//import opt.packet.SendPacket;

/**
 * Created by ABHISHEK MAHATO on 3/24/2017.
 */

public class Proxy implements Runnable {
    String remoteIp;
    int remotePort;
    int localPort;
    static boolean isFirstPacket = true;
    public static ServerSocket ss;
    public PseudoSslWrapUnWrap sslWrapUnWrap;
    private String sni;

    String serverIp;
    int serverPort;

    public final String TAG = "[" + this.getClass().getName() + "]";

    public Proxy(Config config) {
        this.localPort = config.getLocalPort();
        this.remoteIp = config.getRemoteIp();
        this.remotePort = config.getRemotePort();
        this.serverIp = config.getServerIp();
        this.serverPort = config.getServerPort();
        this.sni = config.getSni();
    }

    public void run() {
        try {
            // Print a start-up message
            logger.debug(TAG + "Starting proxy for " + remoteIp + ":" + remotePort
                    + " on port " + localPort);
            // And start running the server
            runServer(remoteIp, remotePort, localPort); // never returns
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static String printHexDump(byte[] bytes, int len) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++) {
                if (i % 20 == 0) {
                    sb.append("\n");
                }
                sb.append(String.format("%02x ", bytes[i] & 0xff));
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error("Ex[printHexDump]: ", e);
        }
        return "failed to print hexdump";
    }

    public Packet psudoWrap(byte[] arr, int offset, int length) {
        Packet packet = new Packet(4096);
        packet.appendInEnd(arr, offset, length);
        int len = sslWrapUnWrap.pseudoSslWrap(packet);
        logger.info("Before Length [" + length + "] After Length [" + len + "]");
        return packet;
    }

    public byte[] addHeader(byte[] info, int length, String ip, int port, int protocol, int action) {
        try {
//            logger.debug(TAG + "Stuffing SIP Proxy Header");
//            logger.debug(TAG + "Original Length : " + length);
            byte[] data = null;
            int i = 0;

            int len = 0;
            if (isFirstPacket)
                len = length + 8;
            else
                len = length;

//            logger.debug("ng", "PROTO_TCP : Adding two bytes of data");
            data = new byte[len];
//            data[i++] = (byte) ((len >> 8) & 0xFF);
//            data[i++] = (byte) (len & 0xFF);

            if (isFirstPacket) {
                String[] labels = ip.split("\\.");

                for (int j = 0; j < labels.length; j++)
                    data[i++] = (byte) Integer.parseInt(labels[j]);

                data[i++] = (byte) ((port >> 8) & 0xFF);
                data[i++] = (byte) (port & 0xFF);

                data[i++] = (byte) (protocol & 0xFF);
                data[i++] = (byte) (action & 0xFF);
                isFirstPacket = false;
            }

//            logger.debug("ng", "Value of i : " + i);

            if (action == ConfigurationConstants.ACTION_PROVISIONING_PROXY)
                logger.debug(TAG + "Provisioning Proxy Request");
            else if (action == ConfigurationConstants.ACTION_BALANCE_PROXY)
                logger.debug(TAG + "Balance Proxy Request");
            else if (action == ConfigurationConstants.ACTION_FORWARD)
                logger.debug(TAG + "ACTION_FORWARD");
            System.arraycopy(info, 0, data, i, length);
//            logger.debug("ng", "Length of data copied: " + length + " bytes");
//            logger.debug("ng", "Final Length : " + data.length);
            return data;
        } catch (Exception e) {
            logger.error(TAG + "Add Header Exception:" + Log.getStackTraceString(e));
        }
        return null;
    }

    public void runServer(String host, int remoteport, int localport)
            throws IOException {
        // Create a ServerSocket to listen for connections with
        final byte[] request = new byte[8192*2];
        byte[] reply = new byte[8192];

        Socket client = null, server = null;
        ss = new ServerSocket(localport, 50, InetAddress.getLocalHost());

//        // Wait for a connection on the local port
//        client = ss.accept();
//        client.setTcpNoDelay(true);
//
//        final InputStream streamFromClient = client.getInputStream();
//        final OutputStream streamToClient = client.getOutputStream();

        try {
            server = new Socket();
            server.connect(new InetSocketAddress(host, remoteport));
//            server.setTcpNoDelay(true);
            isFirstPacket = true;
        } catch (IOException e) {
            logger.error(TAG + Log.getStackTraceString(e));
        }


        sslWrapUnWrap = new PseudoSslWrapUnWrap(false, 4096*16);
        Packet packet = new Packet(4096);
        sslWrapUnWrap.ptlsConnect(this.sni, packet);

        // Get server streams.
        InputStream streamFromServer = server.getInputStream();
        final OutputStream streamToServer = server.getOutputStream();

        streamToServer.write(packet.buffer, packet.bufferOffset, packet.length());
        streamToServer.flush();

        int ret = -1;
        Packet recvPacket = new Packet(65535);

        byte[] message_received = new byte[65535];

        while(ret < 0) {
            int readLen = streamFromServer.read(message_received);
            packet = new Packet(message_received, 0, readLen);

            if (!sslWrapUnWrap.setPseudoSslMsg(packet)) {
                logger.warn("Setting Pseudo Msg Failed");
                return;
            }
            recvPacket = new Packet(8192*8);
            ret = sslWrapUnWrap.pseudoSslUnWrap(recvPacket);

            logger.info("PseudoSslUnwrap return Value [" + ret + "] handshake [" + sslWrapUnWrap.getHandshakeState() + "]");
            switch (sslWrapUnWrap.getHandshakeState()) {
                case SERVER_HELLO_DONE:
                    logger.info("[SINGLE-STREAM-PSEUDOSSL] SERVER HELLO DONE");

                    logger.info("Server IP : " + serverIp);
                    logger.info("Server Port : " + serverPort);

                    byte[] firstPacket = com.jrk_plus.Provisioning.Information.createFirstPacket();
                    firstPacket = Information.addHeader(firstPacket, serverIp, serverPort,
                            ConfigurationConstants.PROTO_TCP, ConfigurationConstants.ACTION_FORWARD);

                    Packet psuedoSslPacket = psudoWrap(firstPacket, 0, firstPacket.length);
                    streamToServer.write(psuedoSslPacket.buffer, psuedoSslPacket.bufferOffset, psuedoSslPacket.length());
                    streamToServer.flush();
                    break;
                default:
                    logger.info("Wrong Message Type");
            }
        }


        while (true) {

            try {
                client = ss.accept();
//                client.setTcpNoDelay(true);
//                client.setSoTimeout(4000);

                final InputStream streamFromClient = client.getInputStream();
                final OutputStream streamToClient = client.getOutputStream();
                // a thread to read the client's requests and pass them
                // to the server. A separate thread for asynchronous.
                Thread t = new Thread() {
                    public void run() {
                        int bytesRead;
                        Packet pseudoSslPacket;
                        try {
                            while ((bytesRead = streamFromClient.read(request)) != -1) {
//                                logger.debug("ng", "Data received from client : " + printHexDump(request, bytesRead));
//                                byte[] dataToSend = addHeader(request, bytesRead, "127.0.0.1",
//                                        1194, ConfigurationConstants.PROTO_TCP,
//                                        ConfigurationConstants.ACTION_FORWARD);
                                byte[] dataToSend = com.jrk_plus.Provisioning.Information.createAppDataInfo(request, 0, bytesRead);
                                pseudoSslPacket = psudoWrap(dataToSend, 0, dataToSend.length);
                                streamToServer.write(pseudoSslPacket.buffer, pseudoSslPacket.bufferOffset, pseudoSslPacket.length());

//                                logger.debug("ng", "Data sent to server : " + printHexDump(dataToSend, dataToSend.length));
                                logger.debug(TAG + "Bytes sent to server : " + dataToSend.length);

                                streamToServer.flush();
                            }
                        } catch (IOException e) {
                            logger.error(TAG + Log.getStackTraceString(e));
                        }
                    }
                };
                // Start the client-to-server request thread running
                t.start();

                // Read the server's responses
                // and pass them back to the client.
                logger.info("[SERVER-STATUS]" + server.isInputShutdown());

                streamFromServer = server.getInputStream();
                int bytesRead = 0, retVal;
                try {
                    logger.debug(TAG + "About to read data from server");
                    while (true) {
                        logger.info("[BYTES-READ 1] : " + bytesRead);
                        bytesRead = streamFromServer.read(reply);
                        if(bytesRead == -1)
                            break;
                        logger.info("[BYTES-READ 2] : " + bytesRead);

                        logger.debug(TAG + "Data received from server : " + bytesRead + " bytes");
//                        recvPacket = new Packet(reply, 0, bytesRead);
                        recvPacket.reset();
                        recvPacket.appendInEnd(reply, 0, bytesRead);
                        if (!sslWrapUnWrap.setPseudoSslMsg(recvPacket)) {
                            logger.warn("Setting Pseudo Msg Failed");
                            return;
                        }
                        recvPacket.reset();
                        retVal = sslWrapUnWrap.pseudoSslUnWrap(recvPacket);
                        while(retVal > 0) {
                            streamToClient.write(recvPacket.buffer, recvPacket.bufferOffset, recvPacket.length());
                            streamToClient.flush();
                            recvPacket.reset();
                            retVal = sslWrapUnWrap.pseudoSslUnWrap(recvPacket);
                        }
                    }
                } catch (IOException e) {
                    logger.error(TAG + Log.getStackTraceString(e));
                }
            } catch (Exception e) {
                logger.error(TAG + Log.getStackTraceString(e));
            }
        }
    }

    public static void stopServer() {
        try {
            ss.close();
        } catch (IOException e) {
            logger.error("Proxy socket close error : " + Log.getStackTraceString(e));
        }
    }
}
