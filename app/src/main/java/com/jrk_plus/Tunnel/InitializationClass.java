package com.jrk_plus.Tunnel;

import android.content.Context;
import androidx.multidex.MultiDexApplication;

import jrk_plus.hu.blint.ssldroid.MyDroidLogger;
import opt.log.OmLogger;

import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 5/24/2017.
 */

public class InitializationClass extends MultiDexApplication {

    private static InitializationClass instance;
    //    private Tracker mTracker;
    private static final String PROPERTY_ID = "UA-89622148-1";
    private static final String PROPERTY_ID_PRO = "UA-89641705-1";

    @Override
    public void onCreate() {
        super.onCreate();
//        if (!BuildConfig.DEBUG)
//            Fabric.with(this, new Crashlytics());

        instance = this;
    }

//    synchronized public Tracker getDefaultTracker() {
////        if (mTracker == null) {
////            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
////            mTracker = analytics.newTracker(BuildConfig.FLAVOR == "pro" ? PROPERTY_ID_PRO : PROPERTY_ID);
////        }
//        return mTracker;
//    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static String getResourceString(int resId) {
        return instance.getString(resId);
    }

    public static InitializationClass getInstance() {
        return instance;
    }

    public InitializationClass() {
        final MyDroidLogger mylogger = new MyDroidLogger();
        opt.log.OmLogger.setLogger(mylogger);
        opt.log.OmLogger.enableAndroidLogging();
        opt.log.OmLogger.setLogLevel(OmLogger.LogLevel.NONE);
        logger.start();


        logger.info("[InitializationClass]", "InitializationClass: ");
//        CrashReportGenerator crashReportGenerator = new CrashReportGenerator();
//
//        new Thread(crashReportGenerator).start();
    }
}
