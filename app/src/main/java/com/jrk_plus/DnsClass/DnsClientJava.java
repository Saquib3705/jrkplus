package com.jrk_plus.DnsClass;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

//import .DNSHeader;
//import dnsclientjava.DNSName;
//import dnsclientjava.DNSProp;
//import dnsclientjava.DNSRecord;


public class DnsClientJava {

    private final Random _random = new Random();
    private ArrayList<String> domainName = new ArrayList<String>();

    public static final byte T_A = 0x01;
    public static final byte T_AAAA = 0x1c;
    public static final byte T_CNAME = 0x05;

    //     ArrayList<String> aListDays = new ArrayList<String>();
//      aListDays.add("Sunday");
//                aListDays.add("Monday");
//                aListDays.add("Tuesday");
//    String domainName = "t.topdialerdialer.com";
    public static DatagramSocket clientSocket;
    public byte[] rcvBuff;
    public byte[] Mesg = null;
    public DatagramPacket udpRcvPackt;
    public static List<String> nameservers;
    boolean EDNS0 = false;
    boolean DirectSend = true;

    private byte[] opcode;
    private byte pack;
    private byte platform;
    private boolean tunnelFlag = false;

    private static final int WIDTH = 20;

    private final String CHAR_LIST = "1234567890";

    public DnsClientJava() {
        this.opcode = "0".getBytes();
        this.pack = (byte)'0';
        this.platform = (byte)'0';
        this.tunnelFlag = false;
    }

    public DnsClientJava(byte[] opcode, byte pack, byte platform) {
        this.opcode = opcode;
        this.pack = pack;
        this.platform = platform;
        this.tunnelFlag = true;
    }

    public int getRandomNumber() {
        int randomInt = 0;
        try {
            Random randomGenerator = new Random();
            randomInt = randomGenerator.nextInt(CHAR_LIST.length());
            if (randomInt == 0) {
                return randomInt;
            } else {
                return randomInt - 1;
            }
        } catch (Exception e) {
            return 1;
        }
    }

    public String getRandomString(int numOfCharactors) {
        try {
            StringBuffer randStr = new StringBuffer();
            for (int i = 0; i < numOfCharactors; i++) {
                int number = getRandomNumber();
                char ch = CHAR_LIST.charAt(number);
                randStr.append(ch);
            }
            return randStr.toString();
        } catch (Exception e) {
            return "12345";
        }

    }

    public void createMessage() {
        if (opcode.length > 8) {
            System.out.println("Invalid Opcode");
            return;
        }
        int index = 0;
        Mesg = new byte[opcode.length + 9];
        System.arraycopy(opcode, 0, Mesg, 0, opcode.length);
        index += opcode.length;

        this.pack = (byte) '1';
        this.platform = (byte) '1';

        Mesg[index++] = '#';
        Mesg[index++] = this.pack;

        Mesg[index++] = '#';
        Mesg[index++] = this.platform;

        Mesg[index++] = '#';
//        String randomPadding = RandomStringUtils.randomAlphanumeric(4);
        String randomPadding = getRandomString(4);
        System.arraycopy(randomPadding.getBytes(), 0, Mesg, index, randomPadding.length());
        index += randomPadding.length();

        System.out.println("---------------------------------------------------");
        System.out.println(printHexDump(Mesg, index));
        System.out.println("---------------------------------------------------");
    }
//
//    public static void main1(String[] args) {
//        try {
//            nameservers = sun.net.dns.ResolverConfiguration.open().nameservers();
//            for (String ns : nameservers) {
//                DnsClientJava dnsReq = new DnsClientJava();
//                DNSHeader result = dnsReq.DnsQuery("www.google.com", T_AAAA, ns);
//
//                for (DNSRecord record : result.ANRecord) {
//                    System.out.println("ipv6 address:  " + printHexDump(record.Data, record.Data.length));
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public DNSHeader SendPacket(byte[] sendData, String nameserver) {
        try {
            clientSocket = new DatagramSocket();
            clientSocket.setSoTimeout(5000);

            InetAddress IPAddress;
//            for (String ns : nameservers) {
            IPAddress = InetAddress.getByName(nameserver);
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 53);
            clientSocket.send(sendPacket);

            byte[] ReceivePacketFormServer = this.ReceivePacket();
            return this.decodeResponse(ReceivePacketFormServer);
//            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public byte[] ReceivePacket() {
        try {
            rcvBuff = new byte[2048];
            udpRcvPackt = new DatagramPacket(rcvBuff, rcvBuff.length);
            clientSocket.receive(udpRcvPackt);
            System.out.println("packet rcvd from: "
                    + udpRcvPackt.getAddress().getHostAddress() + ":" + udpRcvPackt.getPort());
            return udpRcvPackt.getData();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public DNSHeader DnsQuery(String hostName, byte record_type, String nameserver) {
        try {
            int id = _random.nextInt() & 0xFFFF;
            byte[] sbuffer = new byte[512];
            int index = 0;
            sbuffer[index++] = (byte) ((id >> 8) & 0xFF);
            sbuffer[index++] = (byte) (id & 0xFF);
            sbuffer[index++] = (byte) 0x01;
            sbuffer[index++] = (byte) 0x00;

            sbuffer[index++] = 0;
            sbuffer[index++] = 1;

            // Answer count
            sbuffer[index++] = 0;
            sbuffer[index++] = 0;

            // NS count
            sbuffer[index++] = 0;
            sbuffer[index++] = 0;

            // Additional count
            if (EDNS0) {
                sbuffer[index++] = 0;
                sbuffer[index++] = 1;
            } else {
                sbuffer[index++] = 0;
                sbuffer[index++] = 0;
            }

            String DnsFormatNameFromMessage;
            if(this.tunnelFlag) {
                this.createMessage();
                DnsFormatNameFromMessage = CreateDNSFormatMessageFromByte(Mesg, hostName);
            } else
                DnsFormatNameFromMessage = hostName;
            // Query name
            String[] qname = DnsFormatNameFromMessage.split("\\.");
            // Query name
//            String[] qname = hostName.split("\\.");

            for (int i = 0; i < qname.length; i++) {
                byte[] b = qname[i].getBytes("US-ASCII");
                sbuffer[index++] = (byte) b.length;
                System.arraycopy(b, 0, sbuffer, index, b.length);
                index += b.length;
            }

            /*
             A domain name represented as a sequence of labels, where each label consists of a length
             octet followed by that number of octets. The domain name terminates with the zero length
             octet for the null label of the root.
             */
            sbuffer[index++] = 0;
            // Query type 10 for null queries
            sbuffer[index++] = 0;
            sbuffer[index++] = record_type;
            // Query class 1 for Internet
            sbuffer[index++] = 0;
            sbuffer[index++] = 1;
            //Additional Record
            /*
             The OPT RR has RR type 41 ---RFC 6891(EDNS0)
             +------------+--------------+------------------------------+
             | Field Name | Field Type   | Description                  |
             +------------+--------------+------------------------------+
             | NAME       | domain name  | MUST be 0 (root domain)      |
             | TYPE       | u_int16_t    | OPT (41)                     |
             | CLASS      | u_int16_t    | requestor's UDP payload size |
             | TTL        | u_int32_t    | extended RCODE and flags     |
             | RDLEN      | u_int16_t    | length of all RDATA          |
             | RDATA      | octet stream | {attribute,value} pairs      |
             +------------+--------------+------------------------------+
             */
            if (EDNS0) {
                sbuffer[index++] = 0;
                sbuffer[index++] = 0;
                sbuffer[index++] = 0x29;
                sbuffer[index++] = 0x10;
                sbuffer[index++] = 0;
                sbuffer[index++] = 0;
                sbuffer[index++] = 0;
                sbuffer[index++] = 0;
                sbuffer[index++] = 0;
                sbuffer[index++] = 0;
                sbuffer[index++] = 0;
            }

//            String msg = "Padding Data!";
//            byte[] temp = msg.getBytes("UTF-8");
//            
//            System.arraycopy(temp, 0, sbuffer, index, temp.length);
//            index += temp.length;
//                sbuffer[index++] = 61;
//                sbuffer[index++] = 62;
//                sbuffer[index++] = 63;
//                sbuffer[index++] = 64;
//                sbuffer[index++] = 65;
            byte[] result = new byte[index];
            System.arraycopy(sbuffer, 0, result, 0, index);

            return this.SendPacket(result, nameserver);

//            return result;
        } catch (Exception ex) {
            System.out.println("Error in Creating DNS Packet");
        }

        return null;
    }

    public static String printHexDump(byte[] bytes, int len) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++) {
                if (i % WIDTH == 0) {
                    sb.append("\n");
                }
                sb.append(String.format("%02x ", bytes[i] & 0xff));
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error("Ex[printHexDump]: ", e);
        }
        return "failed to print hexdump";
    }

    public DNSHeader decodeResponse(byte[] data) {
        if(data == null)
            return null;
        DNSHeader result = new DNSHeader();
        int index = 0;
        result.id = (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
        result.qr = (data[index] >> 7) & 1;
        result.opcode = (data[index] >> 3) & 0x0F;
        result.aa = (data[index] >> 2) & 1;
        result.tc = (data[index] >> 1) & 1;
        result.rd = data[index] & 1;
        index++;
        result.ra = (data[index] >> 7) & 1;
        result.rcode = data[index] & 0x0F;
        index++;

        int qdcount = (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
        int ancount = (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
        int nscount = (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
        int arcount = (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
        // System.out.println("qdcoutn" + qdcount + ":" + ancount);

        result.QRecord = new ArrayList<DNSRecord>();
        result.ANRecord = new ArrayList<DNSRecord>();
        result.NSRecord = new ArrayList<DNSRecord>();
        result.ARRecord = new ArrayList<DNSRecord>();

        for (int i = 0; i < qdcount; i++) {
            DNSRecord qRecord = new DNSRecord();
            qRecord.Name = decodeName(data, index);
            index = qRecord.Name.Index;
            qRecord.Prop = new DNSProp();
            qRecord.Prop.Type = (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
            qRecord.Prop.Class = (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
            result.QRecord.add(qRecord);
        }

        // Decode answer section
        for (int i = 0; i < ancount; i++) {
            DNSRecord anRecord = new DNSRecord();
            anRecord.Name = decodeName(data, index);
            index = anRecord.Name.Index;
            anRecord.Prop = decodeType(data, index);
            index = anRecord.Prop.Index;

            System.out.println("Answer Record Type :: " + anRecord.Prop.Type);

            if (this.tunnelFlag) {
                if (anRecord.Prop.Type == T_CNAME) {
                    int rdatalen = data[index + 4];
                    anRecord.Data = new byte[rdatalen];
                    System.out.println("Data Length :: " + rdatalen);
                    System.arraycopy(data, index + 5, anRecord.Data, 0, rdatalen);
                    System.out.println("Received From Server Message" + new String(anRecord.Data));
                    try {
                        System.err.println(new String(anRecord.Data));
                        System.out.println("Decoded Message: " + new String(Base32.decode(new String(anRecord.Data))).length());
                        byte[] response = Base32.decode(new String(anRecord.Data));
                    } catch (Base32.DecodingException ex) {
                    }
                    result.ANRecord.add(anRecord);
                    index += anRecord.Prop.Len;
                }
            } else {
                if (anRecord.Prop.Type != T_AAAA && anRecord.Prop.Type != T_A) {
                    System.out.println("Neglecting Record Type :: " + anRecord.Prop.Type);
                    index += anRecord.Prop.Len;
                    continue;
                }

                // Decode data
                int rdatalen = (data[index - 2] & 0xFF) * 256 + (data[index - 1] & 0xFF);
                anRecord.Data = new byte[rdatalen];
                System.out.println("Data Length :: " + rdatalen);
                System.arraycopy(data, index, anRecord.Data, 0, rdatalen);
                System.out.println("Received From Server Message" + new String(anRecord.Data));
//            try {
////                System.err.println(new String(anRecord.Data));
//                System.out.println("HexDump Before: " + printHexDump(anRecord.Data, anRecord.Data.length));
//            } catch (Exception ex) {
//                Logger.getLogger(DnsClientJava.class.getName()).log(Level.SEVERE, null, ex);
//            }
                result.ANRecord.add(anRecord);
                index += anRecord.Prop.Len;
            }
//            break;
        }

        return result;
    }

    private DNSName decodeName(byte[] data, int index) {
        try {
            DNSName name = new DNSName();
            name.Name = "";
            int len = (data[index++] & 0xFF);
            while (len > 0) {
                if (!name.Name.equals("")) {
                    name.Name += ".";
                }
                if ((len & 0xC0) == 0) {
                    name.Name += new String(data, index, len, "US-ASCII");
                    index += len;
                    len = (data[index++] & 0xFF);
                } else {
                    // Compression
                    int ptr = (len & 0x3F) * 256 + (data[index++] & 0xFF);
                    name.Name += decodeName(data, ptr).Name;
                    break;
                }
            }
            name.Index = index;
            return name;
        } catch (Exception ex) {
            System.out.println("decodeName Exception");
        }
        return null;
    }

    private DNSProp decodeType(byte[] data, int index) {
        DNSProp type = new DNSProp();
        type.Type = (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
        type.Class = (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
        type.TTL = (data[index++] & 0xFF) * 256 * 256 * 256 + (data[index++] & 0xFF) * 256 * 256 + (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
        type.Len = (data[index++] & 0xFF) * 256 + (data[index++] & 0xFF);
        type.Index = index;
        return type;
    }

    public String CreateDNSFormatMessageFromByte(byte[] Message, String hostName) {
        System.out.println("inside CreateDNSFormatMessageFromByte");
        try {
            String payload = Base32.encode(Message);
            // String payload = new String(Message);
            // Divide into parts
            int pparts = payload.length() / 63 + 1;
            int plen = payload.length() / pparts;
            int pindex = 0;
            String ppayload = "";
            while (pindex < payload.length()) {
                if (pindex > 0) {
                    ppayload += ".";
                }
                ppayload += payload.substring(pindex, Math.min(pindex + plen, payload.length()));
                pindex += plen;
            }
            String ResultDomainNameFromMessage = (ppayload.equals("") ? hostName : String.format("%s.%s", ppayload, hostName));
            System.out.println("ResultDomainNameFromMessage :::::  " + ResultDomainNameFromMessage);
            return ResultDomainNameFromMessage;
        } catch (Exception ex) {
            System.out.println("Exception in splitting");
            ex.printStackTrace();
        }
        return null;
    }
}
