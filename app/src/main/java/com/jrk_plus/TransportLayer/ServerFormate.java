package com.jrk_plus.TransportLayer;

/**
 * Created by bot on 18/01/2018.
 */

public class ServerFormate {
    String ip;
    int port;

    public ServerFormate(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public String getIp(){
        return ip;
    }

    public int getPort(){
        return port;
    }
}
