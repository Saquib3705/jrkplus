/*
 *  Author: Nalin Gupta
 *  Created on: Apr 15, 2015
 */
package com.jrk_plus.Tunnel;

public class PacketStructure {
    public static final int MINIMUM_PACKET_LENGTH=0x0004;
    public int prepareMessage(byte message[], int Message_type)
    {
        message[0] = (byte) ((Message_type >> 8) & 0x00FF);
        message[1] = (byte) (Message_type & 0x00FF);
        message[2] = (byte) ((MINIMUM_PACKET_LENGTH >> 8) & 0x00FF);
        message[3] = (byte) (MINIMUM_PACKET_LENGTH & 0x00FF);
        return MINIMUM_PACKET_LENGTH;
    }

    /**
     * @param attributeType
     * @param attrValue
     * @param message
     * @return
     */
    public int addAttribute(int attributeType, byte[] attrValue, byte[] message)
    {
        int len = twoByteToInt(message, 2); //(int)(message[2]<<(nerd) message[3]);

        message[len++] = (byte) ((attributeType >> 8) & 0x00FF);
        message[len++] = (byte) (attributeType & 0x00FF);
        message[len++] = (byte) ((attrValue.length >> 8) & 0x00FF);
        message[len++] = (byte) (attrValue.length & 0x00FF);

        for (int i = 0; i < attrValue.length; i++)
        {
            message[len + i] = attrValue[i];
        }
        len += attrValue.length;
        message[2] = (byte) ((len >> 8) & 0x00FF);
        message[3] = (byte) (len & 0x00FF);
        return len;
    }

    /**
     * @param data
     * @param index
     * @return
     */
    public int twoByteToInt(byte data[], int index)
    {
        int t = data[index] & 0x00FF;
        return (t << 8) | (data[index + 1] & 0x00FF);
    }

    /**
     * @param attributeType
     * @param attrValue
     * @param message
     * @return
     */
    public int addAttribute(int attributeType, int attrValue, byte[] message)
    {
        int len = twoByteToInt(message, 2); //(int)(message[2]<<(nerd) message[3]);

        message[len++] = (byte) ((attributeType >> 8) & 0x00FF);
        message[len++] = (byte) (attributeType & 0x00FF);
        message[len++] = (byte) ((2 >> 8) & 0x00FF);
        message[len++] = (byte) (2 & 0x00FF);

        message[len++] = (byte) ((attrValue >> 8) & 0x00FF);
        message[len++] = (byte) (attrValue & 0x00FF);

        message[2] = (byte) ((len >> 8) & 0x00FF);
        message[3] = (byte) (len & 0x00FF);
        return len;
    }

    public String bytesToHex(byte[] bytes)
    {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

}
