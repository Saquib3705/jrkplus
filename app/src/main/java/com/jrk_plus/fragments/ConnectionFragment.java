package com.jrk_plus.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;


import com.app.jrk_plus.R;
import com.github.lzyzsd.circleprogress.CircleProgress;
import com.jrk_plus.NewDashboard.ConnectActivity;
import com.jrk_plus.utilities.PrefManager;


import de.blinkt.openvpn.core.VpnStatus;
import opt.log.OmLogger;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ConnectionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConnectionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConnectionFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ImageView btnCancel, infobtnCancel, helpbtnCancel, actionSettings;
    private View notConnectedLayout, connectedLayout, connectingLayout;
    private Button quickConnect, cancelConnection, disConnectConnection;

    private TextView validityText, userId, /*connectionStatusText,*/
            connectingText, downloadVolumeText,
            uploadVolumeText, downloadSpeedText, uploadSpeedText, serverNameText, textTimeUnit;

    private Chronometer sessionTimeText, connectedTimeMainChorono;

    private CircleProgress connectionProgress;
    private int progress = 0;
    public PrefManager prefManager;


    public ConnectionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConnectionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConnectionFragment newInstance(String param1, String param2) {
        ConnectionFragment fragment = new ConnectionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = (ConnectActivity) getActivity();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
//            System.out.println("connectionFrag:-params: " + mParam1 + "," + mParam2);
        }

        prefManager = new PrefManager(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_connection, container, false);
        setEventListner(view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        if (view == cancelConnection) {
            ((ConnectActivity) getActivity()).cancelConnection();
        } else if (view == disConnectConnection) {
            ((ConnectActivity) getActivity()).disconenctConnection(false);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        void conenctionFragmentCancelConnection();

        void conenctionFragmentDisconnectConnection();
    }

    private void setEventListner(View dialog) {
//        btnCancel = dialog.findViewById(R.id.btnCancel);
        notConnectedLayout = dialog.findViewById(R.id.not_connected_layout);
        connectingLayout = dialog.findViewById(R.id.connecting_layout);
        connectedLayout = dialog.findViewById(R.id.connected_layout);
//        connectingProgress = findViewById(R.id.connecting_progress);
//        quickConnect = dialog.findViewById(R.id.quick_connect);
        cancelConnection = dialog.findViewById(R.id.cancel_connection);
        disConnectConnection = dialog.findViewById(R.id.disconnect_connection);
        connectingText = dialog.findViewById(R.id.connecting_text);
        downloadVolumeText = dialog.findViewById(R.id.download_volume_text);
        uploadVolumeText = dialog.findViewById(R.id.upload_volume_text);
        downloadSpeedText = dialog.findViewById(R.id.download_speed_text);
        uploadSpeedText = dialog.findViewById(R.id.upload_speed_text);
        sessionTimeText = dialog.findViewById(R.id.session_time_text);

        connectionProgress = dialog.findViewById(R.id.connection_progress);
        connectionProgress.setFinishedColor(getResources().getColor(R.color.colorAccent));
        connectionProgress.setUnfinishedColor(getResources().getColor(R.color.expiry_view_text));

        connectionProgress.setPrefixText("Connection ");
        connectionProgress.setTextColor(getResources().getColor(R.color.white));

        serverNameText = dialog.findViewById(R.id.server_name_text);
        serverNameText.setSelected(true);
//        quickConnect.setOnClickListener(this);
        cancelConnection.setOnClickListener(this);
        disConnectConnection.setOnClickListener(this);
//        btnCancel.setOnClickListener(this);


    }

    public void showConnectingLayout() {

    }

    public void showconnectedLayout() {

    }

    public void connectionState(final VpnStatus.ConnectionStatus status) {
        if (prefManager.getConnectionState() == ConnectActivity.VPN_CONNECTION_STATE.CONNECTED || prefManager.getConnectionState() == ConnectActivity.VPN_CONNECTION_STATE.CONNECTING) {

//            Log.d("ConnectionStatus","Inside if case");
            //For Easy parentServer changing status


            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                   /* logger.info("CheckStatus", "Status ===" + status);
                    logger.info("CheckStatus", "vpn current state ===" + VpnStatus.currentState);
                    if (VpnStatus.currentState.equalsIgnoreCase("CONNECTING")) {
                        progress = 0;
                    } else if (VpnStatus.currentState.equalsIgnoreCase("TCP_CONNECT")) {
                        progress = 15;
                    } else if (VpnStatus.currentState.equalsIgnoreCase("WAIT")) {
                        progress = 30;
                    } else if (VpnStatus.currentState.equalsIgnoreCase("AUTH")) {
                        progress = 45;
                    } else if (VpnStatus.currentState.equalsIgnoreCase("GET_CONFIG")) {
                        progress = 60;
                    } else if (VpnStatus.currentState.equalsIgnoreCase("ASSIGN_IP")) {
                        progress = 75;
                    } else if (VpnStatus.currentState.equalsIgnoreCase("ADD_ROUTES")) {
                        progress = 90;
                    } else if (VpnStatus.currentState.equalsIgnoreCase("CONNECTED")) {
                        progress = 100;
                    } else if (VpnStatus.currentState.equalsIgnoreCase("RECONNECTING")) {


                    }

                    if (android.os.Build.VERSION.SDK_INT >= 11 && progress != 100) {
                        // will update the "progress" propriety of seekbar until it reaches progress
                        ObjectAnimator animation = ObjectAnimator.ofInt(connectionProgress, "progress", progress);
                        animation.setDuration(700); // 0.7 second
                        animation.setInterpolator(new DecelerateInterpolator());
                        animation.start();
                    } else if (progress != 100)
                        connectionProgress.setProgress(progress); // no animation on Gingerbread or lower

                    if(connectingText != null)
                        connectingText.setText("Robi VPN " + VpnStatus.currentState + "...");*/
                    OmLogger.logger.debug("ConnectActivity", "Vpn statusConnection===" + VpnStatus.currentState);
                    if (VpnStatus.currentState.equalsIgnoreCase("CONNECTED")) {
                        OmLogger.logger.debug("ConnectActivity", "Changing to connected from status in Conncectionfragment");
//                        ((ConnectActivity) getActivity()).connected(shortCountry);

                        connected();
                    }
                }
            });
        }
//        else{
//            Log.d("ConnectionStatus","Inside else case");
//            ((ConnectActivity) getActivity()).disconenctConnection(false);
//        }
    }

    public void connected() {
//        connectingLayout.setVisibility(View.GONE);
//        connectedLayout.setVisibility(View.VISIBLE);
        sessionTimeText.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onResume() {
        super.onResume();

    }

    ConnectActivity activity;

    public void receiveTraffic(final String inTotal, final String outTotal, final String in, final String out) {
//        activity.setToolBarVisibility(false);
        //saquib
//        System.out.println("vpnconnection" + "received ===" + inTotal + "," + outTotal + "," + in + "," + out);
        if (activity != null)
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sessionTimeText.start();
                    if (downloadSpeedText != null && inTotal != null)
                        downloadSpeedText.setText(inTotal);
                    if (uploadSpeedText != null && outTotal != null)
                        uploadSpeedText.setText(outTotal);
                    if (downloadVolumeText != null && in != null)
                        downloadVolumeText.setText(in);
                    if (uploadVolumeText != null && out != null)
                        uploadVolumeText.setText(out);
                }
            });


    }
}
