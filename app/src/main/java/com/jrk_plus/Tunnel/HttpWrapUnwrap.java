package com.jrk_plus.Tunnel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 2/15/2018.
 */

public class HttpWrapUnwrap {
//    private StringBuffer buffer;
    Map<String, String> headers = new HashMap<>();
    StringBuffer dechunkedMessage;
    StringBuffer finalMessage;

    public String getFinalMessage() {
        return finalMessage.toString();
    }
    //    private ByteBuffer buffer;
//    StringBuffer sb = new StringBuffer();

    public enum MSG_STATE {
        HEADER,
        DATA,
        END
    }

    MSG_STATE msgState;

    public MSG_STATE getMsgState() {
        return msgState;
    }

    public HttpWrapUnwrap() {
//        this.buffer = new StringBuffer(65535);
        this.msgState = MSG_STATE.HEADER;
        this.dechunkedMessage = new StringBuffer(65535);
        this.finalMessage = new StringBuffer(65535);
    }

//    public void removeBytesFromStart(int n) {
//        for (int i = n; i < this.buffer.limit() + n; i++) {
//            if (i < this.buffer.limit()) {
//                this.buffer.put(i - n, this.buffer.get(i));
//            } else {
//                this.buffer.put(i - n, (byte) 0);
//            }
//        }
//    }

    public void parseHTTPHeaders(String message)
            throws IOException {
        if (this.msgState == MSG_STATE.DATA) {
            return;
        }
        int headerIndex = message.indexOf("\r\n\r\n");

        if (headerIndex > 0) {
            this.msgState = MSG_STATE.DATA;
            String headerMessage = message.substring(0, headerIndex);
            String trailerMessage = message.substring(headerIndex + 4, message.length());
            logger.info("[BUFFER] : Header Message : " + headerMessage);
            logger.info("[BUFFER] : Trailer Message : " + trailerMessage);

            String[] headersArray = headerMessage.split("\r\n");
            for (int i = 1; i < headersArray.length - 1; i++) {
                headers.put(headersArray[i].split(": ")[0],
                        headersArray[i].split(": ")[1]);
            }

            if(trailerMessage != null ||  !trailerMessage.equalsIgnoreCase("")) {
                this.dechunkedMessage.append(trailerMessage);
            }
        }



        logger.info("[HEADER] : " + message);
    }

    public void parseData(String message) {
        this.dechunkedMessage.append(message);
        if(message.contains("0\r\n")) {
            String[] partialMessages = this.dechunkedMessage.toString().split("\r\n");
            for(int i = 0; i<partialMessages.length; i++) {
                this.finalMessage.append(partialMessages[i].substring(2));
                logger.info("[BUFFER] : " + this.finalMessage);
            }
            this.msgState = MSG_STATE.END;
        }
    }

    public void setHttpMessage(String message) {
//        this.buffer.append(new String(message, len));
//        this.buffer.append(message);

        if (this.msgState == MSG_STATE.HEADER) {
            try {
                parseHTTPHeaders(message);
                String chunked = headers.get("Transfer-Encoding");
                if (chunked == null) {
                    int content_length = Integer.parseInt(headers.get("Content-Length"));
                    logger.info("[HEADER] : Content-Length : " + content_length);
                    logger.info("[HEADER] : Not Chunked");
//                    response = parseResponse1(content_length);
                } else {
                    logger.info("[HEADER] : Chunked");
//                    parseData(message);
//                    response = parseChunkedResponse();
                }
//                logger.info("[HEADER] : " + response);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            parseData(message);
        }
    }


}
