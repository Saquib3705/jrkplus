/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jrk_plus.DnsClass;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author om
 */
public class Ipv4ToIpv6Wrapping {

    private final static byte[] xor = {(byte) 65, (byte) 45, (byte) 82, (byte) 16};
    private final static String[] ipv6Prefix = {"2406:2000:e4:200::4001",
        "2404:6800:4007:805::4001",
        "2a03:2880:f10c:83::4001",
        "2001:67c:38c::4001",
        "2620:0:862:ed1a::4001",
        "2404:6800:4002:804::4001",
        "2620:109:c002::4001",
        "2a01:578:3::4001",
        "2404:6800:4007:805::4001",
        "2406:2000:e4:1a::4001"};

    public static void main(String[] args) throws UnknownHostException {
        String ipv4 = "192.168.1.1";
        System.out.println("Ipv4=" + ipv4);
        String ipv6 = wrapIpv4ToIpv6(ipv4);
        System.out.println("Ipv6=" + ipv6);
        ipv4 = unwrapIpv6ToIpv4(ipv6);
        System.out.println("Ipv4=" + ipv4);
    }

    public static String wrapIpv4ToIpv6(String ip) throws UnknownHostException {
        byte[] arr = ipToLong(ip);
        System.out.println("" + arr[0] + " " + arr[1] + " " + arr[2] + " " + arr[3]);
        arr[0] ^= xor[0];
        arr[1] ^= xor[1];
        arr[2] ^= xor[2];
        arr[3] ^= xor[3];
        String prefix = ipv6Prefix[((int) (Math.random() * 100)) % ipv6Prefix.length];
        byte[] ipv6 = IPAddressParser.parseIPv6Literal(prefix);
        ipv6[9] = arr[0];
        ipv6[10] = arr[2];
        ipv6[12] = arr[3];
        ipv6[13] = arr[1];
        InetAddress addr = Inet6Address.getByAddress(ipv6);
        return addr.getHostAddress();
    }

    public static String unwrapIpv6ToIpv4(String ip) {
        byte[] ipv6 = IPAddressParser.parseIPv6Literal(ip);
        byte[] arr = new byte[4];
        arr[0] = (byte) ((ipv6[9] ^ xor[0]) & 0xFF);
        arr[1] = (byte) ((ipv6[13] ^ xor[1]) & 0xFF);
        arr[2] = (byte) ((ipv6[10] ^ xor[2]) & 0xFF);
        arr[3] = (byte) ((ipv6[12] ^ xor[3]) & 0xFF);
        return byteArrayToIp(arr);
    }

    public static String byteArrayToIp(byte[] ip) {
        return ((ip[0]) & 0xFF) + "." + ((ip[1]) & 0xFF) + "." + ((ip[2]) & 0xFF) + "." + (ip[3] & 0xFF);
    }

    public static byte[] ipToLong(String ipAddress) {
        long result = 0;
        byte[] arr = new byte[4];
        String[] ipAddressInArray = ipAddress.split("\\.");
        for (int i = 0; i < 4; i++) {
            arr[i] = (byte) (Integer.parseInt(ipAddressInArray[i]) & 0xFF);
        }
        return arr;
    }
}
