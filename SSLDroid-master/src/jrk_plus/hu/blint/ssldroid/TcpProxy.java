package jrk_plus.hu.blint.ssldroid;

import android.content.Context;

import java.io.IOException;

import opt.log.OmLogger;

import static opt.log.OmLogger.logger;

/**
 * This is a modified version of the TcpTunnelGui utility borrowed from the
 * xml.apache.org project.
 */
public class TcpProxy {
    String tunnelName;
    int listenPort;
    String tunnelHost;
    int tunnelPort;
    String keyFile, keyPass;
    jrk_plus.hu.blint.ssldroid.TcpProxyServerThread server = null;
    String sni = null;
    private Context ctx = null;

    public TcpProxy(String tunnelName, int listenPort, String targetHost,
                    int targetPort, String keyFile, String keyPass, String sni, Context ctx) {
        this.tunnelName = tunnelName;
        this.listenPort = listenPort;
        this.tunnelHost = targetHost;
        this.tunnelPort = targetPort;
        this.keyFile = keyFile;
        this.keyPass = keyPass;
        this.sni = sni;
        this.ctx = ctx;

        final jrk_plus.hu.blint.ssldroid.MyDroidLogger logger = new jrk_plus.hu.blint.ssldroid.MyDroidLogger();
        opt.log.OmLogger.setLogger(logger);
        opt.log.OmLogger.enableAndroidLogging();
        opt.log.OmLogger.setLogLevel(OmLogger.LogLevel.NONE);
        logger.start();
    }

    public void serve() throws IOException {
        server = new jrk_plus.hu.blint.ssldroid.TcpProxyServerThread(this.tunnelName, this.listenPort, this.tunnelHost,
                this.tunnelPort, this.keyFile, this.keyPass, this.sni, this.ctx);
        server.start();
    }

    public void stop() {
        logger.info("SSLDroid", "Server : " + server);
        if (server != null) {
            try {
                //close the server socket and interrupt the server thread
                if (server.socketClient != null)
                    server.socketClient.close();
                if (server.st != null) {
                    server.st.close();
                }
                server.ss.close();
                server.interrupt();
            } catch (Exception e) {
                logger.info("SSLDroid", "Inter`rupt failure: " + e.toString());
            }
        }
        logger.info("SSLDroid", "Stopping tunnel " + this.listenPort + ":" + this.tunnelHost + ":" + this.tunnelPort);
    }

    //if the listening socket is still active, we're alive
    public boolean isAlive() {
        return server.ss.isBound();
    }

}
