package com.jrk_plus.fragments;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.net.VpnService;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.jrk_plus.R;
import com.jrk_plus.NewDashboard.AnimatedExpandableListView;
import com.jrk_plus.NewDashboard.ConnectActivity;
import com.jrk_plus.NewDashboard.ExpandableListAdapter;
import com.jrk_plus.model.Server;
import com.jrk_plus.util.PropertiesService;
import com.jrk_plus.utilities.CountryInfo;
import com.jrk_plus.utilities.MessageConstatns;
import com.jrk_plus.utilities.PrefManager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.core.ConfigParser;
import de.blinkt.openvpn.core.OpenVPNService;
import de.blinkt.openvpn.core.ProfileManager;
import de.blinkt.openvpn.core.VPNLaunchHelper;
import de.blinkt.openvpn.core.VpnStatus;

import static androidx.appcompat.app.AppCompatActivity.RESULT_OK;
import static com.jrk_plus.Tunnel.ConfigurationConstants.CONNECT_ACTIVITY_BROADCAST_ACTION;
import static opt.log.OmLogger.logger;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CountriesListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CountriesListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class CountriesListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ExpandableListAdapter listAdapter;
    AnimatedExpandableListView expListView;
    private ExampleAdapter adapter;
    private List<Server> countryList;
    private List<Server> excludedCountryList = new ArrayList<>();
    PrefManager prefManager;
    private CountDownTimer counterTimer = null;
    private String shortCountry = "";
    public ChildItem currentlyClickedChild = null, connectingChildItem = null;
    private int previouslyExpandedPosition = -1;
    public GroupItem connectingGroupItem = null;
    private VpnProfile vpnProfile;
    private static final int START_VPN_PROFILE = 70;
    protected OpenVPNService mService;
    boolean mBound = false;
    private WaitConnectionAsync waitConnection;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView noServerText;

    public CountriesListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CountriesListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CountriesListFragment newInstance(String param1, String param2) {
        CountriesListFragment fragment = new CountriesListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        countryList = ConnectActivity.dbHelper.getUniqueCountries();
        prefManager = new PrefManager(getActivity());

        Intent intent = new Intent(getContext(), OpenVPNService.class);
        intent.setAction(OpenVPNService.START_SERVICE);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_countries_list, container, false);
         logger.info("On view created");
        setEventListner(view);
        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Intent intent = new Intent(getActivity(), OpenVPNService.class);
        intent.setAction(OpenVPNService.START_SERVICE);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        setUpListView();
    }

    private void setUpListView() {
        final ArrayList<CountryInfo> countryListName = new ArrayList<>();
        for (Server server : countryList) {
            String localeCountryName = ConnectActivity.localeCountries.get(server.getCountryShort()) != null ?
                    ConnectActivity.localeCountries.get(server.getCountryShort()) : server.getCountryLong();
            CountryInfo ci = new CountryInfo(localeCountryName, server.getCountryShort());
             logger.info("CountryName", "Entry country Name ====" + ci.getCountryName());
            String trim = ci.getCountryName().trim();
            if ((ci.getCountryName().equalsIgnoreCase("Australia") || ci.getCountryName().equalsIgnoreCase("Canada") || ci.getCountryName().equalsIgnoreCase("Finland") || ci.getCountryName().equalsIgnoreCase("France")
                    || ci.getCountryName().equalsIgnoreCase("Germany") || ci.getCountryName().equalsIgnoreCase("Hongkong") || ci.getCountryName().equalsIgnoreCase("Japan")
                    || ci.getCountryName().equalsIgnoreCase("South korea") || ci.getCountryName().equalsIgnoreCase("Russia") || ci.getCountryName().equalsIgnoreCase("Singapore")
                    || ci.getCountryName().equalsIgnoreCase("Ukraine") || ci.getCountryName().equalsIgnoreCase("United kingdom") || ci.getCountryName().equalsIgnoreCase("India")
                    || ci.getCountryName().equalsIgnoreCase("Austria") || ci.getCountryName().equalsIgnoreCase("Netherland") || ci.getCountryName().equalsIgnoreCase("Spain")
                    || ci.getCountryName().equalsIgnoreCase("United states") || ci.getCountryName().equalsIgnoreCase("Bulgaria"))) {
                excludedCountryList.add(server);
                countryListName.add(ci);
            }
        }
        setUpExpandableView(countryListName);
    }

    //    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(mConnection != null) {
            getActivity().unbindService(mConnection);
            mConnection = null;
        }
    }

    @Override
    public void onRefresh() {
        setUpListView();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void setEventListner(View view) {
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        expListView = view.findViewById(R.id.listView);
        noServerText = view.findViewById(R.id.no_server_text);

        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setUpExpandableView(ArrayList<CountryInfo> list) {


         logger.info("CountriesListFrag", "Country List size ====" + list.size());
        List<GroupItem> items = new ArrayList<GroupItem>();

        // Populate our list with groups and it's children
        for (int i = 0; i < list.size(); i++) {
            GroupItem item = new GroupItem();
            item.parentCountry = list.get(i);
            final List<Server> serverList = ConnectActivity.dbHelper.getServersByCountryCode(list.get(i).getShortCountryName());
            for (int j = 0; j < serverList.size(); j++) {
                ChildItem child = new ChildItem();
                child.childServer = serverList.get(j);
                item.items.add(child);
            }
            items.add(item);
        }
        if (items.size() == 0) {
            noServerText.setVisibility(View.VISIBLE);
        } else {
            noServerText.setVisibility(View.GONE);
        }
        adapter = new ExampleAdapter(getActivity());
        adapter.setData(items);
        expListView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);

        /////////////////////////////

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View view,
                                        int groupPosition, int childPosition, long id) {

                logger.info("CountryListFrag", "Child clicked");
                int iCount;
                int iIdx;
                ChildItem item;
                item = adapter.getChild(groupPosition, childPosition);
                iCount = adapter.getChildrenCount(groupPosition);
                if (item.curSelectedState) {
                    item.curSelectedState = false;
                } else {
                    item.curSelectedState = true;
                    for (iIdx = 0; iIdx < iCount; iIdx++) {
                        if (iIdx != childPosition) {
                            item = adapter.getChild(groupPosition, iIdx);
                            if (item != null) {
                                item.curSelectedState = false;
                            }
                        }
                    }
                }
                adapter.notifyDataSetChanged();
//                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
//                Toast.makeText(
//                        getApplicationContext(),
//                        listDataHeader.get(groupPosition)
//                                + " : "
//                                + listDataChild.get(
//                                listDataHeader.get(groupPosition)).get(
//                                childPosition), Toast.LENGTH_SHORT)
//                        .show();
                return true;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });

        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long l) {
                if (expListView.isGroupExpanded(groupPosition)) {
                    if (currentlyClickedChild != null) {
                        currentlyClickedChild.curSelectedState = false;
                        adapter.notifyDataSetChanged();
                    }
                    expListView.collapseGroupWithAnimation(groupPosition);
                } else {
                    if (previouslyExpandedPosition != -1 && previouslyExpandedPosition != groupPosition) {
                        if (currentlyClickedChild != null) {
                            currentlyClickedChild.curSelectedState = false;
                            adapter.notifyDataSetChanged();
                        }
                        expListView.collapseGroupWithAnimation(previouslyExpandedPosition);
                        previouslyExpandedPosition = groupPosition;
                        expListView.expandGroupWithAnimation(groupPosition);
                    } else {
                        previouslyExpandedPosition = groupPosition;
                        expListView.expandGroupWithAnimation(groupPosition);
                    }
                }
                return true;
            }
        });
    }

    private class GroupItem {
        CountryInfo parentCountry;
        List<ChildItem> items = new ArrayList<ChildItem>();
        boolean isParentOfConnectedChild = false;
    }

    private class ChildItem {
        Server childServer;
        boolean curSelectedState = false;
        boolean isThisChildConnected = false;
    }

    private class ChildHolder {
        TextView serverName, title, connectedText, serverStrengthText;
        ImageView serverStrengthIndicator;
        ImageView connectImage, disconnectImage;
        RadioButton radioButton;
    }

    private class GroupHolder {
        TextView title, connectedText;
        ImageView countryFlag;
    }

    private class ExampleAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
        private LayoutInflater inflater;

        private List<GroupItem> items;

        public ExampleAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<GroupItem> items) {
            this.items = items;
        }

        @Override
        public ChildItem getChild(int groupPosition, int childPosition) {
            return items.get(groupPosition).items.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getRealChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            final ChildHolder holder;
            final GroupItem groupItem = getGroup(groupPosition);
            final ChildItem childItem = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new ChildHolder();
                convertView = inflater.inflate(R.layout.list_item, parent, false);
                holder.title = convertView.findViewById(R.id.textTitle);
                holder.serverName = convertView.findViewById(R.id.server_name);
                holder.serverStrengthIndicator = convertView.findViewById(R.id.server_strength_indicator);
                holder.connectImage = convertView.findViewById(R.id.connect_button);
                holder.disconnectImage = convertView.findViewById(R.id.disconnect_button);
                holder.serverStrengthText = convertView.findViewById(R.id.server_strength_text);
                holder.radioButton = convertView.findViewById(R.id.radio_button);
                holder.connectedText = convertView.findViewById(R.id.connected_text);
                convertView.setTag(holder);
            } else {
                holder = (ChildHolder) convertView.getTag();
            }
            final Server childServer = childItem.childServer;
            int number = childPosition + 1;
            String title = "#".concat(childServer.getCountryShort() + " " + number);
            String shortServerName = "";
            if (childServer.getRandomHostName().length() > 5) {
                shortServerName = childServer.getRandomHostName().substring(0, 5);
            } else {
                shortServerName = childServer.getRandomHostName();
            }
            if (childItem.curSelectedState) {
                currentlyClickedChild = childItem;
                if (holder.connectImage.getVisibility() != View.VISIBLE) {
                    if (prefManager.getConnectionWithServer().equalsIgnoreCase(childServer.getHostName())) {
                        slideIn(holder.disconnectImage, holder.radioButton);
//                        holder.radioButton.setChecked(true);
                    } else {
                        slideIn(holder.connectImage,holder.radioButton);
//                        holder.radioButton.setChecked(true);
                    }
                }
                else {
                    slideOut(holder.connectImage,holder.radioButton);
//                    holder.radioButton.setChecked(false);
                }
            } else {
                if (holder.connectImage.getVisibility() == View.VISIBLE) {
                    slideOut(holder.connectImage, holder.radioButton);
                    holder.radioButton.setChecked(false);
                } else if (holder.disconnectImage.getVisibility() == View.VISIBLE) {
                    slideOut(holder.disconnectImage, holder.radioButton);
                    holder.radioButton.setChecked(false);
                } else {
                    slideOut(holder.connectImage,holder.radioButton);
                    slideOut(holder.disconnectImage,holder.radioButton);
//                    holder.connectImage.setVisibility(View.GONE);
//                    holder.disconnectImage.setVisibility(View.GONE);
//                    holder.radioButton.setChecked(false);
                }
            }

            switch (childServer.getQuality()) {
                case 0:
                    //Inactive
                    holder.serverStrengthText.setText("0%");
                    holder.serverStrengthIndicator.setColorFilter(getActivity().getResources().getColor(R.color.red));
                    break;
                case 1:
                    //Bad
                    holder.serverStrengthText.setText("40%");
                    holder.serverStrengthIndicator.setColorFilter(getActivity().getResources().getColor(R.color.yellow));
                    break;
                case 2:
                    //Good
                    holder.serverStrengthText.setText("75%");
                    holder.serverStrengthIndicator.setColorFilter(getActivity().getResources().getColor(R.color.light_orange));
                    break;
                case 3:
                    //Excellent
                    holder.serverStrengthText.setText("100%");
                    holder.serverStrengthIndicator.setColorFilter(getActivity().getResources().getColor(R.color.green));
                    break;
                default:
                    //Inactive
                    holder.serverStrengthText.setText("0%");
                    holder.serverStrengthIndicator.setColorFilter(getActivity().getResources().getColor(R.color.red));
                    break;
            }

            holder.title.setText(title);
            holder.serverName.setText(shortServerName);
            if (childItem.isThisChildConnected) {
                holder.connectedText.setVisibility(View.VISIBLE);
            } else {
                holder.connectedText.setVisibility(View.GONE);
            }
            holder.connectImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    slideOut(holder.connectImage, holder.radioButton);
//                    holder.radioButton.setChecked(false);
                    if (prefManager.getMaxFailedAttempt() < 10 && !prefManager.isValidityExpired()) {
                        disconnectAndReplaceConnection();  //First disconnect existing connection if it exist
                        startConnection(childServer);
                        childItem.curSelectedState = false;
                        connectingChildItem = childItem;
                        connectingGroupItem = groupItem;
                    } else {
                        if (prefManager.isValidityExpired()) {
                            Toast.makeText(getActivity(), "Your validity expired", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Reattempt exceeded specified number", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });

            holder.disconnectImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    disconnectConnection();
                    slideOut(holder.disconnectImage, holder.radioButton);
//                    holder.radioButton.setChecked(false);
                    childItem.curSelectedState = false;
                    if (connectingChildItem != null) {
                        connectingChildItem.isThisChildConnected = false;
                        connectingChildItem = null;
                    }
                    connectingGroupItem = null;
                }
            });

            holder.radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int iCount;
                    int iIdx;
                    ChildItem item;
                    item = adapter.getChild(groupPosition, childPosition);
                    iCount = adapter.getChildrenCount(groupPosition);
                    if (item.curSelectedState) {
                        item.curSelectedState = false;
                    } else {
                        item.curSelectedState = true;
                        for (iIdx = 0; iIdx < iCount; iIdx++) {
                            if (iIdx != childPosition) {
                                item = adapter.getChild(groupPosition, iIdx);
                                if (item != null) {
                                    item.curSelectedState = false;
                                }
                            }
                        }

                    }

                    adapter.notifyDataSetChanged();
                }
            });
            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return items.get(groupPosition).items.size();
        }

        @Override
        public GroupItem getGroup(int groupPosition) {
            return items.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return items.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupHolder holder;
            GroupItem item = getGroup(groupPosition);
            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.group_item, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.countryFlag = (ImageView) convertView.findViewById(R.id.country_flag);
                holder.connectedText = convertView.findViewById(R.id.connected_text);
                convertView.setTag(holder);
            } else {
                holder = (GroupHolder) convertView.getTag();
            }

            CountryInfo countryInfo = item.parentCountry;
            holder.title.setText(countryInfo.getCountryName());
            String countryShortName = countryInfo.getShortCountryName();
            if (item.isParentOfConnectedChild) {
                holder.connectedText.setVisibility(View.VISIBLE);
            } else {
                holder.connectedText.setVisibility(View.GONE);
            }
            if (countryShortName.equals("DO"))
                countryShortName = "dom";
             logger.info( "Short country name ==" + countryShortName);
            holder.countryFlag
                    .setImageResource(
                            getActivity().getResources().getIdentifier(countryShortName.toLowerCase(),
                                    "drawable",
                                    getActivity().getPackageName()));

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }

    }

    // slide the view from below itself to the current position
    public void slideIn(View view ,RadioButton rb) {

        logger.info("SlideIn");
//        TranslateAnimation animate = new TranslateAnimation(
//                view.getWidth(),                 // fromXDelta
//                0,                 // toXDelta
//                0,  // fromYDelta
//                0);                // toYDelta
//        animate.setDuration(100);
//        animate.setFillAfter(true);
//        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);
        rb.setChecked(true);
    }

    // slide the view from its current position to below itself
    public void slideOut(View view,RadioButton rb) {
        logger.info("SlideOut");
//        TranslateAnimation animate = new TranslateAnimation(
//                0,                 // fromXDelta
//                view.getWidth(),                 // toXDelta
//                0,                 // fromYDelta
//                0); // toYDelta
//        animate.setDuration(time);
//        animate.setFillAfter(true);
//        view.startAnimation(animate);
        view.setVisibility(View.INVISIBLE);
        rb.setChecked(false);
    }

    public void disconnectConnection() {
        if (connectingChildItem != null && connectingGroupItem != null) {
            connectingChildItem.curSelectedState = false;
            connectingChildItem.isThisChildConnected = false;
            connectingGroupItem.isParentOfConnectedChild = false;
            adapter.notifyDataSetChanged();
        }
        stopVPN(false);
        prefManager.setConnectionState(ConnectActivity.VPN_CONNECTION_STATE.NOT_CONNECTED);
        prefManager.setConnectionWithServer("");
        ((ConnectActivity) getActivity()).disConencted(false);
    }

    public void disconnectAndReplaceConnection(){
        if (connectingChildItem != null && connectingGroupItem != null) {
            connectingChildItem.curSelectedState = false;
            connectingChildItem.isThisChildConnected = false;
            connectingGroupItem.isParentOfConnectedChild = false;
            adapter.notifyDataSetChanged();
        }
        stopVPN(true);
        prefManager.setConnectionState(ConnectActivity.VPN_CONNECTION_STATE.NOT_CONNECTED);
        prefManager.setConnectionWithServer("");
        ((ConnectActivity) getActivity()).disConencted(false);

    }

    public void startConnection(final Server server) {
        //start connection
        ((ConnectActivity) getActivity()).disconenctConnection(false);
        prefManager.setConnectionState(ConnectActivity.VPN_CONNECTION_STATE.CONNECTING);
        shortCountry = server.getCountryShort();
        prefManager.setConnectionWithServer(server.getHostName());
        ((ConnectActivity) getActivity()).connecting(shortCountry, 1);
        prepareVpn(server);
    }

    public void connectionConnected() {
        prefManager.setConnectionState(ConnectActivity.VPN_CONNECTION_STATE.CONNECTED);
        if (connectingChildItem != null)
            connectingChildItem.isThisChildConnected = true;
        if (connectingGroupItem != null)
            connectingGroupItem.isParentOfConnectedChild = true;
        adapter.notifyDataSetChanged();
    }

    private boolean loadVpnProfile(Server currentServer) {
        try {
            byte[] data = Base64.decode(currentServer.getConfigData(), Base64.DEFAULT);
            logger.info("[CONFIG] :: " + new String(data));
            ConfigParser cp = new ConfigParser();
            InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(data));
            try {
                cp.parseConfig(isr);
                vpnProfile = cp.convertProfile();
                vpnProfile.mName = currentServer.getCountryLong();
                ProfileManager.getInstance(getActivity()).addProfile(vpnProfile);
            } catch (IOException | ConfigParser.ConfigParseError e) {
                e.printStackTrace();
                return false;
            }
        }catch (IllegalArgumentException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void startEasyVpn() {
        Intent intent = VpnService.prepare(getActivity());

        if (intent != null) {
            VpnStatus.updateStateString("USER_VPN_PERMISSION", "", R.string.state_user_vpn_permission,
                    VpnStatus.ConnectionStatus.LEVEL_WAITING_FOR_USER_INPUT);
            // Start the query
            try {
                startActivityForResult(intent, START_VPN_PROFILE);
            } catch (ActivityNotFoundException ane) {
                // Shame on you Sony! At least one user reported that
                // an official Sony Xperia Arc S image triggers this exception
                VpnStatus.logError(R.string.no_vpn_support_image);
            }
        } else {
            onActivityResult(START_VPN_PROFILE, RESULT_OK, null);
        }
    }

    public void prepareVpn(Server server) {
        if (loadVpnProfile(server)) {
            startEasyVpn();
        } else {
            Toast.makeText(getActivity(), getString(R.string.server_error_loading_profile), Toast.LENGTH_SHORT).show();
        }
    }

    private void stopVPN(boolean isReplace) {
         logger.info("HomeFragment", "Stopping Vpn");
        if (waitConnection != null)
            waitConnection.cancel(false);
        try {
            ProfileManager.setConntectedVpnProfileDisconnected(getActivity());
            if (mService.getManagement() != null)
                mService.getManagement().stopVPN(isReplace);

        } catch (Exception ex) {

        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            OpenVPNService.LocalBinder binder = (OpenVPNService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == START_VPN_PROFILE) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                VPNLaunchHelper.startOpenVpn(vpnProfile, getActivity().getBaseContext());
            } else {
                //Vpn permission  cancel
                Intent intent = new Intent(CONNECT_ACTIVITY_BROADCAST_ACTION);
                intent.putExtra(MessageConstatns.ID,5);
                getActivity().sendBroadcast(intent);
            }
        }
    }

    private class WaitConnectionAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                TimeUnit.SECONDS.sleep(PropertiesService.getAutomaticSwitchingSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            if (!statusConnection) {
//                if (currentServer != null)
//                    dbHelper.setInactive(currentServer.getIp());
//
//                if (fastConnection) {
//                    stopVpn();
//                    newConnecting(getRandomServer(), true, true);
//                } else if (PropertiesService.getAutomaticSwitching()){
//                    if (!inBackground)
//                        showAlert();
//                }
//            }
        }
    }

    public void serverDownloaded(){
        if(adapter != null){
            Log.e("CountryList","Setting adapter");
            countryList = ConnectActivity.dbHelper.getUniqueCountries();
            Log.e("CountryList","country size ==="+countryList.size());
            setUpListView();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopVPN(false);
        if(mConnection != null) {
            getActivity().unbindService(mConnection);
            mConnection = null;
        }
    }
}
