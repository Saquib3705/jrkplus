package com.jrk_plus.TransportLayer;

import android.content.Context;
import android.util.Log;

import com.jrk_plus.Tunnel.HttpWrapUnwrap;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.Arrays;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import opt.enc.stream.StreamEncryption;
import opt.packet.Packet;
import pseudoSsl.PseudoSslWrapUnWrap;

import static com.jrk_plus.Tunnel.PseudoSslInterface.STREAM_VALUE;
import static opt.log.OmLogger.logger;

//import org.apache.http.impl.io.ChunkedInputStream;

/**
 * Created by bot on 01/02/2018.
 */

public class OkktpHttpRequest {

    public static final MediaType MEDIA_TYPE = MediaType.parse("application/json");
    private Context mContext;
    private OkktpHttpResponseEvents mListner;

    public void sendRequest(Context mContext, String url, JSONObject object, final OkktpHttpResponseEvents mListner){

         logger.info("OkktpHttpRequest","Sending Request");
        this.mContext = mContext;
        this.mListner = mListner;
//        final OkHttpClient client = getOkHttpClient();
        final OkHttpClient client = getUnsafeOkHttpClient();
        JSONObject postdata = object;

        RequestBody body = RequestBody.create(MEDIA_TYPE,
                postdata.toString());

        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("User-Agent", "androidVPN")
                .addHeader("Accept-Language", "en-US,en;q=0.5")
                .build();
         logger.info("OkktpHttpRequest","Sending data===="+body);
         Log.d("NewConnectActivity","Request =====>"+request);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.e("Okktp3","failure response ===="+ mMessage);
                mListner.okktpHttpErrorResponse();
            }

            @Override
            public void onResponse(Call call, Response response)
                    throws IOException {
                String mMessage = response.body().string();
                if (response.isSuccessful()){
                    try {
                         logger.info("Okktp3","Successful Response--->"+mMessage);
                        mListner.okktpHttpResponse(mMessage);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public static void setSNIHost(final SSLSocketFactory factory, final SSLSocket socket, final String hostname) {
        try {
            socket.getClass().getMethod("setHostname", String.class).invoke(socket, hostname);
        } catch (Throwable e) {
            // ignore any error, we just can't set the hostname...
        }
    }

    public int sendDomainFrontingRequest(Context context, byte[] info, String sni, final OkktpHttpResponseEvents mListner) {
        this.mContext = context;
        this.mListner = mListner;

        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };
        OutputStream output = null;
        InputStream inputStream = null;

        SSLContext ssl_context = null;
        try {
            ssl_context = SSLContext.getInstance("TLS");
            ssl_context.init(null, trustAllCerts, null);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        final SSLSocketFactory sf = ssl_context.getSocketFactory();
        SSLSocket st = null;
        try {
            st = (SSLSocket) sf.createSocket();
            st.connect(new InetSocketAddress("www.google.com", 443), 20000);
            st.setSoTimeout(20000);
            logger.info("Hello exiting 11");
            setSNIHost(sf, (SSLSocket) st, sni);
            ((SSLSocket) st).startHandshake();
            logger.info("Hello exiting  12");
        } catch (Exception e) {

        }

        try {
            output = st.getOutputStream();
            inputStream = st.getInputStream();

            logger.info("[MESSAGE] : " + new String(info));
            output.write(info);

            logger.info("HTTPS Packet sent...");
//            HttpParser httpParser = new HttpParser();

            SimpleHttpServerConnection simpleHttpServerConnection = new SimpleHttpServerConnection(inputStream);
            String response = simpleHttpServerConnection.readResponse();
            mListner.okktpHttpResponse(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int sendPtlsRequest(Context context, byte[] info, String sni, final OkktpHttpResponseEvents mListner) {
        this.mContext = context;
        this.mListner = mListner;

        logger.info("[PTLS] :: Using PSEUDO_SSl PRotocol");
        Packet packet = new Packet(4096);
        PseudoSslWrapUnWrap sslWrapUnWrap = new PseudoSslWrapUnWrap(false);
        sslWrapUnWrap.ptlsConnect(sni, packet);
        StreamEncryption streamEncryption = StreamEncryption.getStreamEncryptionObject(STREAM_VALUE);
        StreamEncryption streamDecryption = StreamEncryption.getStreamEncryptionObject(STREAM_VALUE);

        byte[] message_received = new byte[4096];

        Socket clientSocket = new Socket();
        int tcpTimeout = 10000;

        try {
            clientSocket.connect(new InetSocketAddress(InetAddress.getByName("88.150.226.228"), 223), tcpTimeout);
            clientSocket.setSoTimeout(tcpTimeout);
            OutputStream outToServer = clientSocket.getOutputStream();
            InputStream input = clientSocket.getInputStream();
            outToServer.write(packet.buffer, packet.bufferOffset, packet.length());
            int ret = -1;
            Packet recvPacket = null;
            while (ret < 0) {
                int readLen = input.read(message_received);
                packet = new Packet(message_received, 0, readLen);

                if (!sslWrapUnWrap.setPseudoSslMsg(packet)) {
                    logger.warn("[PTLS] :: Setting Pseudo Msg Failed");
                    return -1;
                }
                recvPacket = new Packet(4096);
                ret = sslWrapUnWrap.pseudoSslUnWrap(recvPacket);

                logger.info("[PTLS] :: PseudoSslUnwrap return Value [" + ret + "] handshake [" + sslWrapUnWrap.getHandshakeState() + "]");
                switch (sslWrapUnWrap.getHandshakeState()) {
                    case SERVER_HELLO_DONE:
                        logger.info("[PTLS] :: SERVER HELLO DONE");
                        break;
                    default:
                        logger.info("[PTLS] :: Wrong Message Type");
                }
            }

            packet = new Packet(2048);
            streamEncryption.encrypt(info);

            packet.appendInEnd(info);
            sslWrapUnWrap.pseudoSslWrap(packet);
            outToServer.write(packet.buffer, packet.bufferOffset, packet.length());
//                Thread.sleep(5000);

            ret = -1;
            recvPacket.reset();

            HttpWrapUnwrap httpWrapUnwrap = new HttpWrapUnwrap();
            while(true) {
                while (ret < 0) {
                    int readLen = input.read(message_received);
                    logger.info("[PTLS] :: " + opt.utils.HexUtil.getArrayInHexFormat(message_received, 0, readLen));

                    packet = new Packet(message_received, 0, readLen);

                    if (!sslWrapUnWrap.setPseudoSslMsg(packet)) {
                        logger.warn("[PTLS] :: Setting Pseudo Msg Failed");
                        return -1;
                    }
                    ret = sslWrapUnWrap.pseudoSslUnWrap(recvPacket);

//                    logger.info("APP-DATA [" + ret + "] handshake [" + sslWrapUnWrap.getHandshakeState() + "]");
                    switch (sslWrapUnWrap.getHandshakeState()) {
                        case APP_DATA:
                            logger.info("[PTLS] :: SERVER HELLO DONE");
                            break;
                        default:
                            logger.info("[PTLS] :: Wrong Message Type");
                    }
                }

//            if (clientSocket != null)
//                clientSocket.close();

                logger.info("[PTLS] :: " + opt.utils.HexUtil.getArrayInHexFormat(recvPacket.buffer,
                        recvPacket.bufferOffset, recvPacket.length()));

                streamDecryption.decrypt(recvPacket);
//            httpWrapUnwrap.setHttpMessage(recvPacket.buffer, recvPacket.bufferOffset, recvPacket.length());

                logger.info("[PTLS] :: " + recvPacket.length());
                logger.info("[PTLS] :: " + opt.utils.HexUtil.getArrayInHexFormat(recvPacket.buffer,
                        recvPacket.bufferOffset, recvPacket.length()));
                String response = new String(com.jrk_plus.Provisioning.Information.receiveRegistrarInfo(context, Arrays.copyOfRange(recvPacket.buffer, recvPacket.bufferOffset, recvPacket.bufferOffset + recvPacket.length())));
                logger.info("[PTLS] :: " + response);

                httpWrapUnwrap.setHttpMessage(response);
                if(httpWrapUnwrap.getMsgState() == HttpWrapUnwrap.MSG_STATE.END)
                    break;
            }
//            new String(Arrays.copyOfRange(packet.buffer, packet.bufferOffset, packet.bufferOffset + packet.length()));
            mListner.okktpHttpResponse(httpWrapUnwrap.getFinalMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };
            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            SSLContext sc = null;
            try {
                sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
            builder.sslSocketFactory(sc.getSocketFactory());
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private OkHttpClient getOkHttpClient() {
        try {
//            // Create a trust manager that does not validate certificate chains
//            final TrustManager[] trustAllCerts = new TrustManager[] {
//                    new X509TrustManager() {
//                        @Override
//                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
//                        }
//
//                        @Override
//                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
//                        }
//
//                        @Override
//                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                            return new java.security.cert.X509Certificate[]{};
//                        }
//                    }
//            };
//            // Install the all-trusting trust manager
//            final SSLContext sslContext = SSLContext.getInstance("SSL");
//            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
//            // Create an ssl socket factory with our all-trusting manager
//            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
//
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
//            SSLContext sc = null;
//            try {
//                sc = SSLContext.getInstance("SSL");
//                sc.init(null, trustAllCerts, new SecureRandom());
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//            } catch (KeyManagementException e) {
//                e.printStackTrace();
//            }
//            builder.sslSocketFactory(sc.getSocketFactory());
//            builder.hostnameVerifier(new HostnameVerifier() {
//                @Override
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public interface OkktpHttpResponseEvents {
        /**
         * Callback fired when http request is made and response came from http server
         */
        void okktpHttpResponse(String response);

        void okktpHttpErrorResponse();

    }
}
