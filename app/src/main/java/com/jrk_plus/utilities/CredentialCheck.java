package com.jrk_plus.utilities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.jrk_plus.Provisioning.Information;
import com.jrk_plus.Tunnel.Config;
import com.jrk_plus.Tunnel.ConfigurationConstants;
import com.jrk_plus.Tunnel.CredentialResults;
import com.jrk_plus.Tunnel.Provisioning;
import com.jrk_plus.fragments.SelectedServer;


import jrk_plus.hu.blint.ssldroid.TcpProxy;

import static opt.log.OmLogger.logger;

public class CredentialCheck extends AsyncTask<Void, Void, Void> {
    CredentialResults credentialResults = null;
    public Context ctx;
    private String username, password, imei;
    boolean responseReceived = false;
    //    boolean isCredentialCheck = true;
    private SelectedServer selectedServer;
    private int lastErrorCode = 0;
    static TcpProxy tp = null;

    public enum RequestType {
        PROV,
        EASY_VPN,
        SELECTED_SERVER
    }

    RequestType requestType;

    public CredentialCheck(Context ctx, String username, String password, String imei, RequestType requestType,
                           SelectedServer selectedServer) {
        this.ctx = ctx;
        logger.info("[CONTEXT] : " + this.ctx.toString());

        this.username = username;
        this.password = password;
        this.imei = imei;
//        this.isCredentialCheck = isCredentialCheck;
        this.requestType = requestType;
        credentialResults = (CredentialResults) ctx;
        this.selectedServer = selectedServer;
    }

//    public interface TaskDelegate {
//        public void taskCompletionResult(String result);
//    }

    public void sendProvRequest(final String vpnProxyIp, final ConfigurationConstants.TunnelType tunnelType) {
        System.out.println("ExpiryTime:-sendProvRequest-");
        AsyncTask<Object, Integer, Integer> asyncTask = new AsyncTask<Object, Integer, Integer>() {
            @Override
            protected Integer doInBackground(Object[] objects) {
                Config config = new Config();
                config.setRemoteIp(vpnProxyIp);
                config.setRemotePort(265);
                config.setClientChildCount(1);
                config.setClientMppc(20000);
                config.setTunnelType(tunnelType);
//                String version = Build.VERSION.RELEASE;

                config.setSni("web.whatsapp.com");

                Provisioning startProv = new Provisioning(config, (Provisioning.ReceiveProvInfoInterface) ctx, ctx);
                logger.info("[IMEI] :: " + imei);
                int errorCode = startProv.sendProvInfo(ctx, username, password, imei);
                lastErrorCode = errorCode;
                return errorCode;
            }

            @Override
            protected void onPostExecute(Integer errorCode) {
                super.onPostExecute(errorCode);
//                if (!responseReceived) {
                credentialResults.getErrorCode(errorCode);
                if (errorCode == 0) {
                    credentialResults.onTaskCompletion(errorCode, selectedServer, ctx, requestType);
                }
                responseReceived = true;
//                    if (!isCredentialCheck)
//                        credentialResults.onTaskCompletion(errorCode, selectedServer, ctx);
//                }
            }
        };
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String vpnProxyIp = "1.1.1.1";
//        try {
//            DnsResolverAPI dnsResolverAPI = new DnsResolverAPI(ctx.getString(R.string.ptls_domain));
//            vpnProxyIp = dnsResolverAPI.getVpnProxyIp();
//
//            logger.info("[RESOLVER] :: Google Dns Resolved IP: " + vpnProxyIp);
//
//            if (vpnProxyIp == null) {
//                PrefManager prefManager = new PrefManager(this.ctx);
//                String domainNamesList = prefManager.getDomainNames();
//
//                String[] domainNames = domainNamesList.split(";");
//                logger.info("[RESOLVER] :: Domain Name List : " + domainNamesList);
//
//                for (String domain : domainNames) {
//                    Set<String> vpnProxyIpList = DnsResolverAPI.resolveAddress(domain, T_A);
//                    logger.info("[RESOLVER] :: Direct Dns Resolved IP: " + vpnProxyIpList);
//
//                    if (vpnProxyIpList.size() > 0) {
//                        for (String ip : vpnProxyIpList) {
//                            logger.info("[RESOLVER] : IP : " + ip);
//                        }
//                        vpnProxyIp = vpnProxyIpList.iterator().next();
//                        break;
//                    }
////            else
////                vpnProxyIp = "88.150.226.228";
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        logger.info("[RESOLVER] :: Final Resolved IP: " + vpnProxyIp);

        sendProvRequest(vpnProxyIp, ConfigurationConstants.TunnelType.DOMAIN_FRONT);

        Information.provisioningDone = 1;

//        try {
//            Thread.sleep(60 * 1000);
//        }catch (Exception ex)
//        {
//
//        }
//        sendProvRequest(vpnProxyIp, ConfigurationConstants.TunnelType.DOMAIN_FRONT);
//        try {
//            Thread.sleep(60 * 1000);
//        }catch (Exception ex)
//        {
//
//        }
        //sendProvRequest(vpnProxyIp, ConfigurationConstants.TunnelType.DOMAIN_FRONT);
        //sendProvRequest(vpnProxyIp, ConfigurationConstants.TunnelType.PSEUDO_SSL);

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

}

