package com.jrk_plus.Tunnel;

/**
 * Created by ABHISHEK MAHATO on 4/26/2017.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import opt.packet.Packet;
import opt.utils.Base64;

import static opt.log.OmLogger.logger;

/**
 * @author om
 */
public class Http {

    public HttpHeaderType httpHeaderType;

    public enum HttpHeaderType {
        HTTP_HEADER_NONE(0),
        HTTP_HEADER_1(1),
        HTTP_BASE64_HEADER_1(9);

        public final int header;
        private final boolean isBase64Encoding;

        private HttpHeaderType(int ac) {
            header = ac;
            if (header >= 9 && header <= 16) {
                isBase64Encoding = true;
            } else {
                isBase64Encoding = false;
            }
        }

        public static HttpHeaderType getHttpHeaderType(int ty) {
            for (HttpHeaderType footerType : HttpHeaderType.values()) {
                if (ty == footerType.header) {
                    return footerType;
                }
            }
            return HTTP_HEADER_NONE;
        }

        public boolean isBase64EncodingHeader() {
            return isBase64Encoding;
        }
    }

    private static byte[] httpHeader1;
    private static byte[] httpHeader1StartDelimeter;

    private static byte[] httpBase64Header1;
    private static byte[] httpBase64Header1StartDelimeter;
    private static byte[] httpHeaderEndDelimeter;
    private static byte[] httpBase64HeaderEndDelimeter;
    private static byte[] httpRandomHeader1;
    private static byte[] httpRandomHeader1StartDelimeter;
    private static byte[] httpRandomHeader1EndDelimeter;

    public static void setConfigurationData(HttpConfig configData) {
        logger.info("Http Header[] : " + configData.getHttpHeader1());
        logger.info("Http Start Delim[] : " + configData.getHttpHeader1StartDelimeter());
        logger.info("Http End Delim[] : " + configData.getHttpHeaderEndDelimeter());
        try {
//            httpHeader1 = HexUtil.hexToString(configData.getHttpHeader1()).getBytes();
//            httpHeader1StartDelimeter = HexUtil.hexToString(configData.getHttpHeader1StartDelimeter()).getBytes();

            httpHeader1 = configData.getHttpHeader1().getBytes();
            httpHeader1StartDelimeter = configData.getHttpHeader1StartDelimeter().getBytes();
            httpHeaderEndDelimeter = configData.getHttpHeaderEndDelimeter().getBytes();

            httpBase64Header1 = configData.getHttpBase64Header1().getBytes();
            httpBase64Header1StartDelimeter = configData.getHttpBase64Header1StartDelimeter().getBytes();

//            httpHeaderEndDelimeter = HexUtil.hexToString(configData.getHttpHeaderEndDelimeter()).getBytes();
            httpBase64HeaderEndDelimeter = configData.getHttpBase64HeaderEndDelimeter().getBytes();

            httpRandomHeader1 = configData.getHttpRandomHeader1().getBytes();
            httpRandomHeader1StartDelimeter = configData.getHttpRandomHeader1StartDelimeter().getBytes();
            httpRandomHeader1EndDelimeter = configData.getHttpRandomHeader1EndDelimeter().getBytes();
        } catch (Exception e) {
            logger.debug("Exception in Reading Http Header", e);
        }
    }

    private Packet httpMsg;

    public Http(HttpHeaderType hTpye) {
        httpMsg = new Packet(4096);
        this.httpHeaderType = hTpye;
    }

    public boolean setHttpMsg(Packet packet) {
        if (httpMsg.remainingLength() > packet.length()) {
            httpMsg.appendInEnd(packet);
            return true;
        } else {
            return false;
        }
    }

    public int httpWrapClientSide(Packet data, Packet httpData) {
        logger.info("Http Header : " + new String(httpHeader1));
        logger.info("Http Start Delim : " + new String(httpHeader1StartDelimeter));
        logger.info("Http End Delim : " + new String(httpHeaderEndDelimeter));

        if (data == null || httpData == null) {
            logger.error("Sanity Checking Failed in 'httpWrapServerSide' function");
            return -1;
        }
        byte[] header = null;
        byte[] startDelimeter = null;
        switch (httpHeaderType) {
            case HTTP_HEADER_NONE: {
                logger.warn("No HttpHeaderType Found");
                return -1;
            }
            case HTTP_HEADER_1: {
                header = httpHeader1;
                startDelimeter = httpHeader1StartDelimeter;
            }
            break;
            case HTTP_BASE64_HEADER_1: {
                header = httpBase64Header1;
                startDelimeter = httpBase64Header1StartDelimeter;
            }
            break;
        }
        if (header == null || startDelimeter == null || startDelimeter.length <= 0) {
            logger.warn("Wraping Problem.. header [" + header + "] startDelimeter [" + startDelimeter + "]");
            return -1;
        }
        if (httpHeaderType.isBase64EncodingHeader()) {
            httpData.appendInEnd(header);
            httpData.appendInEnd(startDelimeter);
            if (Base64.encode(data, 0, data.length(), httpData) < 0) {
                logger.warn("Base64 Encoding Problem");
                return -1;
            }
            httpData.appendInEnd(httpBase64HeaderEndDelimeter);
        } else {
            httpData.appendInEnd(header);
            httpData.appendInEnd(startDelimeter);
            httpData.appendInEnd(data);
            httpData.appendInEnd(httpHeaderEndDelimeter);
        }
        return httpData.length();
    }

    public int httpUnWrapClientSide(Packet data) {
        if (data == null) {
            logger.error("Sanity Checking Failed in 'httpUnWrapServerSide' function");
            return -1;
        }
        int endDelimeterIndex = -1;
        int ret = -1;
        int startIndexOfData = -1;
        endDelimeterIndex = httpMsg.indexOf(httpHeaderEndDelimeter);
        if (endDelimeterIndex <= 0) {
            logger.debug("Finding Start Delimeter [" + new String(httpBase64Header1StartDelimeter) + "]");
            if ((ret = httpMsg.indexOf(httpBase64Header1StartDelimeter)) >= 0) {
                httpHeaderType = HttpHeaderType.getHttpHeaderType(9);
                startIndexOfData = ret + httpBase64Header1StartDelimeter.length;
            } else if (httpRandomHeader1StartDelimeter.length > 0 && (ret = httpMsg.indexOf(httpRandomHeader1StartDelimeter)) >= 0) {
                httpHeaderType = HttpHeaderType.getHttpHeaderType(17);
                startIndexOfData = ret + httpRandomHeader1StartDelimeter.length;
            } else {
                httpHeaderType = HttpHeaderType.HTTP_HEADER_NONE;
                logger.warn("No HttpData Found to UnWrap");
                httpMsg.reset();
                return -1;
            }
            endDelimeterIndex = httpMsg.indexOf(httpBase64HeaderEndDelimeter);
            if (endDelimeterIndex <= 0) {
                logger.warn("No Http Data Found");
                return -1;
            }
        } else if ((ret = httpMsg.indexOf(httpHeader1StartDelimeter)) >= 0) {
            httpHeaderType = HttpHeaderType.getHttpHeaderType(1);
            startIndexOfData = ret + httpHeader1StartDelimeter.length;
        }
        int len = endDelimeterIndex - startIndexOfData;
        if (ret <= 0 || len <= 0 || endDelimeterIndex <= 0 || httpHeaderType == null || endDelimeterIndex <= startIndexOfData) {
            logger.warn("DataLength [" + len + "] found from Http Message endDelimeterIndex [" + endDelimeterIndex + "] startIndexOfData [" + startIndexOfData + "] Header [" + httpHeaderType + "]");
            logger.error("Some Problem in UnWrapping ");
            return -1;
        }
        logger.debug("DataLength [" + len + "] found from Http Message endDelimeterIndex [" + endDelimeterIndex + "] startIndexOfData [" + startIndexOfData + "] Header [" + httpHeaderType + "]");
        if (httpHeaderType.isBase64EncodingHeader()) {
            if (opt.utils.Base64.decode(httpMsg, startIndexOfData, len, data) < 0) {
                logger.warn("Base64 Decoding Problem");
                return -1;
            }
        } else {
            data.appendInEnd(httpMsg.buffer, httpMsg.bufferOffset + startIndexOfData, len);
        }
        httpMsg.bufferOffset += (endDelimeterIndex + httpHeaderEndDelimeter.length);
        httpMsg.compact();
        logger.debug("Remaining HttpMsg Length in Buffer [" + httpMsg.length() + "]");
        return len;
    }

    /*public int httpUnWrapClientSide(Packet data) {
        if (data == null) {
            logger.error("Sanity Checking Failed in 'httpUnWrapClientSide' function");
            return -1;
        }
        if (httpMsg.length() <= 0) {
            return -1;
        }

        boolean CRLF_GOT = false;
        int i = 0;
        for (i = 0; i <= (httpMsg.length() - SERVER_SIDE_DELIMITER.length); i++) {
            if (httpMsg.get(i) == '\r' && httpMsg.get(i + 1) == '\n'
                    && httpMsg.get(i + 2) == '\r' && httpMsg.get(i + 3) == '\n') {
                CRLF_GOT = true;
                break;
            }
        }
        if (!CRLF_GOT) {
            logger.warn("SERVER_SIDE_DELIMITER not found");
            return -1;
        }
        int delimiterIndex = i;
        int startIndexOfData;
        if (httpMsg.startWith(HTTP_200OK)) {
            int ret = httpMsg.indexOf(X_CONTENT_TYPE_OPTIONS);
            if (ret <= 0) {
                httpMsg.bufferOffset += (delimiterIndex + SERVER_SIDE_DELIMITER.length);
                httpMsg.compact();
                return -1;
            }
            startIndexOfData = ret;
            startIndexOfData += X_CONTENT_TYPE_OPTIONS.length;
            if (delimiterIndex <= startIndexOfData) {
                logger.warn("Delimiter Found early");
                httpMsg.bufferOffset += (delimiterIndex + SERVER_SIDE_DELIMITER.length);
                httpMsg.compact();
                return -1;
            }
            int len = delimiterIndex - startIndexOfData;
            if (data.buffer.length < len) {
                logger.warn("Not Enough Buffer");
                httpMsg.bufferOffset += (delimiterIndex + SERVER_SIDE_DELIMITER.length);
                httpMsg.compact();
                return -1;
            }
            data.appendInEnd(httpMsg.buffer, httpMsg.bufferOffset + startIndexOfData, len);
            httpMsg.bufferOffset += (delimiterIndex + SERVER_SIDE_DELIMITER.length);
            httpMsg.compact();
            return len;
        } else {
            httpMsg.bufferOffset += (delimiterIndex + SERVER_SIDE_DELIMITER.length);
            httpMsg.compact();
            return -1;
        }
    }*/

 /*public static int httpWrapClientSide(Packet data, Packet httpData) {
        if (data == null || httpData == null) {
            logger.error("Sanity Checking Failed in 'httpWrapClientSide' function");
            return -1;
        }
        byte[] verbName = chooseHttpVerb();
        int msgLen = verbName.length + data.length() + CLIENT_SIDE_DELIMITER.length;
        if (httpData.buffer.length < msgLen) {
            logger.error("Not Enough Buffer");
            return -1;
        }
        httpData.appendInEnd(verbName);
        httpData.appendInEnd(data);
        httpData.appendInEnd(CLIENT_SIDE_DELIMITER);
        if (httpData.length() != msgLen) {
            logger.warn("Something Wrong Happened..");
        }
        return httpData.length();
    }*/
}
