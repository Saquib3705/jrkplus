package com.jrk_plus.Provisioning;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PacktUtil {

    private static final int WIDTH = 40;
    public static final Charset charset = Charset.forName("ISO-8859-1");

    public static String getCountryCodeStr(short countryCode) {
        byte b[] = new byte[2];
        setShort(b, 0, countryCode);
        return (new String(b, charset));
    }

    public static short getCountryCodeNum(String countryShortName) {
        return twoByteToShort(countryShortName.getBytes(charset), 0);
    }

    public static void setShort(byte[] buffer, int offset, int val) {
        buffer[offset++] = (byte) ((val >>> 8) & 0x00FF);
        buffer[offset++] = (byte) (val & 0x00FF);
    }

    public static short twoByteToShort(byte data[], int index) {
        short t = (short) (data[index] & 0x00FF);
        return (short) ((t << 8) | (data[index + 1] & 0x00FF));
    }

    public static String catchExceptionStackTrace(Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString(); // stack trace as a string
    }

    public static String printHexDump(byte[] bytes, int len) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++) {
                if (i % WIDTH == 0) {
                    sb.append("\n");
                }
                sb.append(String.format("%02x ", bytes[i] & 0xff));
            }
            return sb.toString();
        } catch (Exception e) {
        }
        return "failed to print hexdump";
    }

    public static long getIpNumber(String ip) {
        String[] ipAddressInArray = ip.split("\\.");
        long result = 0;
        long ipNum = 0;
        for (int x = 3; x >= 0; x--) {
            ipNum = Long.parseLong(ipAddressInArray[3 - x]);
            result |= ipNum << (x << 3);
        }
        return result;
    }

    public static String getIpString(long ipNum) {
        int w = (int) ((ipNum / 16777216) % 256);
        int x = (int) ((ipNum / 65536) % 256);
        int y = (int) ((ipNum / 256) % 256);
        int z = (int) (ipNum % 256);
        String ipStr = w + "." + x + "." + y + "." + z;
        return ipStr;
    }

    public static int prepareMessage(byte message[], int Message_type) {
        message[0] = (byte) ((Message_type >> 8) & 0x00FF);
        message[1] = (byte) (Message_type & 0x00FF);
        message[2] = (byte) ((Constants.MINIMUM_PACKET_LEN >> 8) & 0x00FF);
        message[3] = (byte) (Constants.MINIMUM_PACKET_LEN & 0x00FF);
        return Constants.MINIMUM_PACKET_LEN;
    }

    public static int addAttr(int attributeType, byte[] attrValue, byte[] message) {
        int len = twoByteToInt(message, 2); //(int)(message[2]<<8| message[3]);

        message[len++] = (byte) ((attributeType >> 8) & 0x00FF);
        message[len++] = (byte) (attributeType & 0x00FF);
        message[len++] = (byte) ((attrValue.length >> 8) & 0x00FF);
        message[len++] = (byte) (attrValue.length & 0x00FF);

        for (int i = 0; i < attrValue.length; i++) {
            message[len + i] = attrValue[i];
        }
        len += attrValue.length;
        message[2] = (byte) ((len >> 8) & 0x00FF);
        message[3] = (byte) (len & 0x00FF);
        return len;
    }

    public static int twoByteToInt(byte data[], int index) {
        int t = data[index] & 0x00FF;
        return (t << 8) | (data[index + 1] & 0x00FF);
    }

    public static int addAttr(int attributeType, int attrValue, byte[] message) {
        int len = twoByteToInt(message, 2); //(int)(message[2]<<8| message[3]);

        message[len++] = (byte) ((attributeType >> 8) & 0x00FF);
        message[len++] = (byte) (attributeType & 0x00FF);
        message[len++] = (byte) ((2 >> 8) & 0x00FF);
        message[len++] = (byte) (2 & 0x00FF);

        message[len++] = (byte) ((attrValue >> 8) & 0x00FF);
        message[len++] = (byte) (attrValue & 0x00FF);

        message[2] = (byte) ((len >> 8) & 0x00FF);
        message[3] = (byte) (len & 0x00FF);
        return len;
    }

    public static String longToIp(long ip) {
        return ((ip >> 24) & 0xFF) + "."
                + ((ip >> 16) & 0xFF) + "."
                + ((ip >> 8) & 0xFF) + "."
                + (ip & 0xFF);
    }

    public static long ipToLong(String ipAddress) {
        long result = 0;

        String[] ipAddressInArray = ipAddress.split("\\.");

        for (int i = 3; i >= 0; i--) {
            long ip = Long.parseLong(ipAddressInArray[3 - i]);
            //left shifting 24,16,8,0 and bitwise OR
            //1. 192 << 24
            //1. 168 << 16
            //1. 1   << 8
            //1. 2   << 0
            result |= ip << (i * 8);
        }

        return result;
    }

    public static String calcDate(long millisecs) {
        SimpleDateFormat date_format = new SimpleDateFormat(Constants.DATA_LOG_DATE_FORMAT);
        Date resultdate = new Date(millisecs);
        return date_format.format(resultdate);
    }

    public static boolean isValidIP(String ipAddr) {
        Pattern ptn = Pattern.compile("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$");
        Matcher mtch = ptn.matcher(ipAddr);
        return mtch.find();
    }

    public static boolean isValidPort(int port) {
        return (port > 0 && port <= 65535);
    }

    public static byte[] convertReserverProvisioningConfigIntoByteArray(int magicKey, String ipStr) throws Exception {
        byte[] byteArr = new byte[ipStr.length()];
        int index = 0;
        String[] ipBatch = ipStr.split(";");
        String[] ipSetArr;
        InetAddress tempAddr;
        byte[] tempByteArr;
        int tempPortValue;
        int tempProtocol, tempPriority;

        //magic key
        byte lowerByte = (byte) (magicKey & 0xff);
        byte upperByte = (byte) ((magicKey >> 8) & 0xff);
        byteArr[index++] = upperByte;
        byteArr[index++] = lowerByte;

        for (int i = 0; i < ipBatch.length; i++) {
            ipSetArr = ipBatch[i].split(":");
            for (int j = 0; j < ipSetArr.length; j++) {
                switch (j) {
                    case 0:
                        //ip
                        tempAddr = InetAddress.getByName(ipSetArr[j]);
                        tempByteArr = tempAddr.getAddress();
                        System.arraycopy(tempByteArr, 0, byteArr, index, 4);
                        index = index + 4;
                        break;
                    case 1:
                        //port
                        tempPortValue = Integer.parseInt(ipSetArr[j]);
                        lowerByte = (byte) (tempPortValue & 0xff);
                        upperByte = (byte) ((tempPortValue >> 8) & 0xff);
                        byteArr[index++] = upperByte;
                        byteArr[index++] = lowerByte;
                        break;
                    case 2:
                        //protocol
                        tempProtocol = Integer.parseInt(ipSetArr[j]);
                        byteArr[index++] = (byte) tempProtocol;
                        break;
                    case 3:
                        //priority
                        tempPriority = Integer.parseInt(ipSetArr[j]);
                        byteArr[index++] = (byte) tempPriority;
                        break;
                }
            }
        }
        byte[] finalArr = new byte[index];
        System.arraycopy(byteArr, 0, finalArr, 0, index);
        //logger.debug(Arrays.toString(finalArr));
        return finalArr;
    }

    public static byte[] convertProvisioningConfigIntoByteArray(String ipStr) throws Exception {
        byte[] byteArr = new byte[ipStr.length()];
        int index = 0;

        String[] ipBatch = ipStr.split(";");
        String[] ipSetArr;
        InetAddress tempAddr;
        byte[] tempByteArr;
        int tempPortValue;
        int tempProtocol, tempPriority;

        for (int i = 0; i < ipBatch.length; i++) {
            //logger.debug("ipBatch[" + i + "]: " + ipBatch[i]);
            ipSetArr = ipBatch[i].split(":");
            for (int j = 0; j < ipSetArr.length; j++) {
                switch (j) {
                    case 0:
                        //ip
                        //logger.debug("ipSetArr[j]: " + ipSetArr[j]);
                        tempAddr = InetAddress.getByName(ipSetArr[j]);
                        tempByteArr = tempAddr.getAddress();
                        System.arraycopy(tempByteArr, 0, byteArr, index, 4);
                        index = index + 4;
                        break;
                    case 1:
                        //port
                        tempPortValue = Integer.parseInt(ipSetArr[j]);
                        byte lowerByte = (byte) (tempPortValue & 0xff);
                        byte upperByte = (byte) ((tempPortValue >> 8) & 0xff);
                        byteArr[index++] = upperByte;
                        byteArr[index++] = lowerByte;
                        break;
                    case 2:
                        //protocol
                        tempProtocol = Integer.parseInt(ipSetArr[j]);
                        byteArr[index++] = (byte) tempProtocol;
                        break;
                    case 3:
                        //priority
                        tempPriority = Integer.parseInt(ipSetArr[j]);
                        byteArr[index++] = (byte) tempPriority;
                        break;
                }
            }
        }
        byte[] finalArr = new byte[index];
        System.arraycopy(byteArr, 0, finalArr, 0, index);
        //logger.debug(Arrays.toString(finalArr));
        return finalArr;
    }

    public static Long cantorPairingFunc(int k1, Long k2) {
        return ((((k1 + k2) * (k1 + k2 + 1)) / 2) + k2);
    }

    public long dialerVersionToLong(String dialerVersion) {
        /**
         * dialerVersion must be '1.2.3' format means 2 dot exactly
         */
        long result = 0;
        String[] dialerVersionArr = dialerVersion.split("\\.");
        if (dialerVersionArr.length != 3) {
            throw new NumberFormatException("Dialer Version should be of format x.y.z");
        }
        for (int i = 2; i >= 0; i--) {
            long dialerVersionNum = Long.parseLong(dialerVersionArr[2 - i]);
            //left shifting 16,8,0 and bitwise OR
            //1. 1 << 16
            //2. 2   << 8
            //3. 3   << 0
            result |= dialerVersionNum << (i * 8);
        }
        return result;
    }

//    public static void closeDBConnection1(Connection con, PreparedStatement ps, ResultSet rs) {
//        try {
//            if (rs != null) {
//                rs.close();
//            }
//            if (ps != null) {
//                ps.close();
//            }
//            if (con != null) {
//                DatabaseManager.getInstance().freeConnection(con);
//            }
//        } catch (Exception  e) {
//            throw new RuntimeException(e);
//        }
//    }

//    public static void closeDBConnection(Connection con) {
//        try {
//            if (con != null) {
//                DatabaseManager.getInstance().freeConnection(con);
//            }
//        } catch (Exception  e) {
//            throw new RuntimeException(e);
//        }
//    }

    /**
     * Decrypt the packet passed to this function
     *
     * @param packet
     * @param length length of the packet
     * @return length of packet after decryption
     */
    public static byte[] decrypt(byte[] packet, int length) {
        return packet;
    }
    public static byte[] decrypt1(byte[] packet, int length) {
//         logger.info("Provising","inside decrypt");
//        if (packet == null || length <= 0) {
//             logger.info("Provising","returning null packet ==="+packet +" length =="+length);
//            return null;
//        }
//        byte[] decryptedPacket;
//         logger.info("Provising","inside decrypt 2");
//        int packet_length = -1;
//        try {
//            for (int i = 0; i < length; i++) {
//                packet[i] ^= length;
//            }
//            int paddingLength = packet[length - 1] & 0xFF;
//            packet[length - 1] = 0;
//            int key_start_index = (packet[length - 3] & 0xFF);
//            //logger.debug("Key_start_Index: " + key_start_index);
//            packet[length - 3] = 0;
//            int key_length = (int) (packet[length - 2] & 0xFF);
//            //logger.debug("key_length: " + key_length);
//            packet[length - 2] = 0;
//            packet_length = length - 3;
//            int present_key_index = 0;
//            if (paddingLength > 0) {
//                packet_length -= paddingLength;
//            }
//            if (key_length <= 0) {
//                return null;
//            }
//            for (int i = 0; i < packet_length; i++) {
//                if (i == key_start_index) {
//                    i += (key_length - 1);
//                } else {
//                    packet[i] ^= packet[key_start_index + present_key_index];
//                    present_key_index = (present_key_index + 1) % key_length;
//                }
//            }
//            decryptedPacket = new byte[packet_length];
//             logger.info("Provising","inside decrypt 3" + packet_length);
//
//             logger.info("Provising","inside decrypt 4" + decryptedPacket.length);
//
//            System.arraycopy(packet, 0, decryptedPacket, 0, packet_length);
//             logger.info("Provising","");
//            return decryptedPacket;
//        } catch (Exception e) {
//             logger.info("Provising","Inside exception of Packet Utills");
//            e.printStackTrace();
//            return null;
//        }
        if (packet == null || length <= 0) {
            return null;
        }
        byte[] decryptedPacket;
        int packet_length = -1;
        try {
            for (int i = 0; i < length; i++) {
                packet[i] ^= length;
            }
            System.out.println("packet " + (packet[length-1] & 0xFF) + " packet length " + packet.length);


            int paddingLength = packet[length - 1] & 0xFF;
            System.out.println("padding length: " + paddingLength);

            packet[length - 1] = 0;
            int key_start_index = (packet[length - 3] & 0xFF);
            //logger.debug("Key_start_Index: " + key_start_index);
            System.out.println("Key_start_Index: " + key_start_index);
            packet[length - 3] = 0;
            int key_length = (int) (packet[length - 2] & 0xFF);
            System.out.println("Key_length: " + key_length);

            //logger.debug("key_length: " + key_length);
            packet[length - 2] = 0;
            packet_length = length - 3;
            int present_key_index = 0;
            if (paddingLength > 0) {
                packet_length -= paddingLength;
            }
            if (key_length <= 0) {
                return null;
            }
            System.out.println("packet length: " + packet_length);

            for (int i = 0; i < packet_length; i++) {
                if (i == key_start_index) {
                    i += (key_length - 1);
                } else {
                    packet[i] ^= packet[key_start_index + present_key_index];
                    present_key_index = (present_key_index + 1) % key_length;
                }
            }
            decryptedPacket = new byte[packet_length];
            System.arraycopy(packet, 0, decryptedPacket, 0, packet_length);
            System.out.println("packet length: " + decryptedPacket.length);
            return decryptedPacket;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getRandomNum(int min, int max) {
        if (min >= max) {
            return min;
        }
        return (min + (int) ((Math.random() * 100) % (max - min)));
    }

    /**
     * Encrypt the packet passed to this function
     *
     * @param packet
     * @param length length of the packet
     * @return length of the encrypted packet
     */
    public static byte[] encrypt(byte[] packet, int length) {
        byte[] encryptedPacket;
        encryptedPacket = new byte[length];
        System.arraycopy(packet, 0, encryptedPacket, 0, length);
        return encryptedPacket;
    }
    public static byte[] encrypt1(byte[] packet, int length) {
        if (packet == null || length >= (packet.length - 3) || length <= 0) {
            return null;
        }
        byte key_start_index = 0;
        byte key_length = 0;
        byte[] encryptedPacket;
        int i = 0;
        try {
            byte rand = 0;
            if (true) {
                //byte rand = (byte) (Math.random() * 100);            
                int t = length >> 2;
                if (t < 100) {
                    rand = (byte) ((Math.random() * 1000) % t);
                } else {
                    rand = (byte) (Math.random() * 100);
                }
                rand += 10;
                if (packet.length < (length + 3 + rand)) {
                    rand = (byte) (packet.length - (length + 3));
                }
                length += rand;
                packet[length + 2] = rand;
            } else {
                packet[length + 2] = 0;
            }
            int mid = length / 2;
            key_start_index = (byte) (getRandomNum(mid / 2, mid) & 0x7F);
            key_length = (byte) (getRandomNum(2, mid / 2) & 0x7F);
            if (key_length == 0) {
                key_length = 1;
            }
            int present_key_index = 0;
            for (i = 0; i < length; i++) {
                if (i == key_start_index) {
                    i += (key_length - 1);
                } else {
                    packet[i] ^= packet[key_start_index + present_key_index];
                    present_key_index = (present_key_index + 1) % key_length;
                }
            }
            packet[length] = key_start_index;
            packet[length + 1] = key_length;

            int p_length = length + 3;
            for (i = 0; i < p_length; i++) {
                packet[i] ^= p_length;
            }
            encryptedPacket = new byte[p_length];
            System.arraycopy(packet, 0, encryptedPacket, 0, p_length);
            return encryptedPacket;
        } catch (Exception e) {
            return null;
        }
    }
     

}
