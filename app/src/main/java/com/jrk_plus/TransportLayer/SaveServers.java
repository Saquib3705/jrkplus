package com.jrk_plus.TransportLayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bot on 18/01/2018.
 */

public class SaveServers {

    private List<ServerFormate> serverList = new ArrayList<>() ;

    public SaveServers() {

    }

    public void addServer(String ip ,int port){
        ServerFormate serverFormate = new ServerFormate(ip,port);
        serverList.add(serverFormate);
    }

    public List<ServerFormate> getServerList(){
        return serverList;
    }
}
