package jrk_plus.hu.blint.ssldroid;

import android.util.Log;

import opt.log.LogData;

/**
 * Created by ABHISHEK MAHATO on 4/4/2017.
 */

public class MyDroidLogger extends opt.log.OmLogger {
    @Override
    protected void info(LogData logData) {
        Log.i((String) logData.tag, (String) logData.threadName+ logData.str);
    }

    @Override
    protected void debug(LogData logData) {
        if(logData.ex != null) {
            Log.d((String) logData.tag, (String) logData.threadName+ logData.str, logData.ex);
//            logger.info(logData.threadName, logData.str, logData.ex);
//            try {
//                queue.put(logData.ex.getStackTrace());
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }
        else {
           Log.d((String) logData.tag, (String) logData.threadName+ logData.str);
//            try {
//                queue.put(logData.str);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }
    }

    @Override
    protected void trace(LogData logData) {
        Log.v((String) logData.tag, (String) logData.threadName+ logData.str);
//        try {
//            queue.put(logData.str);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    protected void warn(LogData logData) {
        Log.w((String) logData.tag, (String) logData.threadName+ logData.str);
//        try {
//            queue.put(logData.str);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    protected void error(LogData logData) {
        Log.e((String) logData.tag, (String) logData.threadName+ logData.str);
//        try {
//            queue.put(logData.str);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    protected void fatal(LogData logData) {
        Log.e((String) logData.tag, (String) logData.threadName+ logData.str);
//        try {
//            queue.put(logData.str);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
}
