package com.jrk_plus.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.app.jrk_plus.R;

import static opt.log.OmLogger.logger;


public class SettingsFragmentTest extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String USERNAME = "pref_key_vpn_username";
    private static final String PASSWORD = "pref_key_password";
    private static final String TAG = SettingsFragmentTest.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Preference preferenceUsername = findPreference(USERNAME);
        SharedPreferences preferencesUserName = getPreferenceManager().getSharedPreferences();
        preferenceUsername.setSummary(preferencesUserName.getString("pref_key_vpn_username",""));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        assert view != null;
        view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.home_screen_background));

        return view;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setHasOptionsMenu(true);
        addPreferencesFromResource(R.xml.app_settings_preference);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        //toolbarCustom = (Toolbar) getActivity().findViewById(R.id.custom_toolbar);

       /* ((AppCompatActivity) getActivity()).setSupportActionBar(toolbarCustom);

        if( ((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.app_name);
        }
        ((HomeActivity)getActivity()).settingNavigationDrawerIcon();*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
//            ((ConnectActivity)getActivity()).openingDrawerInFragment();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(USERNAME)) {
            Preference preferenceUsername = findPreference(key);
            preferenceUsername.setSummary(sharedPreferences.getString(key,""));
             logger.info(TAG, "onSharedPreferenceChanged: UserNameChanged");
        } else if (key.equals(PASSWORD)) {
             logger.info(TAG, "onSharedPreferenceChanged: PasswordChanged");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
