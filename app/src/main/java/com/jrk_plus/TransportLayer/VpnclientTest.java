package com.jrk_plus.TransportLayer;///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.madeena.TransportLayer;
//
//import android.util.Log;
//
//import com.madeena.Tunnel.ConfigurationConstants;
//import com.madeena.Provisioning.Constants;
//import com.madeena.Provisioning.PacktUtil;
//import com.madeena.Provisioning.ServerConfig;
//import com.madeena.Provisioning.ServerDetails;
//import com.madeena.utilities.MessageConstatns;
//
//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;
//
//import java.io.StringWriter;
//import java.net.DatagramPacket;
//import java.net.DatagramSocket;
//import java.net.InetAddress;
//import java.util.ArrayList;
//import java.util.Iterator;
//
//import static opt.log.OmLogger.logger;
//
//public class VpnclientTest {
//
//    public VpnclientTest(){
//
//    }
//    public int connectDirectly(){
//            try {
//                JSONObject obj = new JSONObject();
//                obj.put(MessageConstatns.IMEI, "99432299392168");
//                obj.put(MessageConstatns.USER_NAME, "asd");
//                obj.put(MessageConstatns.PLATFORM, "android");
//                obj.put(MessageConstatns.PASSWORD, "1234");
//                obj.put(MessageConstatns.COUNTRY, "IN");
//                obj.put(MessageConstatns.VERSION, "1.1.1");
//                obj.put(MessageConstatns.ISP, "Etisalat");
//                obj.put(MessageConstatns.INTERNAL_VERSION, "1.12");
//                obj.put(MessageConstatns.BUILD_NUMBER, "mflkgjhu784y934.598437");
//                StringWriter out = new StringWriter();
//                obj.writeJSONString(out);
//                String jsonText = out.toString();
//                byte[] message = jsonText.getBytes("utf-8");
//                System.out.println("message" + message.length);
//                byte[] bs2WrapperBuff = new byte[2048];
//
//                PacktUtil.prepareMessage(bs2WrapperBuff, Constants.CONFIGURATION_REQUEST);
//                PacktUtil.addAttr(Constants.ATTR_TYPE_TRANSACTION_ID, (System.currentTimeMillis() + "").getBytes(), bs2WrapperBuff);
//                PacktUtil.addAttr(Constants.ATTR_TYPE_DIALER_IP, "180.10.11.12".getBytes(), bs2WrapperBuff);
//                PacktUtil.addAttr(Constants.ATTR_TYPE_BS1_PACKET, message, bs2WrapperBuff);
//                int len = PacktUtil.twoByteToInt(bs2WrapperBuff, 2);
//                //encrypt the packet
//                byte[] encryptedPacket = PacktUtil.encrypt(bs2WrapperBuff, len);
//                DatagramSocket clientSocket = new DatagramSocket();
//                clientSocket.setSoTimeout(500);
//                InetAddress IPAddress = InetAddress.getByName("88.150.226.227");
//                byte[] sendData = new byte[1024];
//                byte[] receiveData = new byte[65535];
//                DatagramPacket sendPacket = new DatagramPacket(encryptedPacket, encryptedPacket.length, IPAddress, 7896);
//                clientSocket.send(sendPacket);
//                System.out.println("Sending" + encryptedPacket.length);
//                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
//                clientSocket.receive(receivePacket);
//                System.out.println(" receivePacket.getLength()" + receivePacket.getLength());
//                //System.out.println("receivePacket.getData().length" + receivePacket.getData().length);
//                //String modifiedSentence = new String(receivePacket.getData());
//                byte[] decryptedPacket = PacktUtil.decrypt(receivePacket.getData(), receivePacket.getLength());
//
//                int msgLen, msgIndex;
//                int attrType, attrLen;
//                int index;
//                int msgType;
//                byte[] bs1Packet = null;
//                msgType = PacktUtil.twoByteToInt(decryptedPacket, 0);
//                System.out.println(" msg type " + msgType);
//
//                switch (msgType) {
//                    case Constants.CONFIGURATION_RESPONSE: {
//                        msgLen = PacktUtil.twoByteToInt(decryptedPacket, 2);
//                        msgIndex = Constants.MINIMUM_PACKET_LEN;
//                        while (msgIndex < msgLen) {
//                            attrType = PacktUtil.twoByteToInt(decryptedPacket, msgIndex);
//                            msgIndex += 2;
//                            attrLen = PacktUtil.twoByteToInt(decryptedPacket, msgIndex);
//                            msgIndex += 2;
//                            index = msgIndex;
//                            msgIndex += attrLen;
//                            switch (attrType) {
//                                case Constants.ATTR_TYPE_TRANSACTION_ID:
//                                    String Transaction_ID = (new String(decryptedPacket, index, attrLen));
//                                    System.out.println("Transaction_ID" + Transaction_ID);
//                                    break;
//                                case Constants.ATTR_TYPE_BS1_PACKET:
//                                    bs1Packet = new byte[attrLen];
//                                    System.arraycopy(decryptedPacket, index, bs1Packet, 0, attrLen);
//                                    break;
//                            }
//                        }
//                    }
//                }
//                //System.out.println("FROM SERVER:" + new String(bs1Packet));
//                System.out.println("    server details " + new String(bs1Packet));
//                String serverConfigPacket = new String(bs1Packet);
//                System.out.println("FROM SERVER:" + serverConfigPacket);
//                ServerConfig serverConfig = parseProvInfo(serverConfigPacket);
//                clientSocket.close();
//                return serverConfig.errorCode;
//            } catch (Exception Ex) {
//                System.out.println("");
//                Ex.printStackTrace();
//            }
//        return -1;
//    }
//
//    public static ServerConfig parseProvInfo(String info) {
//        ServerConfig serverConfig = new ServerConfig();
//        if (info == null) {
//            logger.info("Response is NULL");
//            serverConfig.errorCode = -1;
//            return serverConfig;
//        }
////        serverConfig = new ServerConfig();
//        String[] sniList = null, backupDnsList = null;
//        JSONArray serverDetails = null;
//        try {
//            JSONParser parser = new JSONParser();
//            Object obj = parser.parse(info);
//            JSONObject jsonObject = (JSONObject) obj;
//            String errorCode = (String) jsonObject.get("Error");
//            if (errorCode != null) {
//                logger.info("[Error] : " + errorCode);
//                serverConfig.errorCode = Integer.parseInt(errorCode);
//                return serverConfig;
//            }
//            sniList = ((String) jsonObject.get("snilist")).split(";");
//            backupDnsList = ((String) jsonObject.get("backupdomainnames")).split(";");
//            serverDetails = (JSONArray) jsonObject.get("serverdetails");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        for (String sni : sniList)
//            serverConfig.sniList.add(sni);
//        for (String backupDns : backupDnsList)
//            serverConfig.backupDns.add(backupDns);
//        ServerDetails server = null;
//        if (serverDetails != null) {
//            Iterator<JSONObject> iterator = serverDetails.iterator();
//            while (iterator.hasNext()) {
//                JSONObject serv = iterator.next();
//                String name = (String) serv.get("servername");
//                Long longIp = (Long) serv.get("serverip");
//                 logger.info("Provising","ServerIp ==="+longIp);
//                 logger.info("Provising","Server Name ==="+name);
//                String ip = PacktUtil.longToIp(longIp);
//                int port, protocol;
//                String[] portAndProtocol = ((String) serv.get("serverportandprotocol")).split(";");
//                for (String pandp : portAndProtocol) {
//                    server = new ServerDetails();
//                    server.setName(name);
//                    server.setIp(ip);
//                    String[] pp = pandp.split(",");
//                    port = Integer.parseInt(pp[0]);
//                    protocol = Integer.parseInt(pp[1]);
//                    ConfigurationConstants.CarrierType carrierType = ConfigurationConstants.CarrierType.DIRECT;
//                    switch (protocol) {
//                        case 1:
//                            carrierType = ConfigurationConstants.CarrierType.DU;
//                            break;
//                        case 2:
//                            carrierType = ConfigurationConstants.CarrierType.ETISALAT;
//                            break;
//                        case 3:
//                            carrierType = ConfigurationConstants.CarrierType.DIRECT;
//                            break;
//                        default:
//                            logger.info("[CARRIER-TYPE] : Protocol error");
//                    }
//                    server.setPort(port);
//                    if (serverConfig.servers.get(carrierType) == null) {
//                        serverConfig.servers.put(carrierType, new ArrayList<ServerDetails>());
//                    }
//                    serverConfig.servers.get(carrierType).add(server);
//                }
////                serverConfig.servers.add(server);
//            }
//        }
//        return serverConfig;
//    }
//}
