package com.jrk_plus.Provisioning;

import android.content.Context;

import com.jrk_plus.TransportLayer.HttpRequestClass;
import com.jrk_plus.utilities.MessageConstatns;

import org.json.JSONException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import static opt.log.OmLogger.logger;

/**
 * Created by bot on 11/01/2018.
 */

public class MessageSendingClass {

    public static void sendPing(Context context) throws JSONException {

         logger.info("Sending----->","Ping");
        org.json.simple.JSONObject object_toSend = new org.json.simple.JSONObject();
        //object_toSend.put(MessageConstatns.TID,System.currentTimeMillis());
        object_toSend.put(MessageConstatns.MSG_TYPE,MessageConstatns.PING_REQUEST);
        //object_toSend.put(MessageConstatns.MTYPE,MessageConstatns.PING_REQUEST_TYPE);
        org.json.simple.JSONObject msgContent = new org.json.simple.JSONObject();
            msgContent.put(MessageConstatns.USER_NAME,"Ws");
            msgContent.put(MessageConstatns.PASSWORD,"s");
            msgContent.put(MessageConstatns.IMEI,"QWdasc ");
        object_toSend.put(MessageConstatns.MSG_CONTENT,msgContent);


//
//        org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
////        obj.put("imei", "99432299392168");
//        obj.put(MessageConstatns.IMEI, imei);
//        obj.put(MessageConstatns.USER_NAME, username);
//        obj.put(MessageConstatns.PASSWORD, password);
//        obj.put(MessageConstatns.PLATFORM, "android");
//        obj.put(MessageConstatns.COUNTRY, "IN");
//        obj.put(MessageConstatns.VERSION, "1.1.1");
//        obj.put(MessageConstatns.ISP, "Etisalat");

        StringWriter out = new StringWriter();
        try {
            object_toSend.writeJSONString(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String jsonText = out.toString();
        byte[] message = null;
        try {
            message = jsonText.getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("message" + message.length);
        byte[] bs2WrapperBuff = new byte[2048];

        PacktUtil.prepareMessage(bs2WrapperBuff, MessageConstatns.CONFIGURATION_REQUEST);
        PacktUtil.addAttr(Constants.ATTR_TYPE_TRANSACTION_ID, (System.currentTimeMillis() + "").getBytes(), bs2WrapperBuff);
        PacktUtil.addAttr(Constants.ATTR_TYPE_DIALER_IP, "180.10.11.12".getBytes(), bs2WrapperBuff);
        PacktUtil.addAttr(Constants.ATTR_TYPE_BS1_PACKET, message, bs2WrapperBuff);
        int len = PacktUtil.twoByteToInt(bs2WrapperBuff, 2);
        //encrypt the packet
        byte[] encryptedPacket = PacktUtil.encrypt(bs2WrapperBuff, len);

//        return encryptedPacket;

        new HttpRequestClass(context).sendHttpsRequest("sdc",2,null,encryptedPacket.toString(),null);
    }
}
