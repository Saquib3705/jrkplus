package com.jrk_plus.Provisioning;

import java.util.ArrayList;

public class ServerConfig {
    public ArrayList<String> sniList;
    public ArrayList<String> backupDns;
    //    public ArrayList<ServerDetails> servers;
    public ArrayList<ServerDetails> servers;
    public int errorCode;

    public ServerConfig() {
        sniList = new ArrayList<>();
        backupDns = new ArrayList<>();
        servers = new ArrayList<>();
        errorCode = 0;
    }
}
