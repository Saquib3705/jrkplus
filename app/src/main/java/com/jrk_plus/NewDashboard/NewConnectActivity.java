package com.jrk_plus.NewDashboard;

import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.provider.Settings;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.app.jrk_plus.R;
import com.jrk_plus.Provisioning.ServerConfig;
import com.jrk_plus.TransportLayer.OkktpHttpRequest;
import com.jrk_plus.Tunnel.CredentialResults;
import com.jrk_plus.Tunnel.Provisioning;
import com.jrk_plus.fragments.SelectedServer;
import com.jrk_plus.utilities.CredentialCheck;
import com.jrk_plus.utilities.ExperimentPermissionClass;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class NewConnectActivity extends AppCompatActivity implements LoaderCallbacks<Cursor>, AdapterView.OnItemSelectedListener,
        ExperimentPermissionClass.PermissionCallBacks, OkktpHttpRequest.OkktpHttpResponseEvents, CredentialResults, Provisioning.ReceiveProvInfoInterface {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mLoginFormView;
    private ProgressDialog progressDialog;
    RadioButton wifi_radio_button, tweak_radio_button;


    String[] servers = {"Server#1", "Server#2", "Server#3", "Server#4", "Server#5"};
    private ExperimentPermissionClass experimentPermissionClass = null;
    private String imeiNo = "xoxoxoxo";
    private CountDownTimer counterTimer = null;
    private CredentialCheck credentialCheck;
    private String username = "917838821488";
    private String password = "KFE8LICV";
    private String identifier = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_connect);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Connecting.....");
        progressDialog.setCancelable(false);
        experimentPermissionClass = new ExperimentPermissionClass(this, this);
        if (!experimentPermissionClass.checkPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE)) {
            experimentPermissionClass.requestPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE);
        }

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.new_connect_custom_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");

        Spinner spin = (Spinner) findViewById(R.id.spinner1);
        Spinner spin2 = (Spinner) findViewById(R.id.spinner2);
        spin.setOnItemSelectedListener(this);
        spin2.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, R.layout.spinner_item, servers);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);
        spin2.setAdapter(aa);
        wifi_radio_button = findViewById(R.id.wifi_radio_button);
        tweak_radio_button = findViewById(R.id.tweak_radio_button);
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }

        for (int i = 0, len = permissions.length; i < len; i++) {
            experimentPermissionClass.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            sendMessageToServer(email, password);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(NewConnectActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onPermissionAllowed(int requestCode) {
        if (requestCode == ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE) {
            getImeiNo();
        }
    }

    @Override
    public void onPermissionDeny(int requestCode) {
        if (requestCode == ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE) {
            getImeiNo();
        }
    }

    @Override
    public void onDialogYesButtonIsClicked() {

    }

    @Override
    public void onDialogNoButtonIsClicked() {

    }

    @Override
    public void getErrorCode(int errorCode) {
        Log.d("NewConnectActivity", "Provisining Error response");
    }

    @Override
    public void onTaskCompletion(int errorCode, SelectedServer selectedServer, Context ctx, CredentialCheck.RequestType requestType) {
        Log.d("NewConnectActivity", "Provisining on task complition response");
    }

    @Override
    public void responseReceived(boolean isError) {
        Log.d("ConnectActivity", "Provisining response in response received(isError)" + isError);
        if (!isError) {
            ServerConfig serverConfig = com.jrk_plus.Provisioning.Information.fetchServerConfig(this);
            if (serverConfig != null)
                Log.d("NewConnectActivity", "server config ======" + serverConfig.servers.size());
        }
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    private void sendMessageToServer(String userId, String mPassword) {

        if (ActivityCompat.checkSelfPermission(NewConnectActivity.this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(NewConnectActivity.this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 123);
        } else {
            TelephonyManager tm = (TelephonyManager) NewConnectActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null)
                identifier = tm.getDeviceId();
            if (identifier == null || identifier.length() == 0)
                identifier = Settings.Secure.getString(NewConnectActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);

            credentialCheck = new CredentialCheck(NewConnectActivity.this, username, password, identifier, CredentialCheck.RequestType.PROV, null);
//            credentialCheck.credentialResults = HomeActivity.this;
            credentialCheck.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }


//        if (progressDialog != null)
//            progressDialog.show();
//        startCountDown();
//        PackageInfo pInfo = null;
//        String version = "1.0.0";
//        try {
//            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//        if (pInfo != null) {
//            version = pInfo.versionName;
//        }
//        String buildVersion = Build.FINGERPRINT;
//        if(buildVersion.length() > 10){
//            buildVersion = buildVersion.substring(0,10);
//        }
//        logger.debug("Register", "Version = = " + version);
//        logger.debug("Register", "Build Number == " + buildVersion);
//        logger.debug("Register", "CHECKSUM == " + AppSignature.getCheckSum(NewConnectActivity.this));
//
//        String url = "http://151.106.17.18:7900/bridgevpn/authenticateUser/1212";
//        final JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put(MessageConstatns.IMEI, imeiNo);
//            jsonObject.put(MessageConstatns.BUILD_NUMBER, buildVersion);
//            jsonObject.put(MessageConstatns.PLATFORM, "android");
//            jsonObject.put(MessageConstatns.CHECKSUM, AppSignature.getCheckSum(NewConnectActivity.this));
//            jsonObject.put(MessageConstatns.VERSION, version);
//            jsonObject.put(MessageConstatns.PASSWORD,"2");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        new OkktpHttpRequest().sendRequest(NewConnectActivity.this, url, jsonObject, this);
    }

    @Override
    public void okktpHttpResponse(String response) {
        Log.d("NewConnectActivity", "Response =====" + response);
        if (!isFinishing() && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public void okktpHttpErrorResponse() {

    }


    private void getImeiNo() {
        if (!experimentPermissionClass.checkPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE)) {
            experimentPermissionClass.requestPermission(ExperimentPermissionClass.PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            System.out.println("IMEI::" + telephonyManager.getDeviceId());
            imeiNo = telephonyManager.getDeviceId();
        }
    }

    public void startCountDown() {

        int countDownInterval = 1000;
        int finishTime = 20;
        if (counterTimer == null) {
            counterTimer = new CountDownTimer(finishTime * 1000, countDownInterval) {
                public void onFinish() {
                    //finish your activity here
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(NewConnectActivity.this, "Server taking too much time in responding..", Toast.LENGTH_LONG).show();
                }

                public void onTick(long millisUntilFinished) {

                }
            };
            counterTimer.start();
        } else {
            counterTimer.cancel();
            counterTimer = new CountDownTimer(finishTime * 1000, countDownInterval) {
                public void onFinish() {
                    //finish your activity here
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(NewConnectActivity.this, "Server taking too much time in responding..", Toast.LENGTH_LONG).show();
                }

                public void onTick(long millisUntilFinished) {
                    //called every 1 sec coz countDownInterval = 1000 (1 sec)
                }
            };
            counterTimer.start();
        }
    }

}
