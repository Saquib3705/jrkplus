package com.jrk_plus.Tunnel;

import android.content.Context;

import com.jrk_plus.fragments.SelectedServer;
import com.jrk_plus.utilities.CredentialCheck.RequestType;

/**
 * Created by ABHISHEK MAHATO on 5/24/2017.
 */

public interface CredentialResults {
    public void getErrorCode(int errorCode);
    public void onTaskCompletion(int errorCode, SelectedServer selectedServer, Context ctx, RequestType requestType);
}