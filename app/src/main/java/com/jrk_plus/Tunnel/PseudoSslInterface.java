package com.jrk_plus.Tunnel;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.concurrent.atomic.AtomicLong;

import opt.enc.stream.StreamEncryption;
import opt.packet.Packet;
import opt.packet.ReceivedPacket;
import opt.packet.SendPacket;
import opt.selector.OmInterface;
import pseudoSsl.PseudoSslWrapUnWrap;

import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 4/24/2017.
 */

public class PseudoSslInterface extends OmInterface {

    public static AtomicLong connectedSocket = new AtomicLong(0);
    public static AtomicLong receivedCounter = new AtomicLong(0);
    private int nRecv = 0;
    private boolean connectionStatus = false;
    public static Config config;
    public static OutputStream streamToClient;
    //    public static DatagramSocket client;
    public PseudoSslWrapUnWrap sslWrapUnWrap;
    private boolean isFirstPacket = true;
    public static final int STREAM_VALUE = 1000;

    StreamEncryption streamEncryption;
    StreamEncryption streamDecryption;

    InetAddress clientAddress;
    int clientPort;

    public boolean isFirstPacket() {
        return isFirstPacket;
    }

    public void setFirstPacket(boolean firstPacket) {
        isFirstPacket = firstPacket;
    }

    public PseudoSslInterface(boolean isSecured, int special) {
        super(isSecured, special);
        streamEncryption = StreamEncryption.getStreamEncryptionObject(STREAM_VALUE);
        streamDecryption = StreamEncryption.getStreamEncryptionObject(STREAM_VALUE);
    }

    public static void setDetails(Config conf, OutputStream clientOutputStream) {
        config = conf;
//        client = clientSocket;
        streamToClient = clientOutputStream;
    }

    public StreamEncryption getStreamEncryption() {
        return streamEncryption;
    }

    public void setClientAddress(InetAddress clientAddress) {
        this.clientAddress = clientAddress;
    }

    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }

    @Override
    public void processData(ReceivedPacket rp) {
        byte[] reply;
        byte[] dataToReceive;
        logger.info("---------Received Data of length [" + rp.receivedLength + "] from [" + rp.remoteAddress + "]------");
        receivedCounter.incrementAndGet();
        nRecv++;
        logger.info("******** Stats ..Connected Count [" + MultiHttpInterface.connectedSocket.get() + "] and receivedCounter [" + MultiHttpInterface.receivedCounter.get() + "]");

//        logger.info(new String(rp.buffer));
//        logger.info("Received data of length : " + rp.length() + " bytes");
//        reply = Arrays.copyOfRange(rp.buffer, rp.bufferOffset, rp.length());

        if (!sslWrapUnWrap.setPseudoSslMsg(rp)) {
            logger.warn("Setting Pseudo Msg Failed");
            return;
        }
        Packet recvPacket = new Packet(8192 * 2);

        int ret = sslWrapUnWrap.pseudoSslUnWrap(recvPacket);

        while (ret >= 0) {
            logger.info("PseudoSslUnwrap return Value [" + ret + "] handshake [" + sslWrapUnWrap.getHandshakeState() + "]");
            switch (sslWrapUnWrap.getHandshakeState()) {
                case SERVER_HELLO_DONE:
                    logger.info("SERVER HELLO DONE");
                    String serverIp = config.getServerIp();
                    int serverPort = config.getServerPort();

                    logger.info("Server IP : " + serverIp);
                    logger.info("Server Port : " + serverPort);

                    byte[] firstPacket = com.jrk_plus.Provisioning.Information.createFirstPacket();
                    firstPacket = Information.addHeader(firstPacket, serverIp, serverPort,
                            ConfigurationConstants.PROTO_TCP, ConfigurationConstants.ACTION_FORWARD);

//                    byte[] provInfo = new byte[2048];
//                    System.arraycopy(firstPacket, 0, provInfo, 0, firstPacket.length);
//                    int provInfoLen = opt.enc.continuous.ContinuousEncryption.encrypt(provInfo, 0, firstPacket.length);
                    streamEncryption.encrypt(firstPacket);

                    Packet psuedoSslPacket = psudoWrap(firstPacket, 0, firstPacket.length);

//        firstPacket = HttpWrapUnwrap.httpWrapClientSide(firstPacket, firstPacket.length);
                    this.sendData(psuedoSslPacket.buffer, psuedoSslPacket.bufferOffset, psuedoSslPacket.length());
                    this.isFirstPacket = false;
                    this.connectionStatus = true;
                    break;
                case APP_DATA:
                    logger.info("[APP-DATA] : " + recvPacket.length() + " bytes");
                    try {
//                    DatagramPacket packet = new DatagramPacket(recvPacket.buffer, recvPacket.bufferOffset, recvPacket.length());
//                    packet.setAddress(clientAddress);
//                    packet.setPort(clientPort);
//
//                    client.send(packet);
//                        while (ret > 0) {
//                        int decryptedLen = opt.enc.continuous.ContinuousEncryption.decrypt(recvPacket.buffer, recvPacket.bufferOffset, recvPacket.length());

                        streamDecryption.decrypt(recvPacket);
//                        logger.info("[DECRYPTED LENGTH] :" + decryptedLen);
//                        if (decryptedLen > 0) {
//                            recvPacket.resetLength(decryptedLen);
                        streamToClient.write(recvPacket.buffer, recvPacket.bufferOffset, recvPacket.length());
                        streamToClient.flush();
//                        }
//                            recvPacket.reset();
//                            ret = sslWrapUnWrap.pseudoSslUnWrap(recvPacket);
//                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    logger.info("Wrong Message Type");
            }
            recvPacket.reset();
            ret = sslWrapUnWrap.pseudoSslUnWrap(recvPacket);
        }
//        if (nRecv == config.getClientMppc())
//            this.close();
    }

    public Packet psudoWrap(byte[] arr, int offset, int length) {
        Packet packet = new Packet(4096);
        packet.appendInEnd(arr, offset, length);
        int len = sslWrapUnWrap.pseudoSslWrap(packet);
        logger.info("Before Length [" + length + "] After Length [" + len + "]");
        return packet;
    }

    public Packet psudoWrap(Packet packet) {
        int length = packet.length();
        int len = sslWrapUnWrap.pseudoSslWrap(packet);
        logger.info("Before Length [" + length + "] After Length [" + len + "]");
        return packet;
    }

    @Override
    public void sendData(byte[] bytes, int i, int i1) {
        logger.info("SID [" + getId() + "] Sending Data Length [" + i1 + "]");
        SendPacket sendPacket = new SendPacket(8192);
        sendPacket.sendToIp = connectIp;
        sendPacket.sendToPort = connectPort;
        sendPacket.appendInEnd(bytes, i, i1);
        addDataForSending(sendPacket);
    }

    @Override
    public void sendData(SendPacket sp) {
        logger.info("SID [" + getId() + "] Sending Data Length [" + sp.length() + "]");
        addDataForSending(sp);
    }

    @Override
    public void receiveTimeout() {

    }

    @Override
    public void socketConnected() {
        logger.info("Connection estabished");
        if (sslWrapUnWrap == null) {
            sslWrapUnWrap = new PseudoSslWrapUnWrap(false, 65535);
        }
        SendPacket packet = new SendPacket(4096);
        sslWrapUnWrap.ptlsConnect(config.getSni(), packet);
        sendData(packet);
    }

    @Override
    public void close() {
        super.close();
    }

    public boolean isConnected() {
        return this.connectionStatus;
    }
}
