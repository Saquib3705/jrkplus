package com.jrk_plus.Validity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.jrk_plus.TransportLayer.HttpRequestClass;
import com.jrk_plus.TransportLayer.OkktpHttpRequest;
import com.jrk_plus.utilities.MessageConstatns;
import com.jrk_plus.utilities.PrefManager;
import com.app.jrk_plus.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.jrk_plus.Tunnel.ConfigurationConstants.CONNECT_ACTIVITY_BROADCAST_ACTION;
import static opt.log.OmLogger.logger;

public class ValidityUpdationActivity extends AppCompatActivity implements CouponRowAdapter.AdapterCallback, View.OnClickListener,
        HttpRequestClass.HttpResponseEvents ,OkktpHttpRequest.OkktpHttpResponseEvents{


    private TextView validityShowingText;
    private CouponRowAdapter mAdapter;
    private RecyclerView coupanRowContainer;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<CoupanInfo> list = new ArrayList<>();
    private Button voucherApplyButton;
    private EditText editTextVoucher;
    private ProgressDialog progressDialog;
    InputMethodManager imm;
    private PrefManager prefManager;
    private TextView validityText;
    private CountDownTimer counterTimer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validity_updation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.custom_validity_updation_toolbar);
        setSupportActionBar(toolbar);
        imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");
        if (toolbar != null && getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        prefManager = new PrefManager(this);
        validityText = (TextView) findViewById(R.id.day_left_text);
        if(!prefManager.isValidityExpired()) {
            validityText.setText(prefManager.getDeactivationDate());
        }else{
            validityText.setText("Expired");
        }


        validityShowingText = (TextView) findViewById(R.id.validityShowingText);
         logger.info("ValidityActivity","Deactivation Date ==="+prefManager.getDeactivationDate());
        String colorText;
        if(prefManager.getDeactivationDate().equals("")){
            colorText= "<font color=\"#000000\">Your validity has  </font>"
                    + "<u><font color=\"#00ff00\"><bold>"+"Expired"+"</bold></font></u>"
                    + "<font color=\"#000000\">. Please extend validity to continue our services.</font>";
        }else{
            colorText= "<font color=\"#000000\">Your validity is expiring on </font>"
                    + "<u><font color=\"#00ff00\"><bold>"+prefManager.getDeactivationDate()+"</bold></font></u>"
                    + "<font color=\"#000000\">. Please extend validity to continue our services.</font>";
        }
        validityShowingText.setText(Html.fromHtml(colorText));
        list.add(new CoupanInfo("1 Day", " $2"));
        list.add(new CoupanInfo("7 Day", " $10"));
        list.add(new CoupanInfo("10 Day", " $15"));
        list.add(new CoupanInfo("15 Day", " $20"));
        list.add(new CoupanInfo("30 Day", " $30"));
        list.add(new CoupanInfo("45 Day", " $45"));
        list.add(new CoupanInfo("60 Day", " $60"));
        list.add(new CoupanInfo("75 Day", " $65"));
        list.add(new CoupanInfo("90 Day", " $75"));
        list.add(new CoupanInfo("100 Day", " $80"));
        list.add(new CoupanInfo("6 Months", " $100"));
        list.add(new CoupanInfo("1 year", " $150"));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait.....");
        progressDialog.setCancelable(false);
        setEventListner();
        setRecyclerView();
    }

    private void setEventListner() {
        voucherApplyButton =  findViewById(R.id.voucher_apply_button);
        editTextVoucher = findViewById(R.id.edit_text_voucher);
        voucherApplyButton.setOnClickListener(this);
    }

    public void setRecyclerView() {
        mAdapter = new CouponRowAdapter(this, list);
        coupanRowContainer = (RecyclerView) findViewById(R.id.coupanContainer);
        mLayoutManager = new LinearLayoutManager(this);
        coupanRowContainer.setLayoutManager(mLayoutManager);
        coupanRowContainer.setItemAnimator(new DefaultItemAnimator());
        coupanRowContainer.setAdapter(mAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onMethodCallback(String validityFee) {
        Intent intent = new Intent(ValidityUpdationActivity.this, WebViewActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        if (view == voucherApplyButton) {
            if(prefManager.getWrongVoucherAppliedCount() < MessageConstatns.WRONG_VOUCHER_APPLIED_CONTINUOUSLY ) {
                if (validate(editTextVoucher)) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.show();
                        }
                    });
                    applyVoucher(editTextVoucher.getText().toString());
                }
            }else if(getTimeDifference(prefManager.getLastWrongAttemptTimestamp(), System.currentTimeMillis()) >= 1){
                prefManager.setWrongVoucherAppliedCount(0);
                if (validate(editTextVoucher)) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.show();
                        }
                    });
                    applyVoucher(editTextVoucher.getText().toString());
                }
            }
            else{
                Toast.makeText(ValidityUpdationActivity.this,"You have exceeded voucher applied attempt,Try after an hour.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void applyVoucher(String voucherCode) {
        //Apply voucher to server and get response
        startCountDown();
        final JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("voucherCode", voucherCode);
            jsonObject.put("mobileNumber","1111");
//            jsonObject.put("mobileNumber", prefManager.getUserName());
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Thread sslThread = new Thread() {
//            @Override
//            public void run() {
//                try {
//                    DnsResolverAPI dnsResolverAPI = new DnsResolverAPI("dashboard.madeenavpn.net");
//                    String voucherIp = dnsResolverAPI.getVpnProxyIp();
//
//                    logger.info("ng","[RESOLVER] :: Google Dns Resolved IP: " + voucherIp);
//
//                    if (voucherIp == null) {
//                        Set<String> vpnProxyIpList = DnsResolverAPI.resolveAddress("188.138.125.18:8081/madeenavpn/", T_A);
//                        logger.info("ng","[RESOLVER] :: Direct Dns Resolved IP: " + vpnProxyIpList);
//
//                        if (vpnProxyIpList.size() > 0) {
//                            for (String ip : vpnProxyIpList) {
//                                logger.info("ng","[RESOLVER] : IP : " + ip);
//                            }
//
//                            voucherIp = vpnProxyIpList.iterator().next();
//                        } else
//                            voucherIp = "151.106.17.18";
//                    }
//                    TrustManager[] trustAllCerts = new TrustManager[]{
//                            new X509TrustManager() {
//                                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                                    return null;
//                                }
//                                public void checkClientTrusted(
//                                        java.security.cert.X509Certificate[] certs, String authType) {
//                                }
//
//                                public void checkServerTrusted(
//                                        java.security.cert.X509Certificate[] certs, String authType) {
//                                }
//                            }
//                    };
//                    SSLSocket st = null;
//                    try {
//                        SSLContext ssl_context = SSLContext.getInstance("TLS");
//                        ssl_context.init(null, trustAllCerts, null);
//                        final SSLSocketFactory sf = ssl_context.getSocketFactory();
//                         logger.info("ng", "--------------------------");
//                         logger.info("ng", "   New Socket Creation (CSV File Download)");
//                         logger.info("ng", "--------------------------");
//                        st = (SSLSocket) sf.createSocket(voucherIp, 443);
//                        setSNIHost(sf, (SSLSocket) st, "www.facebook.com");
//                        ((SSLSocket) st).startHandshake();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    InputStream inputStream = null;
//                    OutputStream outputStream = null;
//
//                    try {
//                        outputStream = st.getOutputStream();
//                        inputStream = st.getInputStream();
//                    } catch (Exception e) {
//
//                    }
//                    String validation_message = "POST /applyVouchers1 HTTP/1.1\r\n" +
//                            "User-Agent: androidVPN\r\n" +
//                            "Accept-Language: en-US,en;q=0.5\r\n" +
//                            "Content-Type: application/json; charset=utf-8\r\n" +
//                            "Content-Length: " +
//                            jsonObject.toString().length() +
//                            "\r\n" +
//                            "Host: dashboard.madeenavpn.net\r\n" +
//                            "Connection: Keep-Alive\r\n" +
//                            "Accept-Encoding: gzip\r\n" +
//                            "\r\n" +
//                            jsonObject.toString();
//
//                    try {
//                        logger.info("[SSL] :: " + validation_message);
//                        outputStream.write(validation_message.getBytes());
//                        outputStream.flush();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    SimpleHttpServerConnection simpleHttpServerConnection = null;
//                    try {
//                        simpleHttpServerConnection = new SimpleHttpServerConnection(inputStream);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    String response = simpleHttpServerConnection.readResponse();
//                    logger.info("[SSL] :: " + response);
////                    ((OkktpHttpRequest.OkktpHttpResponseEvents)MainActivity.this).okktpHttpResponse(response);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//
////        sslThread.start();
        new HttpRequestClass(ValidityUpdationActivity.this).sendSslRequest(ValidityUpdationActivity.this, jsonObject);

//        new HttpRequestClass(ValidityUpdationActivity.this).sendHttpsRequest("https://188.138.125.18/madeenavpn/applyVouchers1/",2,ValidityUpdationActivity.this,null,jsonObject);

//        new OkktpHttpRequest().sendRequest(ValidityUpdationActivity.this,"https://188.138.125.18/madeenavpn/applyVouchers1/",jsonObject,ValidityUpdationActivity.this);
    }

    private boolean validate(EditText editText) {
        boolean isEmpty = false;
        if (!editText.getText().toString().trim().isEmpty()) {
            isEmpty = true;
        } else {
            editText.requestFocus();
            editText.setError("Field Can't be blank");
        }
        return isEmpty;
    }

    @Override
    public void httpResponseForLogin(String response) {
        if (counterTimer != null)
            counterTimer.cancel();
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
         logger.info("voucherCode", "Got response ====" + response);
         System.out.print("Connected");

    }

    @Override
    public void couponAction(final int responseCode ,final String response) {
        if (counterTimer != null)
            counterTimer.cancel();
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (responseCode == 200) {
                    Toast.makeText(ValidityUpdationActivity.this, "Voucher Applied Successfully...",
                            Toast.LENGTH_LONG).show();
                    String finalString = response.substring(1, response.length() - 2);
                    logger.info("Validity","Response ==="+response);
                    long timestamp = Long.parseLong(finalString);
                    logger.info("Validity","TimeStamp ==="+timestamp);

                    Date d = new Date(timestamp);
                    DateFormat f = new SimpleDateFormat("MMM d,yyyy");
                    String validTill = f.format(d);
                    logger.info("Validity","Date ==="+validTill);
                    prefManager.setDeactivationTimeStamp(timestamp);
                    prefManager.setDeactivationDate(validTill);
                    prefManager.setWrongVoucherAppliedCount(0);
                     logger.info("Expiry","Setting expiry false from voucher apply");
                    if(timestamp > System.currentTimeMillis()) {
                         logger.info("Expiry","CurrentMillis =="+ System.currentTimeMillis());
                         logger.info("Expiry","Validity millis =="+timestamp);
                        prefManager.setIsValidityExpired(false);
                        validityText.setText(validTill);
                    }else{
                         logger.info("Expiry","CurrentMillis =="+ System.currentTimeMillis());
                         logger.info("Expiry","Validity millis =="+timestamp);
                        prefManager.setIsValidityExpired(true);
                        validityText.setText("Expired");
                    }
                    String colorText;
                    logger.info("ValidityActivity","Deactivation Date ==="+prefManager.getDeactivationDate());
                    if(prefManager.getDeactivationDate().equals("")){

                        colorText= "<font color=\"#000000\">Your validity is  </font>"
                                + "<u><font color=\"#00ff00\"><bold>"+"Expired"+"</bold></font></u>"
                                + "<font color=\"#000000\">. Please extend validity to continue our services.</font>";


//                        colorText= "<font color=\"#ffffff\">Your validity is  </font>"
//                                + "<u><font color=\"#00ff00\"><bold>"+"Expired"+"</bold></font></u>"
//                                + "<font color=\"#ffffff\">. Please extend validity to continue our services.</font>";
                    }else{

                        colorText= "<font color=\"#000000\">Your validity is expiring on </font>"
                                + "<u><font color=\"#00ff00\"><bold>"+prefManager.getDeactivationDate()+"</bold></font></u>"
                                + "<font color=\"#000000\">. Please extend validity to continue our services.</font>";

//                        colorText= "<font color=\"#ffffff\">Your validity is expiring on </font>"
//                                + "<u><font color=\"#00ff00\"><bold>"+prefManager.getDeactivationDate()+"</bold></font></u>"
//                                + "<font color=\"#ffffff\">. Please extend validity to continue our services.</font>";
                    }
                    validityShowingText.setText(Html.fromHtml(colorText));
                    Intent intent = new Intent(CONNECT_ACTIVITY_BROADCAST_ACTION);
                    intent.putExtra(MessageConstatns.ID,3);
                    sendBroadcast(intent);
                }else if(responseCode == 400) {
                    if(prefManager.getWrongVoucherAppliedCount() == 4)
                        prefManager.setLastWrongAttemptTimestamp(System.currentTimeMillis());
                        Toast.makeText(ValidityUpdationActivity.this, "Invalid Voucher ,You have "+(4-prefManager.getWrongVoucherAppliedCount())+"attempt remaining.",
                            Toast.LENGTH_LONG).show();
                        prefManager.setWrongVoucherAppliedCount(prefManager.getWrongVoucherAppliedCount()+1);
                }else {
                    Toast.makeText(ValidityUpdationActivity.this, "Some Error occured...",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void httpErrorResponse() {
        if (counterTimer != null)
            counterTimer.cancel();
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ValidityUpdationActivity.this, "Got error response from server", Toast.LENGTH_LONG).show();
            }
        });
    }

    public long getTimeDifference(long earlierTimeStamp, long laterTimeStamp){
        //Date1
        Calendar cal1 = Calendar.getInstance();
        cal1.set(1995, 04, 3, 5, 0, 0);
        Date date1 = cal1.getTime();

        //Date2
        Calendar cal2 = Calendar.getInstance();
        cal2.set(2011, 07, 4, 6, 1, 1);
        Date date2 = cal2.getTime();


        //Time Difference Calculations Begin
        long milliSec1 = earlierTimeStamp;
        long milliSec2 = laterTimeStamp;

        long timeDifInMilliSec;
        if(milliSec1 >= milliSec2)
        {
            timeDifInMilliSec = milliSec1 - milliSec2;
        }
        else
        {
            timeDifInMilliSec = milliSec2 - milliSec1;
        }

        long timeDifSeconds = timeDifInMilliSec / 1000;
        long timeDifMinutes = timeDifInMilliSec/ (60 * 1000);
        long timeDifHours = timeDifInMilliSec / (60 * 60 * 1000);
        long timeDifDays = timeDifInMilliSec / (24 * 60 * 60 * 1000);

        System.out.println("Time differences expressed in various units are given below");
        System.out.println(timeDifInMilliSec + " Milliseconds");
        System.out.println(timeDifSeconds + " Seconds");
        System.out.println(timeDifMinutes + " Minutes");
        System.out.println(timeDifHours + " Hours");
        System.out.println(timeDifDays + " Days");
        return timeDifHours;
    }

    public void startCountDown() {

        int countDownInterval = 1000;
        int finishTime = 20;
        if (counterTimer == null) {
            counterTimer = new CountDownTimer(finishTime * 1000, countDownInterval) {
                public void onFinish() {
                    //finish your activity here
                     logger.info("CountDown", "Counter is finished");
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(ValidityUpdationActivity.this, "Server taking too much time  for responding..", Toast.LENGTH_LONG).show();
                }

                public void onTick(long millisUntilFinished) {

                }
            };
            counterTimer.start();
        } else {
            counterTimer.cancel();
            counterTimer = new CountDownTimer(finishTime * 1000, countDownInterval) {
                public void onFinish() {
                    //finish your activity here
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(ValidityUpdationActivity.this, "Server taking too much time  for responding..", Toast.LENGTH_LONG).show();
                }

                public void onTick(long millisUntilFinished) {
                    //called every 1 sec coz countDownInterval = 1000 (1 sec)
                }
            };
            counterTimer.start();
        }
    }

    @Override
    public void okktpHttpResponse(String response) {

        Log.d("ValidityActivity","Got Successful Response");
    }

    @Override
    public void okktpHttpErrorResponse() {
        Log.d("ValidityActivity","Got Error Response");
    }
}
