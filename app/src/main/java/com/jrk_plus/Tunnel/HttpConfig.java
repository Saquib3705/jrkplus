package com.jrk_plus.Tunnel;

/**
 * Created by ABHISHEK MAHATO on 4/26/2017.
 */

public class HttpConfig {
    String httpHeader1;
    String httpHeader1StartDelimeter;
    String httpBase64Header1;
    String httpBase64Header1StartDelimeter;
    String httpHeaderEndDelimeter;
    String httpBase64HeaderEndDelimeter;
    String httpRandomHeader1;
    String httpRandomHeader1StartDelimeter;
    String httpRandomHeader1EndDelimeter;

    public HttpConfig(){
        this.httpHeader1 = null;
        this.httpHeader1StartDelimeter = null;
        this.httpBase64Header1 = null;
        this.httpBase64Header1StartDelimeter = null;
        this.httpHeaderEndDelimeter = null;
        this.httpBase64HeaderEndDelimeter = null;
        this.httpRandomHeader1 = null;
        this.httpRandomHeader1StartDelimeter = null;
        this.httpRandomHeader1EndDelimeter = null;
    }

    public String getHttpHeader1() {
        return httpHeader1;
    }

    public void setHttpHeader1(String httpHeader1) {
        this.httpHeader1 = httpHeader1;
    }

    public String getHttpHeader1StartDelimeter() {
        return httpHeader1StartDelimeter;
    }

    public void setHttpHeader1StartDelimeter(String httpHeader1StartDelimeter) {
        this.httpHeader1StartDelimeter = httpHeader1StartDelimeter;
    }

    public String getHttpBase64Header1() {
        return httpBase64Header1;
    }

    public void setHttpBase64Header1(String httpBase64Header1) {
        this.httpBase64Header1 = httpBase64Header1;
    }

    public String getHttpBase64Header1StartDelimeter() {
        return httpBase64Header1StartDelimeter;
    }

    public void setHttpBase64Header1StartDelimeter(String httpBase64Header1StartDelimeter) {
        this.httpBase64Header1StartDelimeter = httpBase64Header1StartDelimeter;
    }

    public String getHttpHeaderEndDelimeter() {
        return httpHeaderEndDelimeter;
    }

    public void setHttpHeaderEndDelimeter(String httpHeaderEndDelimeter) {
        this.httpHeaderEndDelimeter = httpHeaderEndDelimeter;
    }

    public String getHttpBase64HeaderEndDelimeter() {
        return httpBase64HeaderEndDelimeter;
    }

    public void setHttpBase64HeaderEndDelimeter(String httpBase64HeaderEndDelimeter) {
        this.httpBase64HeaderEndDelimeter = httpBase64HeaderEndDelimeter;
    }

    public String getHttpRandomHeader1() {
        return httpRandomHeader1;
    }

    public void setHttpRandomHeader1(String httpRandomHeader1) {
        this.httpRandomHeader1 = httpRandomHeader1;
    }

    public String getHttpRandomHeader1StartDelimeter() {
        return httpRandomHeader1StartDelimeter;
    }

    public void setHttpRandomHeader1StartDelimeter(String httpRandomHeader1StartDelimeter) {
        this.httpRandomHeader1StartDelimeter = httpRandomHeader1StartDelimeter;
    }

    public String getHttpRandomHeader1EndDelimeter() {
        return httpRandomHeader1EndDelimeter;
    }

    public void setHttpRandomHeader1EndDelimeter(String httpRandomHeader1EndDelimeter) {
        this.httpRandomHeader1EndDelimeter = httpRandomHeader1EndDelimeter;
    }
}
