package com.jrk_plus.Tunnel;


public class ConfigurationConstants {

    public static final int PROTO_UDP = 1;
    public static final int PROTO_TCP = 2;
    public static final int PROTO_SSL = 3;
    public static final int PROTO_HTTP = 4;

    public static final int ACTION_NONE = 0;
    public static final int ACTION_FORWARD = 3;
    public static final int ACTION_PROVISIONING_PROXY = 5;
    public static final int ACTION_BALANCE_PROXY = 6;
    public static final int ACTION_FORWARD_REGISTRAR_PROXY = 18;

    public static final int PROV = 1;
    public static final int OPEN_VPN = 2;
    public static final int SETTINGS_UPDATE = 3;
    public static final int REGISTRAR_MESSAGE = 7;

    public static final int SETTINGS_UPDATE_SUCCESS = 4;
    public static final int SETTINGS_UPDATE_FAILED = 5;
    public static final int PROVISIONING_RESPONSE = 6;
    public static final int REGISTRAR_RESPONSE = 8;
    public static final int CLIENT_IDENTIFIER = 1;
    public static final int DATA_ATTRIBUTE = 2;

    public static final int CLIENT_CHILD_COUNT = 3;
    public static final int CLIENT_MPPC = 4;

    public static final int SERVER_CHILD_COUNT = 5;
    public static final int SERVER_MPPC = 6;

    public static final int MINIMUM_PACKET_LEN = 4;
    public static final String WIFI_SERVICE = "wifi";

    public static final int USER_NOT_FOUND = 1000;
    public static final int INVALID_PASSWORD = 1001;
    public static final int INVALID_MESSGAE_FORMAT = 1002;
    public static final int MESSAGE_PARSING_FAILED = 1003;
    public static final int USERNAME_EXPIRED =1004;
    public static final int IMEI_VALIDATION_FALIED = 1005;
    public static final int UNSUPPORTED_VESRION_UPDATE_APP = 1006;
    public static final int PASSWORD_INVALID = 1007;
    public static final int MAX_ALLOWED_DEVICES_NEGATIVE =1008;
    public static final int USERID_NOT_FOUND =1010;
    public static final int DUPLICATE_REGISTRATION =1016;
    public static final int MISSING_APP_VERSION = 1024;
    public static final int ACTIVATION_LIMIT_REACHED = 1023;
    public static final int MISSING_OS_VERSION_ = 1034;
    public static final int MISSING_PLATFORM_TYPE_ = 1035;
    public static final int INVALID_VOUCHER = -1;

    public enum TunnelType {
        DIRECT,
        SSL,
        PSEUDO_SSL,
        HTTP,
        DOMAIN_FRONT
    };

    public enum MessageType {
        PROV,
        OPEN_VPN,
        SETTINGS_UPDATE,
        REGISTRAR
    };

    public enum CarrierType {
        DU,
        ETISALAT,
        DIRECT
    };

    public enum NetworkType{
        WIFI,
        NET_3G,
        SOCIAL,
        FREE
    };

    public static final String CONNECT_ACTIVITY_BROADCAST_ACTION = "android.intent.action.MadeenaVPN.ConnectActivity";
}
