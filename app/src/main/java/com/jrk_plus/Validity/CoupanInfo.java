package com.jrk_plus.Validity;

/**
 * Created by bot on 23/01/2018.
 */

public class CoupanInfo {

    private String days;
    private String money;      //in dollars

    public CoupanInfo(String days , String money) {
        this.days = days;
        this.money = money;
    }

    public String getDay() {
        return days;
    }

    public void setDay(String days) {
        this.days = days;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
