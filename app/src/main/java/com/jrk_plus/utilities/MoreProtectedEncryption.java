package com.jrk_plus.utilities;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import static opt.log.OmLogger.logger;

public class MoreProtectedEncryption {

    //this is more protected and safe to brute force attackers.
    //static String encryptedFileName = "Enc_File2.txt";
    private static String algorithm = "AES";
    //static SecretKey yourKey = null;

    public static SecretKey generateKey(String password, String username, String salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
//        // Number of PBKDF2 hardening rounds to use. Larger values increase
//        // computation time. You should select a value that causes computation
//        // to take >100ms.
//        final int iterations = 1000;
//
//        // Generate a 256-bit key
//        final int outputKeyLength = 256;
//
//        SecretKeyFactory secretKeyFactory = SecretKeyFactory
//                .getInstance("PBKDF2WithHmacSHA1");
//        KeySpec keySpec = new PBEKeySpec(passphraseOrPin, salt, iterations,
//                outputKeyLength);
//        return secretKeyFactory.generateSecret(keySpec);


        byte[] key = new byte[0];
        try {
            key = (salt + username + password).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        MessageDigest sha = null;
        try {
            sha = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16); // use only first 128 bit

        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
    }

    public static byte[] encodeFile(SecretKey yourKey, byte[] fileData)
            throws Exception {
        byte[] data = yourKey.getEncoded();
        SecretKeySpec skeySpec = new SecretKeySpec(data, 0, data.length,
                algorithm);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

        byte[] encrypted = cipher.doFinal(fileData);

        return encrypted;
    }

    public static byte[] decodeFile(SecretKey yourKey, byte[] fileData)
            throws Exception {
        byte[] data = yourKey.getEncoded();
        SecretKeySpec skeySpec = new SecretKeySpec(data, 0, data.length,
                algorithm);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);

        byte[] decrypted = cipher.doFinal(fileData);

        return decrypted;
    }

    public static byte[] saveFile(String stringToSave, File file, SecretKey yourKey) {
        try {
//            File file = new File(Environment.getExternalStorageDirectory()
//                    + File.separator, encryptedFileName);
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(file));
//            SecretKey yourKey = generateKey(p, generateSalt().toString()
//                    .getBytes());
            byte[] filesBytes = encodeFile(yourKey, stringToSave.getBytes());
            bos.write(filesBytes);
            bos.flush();
            bos.close();
            return filesBytes;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decodeFile(File file, SecretKey yourKey) {
        try {
            byte[] decodedData = decodeFile(yourKey, readFile(file));
            String str = new String(decodedData);
            Log.e("MainActivity", "DECODED FILE CONTENTS : " + str);
            return str;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Null";
    }

    public static byte[] readFile(File file) {
        byte[] contents = null;

//        File file = new File(Environment.getExternalStorageDirectory()
//                + File.separator, encryptedFileName);
        int size = (int) file.length();
        logger.info("MainActivity", "Decrypting file size ===" + size);
        contents = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(
                    new FileInputStream(file));
            try {
                buf.read(contents);
                buf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return contents;
    }

}
