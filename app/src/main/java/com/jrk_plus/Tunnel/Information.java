package com.jrk_plus.Tunnel;

/**
 * Created by ABHISHEK MAHATO on 4/23/2017.
 */

import android.util.Log;

import com.jrk_plus.Tunnel.ConfigurationConstants.MessageType;

import java.util.Arrays;

import opt.packet.Packet;

import static jrk_plus.hu.blint.ssldroid.TcpProxyServerThread.isFirstPacket;
import static opt.log.OmLogger.logger;

public class Information {
    public static int clientId = 0;

    public static byte[] addHeader(byte[] info, String ip, int port, int protocol, int action) {
        try {
            logger.info("Stuffing SIP Proxy Header");
            logger.info("Original Length : " + info.length);
            logger.info("IsFirstPacket : " + isFirstPacket);
            byte[] data = null;
            int i = 0;

            int len = 0;
            len = info.length + 8;
            data = new byte[len];
            String[] labels = ip.split("\\.");

            for (int j = 0; j < labels.length; j++)
                data[i++] = (byte) Integer.parseInt(labels[j]);

            data[i++] = (byte) ((port >> 8) & 0xFF);
            data[i++] = (byte) (port & 0xFF);

            data[i++] = (byte) (protocol & 0xFF);
            data[i++] = (byte) (action & 0xFF);
//            isFirstPacket = false;
//            }

            logger.info("Value of i : " + i);
            if (action == 3)
                logger.debug("ng", "ACTION_FORWARD");
            System.arraycopy(info, 0, data, i, info.length);
            logger.info("Length of data copied: " + info.length + " bytes");
            logger.info("Final Length : " + data.length);
            return data;
        } catch (Exception e) {
            logger.info("Add Header Exception:" + Log.getStackTraceString(e));
        }
        return null;
    }

    public static byte[] createProvInfo(MessageType msgType) {
        byte[] info = new byte[4096];
        PacketStructure packet_structure = new PacketStructure();

        if (msgType == MessageType.SETTINGS_UPDATE)
            packet_structure.prepareMessage(info, ConfigurationConstants.SETTINGS_UPDATE);

        packet_structure.addAttribute(ConfigurationConstants.CLIENT_IDENTIFIER, clientId, info);
        packet_structure.addAttribute(ConfigurationConstants.SERVER_CHILD_COUNT, 1, info);
        packet_structure.addAttribute(ConfigurationConstants.SERVER_MPPC, 20000, info);

        int packetLen = packet_structure.twoByteToInt(info, 2);

        return Arrays.copyOfRange(info, 0, packetLen);
    }

    public static void processProvResponse(byte[] info) {
        PacketStructure packet_structure = new PacketStructure();

        int messageType = packet_structure.twoByteToInt(info, 0);
        int messageLength = packet_structure.twoByteToInt(info, 2);

        int messageIndex = ConfigurationConstants.MINIMUM_PACKET_LEN;

        switch (messageType) {
            case ConfigurationConstants.SETTINGS_UPDATE_SUCCESS:
                while (messageIndex < messageLength) {
                    int attributeType = packet_structure.twoByteToInt(info,
                            messageIndex);
                    messageIndex += 2;
                    int attributeLen = packet_structure.twoByteToInt(info,
                            messageIndex);
                    messageIndex += 2;
                    int index = messageIndex;
                    messageIndex += attributeLen;
                    switch (attributeType) {
                        case ConfigurationConstants.CLIENT_IDENTIFIER:
                            clientId = packet_structure.twoByteToInt(info, index);
                            logger.info("[SETTINGS UPDATE] : Success");
                            logger.info("[CLIENT_ID] : " + clientId);
                            break;
                        default:
                            logger.info("Wrong Attribute Type");
                    }
                }
                break;
            case ConfigurationConstants.SETTINGS_UPDATE_FAILED:
                logger.info("[SETTINGS UPDATE] : Failed");
                break;
            case ConfigurationConstants.OPEN_VPN:
                break;
            case ConfigurationConstants.PROV:
                break;
            default:
                logger.info("Wrong Response");
        }
    }

    public static byte[] createAppDataInfo(Packet packet) {
        byte[] info = new byte[8192];
        byte[] msg = new byte[packet.length()];
        System.arraycopy(packet.buffer, packet.bufferOffset, msg, 0, packet.length());
        PacketStructure packet_structure = new PacketStructure();

        packet_structure.prepareMessage(info, ConfigurationConstants.OPEN_VPN);
        packet_structure.addAttribute(ConfigurationConstants.CLIENT_IDENTIFIER, clientId , info);
        packet_structure.addAttribute(ConfigurationConstants.DATA_ATTRIBUTE, msg, info);

        int packetLen = packet_structure.twoByteToInt(info, 2);

        return Arrays.copyOfRange(info, 0, packetLen);
    }

    public static byte[] createAppDataInfo(byte[] buffer) {
        byte[] info = new byte[8192];
        PacketStructure packet_structure = new PacketStructure();

        packet_structure.prepareMessage(info, ConfigurationConstants.OPEN_VPN);
        packet_structure.addAttribute(ConfigurationConstants.CLIENT_IDENTIFIER, clientId, info);
        packet_structure.addAttribute(ConfigurationConstants.DATA_ATTRIBUTE, buffer, info);

        int packetLen = packet_structure.twoByteToInt(info, 2);

        return Arrays.copyOfRange(info, 0, packetLen);
    }

    public static byte[] createAppDataInfo(byte[] buffer, int offset, int len) {
        byte[] info = new byte[65535];
        byte[] msg = new byte[len];
        System.arraycopy(buffer, offset, msg, 0, len);

        PacketStructure packet_structure = new PacketStructure();

        packet_structure.prepareMessage(info, ConfigurationConstants.OPEN_VPN);
        packet_structure.addAttribute(ConfigurationConstants.CLIENT_IDENTIFIER, clientId, info);
        packet_structure.addAttribute(ConfigurationConstants.DATA_ATTRIBUTE, msg, info);

        int packetLen = packet_structure.twoByteToInt(info, 2);

        return Arrays.copyOfRange(info, 0, packetLen);
    }

    public static byte[] createFirstPacket() {
        byte[] info = new byte[512];
        PacketStructure packet_structure = new PacketStructure();

        logger.info("Value of CLIENT-ID : " + clientId);
        packet_structure.prepareMessage(info, ConfigurationConstants.OPEN_VPN);
        packet_structure.addAttribute(ConfigurationConstants.CLIENT_IDENTIFIER, clientId, info);

        int packetLen = packet_structure.twoByteToInt(info, 2);

        return Arrays.copyOfRange(info, 0, packetLen);
    }
}
