package com.jrk_plus.Tunnel;
/**
 * Created by ABHISHEK MAHATO on 4/23/2017.
 */

import android.content.Context;
import android.util.Log;

import com.jrk_plus.Tunnel.ConfigurationConstants.MessageType;
import com.jrk_plus.Tunnel.ConfigurationConstants.TunnelType;
import com.jrk_plus.Tunnel.Http.HttpHeaderType;
import com.app.jrk_plus.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import opt.enc.stream.StreamEncryption;
import opt.packet.Packet;
import opt.utils.CustomBase64;
import pseudoSsl.PseudoSslWrapUnWrap;

import static com.jrk_plus.Tunnel.PseudoSslInterface.STREAM_VALUE;
import static opt.log.OmLogger.logger;
import static opt.packet.Packet.MAX_BUFFER_LENGTH;


public class Provisioning {
    byte[] info;
    Config config;
    private ReceiveProvInfoInterface mListner;
    Context ctx;
    public static int returncodeforProv = -1;
    public static String provresponse = null;
    public String httpdnsIP;
    public String httpsdnsIP;

    public Provisioning(Config conf, ReceiveProvInfoInterface mListner, Context ctx) {
        Information.clientId = 0;
        this.config = conf;
        this.mListner = mListner;
        this.ctx = ctx;
    }

    private static void setSNIHost(final SSLSocketFactory factory, final SSLSocket socket, final String hostname) {
        try {
            socket.getClass().getMethod("setHostname", String.class).invoke(socket, hostname);
        } catch (Throwable e) {
            // ignore any error, we just can't set the hostname...
        }
    }

    public static String printHexDump(byte[] bytes, int len) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++) {
                if (i % 20 == 0) {
                    sb.append("\n");
                }
                sb.append(String.format("%02x ", bytes[i] & 0xff));
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error("Ex[printHexDump]: ", e);
        }
        return "failed to print hexdump";
    }

    public int sendProvInfo(final Context context, final String username, final String password, final String imei) {
        TunnelType tunnelType = config.getTunnelType();
        String remoteIp = config.getRemoteIp();
        int remotePort = config.getRemotePort();
        String sni = config.getSni();
//        info = Information.createProvInfo(MessageType.SETTINGS_UPDATE);

        info = com.jrk_plus.Provisioning.Information.createProvInfo(context, MessageType.PROV, username, password, imei);
//        info = Information.addHeader(info, "0.0.0.0", 0, 2, ConfigurationConstants.ACTION_FORWARD);
        String ip = "0.0.0.0";
        int port = 0;
        logger.debug("Provising", "Sending Provising info from provising/provising.java at ip= " + ip);
        logger.debug("Provising", "Sending Provising info from provising/provising.java at port= " + port);

        info = com.jrk_plus.Provisioning.Information.addHeader(info, ip,
                port, ConfigurationConstants.PROTO_UDP, ConfigurationConstants.ACTION_PROVISIONING_PROXY);

        Packet packet = new Packet(info, 0, info.length);
        Packet httpPacket = new Packet(4096);
        Socket clientSocket = new Socket();
        int tcpTimeout = 10000;
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                logger.debug("ProvisingInfo", "fetch DNS IP");
                httpdnsIP = DnsResolverAPI.getVpnProxyIp("jrkdirect.nipantel.net");
                if (httpdnsIP != null) {
                    logger.debug("ProvisingInfo", "GOT DNS IP httpdnsIP:: " + httpdnsIP);
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                httpsdnsIP = DnsResolverAPI.getVpnProxyIp("jrkdirect.nipantel.net");
                if (httpsdnsIP != null) {
                    logger.debug("ProvisingInfo", "GOT DNS IP httpsdnsIP: " + httpsdnsIP);
                }
            }
        });
        //executorService.shutdown();

        if (tunnelType == TunnelType.HTTP) {
//            info = HttpWrapUnwrap.httpWrapClientSide(info, info.length);
            logger.debug("ProvisingInfo", "Using HTTP Protocol" + remoteIp);
            Log.d("ProvisingInfo", "Ip ====" + remoteIp);
            Http http = new Http(HttpHeaderType.HTTP_HEADER_1);
            http.httpWrapClientSide(packet, httpPacket);
            try {
                clientSocket.connect(new InetSocketAddress(InetAddress.getByName(remoteIp), remotePort), tcpTimeout);
                clientSocket.setSoTimeout(tcpTimeout);
                OutputStream outToServer = clientSocket.getOutputStream();
                InputStream input = clientSocket.getInputStream();

                outToServer.write(httpPacket.buffer, httpPacket.bufferOffset, httpPacket.length());
                logger.info("[PROV_REQUEST] : Sent");

//                byte[] recvData = HttpWrapUnwrap.receiveHttpMsg(input);
//                recvData = HttpWrapUnwrap.httpUnwrapClientSide(recvData);
                byte[] recvData = new byte[65535];
                int bytesRead = input.read(recvData);
                System.out.println(" $$$$$$$$ " + bytesRead);
                packet = new Packet(recvData, 0, bytesRead);
                logger.info("SET HTTP MSG : " + http.setHttpMsg(packet));
                packet.reset();
                logger.info("UNWRAP : " + http.httpUnWrapClientSide(packet));

                Information.processProvResponse(Arrays.copyOfRange(packet.buffer, packet.bufferOffset, packet.bufferOffset + packet.length()));

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (tunnelType == TunnelType.PSEUDO_SSL) {
            logger.debug("Provising", "Using PSEUDO_SSL Protocol");
            Log.d("ProvisingInfo", "Pseudo ssl Ip ====" + remoteIp);
            packet = new Packet(16384);
            PseudoSslWrapUnWrap sslWrapUnWrap = new PseudoSslWrapUnWrap(false);
            sslWrapUnWrap.ptlsConnect(sni, packet);
            StreamEncryption streamEncryption = StreamEncryption.getStreamEncryptionObject(STREAM_VALUE);
            StreamEncryption streamDecryption = StreamEncryption.getStreamEncryptionObject(STREAM_VALUE);

            byte[] message_received = new byte[16384];
            try {
                clientSocket.connect(new InetSocketAddress(InetAddress.getByName(remoteIp), remotePort), tcpTimeout);
                clientSocket.setSoTimeout(tcpTimeout);
                OutputStream outToServer = clientSocket.getOutputStream();
                InputStream input = clientSocket.getInputStream();
                outToServer.write(packet.buffer, packet.bufferOffset, packet.length());
                int ret = -1;
                Packet recvPacket = null;
                while (ret < 0) {
                    int readLen = input.read(message_received);
                    packet = new Packet(message_received, 0, readLen);

                    if (!sslWrapUnWrap.setPseudoSslMsg(packet)) {
                        logger.warn("Setting Pseudo Msg Failed");
                        return -1;
                    }
                    recvPacket = new Packet(MAX_BUFFER_LENGTH);
                    ret = sslWrapUnWrap.pseudoSslUnWrap(recvPacket);

                    logger.info("PseudoSslUnwrap return Value [" + ret + "] handshake [" + sslWrapUnWrap.getHandshakeState() + "]");
                    switch (sslWrapUnWrap.getHandshakeState()) {
                        case SERVER_HELLO_DONE:
                            logger.info("SERVER HELLO DONE");
                            break;
                        default:
                            logger.info("Wrong Message Type");
                    }
                }

                packet = new Packet(16384);
//                byte[] provInfo = new byte[2048];
//                System.arraycopy(info, 0, provInfo, 0, info.length);
//                logger.info("[HEX-DUMP] : " + printHexDump(info, info.length));

//                int provInfoLen = opt.enc.continuous.ContinuousEncryption.encrypt(provInfo, 0, info.length);
                streamEncryption.encrypt(info);

                packet.appendInEnd(info);
                sslWrapUnWrap.pseudoSslWrap(packet);
                outToServer.write(packet.buffer, packet.bufferOffset, packet.length());
                outToServer.flush();
//                Thread.sleep(5000);

                ret = -1;
                recvPacket.reset();
                while (ret < 0) {
                    int readLen = input.read(message_received);
                    logger.info("[RESPONSE] : " + opt.utils.HexUtil.getArrayInHexFormat(message_received, 0, readLen));

                    packet = new Packet(message_received, 0, readLen);

                    if (!sslWrapUnWrap.setPseudoSslMsg(packet)) {
                        logger.warn("Setting Pseudo Msg Failed");
                        return -1;
                    }
                    ret = sslWrapUnWrap.pseudoSslUnWrap(recvPacket);

//                    logger.info("APP-DATA [" + ret + "] handshake [" + sslWrapUnWrap.getHandshakeState() + "]");
                    switch (sslWrapUnWrap.getHandshakeState()) {
                        case APP_DATA:
                            logger.info("SERVER HELLO DONE");
                            break;
                        default:
                            logger.info("Wrong Message Type");
                    }
                }

                if (clientSocket != null)
                    clientSocket.close();

                logger.info("[ENCRYPTED-RESPONSE-HEX-DUMP] : " + opt.utils.HexUtil.getArrayInHexFormat(recvPacket.buffer,
                        recvPacket.bufferOffset, recvPacket.length()));

//                int decryptedLen = opt.enc.continuous.ContinuousEncryption.decrypt(recvPacket.buffer, recvPacket.bufferOffset, recvPacket.length());
//                if(decryptedLen > 0)
//                    recvPacket.resetLength(decryptedLen);
//                logger.info("[DECRYPTED LENGTH] : "+ decryptedLen);
                streamDecryption.decrypt(recvPacket);
                logger.info("[DECRYPTED LENGTH] : " + recvPacket.length());
                logger.info("[DECRYPTED-RESPONSE-HEX-DUMP] : " + opt.utils.HexUtil.getArrayInHexFormat(recvPacket.buffer,
                        recvPacket.bufferOffset, recvPacket.length()));
//                Information.processProvResponse(Arrays.copyOfRange(recvPacket.buffer, recvPacket.bufferOffset, recvPacket.bufferOffset + recvPacket.length()));
                mListner.responseReceived(false);
                return com.jrk_plus.Provisioning.Information.receiveProvInfo(context, Arrays.copyOfRange(recvPacket.buffer, recvPacket.bufferOffset, recvPacket.bufferOffset + recvPacket.length()));
//                Information.processProvResponse(message_received);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (tunnelType == TunnelType.DOMAIN_FRONT) {

            logger.debug("Provising", "Using Domain Fronting");
            Log.d("ProvisingInfo", "Ip ====" + remoteIp);
            CustomBase64 base64 = new CustomBase64("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_");
            final String START_DELIM = "@@@@";
            final String END_DELIM_FRONT_DOMAIN = "&&&&";

            // StreamEncryption streamEncryption = StreamEncryption.getStreamEncryptionObject(STREAM_VALUE);
            // StreamEncryption streamDecryption = StreamEncryption.getStreamEncryptionObject(STREAM_VALUE);

            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };
            OutputStream output = null;
            InputStream inputStream = null;

            SSLContext ssl_context = null;
            try {
                ssl_context = SSLContext.getInstance("TLS");
                ssl_context.init(null, trustAllCerts, null);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
            final SSLSocketFactory sf = ssl_context.getSocketFactory();
            SSLSocket st = null;

            String GoogleResolveAddr = "172.217.161.4";
            int LengthofDoaminLabel = generateRandomIntIntRange(4, 10);
            String DomainFirstLabel = randomAlphaNumeric(LengthofDoaminLabel);
            String domainName = ctx.getString(R.string.domain_fronting_domain);
            if (DomainFirstLabel != null) {
                domainName = DomainFirstLabel + ".appspot.com";
            }
            try {
                st = (SSLSocket) sf.createSocket();
//                Set<String> GoogleIPList = DnsResolverAPI.resolveAddress(domainName, T_A);
//                logger.info("[RESOLVER] :: Direct Dns Resolved IP: " + GoogleIPList);
//
//                if (GoogleIPList.size() > 0) {
//                    for (String ipaddr : GoogleIPList) {
//                        logger.info("[GoogleIPList ] : IP : " + ipaddr);
//                    }
//                }
//                GoogleResolveAddr = GoogleIPList.iterator().next();
                st.connect(new InetSocketAddress(domainName, 443), 20000);
                st.setSoTimeout(20000);
                logger.debug("Hello exiting 11");
//                setSNIHost(sf, (SSLSocket) st, "google");
                ((SSLSocket) st).startHandshake();
                logger.debug("Hello exiting  12");
            } catch (Exception e) {

            }

//            try {
//                st = (SSLSocket) sf.createSocket();
//                st.connect(new InetSocketAddress(GoogleResolveAddr, 443), 20000);
//                st.setSoTimeout(20000);
//                logger.info("Hello exiting 11");
////                setSNIHost(sf, (SSLSocket) st, "www.appspot.com");
//                ((SSLSocket) st).startHandshake();
//                logger.info("Hello exiting  12");
//            } catch (Exception e) {
//
//            }
//            Packet provPacket = new Packet(info, 0, info.length);

            info = com.jrk_plus.Provisioning.Information.prepareProvInfo(context, username, password, imei);

//            byte[] tempArr = new byte[4096];
//            System.arraycopy(info, 0, tempArr, 0, info.length);
//            int tempLen = opt.enc.continuous.ContinuousEncryption.encrypt(tempArr, 0, info.length);
//            streamEncryption.encrypt(info);
            packet = new Packet(info, 0, info.length);

            Packet provPacket = new Packet(8192);
            base64.encode(packet, 0, packet.length(), provPacket);

            String message = "GET /webtest/"
                    + START_DELIM
                    + provPacket.toString()
                    + END_DELIM_FRONT_DOMAIN
                    + " HTTP/1.1\r\nHost: " + ctx.getString(R.string.domain_fronting_host)
//                    + front_domain
                    + "\r\nScheme : https\r\nUser-Agent: Mozilla/5.0 \r\nAccept: text/html,json\r\nAccept-Language: en-US,en;q=0.8,hi;q=0.6\r\nCookie: CONSENT=YES+IN.en+20160821-19-0\r\nConnection: keep-alive\r\n\r\n";


            try {
                output = st.getOutputStream();
                inputStream = st.getInputStream();

//                logger.info("[MESSAGE] : " + message);
                Log.d("[MESSAGE] : ", "sendProvInfo: " + message);
                output.write(message.getBytes());

//                logger.info("HTTPS Packet sent...");
                Log.d("[MESSAGE] : ", "sendProvInfo: HTTPS Packet sent...");

                byte[] response = new byte[4096];
                Packet recvPacket = new Packet(16384);
                int len = -1;
                String recvStr = "";
                while ((len = inputStream.read(response)) >= 0) {
                    recvStr += (new String(response, 0, len));
                    if (recvStr.indexOf(END_DELIM_FRONT_DOMAIN) > 0) {
                        logger.info("[RESP] : Still running...");
                        break;
                    }
                    logger.info("HTTPS Packet : " + recvStr);
                    logger.info("HTTPS Packet Receiving... : " + len);
                }
                logger.info("[Response] : " + recvStr);
                int startIndex, endIndex;
                startIndex = recvStr.indexOf(START_DELIM);
                endIndex = recvStr.indexOf(END_DELIM_FRONT_DOMAIN);

                if (endIndex < 0) {
                    logger.info("End Deimiter not found...Returning...");
                    return -1;
                }

                String temp = recvStr.substring(startIndex + 4, endIndex);
                provPacket = new Packet(temp.getBytes(), 0, temp.length());
                logger.info("[ANSWER] : " + provPacket.toString());
                packet = new Packet(provPacket.length());

                int decoded_len = base64.decode(provPacket, 0, provPacket.length(), packet);

//                int decryptedLen = opt.enc.continuous.ContinuousEncryption.decrypt(packet.buffer, packet.bufferOffset, packet.length());
//                streamDecryption.decrypt(packet);

                logger.info("[ANSWER-STRING] :: ", " ===" + provPacket);
                logger.info("[ANSWER-LENGTH] : " + packet.length() + " : " + provPacket.length() + " : " + decoded_len);

                mListner.responseReceived(false);
                return com.jrk_plus.Provisioning.Information.processProvInfo(context, Arrays.copyOfRange(packet.buffer, packet.bufferOffset, packet.bufferOffset + packet.length()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            if (clientSocket != null)
                clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        ExecutorService executorService = Executors.newFixedThreadPool(1);
//        executorService.execute(new Runnable() {
        try {
            logger.info("[Provisioning] :: ", "Trying with Http");
            int retvalhttp = HttpDirectrun(context, username, password, imei);
            if (retvalhttp != -1) {
                return retvalhttp;
            }
            logger.info("[Provisioning] :: ", "Trying with Https");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            int retvalhttps = HttpsDirectrun(context, username, password, imei);
            return retvalhttps;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int HttpsDirectrun(final Context context, final String username, final String password, final String imei) {

        logger.debug("Provising", "Using Https Direct Mode");
        CustomBase64 base64 = new CustomBase64("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_");
        final String START_DELIM = "@@@@";
        final String END_DELIM_FRONT_DOMAIN = "&&&&";

        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };
        OutputStream output = null;
        InputStream inputStream = null;

        SSLContext ssl_context = null;
        try {
            ssl_context = SSLContext.getInstance("TLS");
            ssl_context.init(null, trustAllCerts, null);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        final SSLSocketFactory sf = ssl_context.getSocketFactory();
        SSLSocket st = null;
        if (httpsdnsIP == null) {
            httpsdnsIP = "jrkdirect.nipantel.net";
        }

        try {
            st = (SSLSocket) sf.createSocket();
            st.connect(new InetSocketAddress(httpsdnsIP, 443), 20000);
            st.setSoTimeout(20000);
            ((SSLSocket) st).startHandshake();
        } catch (Exception e) {

        }
        info = com.jrk_plus.Provisioning.Information.prepareProvInfo(context, username, password, imei);
        Packet packet = new Packet(info, 0, info.length);

        Packet provPacket = new Packet(8192);
        base64.encode(packet, 0, packet.length(), provPacket);

        String message = "GET /webtest/"
                + START_DELIM
                + provPacket.toString()
                + END_DELIM_FRONT_DOMAIN
                + " HTTP/1.1\r\nHost: " + ctx.getString(R.string.domain_fronting_host)
//                    + front_domain
                + "\r\nScheme : https\r\nUser-Agent: Mozilla/5.0 \r\nAccept: text/html,json\r\nAccept-Language: en-US,en;q=0.8,hi;q=0.6\r\nCookie: CONSENT=YES+IN.en+20160821-19-0\r\nConnection: keep-alive\r\n\r\n";


        try {
            output = st.getOutputStream();
            inputStream = st.getInputStream();
            Log.d("[MESSAGE] : ", "sendProvInfo: " + message);
            output.write(message.getBytes());
            Log.d("[MESSAGE] : ", "sendProvInfo: HTTPS Packet sent...");

            byte[] response = new byte[4096];
            Packet recvPacket = new Packet(16384);
            int len = -1;
            String recvStr = "";
            while ((len = inputStream.read(response)) >= 0) {
                recvStr += (new String(response, 0, len));
                if (recvStr.indexOf(END_DELIM_FRONT_DOMAIN) > 0) {
                    logger.info("[RESP] : Still running...");
                    break;
                }
                logger.info("HTTPS Packet : " + recvStr);
                logger.info("HTTPS Packet Receiving... : " + len);
            }
            logger.info("[Response] : " + recvStr);
            int startIndex, endIndex;
            startIndex = recvStr.indexOf(START_DELIM);
            endIndex = recvStr.indexOf(END_DELIM_FRONT_DOMAIN);

            if (endIndex < 0) {
                logger.info("End Deimiter not found...Returning...");
                return -1;
            }

            String temp = recvStr.substring(startIndex + 4, endIndex);
            provPacket = new Packet(temp.getBytes(), 0, temp.length());
            logger.info("[ANSWER] : " + provPacket.toString());
            packet = new Packet(provPacket.length());

            int decoded_len = base64.decode(provPacket, 0, provPacket.length(), packet);
            logger.info("[ANSWER-STRING] :: ", " ===" + provPacket);
            logger.info("[ANSWER-LENGTH] : " + packet.length() + " : " + provPacket.length() + " : " + decoded_len);

            mListner.responseReceived(false);
            return com.jrk_plus.Provisioning.Information.processProvInfo(context, Arrays.copyOfRange(packet.buffer, packet.bufferOffset, packet.bufferOffset + packet.length()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }


    public int HttpDirectrun(final Context context, final String username, final String password, final String imei) {
        logger.debug("Provising", "Direct Http Method");
        //Log.d("ProvisingInfo","Ip ===="+remoteIp);
        CustomBase64 base64 = new CustomBase64("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_");
        final String START_DELIM = "@@@@";
        final String END_DELIM_FRONT_DOMAIN = "&&&&";

        Socket st = null;
        try {
            if (httpdnsIP == null) {
                httpdnsIP = "jrkdirect.nipantel.net";
            }
            st = new Socket(httpdnsIP, 65324);
        } catch (IOException e) {
            e.printStackTrace();
        }
        info = com.jrk_plus.Provisioning.Information.prepareProvInfo(context, username, password, imei);
        Packet packet = new Packet(info, 0, info.length);

        Packet provPacket = new Packet(8192);
        base64.encode(packet, 0, packet.length(), provPacket);

        String message = "GET /webtest/"
                + START_DELIM
                + provPacket.toString()
                + END_DELIM_FRONT_DOMAIN
                + " HTTP/1.1\r\nHost: " + ctx.getString(R.string.domain_fronting_host)
//                    + front_domain
                + "\r\nScheme : https\r\nUser-Agent: Mozilla/5.0 \r\nAccept: text/html,json\r\nAccept-Language: en-US,en;q=0.8,hi;q=0.6\r\nCookie: CONSENT=YES+IN.en+20160821-19-0\r\nConnection: keep-alive\r\n\r\n";

        OutputStream output;
        InputStream inputStream;
        try {
            output = st.getOutputStream();
            inputStream = st.getInputStream();
            Log.d("[MESSAGE] : ", "sendProvInfo: " + message);
            output.write(message.getBytes());
            Log.d("[MESSAGE] : ", "sendProvInfo: HTTPS Packet sent...");

            byte[] response = new byte[4096];
            Packet recvPacket = new Packet(16384);
            int len = -1;
            String recvStr = "";
            while ((len = inputStream.read(response)) >= 0) {
                recvStr += (new String(response, 0, len));
                if (recvStr.indexOf(END_DELIM_FRONT_DOMAIN) > 0) {
                    logger.info("[RESP] : Still running...");
                    break;
                }
                logger.info("HTTPS Packet : " + recvStr);
                logger.info("HTTPS Packet Receiving... : " + len);
            }
            logger.info("[Response] : " + recvStr);
            int startIndex, endIndex;
            startIndex = recvStr.indexOf(START_DELIM);
            endIndex = recvStr.indexOf(END_DELIM_FRONT_DOMAIN);

            if (endIndex < 0) {
                logger.info("End Deimiter not found...Returning...");
                return -1;
            }

            String temp = recvStr.substring(startIndex + 4, endIndex);
            provPacket = new Packet(temp.getBytes(), 0, temp.length());
            logger.info("[ANSWER] : " + provPacket.toString());
            packet = new Packet(provPacket.length());

            int decoded_len = base64.decode(provPacket, 0, provPacket.length(), packet);

            logger.info("[ANSWER-STRING] :: ", " ===" + provPacket);
            logger.info("[ANSWER-LENGTH] : " + packet.length() + " : " + provPacket.length() + " : " + decoded_len);

            returncodeforProv = com.jrk_plus.Provisioning.Information.processProvInfo(context, Arrays.copyOfRange(packet.buffer, packet.bufferOffset, packet.bufferOffset + packet.length()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returncodeforProv;
    }

// );
//        while(provresponse!=null)
//        {
//            try {
//                Thread.sleep(100);
//            }catch (Exception ex)
//            {
//                logger.info("[Prov Response http] :: "," ===");
//            }
//        }
//        executorService.shutdown();


    private static final String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstwxyz12345";

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static int generateRandomIntIntRange(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public interface ReceiveProvInfoInterface {
        public void responseReceived(boolean isError);
    }
}
