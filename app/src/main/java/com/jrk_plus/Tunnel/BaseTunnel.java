package com.jrk_plus.Tunnel;

import android.util.Log;

import com.jrk_plus.Tunnel.ConfigurationConstants.TunnelType;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SocketChannel;

import opt.packet.Packet;
import opt.selector.OmInterface;
import opt.selector.OmSelector;

import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 4/24/2017.
 */

public class BaseTunnel implements Runnable {
    Config config;
    static OmInterface[] tunnelInterface;
    ServerSocket ss;
    Socket client = null;

    public BaseTunnel(Config conf) {
        this.config = conf;
        tunnelInterface = new OmInterface[config.getClientChildCount()];
    }

    public void registerNewSocket(int j) {
        String bindIp = "0.0.0.0";
        String remoteIp = config.getRemoteIp();
        int remotePort = config.getRemotePort();
        TunnelType tunnelType = config.getTunnelType();
        SocketChannel socketChannel = null;
        try {
            socketChannel = SocketChannel.open();
            socketChannel.socket().bind(new InetSocketAddress(bindIp, 0));
        } catch (IOException e) {
            e.printStackTrace();
        }

//        if (tunnelType == TunnelType.HTTP)
//            tunnelInterface[j] = new MultiHttpInterface(false, j);
        if (tunnelType == TunnelType.PSEUDO_SSL)
            tunnelInterface[j] = new PseudoSslInterface(false, j);
        else
            tunnelInterface[j] = new SslInterface(true, HeaderFormat.ProtocolType.PROTOCOL_SSL_SOCKET.protocol);

        tunnelInterface[j].setReceiveTimeout(60);
        tunnelInterface[j].setSocketCloseTimeout(3000);
        tunnelInterface[j].setConnectIpPort(remoteIp, remotePort);
        logger.info("Connecting to [" + remoteIp + ":" + remotePort + "]");
        OmSelector.registerRequest(socketChannel, tunnelInterface[j]);
    }

    public void run() {
        logger.info("Starting tunnel");
        int localPort = config.getLocalPort();
        int clientChildCount = config.getClientChildCount();
        int clientMppc = config.getClientMppc();
        String serverIp = config.getServerIp();
        int serverPort = config.getServerPort();
        TunnelType tunnelType = config.getTunnelType();
        int socketConnectInterval = 480;
        OmSelector.setMaxChannel(1500);

//        try {
//            client = new DatagramSocket(localPort, InetAddress.getLocalHost());
//        } catch (SocketException e) {
//            e.printStackTrace();
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }


        if (tunnelType == TunnelType.PSEUDO_SSL)
            PseudoSslInterface.setDetails(this.config, null);
        /*************send all socket connection requests**************/
        for (int j = 0; j < clientChildCount; j++) {
            logger.info("Registering socket [ " + j + " ]");
            registerNewSocket(j);
            try {
                Thread.sleep(socketConnectInterval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        /*************************************************************/

        InputStream streamFromClient = null;
        OutputStream streamToClient = null;
        try {
            ss = new ServerSocket(localPort, 50, InetAddress.getLocalHost());
            logger.debug("Local Proxy at port : " + localPort);

            client = ss.accept();
//            client.setSoTimeout(8000);
            client.setTcpNoDelay(true);
            streamFromClient = client.getInputStream();
            streamToClient = client.getOutputStream();
        } catch (Exception e) {
            logger.info(Log.getStackTraceString(e));
        }

        final byte[] request = new byte[65535];
//        byte[] arr = new byte[4096];
//        final DatagramPacket request = new DatagramPacket(arr, 4096);
//        if (tunnelType == TunnelType.HTTP)
//            MultiHttpInterface.setDetails(this.config, client);
        if (tunnelType == TunnelType.PSEUDO_SSL)
            PseudoSslInterface.setDetails(this.config, streamToClient);
//        else
//            SslInterface.setDetails(this.config, streamToClient);

        /******************read from openvpn and forward to tunnel*************************/
        int j = 0;
        int bytesRead = 0;
        byte[] dataTosend;

        while (client != null) {
            j = j % clientChildCount;

            PseudoSslInterface pseudoSslInterface = (PseudoSslInterface) tunnelInterface[j];
            if (pseudoSslInterface != null) {
                logger.info("[CONNECTION-STATUS] : " + pseudoSslInterface.isConnected());
                if (pseudoSslInterface.isConnected()) {
                    for (int i = 0; i < clientMppc; i++) {
                        try {
                            while ((bytesRead = streamFromClient.read(request)) != -1) {
//                        client.receive(request);
//                            dataTosend = Information.createAppDataInfo(request.getData(), request.getOffset(), request.getLength());

                                dataTosend = com.jrk_plus.Provisioning.Information.createAppDataInfo(request, 0, bytesRead);
                                Packet httpPacket;

                                if (tunnelType == TunnelType.HTTP) {
                                    MultiHttpInterface multiHttpInterface = (MultiHttpInterface) tunnelInterface[j];
//                                if (i == 0) {
//                                    multiHttpInterface.setClientAddress(request.getAddress());
//                                    multiHttpInterface.setClientPort(request.getPort());
//                                }
                                    httpPacket = new Packet(8192);
                                    Packet sendPacket = new Packet(dataTosend, 0, dataTosend.length);
//                                Http http = new Http(Http.HttpHeaderType.HTTP_HEADER_1);
                                    multiHttpInterface.http.httpWrapClientSide(sendPacket, httpPacket);
                                } else if (tunnelType == TunnelType.PSEUDO_SSL) {
//                                if (i == 0) {
//                                    pseudoSslInterface.setClientAddress(request.getAddress());
//                                    pseudoSslInterface.setClientPort(request.getPort());
//                                }
//                                if (((PseudoSslInterface) tunnelInterface[j]).isFirstPacket()) {
//                                    dataTosend = Information.addHeader(dataTosend, serverIp, serverPort,
//                                            ConfigurationConstants.PROTO_TCP, ConfigurationConstants.ACTION_FORWARD);
//                                    ((PseudoSslInterface) tunnelInterface[j]).setFirstPacket(false);
//                                }

                                    ((PseudoSslInterface) tunnelInterface[j]).getStreamEncryption().encrypt(dataTosend);
                                    httpPacket = ((PseudoSslInterface) tunnelInterface[j]).psudoWrap(dataTosend, 0, dataTosend.length);
                                } else {
                                    httpPacket = new Packet(dataTosend, 0, dataTosend.length);
                                }

                                tunnelInterface[j].sendData(httpPacket.buffer, httpPacket.bufferOffset, httpPacket.length());

//                            tunnelInterface[j].sendData(dataTosend, 0, dataTosend.length);
                                logger.info("Bytes sent to server : " + bytesRead);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (clientChildCount > 1)
                        registerNewSocket(j);
                }
//            if(!tunnelInterface[j].isRunning()){
//                registerNewSocket(j);
//            }
                j++;
            }
        }

        /**************************************************************************/
    }

    public void stop() {
        try {
            int clientChildCount = config.getClientChildCount();
            for (int j = 0; j < clientChildCount; j++) {
                if (tunnelInterface[j] != null)
                    tunnelInterface[j].close();
            }
            if (client != null) {
                client.close();
                client = null;
            }
            if (ss != null) {
                ss.close();
            }
        } catch (IOException e) {
            logger.info(Log.getStackTraceString(e));
        }
    }
}
