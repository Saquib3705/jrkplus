package com.jrk_plus.Tunnel;

import android.net.SSLCertificateSocketFactory;
import android.os.Build;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 3/24/2017.
 */

public class ProxySSL implements Runnable {

    int localPort;
    TrustManager[] trustAllCerts;
    int sslTimeout;
    SSLSocket c;
    SSLContext sc;
    SSLSocketFactory sf;
    OutputStream output;
    InputStream input;
    public static ServerSocket ss;
    boolean isFirstPacket = true;

    public ProxySSL(int port) {
        this.localPort = port;

        sslTimeout = 10000;
        trustAllCerts = new TrustManager[]{new X509TrustManager() {

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
                // TODO Auto-generated method stub

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
                // TODO Auto-generated method stub

            }
        }};

//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                TrustManagerFactory tmf = TrustManagerFactory
//                        .getInstance(TrustManagerFactory.getDefaultAlgorithm());
//
//                AssetManager assetManager = MainActivity.conProv.getAssets();
//                InputStream keyStoreInputStream = assetManager.open("Dialer");
//
//                // ksName = "/sdcard/Dialer";
//                ksPass = "123456".toCharArray();
//                ctPass = "123456".toCharArray();
//                Security.addProvider(new BouncyCastleProvider());
//                ks = KeyStore.getInstance("BKS");
//
//                kmf = KeyManagerFactory.getInstance(KeyManagerFactory
//                        .getDefaultAlgorithm());
//                kmf.init(ks, ctPass);
//                sc = SSLContext.getInstance("TLS");
//                sc.init(kmf.getKeyManagers(), trustAllCerts, null);
//
//
//                ks.load(keyStoreInputStream, ksPass);
//            }b
//        } catch(Exception e){
//             logger.info("ng", "SSL certificate exception : " + )
//        }
    }

    @Override
    public void run() {
        try {
            String host = "88.150.226.229";
            int remoteport = 221;
            // Print a start-up message
            System.out.println("Starting proxy for " + host + ":" + remoteport
                    + " on port " + localPort);
            // And start running the server
            runServer(host, remoteport, localPort); // never returns
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static String printHexDump(byte[] bytes, int len) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++) {
                if (i % 20 == 0) {
                    sb.append("\n");
                }
                sb.append(String.format("%02x ", bytes[i] & 0xff));
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error("Ex[printHexDump]: ", e);
        }
        return "failed to print hexdump";
    }

    public byte[] addHeader(byte[] info, int length, String ip, int port, int protocol, int action) {
        try {
             logger.info("ng", "Stuffing SIP Proxy Header");
             logger.info("ng", "Original Length : " + length);
            byte[] data = null;
            int i = 0;

            int len = 0;
            if (isFirstPacket)
                len = length + 8;
            else
                len = length;
            /*
            Append buffer length at beginning in case of TCP
             */
            data = new byte[len];

            if (isFirstPacket) {
                String[] labels = ip.split("\\.");

                for (int j = 0; j < labels.length; j++)
                    data[i++] = (byte) Integer.parseInt(labels[j]);

                data[i++] = (byte) ((port >> 8) & 0xFF);
                data[i++] = (byte) (port & 0xFF);

                data[i++] = (byte) (protocol & 0xFF);
                data[i++] = (byte) (action & 0xFF);
                isFirstPacket = false;
            }

             logger.info("ng", "Value of i : " + i);

            if (action == ConfigurationConstants.ACTION_PROVISIONING_PROXY)
                 logger.info("ng", "Provisioning Proxy Request");
            else if (action == ConfigurationConstants.ACTION_BALANCE_PROXY)
                 logger.info("ng", "Balance Proxy Request");
            else if (action == ConfigurationConstants.ACTION_FORWARD)
                 logger.info("ng", "ACTION_FORWARD");
            System.arraycopy(info, 0, data, i, length);
             logger.info("ng", "Length of data copied: " + length + " bytes");
             logger.info("ng", "Final Length : " + data.length);
            return data;
        } catch (Exception e) {
            Log.e("ng", "Add Header Exception:" + Log.getStackTraceString(e));
        }
        return null;
    }

    public void runServer(String host, int remoteport, int localport)
            throws IOException {
        // Create a ServerSocket to listen for connections with
        ss = new ServerSocket(8877);

        final byte[] request = new byte[4096];
        byte[] reply = new byte[4096];

        while (true) {
            Socket client = null, server = null;
            // Wait for a connection on the local port



            try {
                SSLCertificateSocketFactory sf1 = (SSLCertificateSocketFactory) SSLCertificateSocketFactory.getDefault(5000);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
                    sf1.setTrustManagers(trustAllCerts);
                c = (SSLSocket) sf1.createSocket();
                c.setTcpNoDelay(true);
                c.setEnabledProtocols(c.getSupportedProtocols());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    Log.i("ng", "Setting SNI hostname");
                    sf1.setHostname(c, "www.facebook.com");
                } else {
                     logger.info("ng", "No documented SNI support on Android <4.2, trying with reflection");
                    try {
                        sc = SSLContext.getInstance("TLS");
                        sc.init(null, trustAllCerts, null);
                        sf = sc.getSocketFactory();
                        c = (SSLSocket) sf.createSocket();

                        java.lang.reflect.Method setHostnameMethod = c.getClass().getMethod("setHostname", String.class);
                        setHostnameMethod.invoke(c, "www.facebook.com");
                    } catch (Exception e) {
                        Log.e("ng", "TrustManager Exception : " + Log.getStackTraceString(e));
                    }
                }

                try {
                    c.connect(new InetSocketAddress(host, remoteport), sslTimeout);
                    c.setTcpNoDelay(true);
                    c.setSoTimeout(sslTimeout);
                    isFirstPacket = true;
                } catch (IOException e) {
                     logger.info("ng", "BS 1 ssl TEST  3");
                       /* Log.e("ng", "Provisioning init exception: Already " + Log.getStackTraceString(e));*/
                }

                // a thread to read the client's requests and pass them
                // to the server. A separate thread for asynchronous.
                try {
                    client = ss.accept();
                     logger.info("ng", "Connection accepted");
                } catch (Exception e){
                    e.printStackTrace();
                }

                final InputStream streamFromClient = client.getInputStream();
                final OutputStream streamToClient = client.getOutputStream();
                 logger.info("ng", "Stream From Client : " + client.isConnected());
                Thread t = new Thread() {
                    public void run() {
                        int bytesRead;
                        try {
                             logger.info("ng", "Stream From Client 1: " + streamFromClient.toString());
                            while ((bytesRead = streamFromClient.read(request)) != -1) {
                                logger.info("ng", "Data received from client : " + printHexDump(request, bytesRead));
                                byte[] dataToSend = addHeader(request, bytesRead, "88.150.226.226",
                                        1194, ConfigurationConstants.PROTO_TCP,
                                        ConfigurationConstants.ACTION_FORWARD);

                                 logger.info("ng", "Data sent to server : " + printHexDump(dataToSend, dataToSend.length));

                                output = c.getOutputStream();
                                input = c.getInputStream();
                                output.write(dataToSend, 0, dataToSend.length);
                                output.flush();
                            }
                        } catch (IOException e) {
                             logger.info("ng", "SSL Send Exception : " + Log.getStackTraceString(e));
                        }

                        // the client closed the connection to us, so close our
                        // connection to the server.
                        try {
                            output.close();
                        } catch (IOException e) {
                        }
                    }
                };

                // Start the client-to-server request thread running
                t.start();

                // Read the server's responses
                // and pass them back to the client.
                int bytesRead;
                try {
                    while ((bytesRead = input.read(reply)) != -1) {
                        streamToClient.write(reply, 0, bytesRead);
                        streamToClient.flush();
                    }
                } catch (IOException e) {
                }

                // The server closed its connection to us, so we close our
                // connection to our client.
                streamToClient.close();
            } catch (IOException e) {
                System.err.println(e);
            } finally {
                try {
                    if (server != null)
                        server.close();
                    if (client != null)
                        client.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public static void stopServer() {
        try {
            ss.close();
        } catch (IOException e) {
            Log.e("ng", "Proxy socket close error : " + Log.getStackTraceString(e));
        }
    }
}
