package com.jrk_plus.NewDashboard;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;

import com.app.jrk_plus.R;
import com.github.lzyzsd.circleprogress.ArcProgress;
import com.jrk_plus.Login.LoginActivity;
import com.jrk_plus.Provisioning.Information;
import com.jrk_plus.TransportLayer.OkktpHttpRequest;
import com.jrk_plus.Tunnel.Config;
import com.jrk_plus.Tunnel.ConfigurationConstants;
import com.jrk_plus.Tunnel.CredentialResults;
import com.jrk_plus.Tunnel.MainActivity;
import com.jrk_plus.Tunnel.Provisioning;
import com.jrk_plus.database.DBHelper;
import com.jrk_plus.fragments.ConnectionFragment;
import com.jrk_plus.fragments.CountriesListFragment;
import com.jrk_plus.fragments.SelectedServer;
import com.jrk_plus.fragments.ServerListFragment;
import com.jrk_plus.util.CountriesNames;
import com.jrk_plus.util.Stopwatch;
import com.jrk_plus.util.TotalTraffic;
import com.jrk_plus.utilities.AppSignature;
import com.jrk_plus.utilities.CredentialCheck;
import com.jrk_plus.utilities.MessageConstatns;
import com.jrk_plus.utilities.MessageParser;
import com.jrk_plus.utilities.MoreProtectedEncryption;
import com.jrk_plus.utilities.NetworkUtil;
import com.jrk_plus.utilities.PrefManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.crypto.SecretKey;

import de.blinkt.openvpn.core.VpnStatus;
import jrk_plus.hu.blint.ssldroid.TcpProxy;
import okhttp3.OkHttpClient;
import opt.log.OmLogger;

import static com.jrk_plus.Tunnel.ConfigurationConstants.CONNECT_ACTIVITY_BROADCAST_ACTION;
import static opt.log.OmLogger.logger;

public class ConnectActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener,
        Provisioning.ReceiveProvInfoInterface, CredentialResults, OkktpHttpRequest.OkktpHttpResponseEvents, ConnectionFragment.OnFragmentInteractionListener {

    //    private DrawerLayout drawer;
//    private NavigationView navigationView;
//    ActionBarDrawerToggle toggle;
    //    private RadioButton button_country_list, button_recommended;
    private String fragmentTag = "";
    private TextView validityText, userId, /*connectionStatusText,*/
            connectingText, downloadVolumeText,
            uploadVolumeText, downloadSpeedText, uploadSpeedText, serverNameText, textTimeUnit;
    //    private SlidingUpPanelLayout mLayout;
//    private ImageView imageDrawerStatusShowing;
    private View notConnectedLayout, connectedLayout, connectingLayout;
    //    private ProgressBar connectingProgress;
    private Button quickConnect, cancelConnection, disConnectConnection;
    private CountDownTimer counterTimer = null;
    String shortCountry = "";
    private int connectingFrom = -1;//0= from serverList ,1 = from countryList
    private int previouslyConnectedFrom = -1;
    public ViewPager viewPager;
    public PagerAdapter adapter;
    private BroadcastReceiver mReceiver;
    private String connectedServerName = "Server";
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 11;
    private boolean doubleBackToExitPressedOnce = false;
    public static DBHelper dbHelper;
    public static Map<String, String> localeCountries;
    //Server loading activity
    private final int LOAD_ERROR = 0;
    private final int DOWNLOAD_PROGRESS = 1;
    private final int PARSE_PROGRESS = 2;
    private final int LOADING_SUCCESS = 3;
    private final int SWITCH_TO_RESULT = 4;
    private final String BASE_URL = "http://88.150.226.228:347/api/iphone/";
    private StringBuffer provGet = new StringBuffer("AndroidAppCheckSum");
    private final String BASE_IP = "88.150.226.228";
    private int BASE_PORT = 347;
    private final String REMOTE_IP = "88.150.226.227";
    private int REMOTE_PORT = 4444;
    private final String BASE_FILE_NAME = "vpngate.csv";
    private Handler updateHandler;
    //    private NumberProgressBar progressBar;
    private Stopwatch stopwatch;
    private int percentDownload = 0;
    private boolean premiumServers = false;
    private boolean premiumStage = true;
    private final String PREMIUM_URL = "http://easyvpn.rusweb.club/?type=csv";
    private final String PREMIUM_FILE_NAME = "premiumServers.csv";
    private CredentialCheck credentialCheck;
    private String identifier = null;
    private RelativeLayout validity_layout;
    private ProgressDialog progressDialog, progressDialog1;
    private PrefManager prefManager;
    private Timer myTimer = null;
    public static int networkStatus = -1;
    private String imeiNo = "xoxoxoxo";
    //For encryption and decryption only for testing
    String usernameEn = "abcdefghijkl@google.com";
    String passEn = "idQnruVVnbm+8s0Q0WWfDO+n8S8=";
    String salt = "salty";
    public static SecretKey ourKey = null;
    String keys = "";
    private int serverDownlodTimeOut = 30;
    static TcpProxy tp;
    ProgressBar waitingExpiryDate;
    AsyncTask<Object, Integer, Integer> workerAsyncTask = null;
    private Random randomGenerator;
    InitialisingFragment fragment;
    private boolean firstData = true;
    private Chronometer sessionTimeText, connectedTimeMainChorono;
    private boolean gotIlligalExceptionForremovingFragment = false;
    private NetworkChangeReceiver networkReceiver;
    public boolean isProvisiningDone = false;
    private TextView appVersion;
    private com.github.lzyzsd.circleprogress.DonutProgress donutProgress;
    public Dialog dialog, infoDialog, helpDialog;
    private FloatingActionButton statusFloatingActionButton;
    private ImageView btnCancel, infobtnCancel, helpbtnCancel;
    private CardView rechargeButton;
    private PopupWindow popup;
    private ImageButton info, help, logout, setting;
    private ArcProgress connectionProgress;
    private int progress = 0;
    private String expiryTypeText = "DAYS";
    private boolean isDemoApp = true;
    ConnectionFragment connectionFragment;


    @Override
    public void conenctionFragmentCancelConnection() {
        cancelConnection();
    }

    @Override
    public void conenctionFragmentDisconnectConnection() {
        disconenctConnection(false);
    }

    public void setToolBarVisibility(boolean b) {
        top_logo.setVisibility(b ? View.VISIBLE : View.GONE);
        toolbarView.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    public enum VPN_CONNECTION_STATE {
        NOT_CONNECTED(0), CONNECTING(1), CONNECTED(2);
        public final int id;

        VPN_CONNECTION_STATE(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public static VPN_CONNECTION_STATE getEnum(int id) {
            switch (id) {
                case 0:
                    return NOT_CONNECTED;
                case 1:
                    return CONNECTING;
                case 2:
                    return CONNECTED;
            }
            return NOT_CONNECTED;
        }
    }

    Toolbar toolbar;
    View toolbarView;
    LinearLayout top_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);
        setDonutProgress();
        final hu.blint.ssldroid.MyLogger mylogger = new hu.blint.ssldroid.MyLogger();
        opt.log.OmLogger.setLogger(mylogger);
        opt.log.OmLogger.enableAndroidLogging();
        opt.log.OmLogger.setLogLevel(OmLogger.LogLevel.NONE);
        logger.start();
//        Log.d("hello","Starting connect activity");


        logger.debug("hello", "Starting connect activity");
        dbHelper = new DBHelper(this);
        localeCountries = CountriesNames.getCountries();
        networkReceiver = new NetworkChangeReceiver();
        registerReceiver(networkReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        getImeiNo();
        prefManager = new PrefManager(this);
        prefManager.setConnectionState(VPN_CONNECTION_STATE.NOT_CONNECTED);
        prefManager.setConnectionWithServer("");
        registerTheBroadcastReceiver();
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage("Downloading Servers.....");
        progressDialog.setCancelable(false);
        progressDialog.setMax(100);


//        progressDialog1 = new ProgressDialog(this);
//        progressDialog1.setProgressStyle(R.style.Theme_MyDialog);
//        progressDialog1.setMessage("Disconnecting...");
//        progressDialog1.setCancelable(false);

        toolbarView = findViewById(R.id.toolbar);
        top_logo = findViewById(R.id.top_logo);
        toolbar = (Toolbar) findViewById(R.id.custom_connect_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");
        info = toolbar.findViewById(R.id.info);
        help = toolbar.findViewById(R.id.help);
//        setting = toolbar.findViewById(R.id.setting);
        logout = toolbar.findViewById(R.id.logout);
        logout.setOnClickListener(this);
        info.setOnClickListener(this);
        help.setOnClickListener(this);
//        setting.setOnClickListener(this);
//        drawer = (DrawerLayout) findViewById(R.id.connect_drawer_layout);
//        toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
//        navigationView.setItemIconTintList(null);
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                drawer.closeDrawers();
//                switch (item.getItemId()) {
//                    case R.id.validity_change:
//                        startActivity(new Intent(ConnectActivity.this, ValidityUpdationActivity.class));
//                        return true;
//                    case R.id.navigation_help_support:
//                        startActivity(new Intent(ConnectActivity.this, HelpAndSupportActivity.class));
//                    default:
//                        return true;
//                }
//            }
//        });
//        setEventListner(navigationView);

        updateHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.arg1) {
                    case LOAD_ERROR: {
                        progressDialog.setProgress(100);
                        if (!isFinishing() && progressDialog != null && progressDialog.isShowing())
                            progressDialog.cancel();
                        Toast.makeText(ConnectActivity.this, "Error occured during server downloading", Toast.LENGTH_LONG).show();
                    }
                    break;
                    case DOWNLOAD_PROGRESS: {
                        progressDialog.setProgress(msg.arg2);
                    }
                    break;
                    case PARSE_PROGRESS: {
                        progressDialog.setProgress(msg.arg2);
                    }
                    break;
                    case LOADING_SUCCESS: {
                        progressDialog.setProgress(100);
                        if (!isFinishing() && progressDialog != null && progressDialog.isShowing())
                            progressDialog.cancel();
                        prefManager.setIsFirstTimeServerDownloaded(true);
                        Message end = new Message();
                        end.arg1 = SWITCH_TO_RESULT;
                        updateHandler.sendMessageDelayed(end, 500);
                    }
                    break;
                    case SWITCH_TO_RESULT: {
                    }
                }
                return true;
            }
        });

        try {
            try {
                //this.ourKey = MoreProtectedEncryption.generateKey(passEn, usernameEn, salt);
                this.ourKey = MoreProtectedEncryption.generateKey(AppSignature.getAppSignature(this), usernameEn, salt);
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
//        sendProvisiningRequest();
        adapter = new PagerAdapter(getSupportFragmentManager());
        setEventListner();
        setEventListnerForDrawer();


//        downloadCountryServer();

//        fragment = new InitialisingFragment();
//        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.fragment_container, fragment, "IF");
//        fragmentTransaction.commit();


        if (!prefManager.isValidityExpired()) {
            validityText.setText(prefManager.getDeactivationDate());
            gotProvisiongResponse();
        } else {
            validityText.setText("Expired");
            textTimeUnit.setText(expiryTypeText);
            gotValidityExpiredFromProvisining();
        }

        getImeiNo();
        startSendingPing(prefManager.getKeepAliveTime());

    }

    private void setDonutProgress() {

        donutProgress = findViewById(R.id.donut_progress);
        donutProgress.setFinishedStrokeColor(getResources().getColor(R.color.red));
        donutProgress.setDonut_progress("10");
        donutProgress.setText("--");
        donutProgress.setFinishedStrokeWidth(10);
        donutProgress.setUnfinishedStrokeWidth(4);
        donutProgress.setTextSize(60);
        donutProgress.setTextColor(getResources().getColor(R.color.white));
    }

    @SuppressLint("HardwareIds")
    private void sendProvisiningRequest() {
        //getting credentials or server from provising

//        identifier = Secure.getString(ConnectActivity.this.getContentResolver(), Secure.ANDROID_ID);
        identifier = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Log.e("ConnectActivity", "Connected to mobile- " + Information.provisioningDone);

        if (Information.provisioningDone == 0) {
            //System.out.println("identifier:-1- " + identifier + "," + imeiNo);
            credentialCheck = new CredentialCheck(ConnectActivity.this, prefManager.getUserName(), prefManager.getPassword(), identifier, CredentialCheck.RequestType.PROV, null);
//            credentialCheck.credentialResults = HomeActivity.this;
            credentialCheck.execute();
        }

       /* if (ActivityCompat.checkSelfPermission(ConnectActivity.this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ConnectActivity.this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 123);
        } else {
            TelephonyManager tm = (TelephonyManager) ConnectActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null)
                identifier = tm.getDeviceId();
            if (identifier == null || identifier.length() == 0)
                identifier = Settings.Secure.getString(ConnectActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);

            credentialCheck = new CredentialCheck(ConnectActivity.this, prefManager.getUserName(), prefManager.getPassword(), identifier, CredentialCheck.RequestType.PROV, null);
//            credentialCheck.credentialResults = HomeActivity.this;
            credentialCheck.execute();
        }*/

    }

    private void createWorkerAsyncTask(final int errorCode, final SelectedServer selectedServer, final Context ctx, final CredentialCheck.RequestType requestType) {
        if (workerAsyncTask == null) {
            workerAsyncTask = new AsyncTask<Object, Integer, Integer>() {
                @Override
                protected Integer doInBackground(Object[] objects) {
                    logger.debug("WorkerAsynk", "Thread Start Running");
                    executeTask(errorCode, selectedServer, ctx, requestType);
                    return null;
                }

            };
            workerAsyncTask.execute();
        } else {
            workerAsyncTask.cancel(true);
            logger.debug("WorkerAsynk", "Thread isCancelled==" + workerAsyncTask.isCancelled());
            workerAsyncTask = null;
            createWorkerAsyncTask(errorCode, selectedServer, ctx, requestType);

        }
    }

    public void executeTask(int errorCode, SelectedServer selectedServer, Context ctx,
                            CredentialCheck.RequestType requestType) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                waitingExpiryDate.setVisibility(View.GONE);
                prefManager.setIsProvisiningDone(true);
                if (!prefManager.isValidityExpired()) {
                    removeInitialisingFragment();
                    validityText.setText(prefManager.getDeactivationDate());
                } else {
                    validityText.setText("Expired");
                    donutProgress.setText("0");
                    textTimeUnit.setText(expiryTypeText);
                    donutProgress.setProgress(0);

                }
            }
        });

        if (requestType == CredentialCheck.RequestType.PROV) {
            isProvisiningDone = true;
            if (!prefManager.isValidityExpired()) {
                gotProvisiongResponse();
            } else {
                gotValidityExpiredFromProvisining();
            }
            return;
        } else if (requestType == CredentialCheck.RequestType.SELECTED_SERVER) {
            Config config = new Config();
            config.setLocalIp("0.0.0.0");
            config.setLocalPort(1195);

            config.setClientChildCount(1);
            config.setClientMppc(20000);
            config.setRemoteIp(selectedServer.ip);
            config.setRemotePort(selectedServer.port);
            config.setSni((String) selectedServer.sniList.get(0));

            String tunnelName = "testVpn";
            String keyFile = "";
            String keyPass = "";
            if (tp != null) {
                logger.info("[TUNNEL] :: Stopping previous tunnel...");
                tp.stop();
            }

            logger.info("[TUNNEL] :: About to start tunnel...");
            logger.info("[TUNNEL] : " + config.getRemoteIp());
            logger.info("[TUNNEL] : " + config.getRemotePort());
            logger.info("[TUNNEL] : " + config.getSni());
            tp = new TcpProxy(tunnelName, config.getLocalPort(), config.getRemoteIp(),
                    config.getRemotePort(), keyFile, keyPass, config.getSni(), ctx);
            try {
                tp.serve();
            } catch (Exception e) {
                logger.debug("HomeActivity", "Exception ===" + Log.getStackTraceString(e));
            }

//            configureAndStartVpn(config, ctx);

        } else if (requestType == CredentialCheck.RequestType.EASY_VPN) {
//            fragment.prepareVpn();
        }
    }


    private void downloadCountryServer() {
        final AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                downloadCSVFile(BASE_URL, BASE_FILE_NAME);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        };
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void downloadCSVFile(String url, String fileName) {

        stopwatch = new Stopwatch();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        AndroidNetworking.download(url, getCacheDir().getPath(), fileName)
                .setTag("downloadCSV")
                .setPriority(Priority.MEDIUM)
                .setOkHttpClient(okHttpClient)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        if (totalBytes <= 0) {
                            // when we dont know the file size, assume it is 1200000 bytes :)
                            totalBytes = 1200000;
                        }
                        if (!premiumServers || !premiumStage) {
                            if (percentDownload <= 95)
                                percentDownload = percentDownload + (int) ((100 * bytesDownloaded) / totalBytes);
                        } else {
                            percentDownload = percentDownload + (int) ((100 * bytesDownloaded) / totalBytes);
                        }
                        Message msg = new Message();
                        msg.arg1 = DOWNLOAD_PROGRESS;
                        msg.arg2 = percentDownload;
                        updateHandler.sendMessage(msg);
                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        percentDownload = 100;
                        Message msg = new Message();
                        msg.arg1 = DOWNLOAD_PROGRESS;
                        msg.arg2 = percentDownload;
                        updateHandler.sendMessage(msg);
                        logger.debug("HomeActivity", "Csv File Download Complete");
                        prefManager.setLastTimeServerDownloadedTimestamp(System.currentTimeMillis());
                        prefManager.setIsCountryServerDownloadeOnce(true);
                        if (premiumServers && premiumStage) {
                            premiumStage = false;
                            downloadCSVFile(PREMIUM_URL, PREMIUM_FILE_NAME);
                        } else {
                            parseCSVFile(BASE_FILE_NAME);
                        }
                        serverDownloadedSuccessfully();
                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(ConnectActivity.this, "Server Downloading Error,Please try again", Toast.LENGTH_SHORT).show();
                        Message msg = new Message();
                        msg.arg1 = LOAD_ERROR;
                        msg.arg2 = R.string.network_error;
                        updateHandler.sendMessage(msg);
                    }
                });
    }

    private void parseCSVFile(String fileName) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(getCacheDir().getPath().concat("/").concat(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
            Message msg = new Message();
            msg.arg1 = LOAD_ERROR;
            msg.arg2 = R.string.csv_file_error;
            updateHandler.sendMessage(msg);
        }
        if (reader != null) {
            try {
                int startLine = 2;
                int type = 0;
                if (premiumServers && premiumStage) {
                    startLine = 0;
                    type = 1;
                } else {
                    dbHelper.clearTable();
                }
                int counter = 0;
                String line = null;
                while ((line = reader.readLine()) != null) {
                    if (counter >= startLine) {
                        dbHelper.putLine(line, type);
                    }
                    counter++;
                    if (!premiumServers || !premiumStage) {
                        Message msg = new Message();
                        msg.arg1 = PARSE_PROGRESS;
                        msg.arg2 = counter;// we know that the server returns 100 records
                        updateHandler.sendMessage(msg);
                    }
                }
                if (premiumServers && !premiumStage) {
                    premiumStage = true;
                    parseCSVFile(PREMIUM_FILE_NAME);
                } else {
                    Message end = new Message();
                    end.arg1 = LOADING_SUCCESS;
                    updateHandler.sendMessageDelayed(end, 200);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Message msg = new Message();
                msg.arg1 = LOAD_ERROR;
                msg.arg2 = R.string.csv_file_error_parsing;
                updateHandler.sendMessage(msg);
            }
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        /*if (id == R.id.nav_camera) {
            switchCompat = (SwitchCompat) navigationView.getMenu().findItem(R.id.nav_camera).getActionView().findViewById(R.id.toggle);
            switchCompat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(HomeActivity.this, "Works!", Toast.LENGTH_SHORT).show();
                }
            });
            // Handle the camera action
        }*/
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.connect_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {

        this.moveTaskToBack(true);

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.connect_drawer_layout);
////        if (drawer.isDrawerOpen(GravityCompat.START)) {
////            drawer.closeDrawer(GravityCompat.START);
////        } else if (mLayout != null &&
////                (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
////            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
////        } else {
////            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
////                getSupportFragmentManager().popBackStack();
////            } else if (!doubleBackToExitPressedOnce) {
////                this.doubleBackToExitPressedOnce = true;
////                Toast.makeText(this, "Please click BACK again to exit.", Toast.LENGTH_SHORT).show();
////
////                new Handler().postDelayed(new Runnable() {
////
////                    @Override
////                    public void run() {
////                        doubleBackToExitPressedOnce = false;
////                    }
////                }, 2000);
////            } else {
////                super.onBackPressed();
////            }
////        }
/*
        if(prefManager.getConnectionState() == VPN_CONNECTION_STATE.CONNECTED || prefManager.getConnectionState() == VPN_CONNECTION_STATE.CONNECTING){
//            moveTaskToBack(false);
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.disconnect_dialog_message)
                    .setTitle(R.string.exit_dialog_title);
            builder.setIcon(R.drawable.ic_exit_to_app_black_24dp);
            // Add the buttons
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    disconenctConnection(true);
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                    dialog.cancel();
                }
            });
            builder.create();
            if (!isFinishing())
                builder.show();
        }else {*/
           /* final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.exit_dialog_message)
                    .setTitle(R.string.exit_dialog_title);
            builder.setIcon(R.drawable.ic_exit_to_app_black_24dp);
            // Add the buttons
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ConnectActivity.super.onBackPressed();
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                    dialog.cancel();
                }
            });
            builder.create();
            if (!isFinishing())
                builder.show();*/
//        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        toggle.onConfigurationChanged(newConfig);
    }

    private void setEventListner() {
//        button_recommended =  findViewById(R.id.button_recommended);
//        button_country_list =  findViewById(R.id.button_country_list);
//        button_favorites =  findViewById(R.id.button_favorites);
        rechargeButton = findViewById(R.id.recharge_button);
//        validityText = findViewById(R.id.day_left_text);
        validityText = findViewById(R.id.validity_text_new);
        waitingExpiryDate = findViewById(R.id.waiting_expiry_date);
        connectedTimeMainChorono = findViewById(R.id.connected_time_main_chorono);
        textTimeUnit = findViewById(R.id.text_time_unit);
//        mLayout                 = findViewById(R.id.sliding_layout);
        viewPager = findViewById(R.id.viewPager);
        validity_layout = findViewById(R.id.validity_layout);

        statusFloatingActionButton = findViewById(R.id.status_floating_action_button);
        statusFloatingActionButton.setOnClickListener(this);

        rechargeButton.setOnClickListener(this);

        setPagerAdapter();

//        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
//            @Override
//            public void onPanelSlide(View panel, float slideOffset) {
//            }
//
//            @Override
//            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
//                if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
//                    imageDrawerStatusShowing.setImageDrawable(ConnectActivity.this.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));
//                } else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
//                    imageDrawerStatusShowing.setImageDrawable(ConnectActivity.this.getResources().getDrawable(R.drawable.ic_expand_less_black_24dp));
//                }
//            }
//        });
//        mLayout.setFadeOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//            }
//        });

//        button_recommended.setOnClickListener(this);
//        button_country_list.setOnClickListener(this);
//        button_favorites.setOnClickListener(this);
        validity_layout.setOnClickListener(this);
    }

    private void setEventListnerForDrawer() {
//        imageDrawerStatusShowing = findViewById(R.id.image_drawer_status_showing);
//        connectionStatusText = findViewById(R.id.connection_status_text);

        setLayout();
    }

    public void setPagerAdapter() {
        viewPager.setAdapter(adapter);
//        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                if (position == 0) {
//                    button_recommended.setChecked(true);
//                } else if (position == 1) {
//                    button_country_list.setChecked(true);
//                }
//                else if (position == 2) {
//                    button_favorites.setChecked(true);
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View view) {
//        if (view == button_recommended) {
//            viewPager.setCurrentItem(0);
//        } else if (view == button_country_list) {
//            viewPager.setCurrentItem(1);
//        }
//        else if (view == button_favorites) {
//            viewPager.setCurrentItem(2);
//        }
        if (view == quickConnect) {
            quickConnection();
        } else if (view == cancelConnection) {
            cancelConnection();
        } else if (view == disConnectConnection) {
            disconenctConnection(false);
        } else if (view == validity_layout) {
//            openValidityUpdationActivity();
        } else if (view == rechargeButton) {
//            openValidityUpdationActivity();
        } else if (view == statusFloatingActionButton) {
            openDialog();
//            showPopup(ConnectActivity.this);
        } else if (view == btnCancel) {
            if (dialog != null) {
                dialog.dismiss();
            }

//            if(popup != null)
//                popup.dismiss();
        } else if (view == info) {
            openInfoDialog();
        } else if (view == help) {
            openHelpDialog();
        } else if (view == infobtnCancel) {
            if (infoDialog != null)
                infoDialog.dismiss();
        } else if (view == helpbtnCancel) {
            if (helpDialog != null)
                helpDialog.dismiss();
        } else if (view == logout) {
            doLogoutProcess();
        }
    }

    private void openValidityUpdationActivity() {
//        Intent intent = new Intent(ConnectActivity.this, ValidityUpdationActivity.class);
//        startActivity(intent);
    }

    public class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int index) {

            switch (index) {
                case 0:
                    // Top Rated fragment activity
                    return new ServerListFragment();
//                case 1:
//                    // Games fragment activity
//                    return new CountriesListFragment();
//                case 2:
//                    // Movies fragment activity
//                    return new FavouritesServerFragment();
            }

            return null;
        }

        @Override
        public int getCount() {
            // get item count - equal to number of tabs
            return 1;
        }

    }


    public void switchContent(Fragment fragment, String tag) {
//        getSupportFragmentManager().beginTransaction().
//                remove(getSupportFragmentManager().findFragmentByTag(fragmentTag)).commit();
        fragmentTag = tag;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.connect_fragment_container, fragment, fragmentTag)
                .commitAllowingStateLoss();
    }

    public void connecting(String countryShort, int connectingFrom) {

        if (!isDemoApp) {
            if (dialog == null || !dialog.isShowing())
                openDialog();
        } else {
//            showConnectionStatusFragment();
        }

        this.connectingFrom = connectingFrom;
//            if (this.previouslyConnectedFrom == 0) {
//                ServerListFragment fragment = (ServerListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 0));
//                if (fragment != null)
//                    fragment.disconnectConnection();
//            } else if (this.previouslyConnectedFrom == 1) {
//                CountriesListFragment fragment = (CountriesListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 1));
//                if (fragment != null)
//                    fragment.disconnectConnection();
//            }
        VpnStatus.resetCurrentState();
        prefManager.setConnectionState(VPN_CONNECTION_STATE.CONNECTING);
        this.connectingFrom = connectingFrom;
        shortCountry = countryShort;
//        startCountDown();
        connectingViewRelatedWork(countryShort);
    }

    public void connected(String countryShort) {
        prefManager.setConnectionState(VPN_CONNECTION_STATE.CONNECTED);
        connectedViewRelatedWork(countryShort);
    }

    public void disConencted(boolean isCancel) {
        if (counterTimer != null) {
            counterTimer.cancel();
        }
        prefManager.setConnectionWithServer("");

        if (!isCancel)
            disconnectedViewRelatedWork();

    }

    public void setLayout() {

//        Log.d("SettingLayout","status ==="+prefManager.getConnectionState());

        if (!isDemoApp) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (prefManager.getConnectionState() == VPN_CONNECTION_STATE.NOT_CONNECTED) {
                        //Not connected
                        logger.info("ConnectActivity", "Layout Not connected");
                        if (connectedTimeMainChorono != null) {
                            connectedTimeMainChorono.stop();
                            connectedTimeMainChorono.setText("00:00");
                        }
//                    Drawable myFabSrc = getResources().getDrawable(R.drawable.ic_wifi);
//                    //copy it in a new one
//                    Drawable willBeWhite = myFabSrc.getConstantState().newDrawable();
//                    //set the color filter, you can use also Mode.SRC_ATOP
//                    willBeWhite.mutate().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY);
                        //set it to your fab button initialized before
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            statusFloatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_wifi, getTheme()));
                        } else {
                            statusFloatingActionButton.setImageResource(R.drawable.ic_wifi);
                        }
//                    statusFloatingActionButton.setImageDrawable(willBeWhite);

                        if (dialog != null) {
                            notConnectedLayout.setVisibility(View.VISIBLE);
                            connectingLayout.setVisibility(View.GONE);
                            connectedLayout.setVisibility(View.GONE);
                            if (sessionTimeText != null) {
                                sessionTimeText.stop();
                                sessionTimeText.setText("00:00");
                            }
                        }
                    } else if (prefManager.getConnectionState() == VPN_CONNECTION_STATE.CONNECTING) {
                        //Connecting
                        logger.info("ConnectActivity", "Layout connecting");

                        if (dialog != null) {
                            notConnectedLayout.setVisibility(View.GONE);
                            connectingLayout.setVisibility(View.VISIBLE);
                            connectedLayout.setVisibility(View.GONE);
                            connectionProgress.setProgress(progress);
                            connectingText.setTextColor(getResources().getColor(R.color.black));
                            connectingText.setText("Initializing connection ....");
                        }

                    } else if (prefManager.getConnectionState() == VPN_CONNECTION_STATE.CONNECTED) {
                        //Conencted
                        logger.info("ConnectActivity", "Layout connected");
                        if (android.os.Build.VERSION.SDK_INT >= 11) {
                            // will update the "progress" propriety of seekbar until it reaches progress
                            ObjectAnimator animation = ObjectAnimator.ofInt(connectionProgress, "progress", 100);
                            animation.setDuration(700); // 0.7 second
                            animation.setInterpolator(new DecelerateInterpolator());
                            animation.start();
                        } else
                            connectionProgress.setProgress(100);
                        progress = 0;

                        if (connectedTimeMainChorono != null)
                            connectedTimeMainChorono.start();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            statusFloatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_wifi_yellow, getTheme()));
                        } else {
                            statusFloatingActionButton.setImageResource(R.drawable.ic_wifi_yellow);
                        }
                        if (dialog != null) {
                            notConnectedLayout.setVisibility(View.GONE);
                            connectingLayout.setVisibility(View.GONE);
                            connectedLayout.setVisibility(View.VISIBLE);
                            serverNameText.setText(connectedServerName);
                            if (sessionTimeText != null) {
                                sessionTimeText.setText(connectedTimeMainChorono.getText());
                                sessionTimeText.start();
                            }
                        }
                    }
                }
            });
        } else {
            if (prefManager.getConnectionState() == VPN_CONNECTION_STATE.NOT_CONNECTED) {
                //Not connected
                logger.info("ConnectActivity", "Layout Not connected");
                if (connectedTimeMainChorono != null) {
                    connectedTimeMainChorono.stop();
                    connectedTimeMainChorono.setText("00:00");
                }
//                showHideLayout(1);
                if (sessionTimeText != null) {
                    sessionTimeText.stop();
                    sessionTimeText.setText("00:00");
                }

            } else if (prefManager.getConnectionState() == VPN_CONNECTION_STATE.CONNECTING) {
                logger.info("ConnectActivity", "Layout connecting");
//                connectionFragment.showConnectingLayout();

//                showHideLayout(2);

            } else if (prefManager.getConnectionState() == VPN_CONNECTION_STATE.CONNECTED) {
                logger.info("ConnectActivity", "Layout connected");
                showConnectionStatusFragment();
                connectionFragment.showconnectedLayout();

//                if (android.os.Build.VERSION.SDK_INT >= 11) {
//                    // will update the "progress" propriety of seekbar until it reaches progress
//                    ObjectAnimator animation = ObjectAnimator.ofInt(connectionProgress, "progress", 100);
//                    animation.setDuration(700); // 0.7 second
//                    animation.setInterpolator(new DecelerateInterpolator());
//                    animation.start();
//                } else
//                    connectionProgress.setProgress(100);
//                progress = 0;
//
//                if (connectedTimeMainChorono != null)
//                    connectedTimeMainChorono.start();
//
////                showHideLayout(3);
//                if (sessionTimeText != null) {
//                    sessionTimeText.setText(connectedTimeMainChorono.getText());
//                    sessionTimeText.start();
//                }
//                connectionProgress.setProgress(100);

            }
        }

    }

    public void quickConnection() {

        if (!prefManager.isValidityExpired() && !prefManager.isPasswordInvalid()) {
            if (!prefManager.isValidityExpired()) {
                ServerListFragment fragment = (ServerListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 0));
                if (fragment != null) {
                    fragment.startQuickConnection();
                    connectingFrom = 0;
                }
            } else {
                Toast.makeText(ConnectActivity.this, "Your validity expired", Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(ConnectActivity.this, ValidityUpdationActivity.class);
//                startActivity(intent);
            }
        } else {
            if (prefManager.isValidityExpired()) {
                Toast.makeText(ConnectActivity.this, "Your validity expired", Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(ConnectActivity.this, ValidityUpdationActivity.class);
//                startActivity(intent);
            } else if (prefManager.isPasswordInvalid()) {
                Toast.makeText(ConnectActivity.this, "Password has changed,please logout and login again with changed password", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(ConnectActivity.this, "Reattempt exceeded specified number", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void cancelConnection() {

        Fragment fragment = getSupportFragmentManager().findFragmentByTag("CF");
        if (fragment == null) {

            if (counterTimer != null)
                counterTimer.cancel();
//        if (connectingProgress.getVisibility() == View.VISIBLE)
//            connectingProgress.setVisibility(View.GONE);
//        connectionStatusText.setText("Not Connected");

//        cancelConnection.setEnabled(false);
//        cancelConnection.setAlpha(0.2f);
//        progress = 0;
//
//        connectingText.setText("Disconnecting ,please wait...");
//        connectingText.setTextColor(getResources().getColor(R.color.red));
//        connectionProgress.setProgress(progress);
//
//        startTimer();
        } else if (prefManager.getConnectionState() != VPN_CONNECTION_STATE.CONNECTING) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        disconenctConnection(true);


    }

    public void disconenctConnection(boolean isCancel) {
//        prefManager.setConnectionState(VPN_CONNECTION_STATE.NOT_CONNECTED);

        Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("CF");

        if (fragment1 != null && prefManager.getConnectionState() != VPN_CONNECTION_STATE.CONNECTING) {
            getSupportFragmentManager().beginTransaction().remove(fragment1).commitAllowingStateLoss();
            connectionFragment = null;
        }


        ServerListFragment fragment = (ServerListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 0));
        if (fragment != null) {
            fragment.disconnectConnection(false);
        }
        disConencted(isCancel);
//
//        if (previouslyConnectedFrom == 0) {
//            ServerListFragment fragment = (ServerListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 0));
//            if (fragment != null)
//                fragment.disconnectConnection();
//        } else if (previouslyConnectedFrom == 1) {
//            CountriesListFragment fragment = (CountriesListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 1));
//            if (fragment != null)
//                fragment.disconnectConnection();
//        } else {
//            ServerListFragment fragment = (ServerListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 0));
//            if (fragment != null)
//                fragment.disconnectConnection();
//            CountriesListFragment fragment1 = (CountriesListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 1));
//            if (fragment1 != null)
//                fragment1.disconnectConnection();
//            disConencted();
//        }


    }

    public void startCountDown() {

        int countDownInterval = 1000;
        int finishTime = 10;
        if (counterTimer == null) {
            counterTimer = new CountDownTimer(finishTime * 1000, countDownInterval) {
                public void onFinish() {
                    if (!isProvisiningDone && isNetworkAvailable())
                        sendProvisiningRequest();
                }

                public void onTick(long millisUntilFinished) {

                }
            };
            counterTimer.start();
        } else {
            counterTimer.cancel();
            counterTimer = new CountDownTimer(finishTime * 1000, countDownInterval) {
                public void onFinish() {
                    if (!isProvisiningDone && isNetworkAvailable())
                        sendProvisiningRequest();
                }

                public void onTick(long millisUntilFinished) {

                }
            };
            counterTimer.start();
        }
    }

    private void connectingViewRelatedWork(String countryShort) {
//        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
//        connectionStatusText.setText("Connecting to " + countryShort);
//        connectingText.setText("Initializing connection ....");
//        connectingProgress.setVisibility(View.VISIBLE);
        connectedServerName = countryShort;
        if (serverNameText != null)
            serverNameText.setText(countryShort);
        prefManager.setConnectionState(VPN_CONNECTION_STATE.CONNECTING);
        setLayout();
    }

    private void connectedViewRelatedWork(String countryShort) {
        previouslyConnectedFrom = connectingFrom;
//        logger.info("TESTINGG " +connectingFrom );
//        if (connectingFrom == 0) {
//            ServerListFragment fragment = (ServerListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 0));
//            if (fragment != null)
//                fragment.connectionConnected();
//        } else if (connectingFrom == 1) {
////            CountriesListFragment fragment =(CountriesListFragment) adapter.getItem(1);
//            CountriesListFragment fragment = (CountriesListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 1));
//            if (fragment != null)
//                fragment.connectionConnected();
//        }
//        if (counterTimer != null) {
//            counterTimer.cancel();
//        }
//        connectionStatusText.setText("Connected to " + countryShort);
        if (serverNameText != null)
            serverNameText.setText(countryShort);
//        connectingProgress.setVisibility(View.GONE);
        setLayout();
    }

    private void disconnectedViewRelatedWork() {
//        connectionStatusText.setText("Not Connected");
//        connectingProgress.setVisibility(View.GONE);
        setLayout();
    }

    private String getFragmentTag(int viewPagerId, int fragmentPosition) {
        return "android:switcher:" + viewPagerId + ":" + fragmentPosition;
    }

    private void changeServerStatus(final VpnStatus.ConnectionStatus status) {


        if (!isDemoApp) {
            if (prefManager.getConnectionState() == VPN_CONNECTION_STATE.CONNECTED || prefManager.getConnectionState() == VPN_CONNECTION_STATE.CONNECTING) {

//            Log.d("ConnectionStatus","Inside if case");
                //For Easy parentServer changing status
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        logger.info("CheckStatus", "Status ===" + status);

                        logger.info("CheckStatus", "vpn current state ===" + VpnStatus.currentState);
                        if (VpnStatus.currentState.equalsIgnoreCase("CONNECTING")) {
                            progress = 0;
                        } else if (VpnStatus.currentState.equalsIgnoreCase("TCP_CONNECT")) {
                            progress = 15;
                        } else if (VpnStatus.currentState.equalsIgnoreCase("WAIT")) {
                            progress = 30;
                        } else if (VpnStatus.currentState.equalsIgnoreCase("AUTH")) {
                            progress = 45;
                        } else if (VpnStatus.currentState.equalsIgnoreCase("GET_CONFIG")) {
                            progress = 60;
                        } else if (VpnStatus.currentState.equalsIgnoreCase("ASSIGN_IP")) {
                            progress = 75;
                        } else if (VpnStatus.currentState.equalsIgnoreCase("ADD_ROUTES")) {
                            progress = 90;
                        } else if (VpnStatus.currentState.equalsIgnoreCase("CONNECTED")) {
                            progress = 100;
                        } else if (VpnStatus.currentState.equalsIgnoreCase("INITIALIZING")) {


                        }

                        if (android.os.Build.VERSION.SDK_INT >= 11 && progress != 100) {
                            // will update the "progress" propriety of seekbar until it reaches progress
                            ObjectAnimator animation = ObjectAnimator.ofInt(connectionProgress, "progress", progress);
                            animation.setDuration(700); // 0.7 second
                            animation.setInterpolator(new DecelerateInterpolator());
                            animation.start();
                        } else if (progress != 100)
                            connectionProgress.setProgress(progress); // no animation on Gingerbread or lower

                        if (connectingText != null)
                            connectingText.setText("Jrk PLUS VPN " + VpnStatus.currentState + "...");
                        logger.info("ConnectActivity", "Vpn status===" + VpnStatus.currentState);
                        if (VpnStatus.currentState.equalsIgnoreCase("CONNECTED")) {
                            logger.info("ConnectActivity", "Changing to connected from status");
                            connected(shortCountry);
                        }
                    }
                });
            } else {
                Log.d("ConnectionStatus", "Inside else case");
                disconenctConnection(false);
            }
        } else {
            ServerListFragment fragment = (ServerListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 0));
            if (fragment != null) {
                fragment.connectionState(status);
            }
        }
    }

    private void receiveStatus(Intent intent) {

        if (VpnStatus.isVPNActive()) {
            changeServerStatus(VpnStatus.ConnectionStatus.valueOf(intent.getStringExtra("status")));
        }

    }

    public void registerTheBroadcastReceiver() {

        IntentFilter intentFilter = new IntentFilter(
                CONNECT_ACTIVITY_BROADCAST_ACTION);

        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

//                System.out.println("vpnconnection inBroadcast:-" + intent.getIntExtra(MessageConstatns.ID, -1));

                if (intent != null) {
                    switch (intent.getIntExtra(MessageConstatns.ID, -1)) {
                        case 1:
                            receiveStatus(intent);
                            break;
                        case 2:
                            getImeiNo();
                            startSendingPing(intent.getIntExtra(MessageConstatns.KEEP_ALIVE_TIMER, -1));
                            break;
                        case 3:
                            //change validity text
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    waitingExpiryDate.setVisibility(View.GONE);
                                    if (!prefManager.isValidityExpired()) {
                                        validityText.setText(prefManager.getDeactivationDate());
                                    } else {
                                        validityText.setText("Expired");
                                    }
                                }
                            });
                            break;
                        case 4:
                            receiveTraffic(intent);
                            break;
                        case 5:
                            gotCancelVpnRequest();
                            break;
                        case 6:
                            gotDisconnectionFromVPNService();
                            break;
                        default:
                            break;
                    }
                }
            }
        };
        //registering our receiver
        this.registerReceiver(mReceiver, intentFilter);
    }

    private void gotDisconnectionFromVPNService() {

        disconenctConnection(false);
    }

    private void startSendingPing(int retryAfter) {

        if (retryAfter != -1) {
            if (myTimer == null) {
                myTimer = new Timer();
                myTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        TimerMethod();
                    }

                }, 0, (retryAfter - 1) * 1000);
            } else {
                myTimer.cancel();
                myTimer = new Timer();
                myTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        TimerMethod();
                    }
                }, 0, (retryAfter - 1) * 1000);
            }
        }
    }

    private void TimerMethod() {

        Log.d("ConnectActivity", "SendingPing");
//        if(isNetworkAvailable()) {
//            AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {
//                @Override
//                protected Void doInBackground(Void... voids) {
//                    //send ping message after every specified interval
//                    PackageInfo pInfo = null;
//                    String version = "1.0.0";
//                    try {
//                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//                    } catch (PackageManager.NameNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                    if (pInfo != null) {
//                        version = pInfo.versionName;
//                    }
//                    JSONObject jsonObject = new JSONObject();
//                    try {
//                        jsonObject.put(MessageConstatns.IMEI, imeiNo);
//                        jsonObject.put(MessageConstatns.BUILD_NUMBER, Build.FINGERPRINT);
//                        jsonObject.put(MessageConstatns.PLATFORM, "android");
//                        jsonObject.put(MessageConstatns.CHECKSUM, getCheckSum());
//                        jsonObject.put(MessageConstatns.VERSION, version);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    new OkktpHttpRequest().sendRequest(HomeActivity.this, "https://dashboard.madeenavpn.net".concat("/pingRequest/" + prefManager.getMobileNumber()), jsonObject, HomeActivity.this);
//                    return null;
//                }
//            };
//            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        }
        if (isNetworkAvailable()) {
//            AsyncTask<Void, Void, Void> asyncTaskDomainFront = new AsyncTask<Void, Void, Void>() {
//                @Override
//                protected Void doInBackground(Void... voids) {
//                    try {
//                        PackageInfo pInfo = null;
//                        String version = "1.0.0";
//                        try {
//                            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//                        } catch (PackageManager.NameNotFoundException e) {
//                            e.printStackTrace();
//                        }
//                        if (pInfo != null) {
//                            version = pInfo.versionName;
//                        }
//                        String buildVersion = Build.FINGERPRINT;
//                        if (buildVersion.length() > 10) {
//                            buildVersion = buildVersion.substring(0, 10);
//                        }
//                        JSONObject jsonObject = new JSONObject();
//                        try {
//                            jsonObject.put(MessageConstatns.IMEI, imeiNo);
//                            jsonObject.put(MessageConstatns.BUILD_NUMBER, buildVersion);
//                            jsonObject.put(MessageConstatns.PLATFORM, "android");
//                            jsonObject.put(MessageConstatns.CHECKSUM, AppSignature.getCheckSum(ConnectActivity.this));
//                            jsonObject.put(MessageConstatns.VERSION, version);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                        CustomBase64 base64 = new CustomBase64("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_");
//                        Packet packet = new Packet(jsonObject.toString().getBytes(), 0, jsonObject.toString().length());
//                        Packet provPacket = new Packet(2048);
//                        base64.encode(packet, 0, packet.length(), provPacket);
//
////                    String message = jsonObject.toString();
////                    String encodedMessage = Base64.encodeToString(message.getBytes(), Base64.DEFAULT);
//
//                        String validation_message = "GET /pingRequest/" + prefManager.getMobileNumber() + "/" +
//                                provPacket.toString() +
//                                " HTTP/1.1\r\n" +
//                                "User-Agent: androidVPN\r\n" +
//                                "Accept-Language: en-US,en;q=0.5\r\n" +
//                                "Host: social-188310.appspot.com\r\n" +
//                                "Connection: Keep-Alive\r\n" +
//                                "Accept-Encoding: gzip\r\n" +
//                                "\r\n";
//
////                        String validation_message = "POST /pingRequest/" + prefManager.getMobileNumber() + " HTTP/1.1\r\n" +
////                                "User-Agent: androidVPN\r\n" +
////                                "Accept-Language: en-US,en;q=0.5\r\n" +
////                                "Content-Type: application/json; charset=utf-8\r\n" +
////                                "Content-Length: " +
////                                jsonObject.toString().length() +
////                                "\r\n" +
////                                "Host: social-188310.appspot.com\r\n" +
////                                "Connection: Keep-Alive\r\n" +
////                                "Accept-Encoding: gzip\r\n" +
////                                "\r\n" +
////                                jsonObject.toString();
//
//                        new OkktpHttpRequest().sendDomainFrontingRequest(ConnectActivity.this, validation_message.getBytes(),
//                                "www.facebook.com", ConnectActivity.this);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    return null;
//                }
//            };
//            asyncTaskDomainFront.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//
//            AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {
//                @Override
//                protected Void doInBackground(Void... voids) {
//                    try {
//                        TrustManager[] trustAllCerts = new TrustManager[]{
//                                new X509TrustManager() {
//                                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                                        return null;
//                                    }
//
//                                    public void checkClientTrusted(
//                                            java.security.cert.X509Certificate[] certs, String authType) {
//                                    }
//
//                                    public void checkServerTrusted(
//                                            java.security.cert.X509Certificate[] certs, String authType) {
//                                    }
//                                }
//                        };
//                        SSLSocket st = null;
//                        try {
//                            SSLContext ssl_context = SSLContext.getInstance("TLS");
//                            ssl_context.init(null, trustAllCerts, null);
//                            final SSLSocketFactory sf = ssl_context.getSocketFactory();
//                            st = (SSLSocket) sf.createSocket("dashboard.madeenavpn.net", 443);
//                            setSNIHost(sf, (SSLSocket) st, "www.facebook.com");
//                            ((SSLSocket) st).startHandshake();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                        InputStream inputStream = null;
//                        OutputStream outputStream = null;
//                        try {
//                            outputStream = st.getOutputStream();
//                            inputStream = st.getInputStream();
//                        } catch (Exception e) {
//
//                        }
//
//                        PackageInfo pInfo = null;
//                        String version = "1.0.0";
//                        try {
//                            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//                        } catch (PackageManager.NameNotFoundException e) {
//                            e.printStackTrace();
//                        }
//                        if (pInfo != null) {
//                            version = pInfo.versionName;
//                        }
//                        String buildVersion = Build.FINGERPRINT;
//                        if (buildVersion.length() > 10) {
//                            buildVersion = buildVersion.substring(0, 10);
//                        }
//                        JSONObject jsonObject = new JSONObject();
//                        try {
//                            jsonObject.put(MessageConstatns.IMEI, imeiNo);
//                            jsonObject.put(MessageConstatns.BUILD_NUMBER, buildVersion);
//                            jsonObject.put(MessageConstatns.PLATFORM, "android");
//                            jsonObject.put(MessageConstatns.CHECKSUM, AppSignature.getCheckSum(ConnectActivity.this));
//                            jsonObject.put(MessageConstatns.VERSION, version);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        String validation_message = "POST /pingRequest/" + prefManager.getMobileNumber() + " HTTP/1.1\r\n" +
//                                "User-Agent: androidVPN\r\n" +
//                                "Accept-Language: en-US,en;q=0.5\r\n" +
//                                "Content-Type: application/json\r\n" +
//                                "Content-Length: " +
//                                jsonObject.toString().length() +
//                                "\r\n" +
//                                "Host: dashboard.madeenavpn.net\r\n" +
//                                "Connection: Keep-Alive\r\n" +
//                                "Accept-Encoding: gzip\r\n" +
//                                "\r\n" +
//                                jsonObject.toString();
//
//                        try {
//                            logger.info("[PING] :: " + validation_message);
//                            outputStream.write(validation_message.getBytes());
//                            outputStream.flush();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//
//                        SimpleHttpServerConnection simpleHttpServerConnection = null;
//                        try {
//                            simpleHttpServerConnection = new SimpleHttpServerConnection(inputStream);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        String response = simpleHttpServerConnection.readResponse();
//                        logger.info("[PING] :: " + response);
//
//                        ((OkktpHttpRequest.OkktpHttpResponseEvents) ConnectActivity.this).okktpHttpResponse(response);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    return null;
//                }
//            };
//            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    private boolean isNetworkAvailable() {
        //checks whethe networ is available or not
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void responseReceived(boolean isError) {
        logger.debug("ConnectActivity", "Response of provising received in home activity");
    }

    @Override
    public void getErrorCode(int errorCode) {
        logger.info("Value of Error Code : " + errorCode);
        if (errorCode != 0) {

            logger.info("Client not initiliazed...Aborting.");
            String errorMessage = parseErrorCode(errorCode);
            if (errorCode == ConfigurationConstants.USERNAME_EXPIRED) {
                logger.debug("Expiry", "Setting Expiry true from user");
                prefManager.setIsValidityExpired(true);
                waitingExpiryDate.setVisibility(View.GONE);
                validityText.setText("Expired");
                gotValidityExpiredFromProvisining();
            }
            if (errorCode == ConfigurationConstants.PASSWORD_INVALID) {
                logger.debug("PAssword", "Invalid");
                prefManager.setPasswordInvalid(true);
                waitingExpiryDate.setVisibility(View.GONE);
                Toast.makeText(ConnectActivity.this, "Password has changed,please logout and login again with changed password", Toast.LENGTH_LONG).show();
            }
            if (errorCode != ConfigurationConstants.USER_NOT_FOUND && errorCode != ConfigurationConstants.INVALID_PASSWORD && errorCode != ConfigurationConstants.INVALID_MESSGAE_FORMAT && errorCode != ConfigurationConstants.MESSAGE_PARSING_FAILED
                    && errorCode != ConfigurationConstants.USERNAME_EXPIRED && errorCode != ConfigurationConstants.IMEI_VALIDATION_FALIED && errorCode != ConfigurationConstants.UNSUPPORTED_VESRION_UPDATE_APP && errorCode != ConfigurationConstants.PASSWORD_INVALID && errorCode != ConfigurationConstants.MAX_ALLOWED_DEVICES_NEGATIVE) {
                startCountDown();
            }
        } else {
            logger.debug("Expiry", "Setting expiry false from user expired");
            prefManager.setIsValidityExpired(false);
            savingUsernamePasswordToSharedPreference();
        }
    }

    private void savingUsernamePasswordToSharedPreference() {
        SharedPreferences preferencesUsernamePassword = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferencesUsernamePassword.edit();
        editor.putString("pref_key_vpn_username", prefManager.getUserName());
        editor.putString("pref_key_password", prefManager.getPassword());
        editor.apply();
    }

    @Override
    public void onTaskCompletion(int errorCode, SelectedServer selectedServer, Context ctx, CredentialCheck.RequestType requestType) {

        createWorkerAsyncTask(errorCode, selectedServer, ctx, requestType);
    }

    String parseErrorCode(int errorCode) {
        switch (errorCode) {
            case ConfigurationConstants.USER_NOT_FOUND:
                return "Invalid Credentials";
            case ConfigurationConstants.INVALID_PASSWORD:
                return "Invalid Credentials";
            case ConfigurationConstants.INVALID_MESSGAE_FORMAT:
                return "Sorry! Something went wrong.";
            case ConfigurationConstants.MESSAGE_PARSING_FAILED:
                return "Sorry! Something went wrong.";
            case ConfigurationConstants.USERNAME_EXPIRED:
                return "User Account Expired.";
            case ConfigurationConstants.IMEI_VALIDATION_FALIED:
                return "Account linked to another device.";
            case ConfigurationConstants.UNSUPPORTED_VESRION_UPDATE_APP:
                return "Unsupported Version. Please update app";
            case ConfigurationConstants.PASSWORD_INVALID:
                return "Invalid Credentials";
            case ConfigurationConstants.MAX_ALLOWED_DEVICES_NEGATIVE:
                return "Maximum allowed devices exceeded.";
            case ConfigurationConstants.USERID_NOT_FOUND:
                return "UserID Not Found";
            case ConfigurationConstants.DUPLICATE_REGISTRATION:
                return "Multiple Login Not Allowed !!";
            default:
                return "Sorry! Something went wrong.";
        }
    }

    @Override
    public void okktpHttpResponse(final String response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (response != null) {
                    String jsonFormattedString = response.replaceAll("\\\\", "");

                    String jsonStringFinal = jsonFormattedString.substring(1, jsonFormattedString.length() - 1);

                    logger.debug("HomeActivity", "Okkhttp Response =====" + jsonStringFinal);
                    final MessageParser messageParser = new MessageParser(jsonStringFinal);
                    if (!messageParser.error) {
                        if (messageParser.validityInfo > System.currentTimeMillis()) {
                            Date d = new Date(messageParser.validityInfo);
                            DateFormat f = new SimpleDateFormat("MMM d,yyyy");
                            String validTill = f.format(d);
                            prefManager.setDeactivationTimeStamp(messageParser.validityInfo);
                            prefManager.setDeactivationDate(validTill);
                            prefManager.setIsValidityExpired(false);
                            if (isProvisiningDone) {
                                waitingExpiryDate.setVisibility(View.GONE);
                                validityText.setText(validTill);
                            }
                        } else {
                            stopVpnWorking(messageParser, "Expired");
                        }
                    } else {
                        //Got error response in ping
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (messageParser.errorCode == 2010) {
                                    stopVpnWorking(messageParser, "Invalid IMEI");
                                    if (myTimer != null) {
                                        myTimer.cancel();
                                    }
                                    clearAndGetOut();
                                } else if (messageParser.errorCode == 2011) {
                                    stopVpnWorking(messageParser, "Invalid Mobile No");
                                    if (myTimer != null) {
                                        myTimer.cancel();
                                    }
                                    clearAndGetOut();
                                } else if (messageParser.errorCode == 2012) {
                                    stopVpnWorking(messageParser, "Duplicate Registration");
                                    if (myTimer != null) {
                                        myTimer.cancel();
                                    }
                                    clearAndGetOut();
                                } else if (messageParser.errorCode == 2014) {
                                    stopVpnWorking(messageParser, "CHECKSUM_VALIDATION_FAILED");
                                    if (myTimer != null) {
                                        myTimer.cancel();
                                    }
                                    clearAndGetOut();
                                } else if (messageParser.errorCode == 2015) {
                                    stopVpnWorking(messageParser, "USER_DEACTIVATED");
                                    if (myTimer != null) {
                                        myTimer.cancel();
                                    }
                                    clearAndGetOut();
                                } else if (messageParser.errorCode == 2016) {
                                    stopVpnWorking(messageParser, "USER_EXPIRED");
                                    if (myTimer != null) {
                                        myTimer.cancel();
                                    }
                                    clearAndGetOut();
                                } else {
                                    stopVpnWorking(messageParser, "Error Response");
//                                if(myTimer != null){
//                                    myTimer.cancel();
//                                }
//                                clearAndGetOut();5
                                }
                            }
                        });

                    }
                }
            }
        });
    }

    @Override
    public void okktpHttpErrorResponse() {

    }

    private void stopVpnWorking(MessageParser messageParser, String text) {
        Date d = new Date(messageParser.validityInfo);
        DateFormat f = new SimpleDateFormat("MMM d,yyyy");
        String validTill = f.format(d);
        prefManager.setDeactivationTimeStamp(messageParser.validityInfo);
        prefManager.setDeactivationDate(validTill);
        if (isProvisiningDone && !text.equals("Error Response")) {
            waitingExpiryDate.setVisibility(View.GONE);
            prefManager.setIsValidityExpired(true);
            validityText.setText(text);
        }
//        HomeFragment fragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag("home_fragment");
//        if(fragment != null)
//            fragment.disableUi();
    }

    private void clearAndGetOut() {
        prefManager.clearPreferenceData();
        Intent intent = new Intent(ConnectActivity.this, MainActivity.class);
        startActivity(intent);
        ConnectActivity.this.finish();
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        logger.info("onActivityResult:- ", requestCode);
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE) {
            TelephonyManager tm = (TelephonyManager) ConnectActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null)
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
           /* identifier = tm.getDeviceId();
            if (identifier == null || identifier.length() == 0)*/
//            identifier = Secure.getString(ConnectActivity.this.getContentResolver(), Secure.ANDROID_ID);

            identifier = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            getImeiNo();
            logger.info("identifier:- ", identifier, ",", imeiNo);
            credentialCheck = new CredentialCheck(ConnectActivity.this, prefManager.getUserName(), prefManager.getPassword(), identifier, CredentialCheck.RequestType.PROV, null);
            credentialCheck.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @SuppressLint("HardwareIds")
    private void getImeiNo() {

       /* if (ActivityCompat.checkSelfPermission(ConnectActivity.this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            System.out.println("IMEI:: 1");
            ActivityCompat.requestPermissions(ConnectActivity.this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 123);
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            System.out.println("IMEI::" + telephonyManager.getDeviceId());
            imeiNo = telephonyManager.getDeviceId();
        }*/
        imeiNo = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    @Override
    protected void onDestroy() {

        if (this.mReceiver != null)
            this.unregisterReceiver(this.mReceiver);
        if (this.networkReceiver != null) {
            this.unregisterReceiver(this.networkReceiver);
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (gotIlligalExceptionForremovingFragment && !prefManager.isValidityExpired()) {
            removeInitialisingFragment();
        } else if (prefManager.isValidityExpired()) {
            gotValidityExpiredFromProvisining();
        } else {

        }
    }

    private void removeInitialisingFragment() {
        try {
            if (prefManager.getDeactivationTimeStamp() > System.currentTimeMillis()) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag("IF");
                if (fragment != null)
                    getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
                if (gotIlligalExceptionForremovingFragment) {
                    gotIlligalExceptionForremovingFragment = false;
                    gotProvisiongResponse();
                }
                long x = getTimeDifference(prefManager.getDeactivationTimeStamp(), System.currentTimeMillis());
                donutProgress.setText(Long.toString(x));
                textTimeUnit.setText(expiryTypeText);
                if (expiryTypeText.equalsIgnoreCase("DAYS") && x >= 365)
                    donutProgress.setProgress(98);
                else if (expiryTypeText.equalsIgnoreCase("DAYS") && (x < 365 && x >= 150))
                    donutProgress.setProgress(70);
                else if (expiryTypeText.equalsIgnoreCase("DAYS") && (x < 150 && x >= 50))
                    donutProgress.setProgress(50);
                else if (expiryTypeText.equalsIgnoreCase("DAYS") && (x < 50 && x >= 3)) {
                    donutProgress.setFinishedStrokeColor(getResources().getColor(R.color.green));
                    donutProgress.setProgress(50);
                } else if (expiryTypeText.equalsIgnoreCase("DAYS") && (x < 3 && x > 1)) {
                    donutProgress.setFinishedStrokeColor(getResources().getColor(R.color.light_orange));
                    donutProgress.setProgress(15);
                } else {
                    donutProgress.setFinishedStrokeColor(getResources().getColor(R.color.red));
                    donutProgress.setProgress(3);
                }
            } else {
                donutProgress.setText("0");
                textTimeUnit.setText(expiryTypeText);
                donutProgress.setProgress(0);

            }
        } catch (IllegalStateException e) {
            gotIlligalExceptionForremovingFragment = true;
        } catch (Exception e) {
            gotIlligalExceptionForremovingFragment = true;
        }
    }

//    private void setEventListner(NavigationView navigationView) {
//        validity_layout = findViewById(R.id.validity_layout);
//        appVersion = findViewById(R.id.appVersion);
//        validityText = findViewById(R.id.day_left_text);
//        waitingExpiryDate = findViewById(R.id.waiting_expiry_date);
//        View header = navigationView.getHeaderView(0);
//        userId = (TextView) header.findViewById(R.id.userId);
//        userId.setText(prefManager.getMobileNumber());
//        validity_layout.setOnClickListener(this);
//        PackageInfo pInfo = null;
//        try {
//            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        if (pInfo != null) {
//            String version = pInfo.versionName;
//            appVersion.setText(version);
//        }
//    }

    private void gotProvisiongResponse() {
        ServerListFragment fragment = (ServerListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 0));
        if (fragment != null) {
            Log.e("ServerList", "Got provisining");
            fragment.gotProvisiongResponse();
        }
    }

    private void gotValidityExpiredFromProvisining() {
        InitialisingFragment fragment = (InitialisingFragment) getSupportFragmentManager().findFragmentByTag("IF");
        if (fragment != null) {
            Log.e("CountryList", "Got provisining");
            fragment.gotValidationExpired();
        }
        if (prefManager.getDeactivationTimeStamp() > System.currentTimeMillis()) {
            long x = getTimeDifference(prefManager.getDeactivationTimeStamp(), System.currentTimeMillis());
            donutProgress.setText(Long.toString(x));
            textTimeUnit.setText(expiryTypeText);
            if (expiryTypeText.equalsIgnoreCase("DAYS") && x >= 365)
                donutProgress.setProgress(98);
            else if (expiryTypeText.equalsIgnoreCase("DAYS") && (x < 365 && x >= 150))
                donutProgress.setProgress(70);
            else if (expiryTypeText.equalsIgnoreCase("DAYS") && (x < 150 && x >= 50))
                donutProgress.setProgress(50);
            else if (expiryTypeText.equalsIgnoreCase("DAYS") && (x < 50 && x >= 10)) {
                donutProgress.setFinishedStrokeColor(getResources().getColor(R.color.orange));
                donutProgress.setProgress(50);
            } else if (expiryTypeText.equalsIgnoreCase("DAYS") && (x < 10 && x > 1)) {
                donutProgress.setFinishedStrokeColor(getResources().getColor(R.color.red));
                donutProgress.setProgress(15);
            } else {
                donutProgress.setFinishedStrokeColor(getResources().getColor(R.color.red));
                donutProgress.setProgress(3);
            }
        } else {
            donutProgress.setText("0");
            textTimeUnit.setText(expiryTypeText);
            donutProgress.setProgress(0);
        }
    }

    private void receiveTraffic(Intent intent) {
//        System.out.println("vpnconnection"+ "receiving traffic ===" + VpnStatus.isVPNActive());
        if (VpnStatus.isVPNActive()) {
            String in = "";
            String out = "";
            if (firstData) {
                firstData = false;
            } else {
                in = intent.getStringExtra(TotalTraffic.DOWNLOAD_SESSION);
                out = intent.getStringExtra(TotalTraffic.UPLOAD_SESSION);
            }

            String inTotal = intent.getStringExtra(TotalTraffic.DOWNLOAD_ALL);

            String outTotal = intent.getStringExtra(TotalTraffic.UPLOAD_ALL);

            if (downloadSpeedText != null && inTotal != null)
                downloadSpeedText.setText(inTotal);
            if (uploadSpeedText != null && outTotal != null)
                uploadSpeedText.setText(outTotal);
            if (downloadVolumeText != null && in != null)
                downloadVolumeText.setText(in);
            if (uploadVolumeText != null && out != null)
                uploadVolumeText.setText(out);

            if (connectionFragment != null) {
                connectionFragment.receiveTraffic(inTotal, outTotal, in, out);
            }
        }
    }


    private void gotCancelVpnRequest() {
        Toast.makeText(ConnectActivity.this, "VPN permission cancelled.", Toast.LENGTH_LONG).show();
        disconenctConnection(false);
    }

    private void serverDownloadedSuccessfully() {
        CountriesListFragment fragment = (CountriesListFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(viewPager.getId(), 1));
        if (fragment != null) {
            Log.e("CountryList", "Got provisining");
            fragment.serverDownloaded();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        logger.info("ConnectActivity", "OnPause");
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        //This class is used to analyse the network connection changing condition
        Context mContext;

        @Override
        public void onReceive(final Context context, final Intent intent) {
            this.mContext = context;
            int status = NetworkUtil.getConnectivityStatus(context);

            if (status == NetworkUtil.TYPE_NOT_CONNECTED) {
                Log.e("ConnectActivity", "Not Connected");
                if (networkStatus != status || networkStatus == -1) {
                    networkStatus = status;
                } else {
                    networkStatus = status;
                }
            } else if (status == NetworkUtil.TYPE_WIFI) {
                Log.e("ConnectActivity", "Connected to wifi");
                if (!isProvisiningDone) {
                    sendProvisiningRequest();
                }
                if (networkStatus == status || networkStatus == -1) {
                    networkStatus = status;
                } else {
                    networkStatus = status;
                    //call to refresh list
                    gotProvisiongResponse();
                }
            } else if (status == NetworkUtil.TYPE_MOBILE) {

                if (!isProvisiningDone) {

                    sendProvisiningRequest();
                }
                if (networkStatus == status || networkStatus == -1) {
                    networkStatus = status;
                } else {
                    networkStatus = status;
                    //call to refresh list
                    gotProvisiongResponse();
                }
            }
        }
    }


    public void openDialog() {
        dialog = new Dialog(ConnectActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setContentView(R.layout.layout_dialog);
        dialog.show();
        setDialogEventListner(dialog);


    }

    public void setDialogEventListner(Dialog dialog) {

        btnCancel = dialog.findViewById(R.id.btnCancel);
        notConnectedLayout = dialog.findViewById(R.id.not_connected_layout);
        connectingLayout = dialog.findViewById(R.id.connecting_layout);
        connectedLayout = dialog.findViewById(R.id.connected_layout);
//        connectingProgress = findViewById(R.id.connecting_progress);
        quickConnect = dialog.findViewById(R.id.quick_connect);
        cancelConnection = dialog.findViewById(R.id.cancel_connection);
        disConnectConnection = dialog.findViewById(R.id.disconnect_connection);
        connectingText = dialog.findViewById(R.id.connecting_text);
        downloadVolumeText = dialog.findViewById(R.id.download_volume_text);
        uploadVolumeText = dialog.findViewById(R.id.upload_volume_text);
        downloadSpeedText = dialog.findViewById(R.id.download_speed_text);
        uploadSpeedText = dialog.findViewById(R.id.upload_speed_text);
        sessionTimeText = dialog.findViewById(R.id.session_time_text);
        connectionProgress = dialog.findViewById(R.id.connection_progress);
        connectionProgress.setFinishedStrokeColor(getResources().getColor(R.color.madeena_text_color));
        connectionProgress.setUnfinishedStrokeColor(getResources().getColor(R.color.expiry_view_text));
        connectionProgress.setBottomText("Connection");
        connectionProgress.setTextColor(getResources().getColor(R.color.madeena_text_color));

        serverNameText = dialog.findViewById(R.id.server_name_text);
        serverNameText.setSelected(true);
        quickConnect.setOnClickListener(ConnectActivity.this);


        cancelConnection.setOnClickListener(this);
        disConnectConnection.setOnClickListener(this);

        btnCancel.setOnClickListener(this);

//        progressDialog1 = new ProgressDialog(dialog.getContext());
//        progressDialog1.setProgressStyle(R.style.Theme_MyDialog);
//        progressDialog1.setMessage("Disconnecting...");
//        progressDialog1.setCancelable(false);

        setLayout();
    }


    // The method that displays the popup.
    private void showPopup(final AppCompatActivity context) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
//        int width = displayMetrics.widthPixels;

//        int screenHeight = getScreenHeightInDPs(context);
        int popupWidth, popupHeight;
        popupHeight = displayMetrics.heightPixels - 250;
        popupWidth = displayMetrics.widthPixels - 30;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            popupWidth = screenHeight;
//            popupHeight = screenHeight;
//        } else {
//            popupWidth = screenHeight / 2;
//            popupHeight = screenHeight / 2;
//        }
        // Inflate the popup_layout.xml
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.connection_status_layout, viewGroup);
        setPopupEventListner(layout);
        // Creating the PopupWindow
        popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);
        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 30;
        int OFFSET_Y = 30;
        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());
        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.CENTER, 0, 110);
    }

    private void setPopupEventListner(View dialog) {

        btnCancel = dialog.findViewById(R.id.btnCancel);
        notConnectedLayout = dialog.findViewById(R.id.not_connected_layout);
        connectingLayout = dialog.findViewById(R.id.connecting_layout);
        connectedLayout = dialog.findViewById(R.id.connected_layout);
//        connectingProgress = findViewById(R.id.connecting_progress);
        quickConnect = dialog.findViewById(R.id.quick_connect);
        cancelConnection = dialog.findViewById(R.id.cancel_connection);
        disConnectConnection = dialog.findViewById(R.id.disconnect_connection);
        connectingText = dialog.findViewById(R.id.connecting_text);
        downloadVolumeText = dialog.findViewById(R.id.download_volume_text);
        uploadVolumeText = dialog.findViewById(R.id.upload_volume_text);
        downloadSpeedText = dialog.findViewById(R.id.download_speed_text);
        uploadSpeedText = dialog.findViewById(R.id.upload_speed_text);
        sessionTimeText = dialog.findViewById(R.id.session_time_text);
//        serverNameText = dialog.findViewById(R.id.server_name_text);

        quickConnect.setOnClickListener(this);
        cancelConnection.setOnClickListener(this);
        disConnectConnection.setOnClickListener(this);

        btnCancel.setOnClickListener(this);
        setLayout();
    }

    private void openInfoDialog() {
        infoDialog = new Dialog(ConnectActivity.this);
        infoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        infoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        infoDialog.getWindow().setContentView(R.layout.info_dialog_layout);
        TextView userIdText = infoDialog.findViewById(R.id.user_id_text);
        userIdText.setText(prefManager.getUserName());
        final TextView passwordText = infoDialog.findViewById(R.id.password_text);
        final ImageView passwordShowHideButton = infoDialog.findViewById(R.id.password_show_hide_button);
        passwordShowHideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (passwordText.getText().equals("*******")) {
                    passwordText.setText(prefManager.getPassword());
                    passwordShowHideButton.setImageResource(R.drawable.ic_visibility_black_24dp);

                } else {
                    passwordText.setText("*******");
                    passwordShowHideButton.setImageResource(R.drawable.ic_visibility_off);
                }
            }
        });
        infobtnCancel = infoDialog.findViewById(R.id.infobtnCancel);
        infobtnCancel.setOnClickListener(this);
        infoDialog.show();


    }

    private void openHelpDialog() {

        helpDialog = new Dialog(ConnectActivity.this);
        helpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        helpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        helpDialog.getWindow().setContentView(R.layout.help_dialog_layout);
        helpbtnCancel = helpDialog.findViewById(R.id.helpbtnCancel);
        TextView textViewVersion = helpDialog.findViewById(R.id.tv_version);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            textViewVersion.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            helpDialog.findViewById(R.id.version).setVisibility(View.GONE);
//            e.printStackTrace();
        }

//        textViewVersion.setText(BuildConfig.VERSION_NAME);
        helpbtnCancel.setOnClickListener(this);
        helpDialog.show();
    }

    private void openSettingDialog() {

    }

    public long getTimeDifference(long earlierTimeStamp, long laterTimeStamp) {
        //Date1
        Calendar cal1 = Calendar.getInstance();
        cal1.set(1995, 04, 3, 5, 0, 0);
        Date date1 = cal1.getTime();

        //Date2
        Calendar cal2 = Calendar.getInstance();
        cal2.set(2011, 07, 4, 6, 1, 1);
        Date date2 = cal2.getTime();


        //Time Difference Calculations Begin
        long milliSec1 = earlierTimeStamp;
        long milliSec2 = laterTimeStamp;

        long timeDifInMilliSec;
        if (milliSec1 >= milliSec2) {
            timeDifInMilliSec = milliSec1 - milliSec2;
        } else {
            timeDifInMilliSec = milliSec2 - milliSec1;
        }

        long timeDifSeconds = timeDifInMilliSec / 1000;
        long timeDifMinutes = timeDifInMilliSec / (60 * 1000);
        long timeDifHours = timeDifInMilliSec / (60 * 60 * 1000);
        long timeDifDays = timeDifInMilliSec / (24 * 60 * 60 * 1000);

        System.out.println("Time differences expressed in various units are given below");
        System.out.println(timeDifInMilliSec + " Milliseconds");
        System.out.println(timeDifSeconds + " Seconds");
        System.out.println(timeDifMinutes + " Minutes");
        System.out.println(timeDifHours + " Hours");
        System.out.println(timeDifDays + " Days");
        if (timeDifDays > 0) {
            expiryTypeText = "DAYS";
            return timeDifDays;
        } else if (timeDifHours > 0) {
            expiryTypeText = "HRS";
            return timeDifHours;
        } else {
            expiryTypeText = "MIN";
            return timeDifMinutes;
        }
    }


    private void doLogoutProcess() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.logout_dialog)
                .setTitle(R.string.logout_dialog_title);
        builder.setIcon(R.drawable.ic_exit_to_app_black_24dp);
        // Add the buttons
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startLogoutProcess();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.cancel();
            }
        });
        builder.create();
        if (!isFinishing())
            builder.show();

    }

    private void startLogoutProcess() {

        if (prefManager.getConnectionState() == VPN_CONNECTION_STATE.CONNECTED || prefManager.getConnectionState() == VPN_CONNECTION_STATE.CONNECTING) {
            cancelConnection();
            logoutWork();
        } else {
            logoutWork();
        }
    }

    private void logoutWork() {

//        prefManager.clearPreferenceData();
//        Intent intent = new Intent(ConnectActivity.this,MainActivity.class);
//        startActivity(intent);
//        ConnectActivity.this.finish();


        prefManager.setIsLogin(false);
        prefManager.setIsLogoutDone(true);
        prefManager.setPasswordInvalid(false);
        Intent intent = new Intent(ConnectActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
        startActivity(intent);
        ConnectActivity.this.finish();
    }


    public void startTimer() {

        Timer buttonTimer = new Timer();
        buttonTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        cancelConnection.setEnabled(true);
                        cancelConnection.setAlpha(1.0f);
                        progress = 0;

//                        connectingText.setText("Initializing connection ....");
                        connectingText.setTextColor(getResources().getColor(R.color.black));
                        connectionProgress.setProgress(progress);

                        disconnectedViewRelatedWork();
                    }
                });
            }
        }, 2000);
    }

    private void showConnectionStatusFragment() {
        logger.info("TESTING ", "reachedatConnetion ");
        setToolBarVisibility(false);
        connectionFragment = new ConnectionFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, connectionFragment, "CF");
        fragmentTransaction.commitAllowingStateLoss();

    }

}
