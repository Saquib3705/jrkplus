package com.jrk_plus.TransportLayer;

import com.jrk_plus.Tunnel.ConfigurationConstants;

/**
 * Created by bot on 15/01/2018.
 */

public class SendPacketRequest implements SendRequestLayerInterface {

    @Override
    public String createPacket(int protocol) {
        switch (protocol){
            case ConfigurationConstants.PROTO_TCP :
                return tcpPacket();
            case ConfigurationConstants.PROTO_UDP :
                return udpPacket();
            case ConfigurationConstants.PROTO_SSL :
                return sslPacket();
            case ConfigurationConstants.PROTO_HTTP :
                return httpPacket();
            default:
                return null;
        }
    }

    @Override
    public void sendPacket(String ip, int port, String packet, int protocol) {
        switch (protocol){
            case ConfigurationConstants.PROTO_TCP :
                sendPacketOnTCP(ip,port,packet,protocol);
                break;
            case ConfigurationConstants.PROTO_UDP :
                sendPacketOnUDP(ip,port,packet,protocol);
                break;
            case ConfigurationConstants.PROTO_SSL :
                sendPacketOnSSL(ip,port,packet,protocol);
                break;
            case ConfigurationConstants.PROTO_HTTP :
                sendPacketOnHTTP(ip,port,packet,protocol);
                break;
            default:
                break;
        }
    }

    private String tcpPacket(){
        //Create tcp packet here
        return "xy";
    }

    private String udpPacket(){
        //Create tcp packet here
        return "xy";
    }
    private String sslPacket(){
        //Create tcp packet here
        return "xy";
    }
    private String httpPacket(){
        //Create tcp packet here
        return "xy";
    }
    private void sendPacketOnTCP(String ip, int port, String packet, int protocol){
        //Do code for packet sending on tcp
    }

    private void sendPacketOnUDP(String ip, int port, String packet, int protocol){
        //Do code for packet sending on tcp
    }

    private void sendPacketOnSSL(String ip, int port, String packet, int protocol){
        //Do code for packet sending on tcp
    }

    private void sendPacketOnHTTP(String ip, int port, String packet, int protocol){
        //Do code for packet sending on tcp
    }
}
