package jrk_plus.hu.blint.ssldroid;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.util.List;


import jrk_plus.hu.blint.ssldroid.db.SSLDroidDbAdapter;

import static opt.log.OmLogger.logger;

public class NetworkChangeReceiver extends BroadcastReceiver {

    public static Intent createExplicitFromImplicitIntent(Context context, Intent implicitIntent) {
        //Retrieve all services that can match the given intent
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfo = pm.queryIntentServices(implicitIntent, 0);

        //Make sure only one match was found
        if (resolveInfo == null || resolveInfo.size() != 1) {
            return null;
        }

        //Get component info and create ComponentName
        ResolveInfo serviceInfo = resolveInfo.get(0);
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);

        //Create a new intent. Use the old one for extras and such reuse
        Intent explicitIntent = new Intent(implicitIntent);

        //Set the component to be explicit
        explicitIntent.setComponent(component);

        return explicitIntent;
    }

    private boolean isStopped(Context context){
	Boolean stopped = false;
	SSLDroidDbAdapter dbHelper;
	dbHelper = new SSLDroidDbAdapter(context);
        dbHelper.open();
        Cursor cursor = dbHelper.getStopStatus();

        int tunnelcount = cursor.getCount();
         logger.info("SSLDroid", "Tunnelcount: "+tunnelcount);
        
        //don't start if the stop status field is available
        if (tunnelcount != 0){
            stopped = true;
        }
        
        cursor.close();
        dbHelper.close();
        
	return stopped;
    }
    
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        if ( activeNetInfo == null ) {
            Intent i = new Intent();
            i = createExplicitFromImplicitIntent(context, i);
            intent.setAction("alMadeena.hu.blint.ssldroid.SSLDroid");
            context.stopService(intent);
            return;
        }
         logger.info("SSLDroid", activeNetInfo.toString());
        if (activeNetInfo.isAvailable()) {
            Intent i = new Intent();
            i = createExplicitFromImplicitIntent(context, i);
            intent.setAction("alMadeena.hu.blint.ssldroid.SSLDroid");
            context.stopService(intent);
            if (!isStopped(context))
        	context.startService(intent);
            else
        	Log.w("SSLDroid", "Not starting service as directed by explicit stop");
        }
    }
}

