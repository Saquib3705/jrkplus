package com.jrk_plus.Provisioning;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.jrk_plus.Tunnel.ConfigurationConstants;
import com.jrk_plus.Tunnel.ConfigurationConstants.MessageType;
import com.jrk_plus.Tunnel.PacketStructure;
import com.jrk_plus.utilities.MessageConstatns;
import com.jrk_plus.utilities.PrefManager;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static jrk_plus.hu.blint.ssldroid.TcpProxyServerThread.isFirstPacket;
import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 5/4/2017.
 */

public class Information {
    public static int clientId = 0;
    public static int provisioningDone = 0;

    private static final int WIDTH = 20;

    public static String printHexDump(byte[] bytes, int len) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++) {
                if (i % WIDTH == 0) {
                    sb.append("\n");
                }
                sb.append(String.format("%02x ", bytes[i] & 0xff));
            }
            return sb.toString();
        } catch (Exception e) {
            //logger.error("Ex[printHexDump]: ", e);
        }
        return "failed to print hexdump";
    }

    public static byte[] addHeader(byte[] info, String ip, int port, int protocol, int action) {
        try {
            logger.info("ng", "Stuffing SIP Proxy Header");
            logger.info("ng", "Original Length : " + info.length);
            logger.info("ng", "IsFirstPacket : " + isFirstPacket);
            byte[] data = null;
            int i = 0;

            int len = 0;
            len = info.length + 8;
            data = new byte[len];
            String[] labels = ip.split("\\.");

            for (int j = 0; j < labels.length; j++)
                data[i++] = (byte) Integer.parseInt(labels[j]);

            data[i++] = (byte) ((port >> 8) & 0xFF);
            data[i++] = (byte) (port & 0xFF);

            data[i++] = (byte) (protocol & 0xFF);
            data[i++] = (byte) (action & 0xFF);
//            isFirstPacket = false;
//            }

            logger.info("Value of i : " + i);
            if (action == 3)
                logger.info("ng", "ACTION_FORWARD");
            System.arraycopy(info, 0, data, i, info.length);
            logger.info("ng", "Length of data copied: " + info.length + " bytes");
            logger.info("ng", "Final Length : " + data.length);
            return data;
        } catch (Exception e) {
            logger.info("ng", "Add Header Exception:" + Log.getStackTraceString(e));
        }
        return null;
    }

    public static byte[] prepareProvInfo(Context context, String username, String password, String imei) {

        PrefManager prefManager = new PrefManager(context);
        PackageInfo pInfo = null;
        String version = "1.0.0";
        String os_version = Build.VERSION.RELEASE;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (pInfo != null) {
            version = pInfo.versionName;
        }

        JSONObject obj = new JSONObject();
        logger.info("Information", "Mobile No ====" + username);
        Log.d("Information", "IMEI sending ===" + imei);
        obj.put(MessageConstatns.MOBILE_NUMBER, username);
        obj.put(MessageConstatns.IMEI, imei);
        // logger.info("Information"," ======"+ new String(Base64.decode(prefManager.getKey(), Base64.DEFAULT)));
        logger.info("Inforation", "Password ====" + password);

        obj.put(MessageConstatns.PASSWORD, password);
        obj.put(MessageConstatns.PLATFORM, "android");
        obj.put(MessageConstatns.COUNTRY, "IN");
        obj.put(MessageConstatns.VERSION, version);
        obj.put(MessageConstatns.SERVER_VERSION, "1");
        obj.put(MessageConstatns.ISP, "Etisalat");
        obj.put(MessageConstatns.IS_MODE_ENABLED, "1");
        obj.put("app_version", "v1.1");
        obj.put("os_version", os_version);
        obj.put("PlatFormType", "0");

        String buildVersion = Build.FINGERPRINT;
        if (buildVersion.length() > 10) {
            buildVersion = buildVersion.substring(0, 10);
        }
        obj.put(MessageConstatns.BUILD_NUMBER, buildVersion);
        StringWriter out = new StringWriter();
        try {
            obj.writeJSONString(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String jsonText = out.toString();
        byte[] message = null;
        try {
            message = jsonText.getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        logger.info("[JSON TEXT] :: " + jsonText);
        byte[] bs2WrapperBuff = new byte[2048];

        PacktUtil.prepareMessage(bs2WrapperBuff, MessageConstatns.CONFIGURATION_REQUEST);
        PacktUtil.addAttr(Constants.ATTR_TYPE_TRANSACTION_ID, (System.currentTimeMillis() + "").getBytes(), bs2WrapperBuff);
        PacktUtil.addAttr(Constants.ATTR_TYPE_DIALER_IP, "180.10.11.12".getBytes(), bs2WrapperBuff);
        PacktUtil.addAttr(Constants.ATTR_TYPE_BS1_PACKET, message, bs2WrapperBuff);
        int len = PacktUtil.twoByteToInt(bs2WrapperBuff, 2);
        //encrypt the packet
        byte[] encryptedPacket = PacktUtil.encrypt(bs2WrapperBuff, len);
        System.out.println("message : " + message.length);
        logger.info("[HEX-DUMP - Packet Length] : " + encryptedPacket.length);
        logger.info("[HEX-DUMP] : " + printHexDump(encryptedPacket, encryptedPacket.length));
        return encryptedPacket;
    }

    public static byte[] createProvInfo(Context context, MessageType msgType, String username, String password, String imei) {
        byte[] info = new byte[65535];
        PacketStructure packet_structure = new PacketStructure();
        int packetLen = 0;
        clientId = 0;
        if (msgType == MessageType.PROV) {
            packet_structure.prepareMessage(info, ConfigurationConstants.PROV);
            packet_structure.addAttribute(ConfigurationConstants.CLIENT_IDENTIFIER, clientId, info);
            byte[] encrypted_message = prepareProvInfo(context, username, password, imei);
            packet_structure.addAttribute(ConfigurationConstants.DATA_ATTRIBUTE, encrypted_message, info);
        } else if (msgType == MessageType.SETTINGS_UPDATE) {
            packet_structure.prepareMessage(info, ConfigurationConstants.SETTINGS_UPDATE);
            packet_structure.addAttribute(ConfigurationConstants.CLIENT_IDENTIFIER, clientId, info);
            packet_structure.addAttribute(ConfigurationConstants.SERVER_CHILD_COUNT, 1, info);
            packet_structure.addAttribute(ConfigurationConstants.SERVER_MPPC, 20000, info);
        }
        packetLen = packet_structure.twoByteToInt(info, 2);
        return Arrays.copyOfRange(info, 0, packetLen);
    }

    public static byte[] createRegistrarInfo(MessageType msgType, String registrarMessage) {
        byte[] info = new byte[65535];
        PacketStructure packet_structure = new PacketStructure();
        int packetLen = 0;
//        clientId = 0;
        if (msgType == MessageType.REGISTRAR) {
            packet_structure.prepareMessage(info, ConfigurationConstants.REGISTRAR_MESSAGE);
            packet_structure.addAttribute(ConfigurationConstants.CLIENT_IDENTIFIER, clientId, info);
            packet_structure.addAttribute(ConfigurationConstants.DATA_ATTRIBUTE, registrarMessage.getBytes(), info);
        }
        packetLen = packet_structure.twoByteToInt(info, 2);
        return Arrays.copyOfRange(info, 0, packetLen);
    }

    public static ServerConfig newParsingProvInfo(String info, Context context) {
//        NEW----->
//        {"DemoActivationServerProfile":"1","DemoActivationDays":"1","MaxRetryAttempts":"5","KeepAliveTimer":"50","ProxyCount":"2",
//                "serverdetails":[{"snilist":"ww.ee.ee,yy;","serverport":1,"servername":"ss","serverip":"1.2.3.4","serverprimaryip":"1.2.3.4","serverprotocol":1},
//            {"snilist":"qqq.i.iu,kk;","serverport":12,"servername":"qwertyu","serverip":"www.india.com","serverprimaryip":"16909060","serverprotocol":0},
//            {"snilist":"ww.ee.ee,yy;","serverport":1111,"servername":"aaaa","serverip":"aaa.a.aa","serverprimaryip":"1.2.3.4","serverprotocol":0}]}
//
//
//
//
//        OLD----->
//
//        {"snilist":"www.twitter.com,du;www.twitter.com,etisalat;mail.google.com,wifi;","backupdomainnames":"www.mypcadda.com;",
//                "serverdetails":[{"servername":"Server_Direct","serverip":1170576042,"serverprimaryip":1170576042,"serverportandprotocol":"1194,3;"},
//            {"servername":"SERVER_DU","serverip":1486283491,"serverprimaryip":1170576042,"serverportandprotocol":"4434,1;"},
//            {"servername":"Server_Etisalat_1","serverip":1486283491,"serverprimaryip":1170576042,"serverportandprotocol":"223,2;"},
//            {"servername":"Server_Etisalat","serverip":1486283491,"serverprimaryip":1170576042,"serverportandprotocol":"223,2;"}]}
//
//

        ServerConfig serverConfig = new ServerConfig();

        if (info == null) {
            logger.info("ng", "Response is NULL");
            serverConfig.errorCode = -1;
            return serverConfig;
        }
//        serverConfig = new ServerConfig();
        JSONArray serverDetails = null;
        PrefManager prefManager = new PrefManager(context);

        logger.info("Information", "Server Detail ====" + serverDetails);
        try {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(info);
            JSONObject jsonObject = (JSONObject) obj;

            String errorCode = (String) jsonObject.get("Error");
            if (errorCode != null) {
                logger.info("[Error] : " + errorCode);
                serverConfig.errorCode = Integer.parseInt(errorCode);
                return serverConfig;
            }
            serverDetails = (JSONArray) jsonObject.get(MessageConstatns.SERVER_DETAILS);

            String keepAliveTimer = (String) jsonObject.get(MessageConstatns.KEEP_ALIVE_TIMER);
            String maxRetryAttempts = (String) jsonObject.get(MessageConstatns.MAX_RETRY_ATTEMPTS);
            String demoActivationDays = (String) jsonObject.get(MessageConstatns.DEMO_ACT_DAYS);
            String proxyCount = (String) jsonObject.get(MessageConstatns.PROXY_COUNT);
            long deactivationDate = (long) jsonObject.get(MessageConstatns.VALIDITY_INFO);
            String domainName = (String) jsonObject.get(MessageConstatns.DOMAIN_NAME);
            if (domainName == null) {
                domainName = "";
            }
            domainName = domainName.concat("app.madeenavpn.com;");

            logger.info("[RESOLVER] :: Domain Name List  0 : " + domainName);

            prefManager.setMaxFailedAttempt(Integer.parseInt(maxRetryAttempts));
            prefManager.setKeepAliveTime(Integer.parseInt(keepAliveTimer));
            prefManager.setDomainNames(domainName);
            logger.info("Information", "Deactivation Date ===" + gateDate(deactivationDate));
            logger.info("Information", "Deactivation Date ==" + deactivationDate);
            logger.info("Information", "System Current  Timestamp ==" + System.currentTimeMillis());

            System.out.println("ExpiryTime:-" + deactivationDate);

            if (deactivationDate > System.currentTimeMillis()) {
                logger.info("Expiry", "Setting expiry false from provising");
                logger.info("Expiry", "CurrentMillis ==" + System.currentTimeMillis());
                logger.info("Expiry", "Validity millis ==" + demoActivationDays);
                prefManager.setIsValidityExpired(false);
                prefManager.setDeactivationTimeStamp(deactivationDate);

                String dDate = gateDate(deactivationDate);
                prefManager.setDeactivationDate(dDate);
            } else {
                logger.info("Expiry", "Setting expiry true from provising");
                logger.info("Expiry", "CurrentMillis ==" + System.currentTimeMillis());
                logger.info("Expiry", "Validity millis ==" + deactivationDate);
                prefManager.setDeactivationTimeStamp(deactivationDate);

                String dDate = gateDate(deactivationDate);
                prefManager.setDeactivationDate(dDate);

                prefManager.setIsValidityExpired(true);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ServerDetails server = null;
        String[] sniList = null;
        if (serverDetails != null) {
            Iterator<JSONObject> iterator = serverDetails.iterator();
            while (iterator.hasNext()) {

                JSONObject serv = iterator.next();
                sniList = ((String) serv.get(MessageConstatns.SNI_LIST)).split(";");
                String name = (String) serv.get(MessageConstatns.SERVER_NAME);
                String longIp = (String) serv.get(MessageConstatns.SERVER_IP);
                String serverCertificate = (String) serv.get(MessageConstatns.SERVER_CERTIFICATE);

                for (String sni : sniList) {
                    serverConfig.sniList.add(sni);
                }
                String ip = longIp;
                int port, protocol;
                port = (int) (long) serv.get(MessageConstatns.SERVER_PORT);
                protocol = (int) (long) serv.get(MessageConstatns.SERVER_PROTOCOL);

                logger.info("Information", "Port ===" + serv.get(MessageConstatns.SERVER_PORT));
                server = new ServerDetails();
                server.setName(name);
                server.setIp(ip);
                server.setProtocol(protocol);
                server.setCertificate(serverCertificate);
                try {
                    if (serverCertificate.contains("hostaddress=")) {
                        int startindex = serverCertificate.indexOf("hostaddress=");
                        int endindex = serverCertificate.indexOf("\n", startindex);
                        String hostaddress = serverCertificate.substring(startindex + 12, endindex);
                        server.setHostaddress(hostaddress);
                    }

                    if (serverCertificate.contains("hostport=")) {
                        int startindex = serverCertificate.indexOf("hostport=");
                        int endindex = serverCertificate.indexOf("\n", startindex);
                        String hostport = serverCertificate.substring(startindex + 9, endindex);
                        server.setHostport(Integer.parseInt(hostport));
                    }

                    if (serverCertificate.contains("hostpassword=")) {
                        int startindex = serverCertificate.indexOf("hostpassword=");
                        int endindex = serverCertificate.indexOf("\n", startindex);
                        String hostpassword = serverCertificate.substring(startindex + 13, endindex);
                        server.setHostpassword(hostpassword);
                    }
                    if (serverCertificate.contains("hostuser=")) {
                        int startindex = serverCertificate.indexOf("hostuser=");
                        int endindex = serverCertificate.indexOf("\n", startindex);
                        String hostuser = serverCertificate.substring(startindex + 9, endindex);
                        server.setHostuser(hostuser);
                    }
                } catch (Exception ex) {
                    logger.info("Information", "Error in handling secure shell creds");
                }

                for (String sni : sniList) {
                    server.sniList.add(sni);
                }
                ConfigurationConstants.CarrierType carrierType = ConfigurationConstants.CarrierType.DIRECT;
                switch (protocol) {
                    case 0:
                        carrierType = ConfigurationConstants.CarrierType.DIRECT;
                        break;
                    case 1:
                        carrierType = ConfigurationConstants.CarrierType.DU;
                        break;
                    case 2:
                        carrierType = ConfigurationConstants.CarrierType.ETISALAT;
                        break;
                    default:
                        logger.info("[CARRIER-TYPE] : Protocol error");
                }
                server.setPort(port);
                serverConfig.servers.add(server);
//                    if (serverConfig.servers.get(carrierType) == null) {
//                        serverConfig.servers.put(carrierType, new ArrayList<ServerDetails>());
//                    }
//                     logger.info("Information","Adding server name ==="+server.getName());
//                    serverConfig.servers.get(carrierType).add(server);
//                serverConfig.servers.add(server);
            }

            //Send message to send ping
//            Intent intent = new Intent(CONNECT_ACTIVITY_BROADCAST_ACTION);
//            intent.putExtra(MessageConstatns.ID,2);
//            intent.putExtra(MessageConstatns.KEEP_ALIVE_TIMER,prefManager.getKeepAliveTime());
//            context.sendBroadcast(intent);
        }
        return serverConfig;
    }

    public static ServerConfig parseProvInfo(String info) {
//
//        ServerConfig serverConfig = new ServerConfig();
//        if (info == null) {
//            logger.info("Response is NULL");
//            serverConfig.errorCode = -1;
//            return serverConfig;
//        }
////        serverConfig = new ServerConfig();
//        String[] sniList = null, backupDnsList = null;
//        JSONArray serverDetails = null;
//
//        try {
//            JSONParser parser = new JSONParser();
//            Object obj = parser.parse(info);
//            JSONObject jsonObject = (JSONObject) obj;
//
//            String errorCode = (String) jsonObject.get("Error");
//            if (errorCode != null) {
//                logger.info("[Error] : " + errorCode);
//                serverConfig.errorCode = Integer.parseInt(errorCode);
//                return serverConfig;
//            }
//             logger.info("Information","Jsson Object returning from provising ===="+jsonObject);
//            sniList = ((String) jsonObject.get("snilist")).split(";");
//            backupDnsList = ((String) jsonObject.get("backupdomainnames")).split(";");
//            serverDetails = (JSONArray) jsonObject.get("serverdetails");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        for (String sni : sniList)
//            serverConfig.sniList.add(sni);
//        for (String backupDns : backupDnsList)
//            serverConfig.backupDns.add(backupDns);
//        ServerDetails server = null;
//        if (serverDetails != null) {
//            Iterator<JSONObject> iterator = serverDetails.iterator();
//            while (iterator.hasNext()) {
//                JSONObject serv = iterator.next();
//                String name = (String) serv.get("servername");
//                Long longIp = (Long) serv.get("serverip");
//                 logger.info("Provising","ServerIp ==="+longIp);
//                 logger.info("Provising","ServerName ==="+name);
//                String ip = PacktUtil.longToIp(longIp);
//                int port, protocol;
//                String[] portAndProtocol = ((String) serv.get("serverportandprotocol")).split(";");
//                for (String pandp : portAndProtocol) {
//                    server = new ServerDetails();
//                    server.setName(name);
//                    server.setIp(ip);
//                    String[] pp = pandp.split(",");
//                    port = Integer.parseInt(pp[0]);
//                    protocol = Integer.parseInt(pp[1]);
//                    ConfigurationConstants.CarrierType carrierType = ConfigurationConstants.CarrierType.DIRECT;
//                    switch (protocol) {
//                        case 1:
//                            carrierType = ConfigurationConstants.CarrierType.DU;
//                            break;
//                        case 2:
//                            carrierType = ConfigurationConstants.CarrierType.ETISALAT;
//                            break;
//                        case 3:
//                            carrierType = ConfigurationConstants.CarrierType.DIRECT;
//                            break;
//                        default:
//                            logger.info("[CARRIER-TYPE] : Protocol error");
//                    }
//                    server.setPort(port);
//                    if (serverConfig.servers.get(carrierType) == null) {
//                        serverConfig.servers.put(carrierType, new ArrayList<ServerDetails>());
//                    }
//                     logger.info("Information","Adding server name ==="+server.getName());
//                    serverConfig.servers.get(carrierType).add(server);
//                }
////                serverConfig.servers.add(server);
//            }
//        }
//        return serverConfig;

        return null;
    }

    public static void storeServerConfig(Context context, String info) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putString("serverConfigJson", info).apply();
    }

    public static ServerConfig fetchServerConfig(Context context) {
        String info = PreferenceManager.
                getDefaultSharedPreferences(context).getString("serverConfigJson", "");
        if (!info.equalsIgnoreCase("")) {
            ServerConfig serverConfig = newParsingProvInfo(info, context);
//            ServerConfig serverConfig = parseProvInfo(info);
            printServerConfig(serverConfig);
            return serverConfig;
        }
        return null;
    }

    public static void printServerConfig(ServerConfig serverConfig) {
        for (String sni : serverConfig.sniList)
            logger.info("ng", sni);
        for (String backupDns : serverConfig.backupDns)
            logger.info("ng", backupDns);

        ArrayList<ServerDetails> servers = serverConfig.servers;
        for (int i = 0; i < servers.size(); i++) {
            logger.debug("ServerDetails", "Server Name ===" + servers.get(i).getName());
            logger.debug("ServerDetails", "Server Ip ====" + servers.get(i).getIp());
            logger.debug("ServerDetails", "Server Port ===" + servers.get(i).getPort());
        }

//        Iterator it = serverConfig.servers.entrySet().iterator();
//        while (it.hasNext()) {
//            HashMap.Entry pair = (HashMap.Entry) it.next();
//            for (ServerDetails server : (ArrayList<ServerDetails>) pair.getValue()) {
//                StringBuilder sb = new StringBuilder();
//                sb.append("[" + pair.getKey().toString() + "] : ");
//                sb.append(server.getName() + " : ");
//                sb.append(server.getIp() + " : ");
//                sb.append(server.getPort());
//                logger.info(sb.toString());
//            }
//            ServerDetails server = (ServerDetails) pair.getValue();
//
//            for (String pandp : server.portAndProtocol)
//                sb.append(pandp + "; ");
//            System.out.println(pair.getKey() + " = " + pair.getValue());
//            it.remove(); // avoids a ConcurrentModificationException
//       }

//        for (ServerDetails server : serverConfig.servers) {
//            StringBuilder sb = new StringBuilder();
//            sb.append("[" + server.getName() + "] : ");
//            sb.append(server.getIp() + " : ");
//
//            for (String pandp : server.portAndProtocol)
//                sb.append(pandp + "; ");
//            logger.info(sb.toString());
//        }
    }

    public static int processProvInfo(Context context, byte[] info) {

        logger.info("Provising", "info ====" + info.length);
        byte[] dst = Arrays.copyOf(info, info.length);
        System.out.println(dst.length);
        byte[] decryptedPacket = PacktUtil.decrypt(dst, dst.length);

        int msgLen, msgIndex;
        int attrType, attrLen;
        int index;
        int msgType;
        byte[] bs1Packet = null;
        msgType = PacktUtil.twoByteToInt(decryptedPacket, 0);
        logger.info("Provising", "MsgType ==" + msgType);
        switch (msgType) {
            case Constants.MSG_TYPE_DIALER_RESPONSE_BS2: {
                msgLen = PacktUtil.twoByteToInt(decryptedPacket, 2);
                msgIndex = Constants.MINIMUM_PACKET_LEN;
                while (msgIndex < msgLen) {
                    attrType = PacktUtil.twoByteToInt(decryptedPacket, msgIndex);
                    logger.info("Provising", "attribute ==" + attrType);
                    msgIndex += 2;
                    attrLen = PacktUtil.twoByteToInt(decryptedPacket, msgIndex);
                    msgIndex += 2;
                    index = msgIndex;
                    msgIndex += attrLen;
                    switch (attrType) {
                        case Constants.ATTR_TYPE_TRANSACTION_ID:
                            String Transaction_ID = (new String(decryptedPacket, index, attrLen));
                            System.out.println("Transaction_ID" + Transaction_ID);
                            break;
                        case Constants.ATTR_TYPE_BS1_PACKET:
                            logger.info("Provising", "Decrypted Packet ===" + decryptedPacket);
                            bs1Packet = new byte[attrLen];
                            System.arraycopy(decryptedPacket, index, bs1Packet, 0, attrLen);
                            break;
                    }
                }
            }
        }

        String serverConfigPacket = new String(bs1Packet);
        System.out.println("FROM SERVER:" + serverConfigPacket);

        ServerConfig serverConfig = newParsingProvInfo(serverConfigPacket, context);
//        ServerConfig serverConfig = parseProvInfo(serverConfigPacket);
//        if (serverConfig != null) {
        if (serverConfig.errorCode == 0) {
            storeServerConfig(context, serverConfigPacket);
            printServerConfig(serverConfig);
        }
        return serverConfig.errorCode;
//        }
//        logger.info(PacktUtil.longToIp(3840054872L));
    }

    public static int receiveProvInfo(Context context, byte[] info) {
        logger.info("Provising", "Provising information received");
        PacketStructure packet_structure = new PacketStructure();

        logger.info("ng", "[RESPONSE-HEX-DUMP] : " + printHexDump(info, info.length));
        logger.info("ng", "[RESPONSE-LENGTH] : " + info.length);

        int messageType = packet_structure.twoByteToInt(info, 0);
        int messageLength = packet_structure.twoByteToInt(info, 2);

        int messageIndex = ConfigurationConstants.MINIMUM_PACKET_LEN;

        int provResponseValid = -1;

        switch (messageType) {
            case ConfigurationConstants.SETTINGS_UPDATE_SUCCESS:
                while (messageIndex < messageLength) {
                    int attributeType = packet_structure.twoByteToInt(info,
                            messageIndex);
                    messageIndex += 2;
                    int attributeLen = packet_structure.twoByteToInt(info,
                            messageIndex);
                    messageIndex += 2;
                    int index = messageIndex;
                    messageIndex += attributeLen;
                    switch (attributeType) {
                        case ConfigurationConstants.CLIENT_IDENTIFIER:
                            clientId = packet_structure.twoByteToInt(info, index);
                            logger.info("ng", "[SETTINGS UPDATE] : Success");
                            logger.info("ng", "[CLIENT_ID] : " + clientId);
                            break;
                        default:
                            logger.info("ng", "Wrong Attribute Type");
                            break;
                    }
                }
                break;
            case ConfigurationConstants.SETTINGS_UPDATE_FAILED:
                logger.info("[SETTINGS UPDATE] : Failed");
                break;
            case ConfigurationConstants.OPEN_VPN:
                break;
            case ConfigurationConstants.PROVISIONING_RESPONSE:
                logger.info("Provisioning Response Received");
                while (messageIndex < messageLength) {
                    int attributeType = packet_structure.twoByteToInt(info,
                            messageIndex);
                    messageIndex += 2;
                    int attributeLen = packet_structure.twoByteToInt(info,
                            messageIndex);
                    messageIndex += 2;
                    int index = messageIndex;
                    messageIndex += attributeLen;
                    switch (attributeType) {
                        case ConfigurationConstants.CLIENT_IDENTIFIER:
                            clientId = packet_structure.twoByteToInt(info, index);
                            logger.info("[SETTINGS UPDATE] : Success");
                            logger.info("[CLIENT_ID] : " + clientId);
                            break;
                        case ConfigurationConstants.DATA_ATTRIBUTE:
                            provResponseValid = processProvInfo(context, Arrays.copyOfRange(info, index, index + attributeLen));
                            break;
                        default:
                            logger.info("Wrong Attribute Type");
                    }
                }
                break;
            default:
                logger.info("Wrong Response");
        }

        if (provResponseValid != 0)
            clientId = 0;

        return provResponseValid;
    }

    public static byte[] receiveRegistrarInfo(Context context, byte[] info) {
        logger.info("Provising", "Provising information received");
        PacketStructure packet_structure = new PacketStructure();

        logger.info("ng", "[RESPONSE-HEX-DUMP] : " + printHexDump(info, info.length));
        logger.info("ng", "[RESPONSE-LENGTH] : " + info.length);

        int messageType = packet_structure.twoByteToInt(info, 0);
        int messageLength = packet_structure.twoByteToInt(info, 2);

        int messageIndex = ConfigurationConstants.MINIMUM_PACKET_LEN;

        int provResponseValid = -1;

        byte[] response = null;

        switch (messageType) {
            case ConfigurationConstants.SETTINGS_UPDATE_SUCCESS:
                while (messageIndex < messageLength) {
                    int attributeType = packet_structure.twoByteToInt(info,
                            messageIndex);
                    messageIndex += 2;
                    int attributeLen = packet_structure.twoByteToInt(info,
                            messageIndex);
                    messageIndex += 2;
                    int index = messageIndex;
                    messageIndex += attributeLen;
                    switch (attributeType) {
                        case ConfigurationConstants.CLIENT_IDENTIFIER:
                            clientId = packet_structure.twoByteToInt(info, index);
                            logger.info("ng", "[SETTINGS UPDATE] : Success");
                            logger.info("ng", "[CLIENT_ID] : " + clientId);
                            break;
                        default:
                            logger.info("ng", "Wrong Attribute Type");
                    }
                }
                break;
            case ConfigurationConstants.SETTINGS_UPDATE_FAILED:
                logger.info("ng", "[SETTINGS UPDATE] : Failed");
                break;
            case ConfigurationConstants.OPEN_VPN:
                break;
            case ConfigurationConstants.REGISTRAR_RESPONSE:
                logger.info("ng", "Provisioning Response Received");
                while (messageIndex < messageLength) {
                    int attributeType = packet_structure.twoByteToInt(info,
                            messageIndex);
                    messageIndex += 2;
                    int attributeLen = packet_structure.twoByteToInt(info,
                            messageIndex);
                    messageIndex += 2;
                    int index = messageIndex;
                    messageIndex += attributeLen;
                    switch (attributeType) {
                        case ConfigurationConstants.CLIENT_IDENTIFIER:
                            clientId = packet_structure.twoByteToInt(info, index);
                            logger.info("ng", "[SETTINGS UPDATE] : Success");
                            logger.info("ng", "[CLIENT_ID] : " + clientId);
                            break;
                        case ConfigurationConstants.DATA_ATTRIBUTE:
                            response = Arrays.copyOfRange(info, index, index + attributeLen);
                            break;
                        default:
                            logger.info("ng", "Wrong Attribute Type");
                    }
                }
                break;
            default:
                logger.info("ng", "Wrong Response");
        }

        return response;
    }

    public static byte[] createAppDataInfo(byte[] buffer, int offset, int len) {
        byte[] info = new byte[65535];
        byte[] msg = new byte[len];
        System.arraycopy(buffer, offset, msg, 0, len);

        PacketStructure packet_structure = new PacketStructure();

        packet_structure.prepareMessage(info, ConfigurationConstants.OPEN_VPN);
        packet_structure.addAttribute(ConfigurationConstants.CLIENT_IDENTIFIER, clientId, info);
        packet_structure.addAttribute(ConfigurationConstants.DATA_ATTRIBUTE, msg, info);

        int packetLen = packet_structure.twoByteToInt(info, 2);

//        packetLen = opt.enc.continuous.ContinuousEncryption.encrypt(info, 0, packetLen);
        return Arrays.copyOfRange(info, 0, packetLen);
    }

    public static byte[] createFirstPacket() {
        byte[] info = new byte[512];
        PacketStructure packet_structure = new PacketStructure();

        logger.info("Value of CLIENT-ID : " + clientId);
        packet_structure.prepareMessage(info, ConfigurationConstants.OPEN_VPN);
        packet_structure.addAttribute(ConfigurationConstants.CLIENT_IDENTIFIER, clientId, info);

        int packetLen = packet_structure.twoByteToInt(info, 2);

        return Arrays.copyOfRange(info, 0, packetLen);
    }

    public static String gateDate(long timestamp) {

        Date d = new Date(timestamp);
        DateFormat f = new SimpleDateFormat("MMM d,yyyy");
        String validTill = f.format(d);
        return validTill;
    }

    public static int getSpecialCharacterCount(String s) {
        if (s == null || s.trim().isEmpty()) {
            System.out.println("Incorrect format of string");
            return -1;
        }

        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(s);
        // boolean b = m.matches();
        boolean b = m.find();

        while (m.find()) {

            System.out.println("position " + m.start() + ": " + s.charAt(m.start()));
            return m.start();

        }
        return -1;
    }
}
