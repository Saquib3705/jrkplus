package com.jrk_plus.TransportLayer;

/**
 * Created by bot on 15/01/2018.
 */

public interface SendRequestLayerInterface {

    /*
      ** create packet for different protocols
     */
    public String createPacket(int protocol);
    /*
    **send any packet by using this interface
     */
    public void sendPacket(String ip, int port, String packet, int protocol);
}
