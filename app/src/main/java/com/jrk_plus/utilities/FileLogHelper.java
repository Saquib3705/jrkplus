package com.jrk_plus.utilities;

/**
 * Created by bot on 15/02/2018.
 */

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Locale;

import static opt.log.OmLogger.logger;


public class FileLogHelper {
    private static final String cmdBegin = "logcat -f ";
    private static final boolean shouldLog = true; //TODO: set to false in final version of the app
    private static final String TAG = "FileLogHelper";
    private static FileLogHelper mInstance;
    private String logFileAbsolutePath;
    private String cmdEnd = " *:F";
    private boolean isLogStarted = false;

    private FileLogHelper() {
    }

    public static FileLogHelper getInstance() {
        if (mInstance == null) {
            mInstance = new FileLogHelper();
        }
        return mInstance;
    }

    private void initLog() {
        if (!isLogStarted && shouldLog) {
            SimpleDateFormat dF = new SimpleDateFormat("yy-MM-dd_HH_mm''ss", Locale.getDefault());
            String fileName = "logcat_madeena_testing.txt";
            File outputFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/logcat/");
            if (outputFile.mkdirs() || outputFile.isDirectory()) {
                logFileAbsolutePath = outputFile.getAbsolutePath() + "/" + fileName;
                File prevLogFile = new File(logFileAbsolutePath);
                 logger.info(TAG, "Size of file before ===" + prevLogFile.length());
                PrintWriter writer = null;
                try {
                    writer = new PrintWriter(prevLogFile);
                    writer.append("Hi  i am here");
                    writer.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                 logger.info(TAG, "Size of file after ===" + prevLogFile.length());
                startLog();
            }
        }
    }

    private void startLog() {
        if (shouldLog) {
//            try {
                File prevLogFile = new File(logFileAbsolutePath);
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(prevLogFile);
                writer.append("Hi are you there");
                writer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }//                prevLogFile.delete();
//                Runtime.getRuntime().exec(cmdBegin + logFileAbsolutePath + cmdEnd);
                isLogStarted = true;
//            } catch (IOException ignored) {
//                Log.e(TAG, "initLogCat: failed");
//            }
        }
    }

    /**
     * Add a new tag to file log.
     *
     * @param tag      The android {@link Log} tag, which should be logged into the file.
     * @param priority The priority which should be logged into the file. Can be V, D, I, W, E, F
     * @see <a href="http://developer.android.com/tools/debugging/debugging-log.html#filteringOutput">Filtering Log Output</a>
     */
    private void addLogTag(String tag, String priority) {
        String newEntry = " " + tag + ":" + priority;
        if (!cmdEnd.contains(newEntry)) {
            cmdEnd = newEntry + cmdEnd;
             logger.info(TAG, "Islog Started ===" + isLogStarted);
             logger.info(TAG, "shouldLog == " + shouldLog);
            if (isLogStarted) {
                startLog();
            } else {
                initLog();
            }
        }
    }

    /**
     * Add a new tag to file log with default priority, which is Verbose.
     *
     * @param tag The android {@link Log} tag, which should be logged into the file.
     */
    public void addLogTag(String tag) {
        addLogTag(tag, "V");
    }

    private void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                delete(c);
            }
        } else if (f.getAbsolutePath().endsWith("FIR")) {
            if (!f.delete()) {
                new FileNotFoundException("Failed to delete file: " + f);
            }
        }
    }
}
