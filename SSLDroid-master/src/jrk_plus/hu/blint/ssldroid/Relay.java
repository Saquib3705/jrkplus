package jrk_plus.hu.blint.ssldroid;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;

import static jrk_plus.hu.blint.ssldroid.TcpProxyServerThread.isFirstPacket;
import static opt.log.OmLogger.logger;

public class Relay extends Thread {
    /**
     *
     */
    private final TcpProxyServerThread tcpProxyServerThread;
    private InputStream in;
    private OutputStream out;
    private String side;
    private int sessionid;
    private final static int BUFSIZ = 4096;
    private byte buf[] = new byte[BUFSIZ];

    public Relay(TcpProxyServerThread tcpProxyServerThread, InputStream in, OutputStream out, String side, int sessionid) {
        this.tcpProxyServerThread = tcpProxyServerThread;
        this.in = in;
        this.out = out;
        this.side = side;
        this.sessionid = sessionid;
    }

    public static String printHexDump(byte[] bytes, int len) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++) {
                if (i % 20 == 0) {
                    sb.append("\n");
                }
                sb.append(String.format("%02x ", bytes[i] & 0xff));
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error("Ex[printHexDump]: ", e);
        }
        return "failed to print hexdump";
    }

    public static byte[] addHeader(byte[] info, int length, String ip, int port, int protocol, int action) {
        try {
             logger.info("ng", "Stuffing SIP Proxy Header");
             logger.info("ng", "Original Length : " + length);
             logger.info("ng", "IsFirstPacket : " + isFirstPacket);
            byte[] data = null;
            int i = 0;

            int len = 0;
//            if (isFirstPacket)
            len = length + 8;
//            else
//                len = length;

//             logger.info("ng", "PROTO_TCP : Adding two bytes of data");
            data = new byte[len];
//            data[i++] = (byte) ((len >> 8) & 0xFF);
//            data[i++] = (byte) (len & 0xFF);

//            if (isFirstPacket) {
            String[] labels = ip.split("\\.");

            for (int j = 0; j < labels.length; j++)
                data[i++] = (byte) Integer.parseInt(labels[j]);

            data[i++] = (byte) ((port >> 8) & 0xFF);
            data[i++] = (byte) (port & 0xFF);

            data[i++] = (byte) (protocol & 0xFF);
            data[i++] = (byte) (action & 0xFF);
            isFirstPacket = false;
//            }

             logger.info("ng", "Value of i : " + i);
            if (action == 3)
                 logger.info("ng", "ACTION_FORWARD");
            System.arraycopy(info, 0, data, i, length);
             logger.info("ng", "Length of data copied: " + length + " bytes");
             logger.info("ng", "Final Length : " + data.length);
            return data;
        } catch (Exception e) {
            Log.e("ng", "Add Header Exception:" + Log.getStackTraceString(e));
        }
        return null;
    }

    public void run() {
        int n = 0;

        try {
            while ((n = in.read(buf)) > 0) {
                if (Thread.interrupted()) {
                    // We've been interrupted: no more relaying
                     logger.info("SSLDroid", this.tcpProxyServerThread.tunnelName + "/" + sessionid + ": Interrupted " + side + " thread");
                    try {
                        in.close();
                        out.close();
                    } catch (IOException e) {
                         logger.info("SSLDroid", this.tcpProxyServerThread.tunnelName + "/" + sessionid + ": " + e.toString());
                    }
                    return;
                }

//                if (this.side.equalsIgnoreCase("client") && isFirstPacket) {
//                    byte[] outData = addHeader(buf, n, "127.0.0.1", 1194, 2, 3);
//                    out.write(outData, 0, outData.length);
//                     logger.info("ng", "First Packet : " + outData.length);
//                    out.flush();
//                    for (int i = 0; i < outData.length; i++) {
//                        if (outData[i] == 7)
//                            outData[i] = '#';
//                    }
//                } else {
                    out.write(buf, 0, n);
//                     logger.info("ng", "Packet : (" + this.side + ") : " + n);
//                    if (this.side.equalsIgnoreCase("server"))
//                         logger.info("ng", "Data received from server: " + printHexDump(buf, n));
                    for (int i = 0; i < n; i++) {
                        if (buf[i] == 7)
                            buf[i] = '#';
                    }

//                }


//                try {
//                    Thread.sleep(200);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }


            }
        } catch (SocketException e) {
             logger.info("SSLDroid", this.tcpProxyServerThread.tunnelName + "/" + sessionid + ": " + e.toString());
        } catch (IOException e) {
             logger.info("SSLDroid", this.tcpProxyServerThread.tunnelName + "/" + sessionid + ": " + e.toString());
        } finally {
            try {
                in.close();
                out.close();
            } catch (IOException e) {
                 logger.info("SSLDroid", this.tcpProxyServerThread.tunnelName + "/" + sessionid + ": " + e.toString());
            }
        }
         logger.info("SSLDroid", this.tcpProxyServerThread.tunnelName + "/" + sessionid + ": Quitting " + side + "-side stream proxy...");
    }
}