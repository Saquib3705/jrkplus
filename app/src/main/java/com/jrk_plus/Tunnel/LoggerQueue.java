package com.jrk_plus.Tunnel;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by ABHISHEK MAHATO on 5/30/2017.
 */

public interface LoggerQueue {
    BlockingQueue queue = new ArrayBlockingQueue(8192*8);
}
