package com.jrk_plus.Tunnel;

/**
 * Created by ABHISHEK MAHATO on 4/24/2017.
 */

public class Config {
    String localIp, remoteIp, serverIp, sni;
    int localPort, remotePort, serverPort;
    int clientChildCount, clientMppc;
    ConfigurationConstants.TunnelType tunnelType;

    public Config(){
        this.localIp = null;
        this.localPort = 0;
        this.remoteIp = null;
        this.remotePort = 0;
        this.clientChildCount = 1;
        this.clientMppc = 1;
        this.serverIp = null;
        this.serverPort = 0;
        this.sni = null;
        this.tunnelType = ConfigurationConstants.TunnelType.PSEUDO_SSL;
    }

    public String getLocalIp() {
        return localIp;
    }

    public void setLocalIp(String localIp) {
        this.localIp = localIp;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public int getLocalPort() {
        return localPort;
    }

    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getClientChildCount() {
        return clientChildCount;
    }

    public void setClientChildCount(int clientChildCount) {
        this.clientChildCount = clientChildCount;
    }

    public int getClientMppc() {
        return clientMppc;
    }

    public void setClientMppc(int clientMppc) {
        this.clientMppc = clientMppc;
    }

    public String getSni() {
        return sni;
    }

    public void setSni(String sni) {
        this.sni = sni;
    }

    public ConfigurationConstants.TunnelType getTunnelType() {
        return tunnelType;
    }

    public void setTunnelType(ConfigurationConstants.TunnelType tunnelType) {
        this.tunnelType = tunnelType;
    }
}
