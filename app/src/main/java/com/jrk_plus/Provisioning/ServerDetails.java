package com.jrk_plus.Provisioning;

import java.util.ArrayList;

/**
 * Created by ABHISHEK MAHATO on 5/8/2017.
 */
public class ServerDetails {

    enum Protocol {
        UDP,
        TCP,
        SSL,
        PSEUDOSSL
    }

    ;
    private String name;
    private String ip;
    public int port;
    public boolean isSelected = false;
    public boolean ischecked = false;
    public ArrayList<String> sniList = new ArrayList<>();
    public String certificate = "";
//    public ArrayList<String> portAndProtocol;

    private int protocol;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHostaddress() {
        return hostaddress;
    }

    public void setHostaddress(String hostaddress) {
        this.hostaddress = hostaddress;
    }

    public int getHostport() {
        return hostport;
    }

    public void setHostport(int hostport) {
        this.hostport = hostport;
    }

    public String getHostpassword() {
        return hostpassword;
    }

    public void setHostpassword(String hostpassword) {
        this.hostpassword = hostpassword;
    }

    public String hostaddress;
    public int hostport;
    public String hostpassword;

    public String getHostuser() {
        return hostuser;
    }

    public void setHostuser(String hostuser) {
        this.hostuser = hostuser;
    }

    public String hostuser;


    public int getProtocol() {
        return protocol;
    }

    public void setProtocol(int protocol) {
        this.protocol = protocol;
    }

    public ServerDetails() {
        this.port = -1;
    }

    public ArrayList<String> getSniList() {
        return sniList;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }
}
