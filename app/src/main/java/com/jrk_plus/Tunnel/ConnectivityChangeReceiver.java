package com.jrk_plus.Tunnel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import org.apache.http.conn.util.InetAddressUtils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 5/4/2017.
 */

public class ConnectivityChangeReceiver extends BroadcastReceiver {

    String state = "NULL";
    ConfigurationConstants.CarrierType carrierType;
    Context context;

    public ConnectivityChangeReceiver(Context con){
        this.context = con;
    }

    public String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections
                    .list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf
                        .getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils
                                .isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6
                                // port
                                // suffix
                                return delim < 0 ? sAddr : sAddr.substring(
                                        0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    private  boolean checkWifiOnAndConnected() {
        WifiManager wifiMgr = (WifiManager) this.context.getSystemService(Context.WIFI_SERVICE);

        if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

            return wifiInfo.getNetworkId() != -1;
        }
        else {
            return false; // Wi-Fi adapter is OFF
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        boolean noConnectivity = intent.getBooleanExtra(
                ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
        String reason = intent
                .getStringExtra(ConnectivityManager.EXTRA_REASON);
        boolean isFailover = intent.getBooleanExtra(
                ConnectivityManager.EXTRA_IS_FAILOVER, false);

        String extras = intent
                .getStringExtra(ConnectivityManager.EXTRA_EXTRA_INFO);

        NetworkInfo currentNetworkInfo = (NetworkInfo) intent
                .getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
        // NetworkInfo otherNetworkInfo = (NetworkInfo)
        // intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);

        logger.debug("Network Changed" + noConnectivity);
        logger.debug("Network Changed" + reason);
        logger.debug("Network Changed" + isFailover);
        logger.debug("Network Changed" + extras);

        if (extras == null)
            extras = "";
        if (currentNetworkInfo != null) {
            String testNetWork = currentNetworkInfo.toString();
            logger.debug("Network Changed world" + testNetWork);
            logger.debug(extras + " Detected.");
            if (extras.toLowerCase().contains("etisalat")) {
                carrierType = ConfigurationConstants.CarrierType.ETISALAT;
            } else if (extras.contains("du") || extras.contains("DU")) {
                carrierType = ConfigurationConstants.CarrierType.DU;
            } else
                carrierType = ConfigurationConstants.CarrierType.DU;
        }


        if (currentNetworkInfo != null) {
            if (currentNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) {
//                if (currentNetworkInfo.isConnected() && CallUtils.flagStopProv) {
//                    CallUtils.flagStopProv = false;
//                    Message m1 = Message.obtain(MainActivity.handler_,
//                            MainActivity.START_PROV);
//                    m1.sendToTarget();
//                }
                logger.debug("NetworkState","State =="+state);
                if (!currentNetworkInfo.isConnected()) {
                    if (state.equals("NULL")) {
                        state = "DISCONNECTED";
//                        return;
                    } else if (state.equals("CONNECTED")) {
                        // destroyOnOpcodeChange();
                        state = "DISCONNECTED";
                    }
                } else {
                    if (state.equals("NULL")) {
                        state = "CONNECTED";
//                        return;
                    } else if (state.equals("DISCONNECTED")) {
                        state = "CONNECTED";
                        @SuppressWarnings("WrongConstant") WifiManager wifiManager = (WifiManager) context.getSystemService(ConfigurationConstants.WIFI_SERVICE);
                        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                        int ipAddress = wifiInfo.getIpAddress();
                        String ipString = String.format("%d.%d.%d.%d",
                                (ipAddress & 0xff),
                                (ipAddress >> 8 & 0xff),
                                (ipAddress >> 16 & 0xff),
                                (ipAddress >> 24 & 0xff));
                        logger.debug("IP=" + ipString);
							/*
							 * if(MyApp.isAppInit==false) { Message m =
							 * Message.obtain(handler ,12); m.sendToTarget(); }
							 */

//                        SettingsTunnel.updateMediaSocket(ipString,
//                                ipString.length());

                    }
                }
            } else if (currentNetworkInfo.getTypeName().equalsIgnoreCase(
                    "MOBILE")) {
//                if (currentNetworkInfo.isConnected() && CallUtils.flagStopProv) {
//                    CallUtils.flagStopProv = false;
//                    Message m1 = Message.obtain(MainActivity.handler_,
//                            MainActivity.START_PROV);
//                    m1.sendToTarget();
//                }
                if (!currentNetworkInfo.isConnected()) {
                    if (state.equals("NULL")) {
                        state = "DISCONNECTED";
//                        return;
                    } else if (state.equals("CONNECTED")) {
                        // destroyOnOpcodeChange();
                        state = "DISCONNECTED";
                    }
                } else {
                    if (state.equals("NULL")) {
                        state = "CONNECTED";
//                        return;
                    } else if (state.equals("DISCONNECTED")) {
                        state = "CONNECTED";
                        String ipMobile = getIPAddress(true);
                        logger.debug("IP Mobile =" + ipMobile);


//                        SettingsTunnel.updateMediaSocket(ipMobile,
//                                ipMobile.length());

                    }
                }
            }
        }

        logger.debug("Network Changed world [WIFI] : " + checkWifiOnAndConnected());

        Intent i = new Intent("carrierIntent");
        // Data you need to pass to activity
        i.putExtra("carrier", carrierType);
        i.putExtra("wifi_enabled", checkWifiOnAndConnected());
        i.putExtra("connection_status", state);
        logger.debug("BroadcastReceiver", "Sending broadcast");

        LocalBroadcastManager.getInstance(context).sendBroadcast(i);

//        connectionType.getCarrierType(carrierType);

    }
}
