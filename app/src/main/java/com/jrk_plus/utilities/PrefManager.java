package com.jrk_plus.utilities;


import android.content.Context;
import android.content.SharedPreferences;

import com.jrk_plus.NewDashboard.ConnectActivity;


public class PrefManager {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;

    // shared pref mode
    private int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "vpn-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private static final String IS_LOGGED_IN = "IsLoggedIn";

    private static final String LOGGED_IN = "LoggedIn";

    private static final String IS_FIRST_TIME_SERVER_DOWNLOADED = "IsFirstTimeServerDownloaded";

    private static final String MOBILE_NUMBER = "mobile_number";

    private static final String KEY = "key";

    private static final String MAX_FAILED_RE_ATTEMPT = "max_failed_re_attempt";

    private static final String IS_VALIDITY_EXPIRED = "is_validity_expired";

    private static final String INVALID_PASSWORD = "invalid_password";

    private static final String KEEP_ALIVE = "keep_alive";

    private static final String USER_NAME = "user_name";

    private static final String PASSWORD = "password";

    private static final String DEACTIVATION_TIME = "deactivation_time";

    private static final String DOMAIN_NAME = "domain_name";

    private static final String WRONG_VOUCHER_APPLIED_COUNT = "wrong_voucher_applied_count";

    private static final String LAST_WRONG_ATTEMPT_TIMESTAMP = "last_wrong_attempt_timestamp";

    private static final String LAST_TIME_SERVER_DOWNLOADED_TIMESTAMP = "last_time_server_downloaded_timestamp";

    private static final String IS_COUNTRY_SERVER_DOWNLOADE_ONCE = "is_country_server_downloaded_once";

    private static final String IS_UI_ENABLED = "is_ui_enabled";

    private static final String IS_PROVISINING_DONE = "is_provisining_done";

    private static final String CONNECTION_STATE = "connection_state";

    private static final String CONNECTION_WITH_SERVER = "connection_with_server";

    private static final String DEACTIVATION_TIME_STAMP = "deactivation_time_stamp";

    private static final String IS_LOGOUT_DONE = "is_logout_done";

    private static final String REMEMBER_ME = "remember_me";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor = pref.edit();
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.apply();
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLogin(boolean isLoggedIn) {
        editor = pref.edit();
        editor.putBoolean(IS_LOGGED_IN, isLoggedIn);
        editor.apply();
        editor.commit();
    }

    public void setIsLogin(boolean isLoggedIn) {
        editor = pref.edit();
        editor.putBoolean(LOGGED_IN, isLoggedIn);
        editor.apply();
        editor.commit();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(LOGGED_IN, false);
    }

    public boolean isFirstTimeLogin() {
        return pref.getBoolean(IS_LOGGED_IN, true);
    }

    public boolean getIsFirstTimeServerDownloaded() {
        return pref.getBoolean(IS_FIRST_TIME_SERVER_DOWNLOADED, false);
    }

    public void setIsFirstTimeServerDownloaded(boolean isDownloaded) {
        editor = pref.edit();
        editor.putBoolean(IS_FIRST_TIME_SERVER_DOWNLOADED, isDownloaded);
        editor.apply();
        editor.commit();
    }

    public String getMobileNumber() {
        return pref.getString(MOBILE_NUMBER, "");
    }

    public void setMobileNumber(String mobileNumber) {
        editor = pref.edit();
        editor.putString(MOBILE_NUMBER, mobileNumber);
        editor.apply();
        editor.commit();
    }

    public String getKey() {
        return pref.getString(KEY, "");
    }

    public void setKey(String password) {
        editor = pref.edit();
        editor.putString(KEY, password);
        editor.apply();
        editor.commit();
    }

    public void setIsValidityExpired(boolean isExpired) {
        editor = pref.edit();
        editor.putBoolean(IS_VALIDITY_EXPIRED, isExpired);
        editor.apply();
        editor.commit();
    }

    public boolean isValidityExpired() {
        return pref.getBoolean(IS_VALIDITY_EXPIRED, false);
    }


    public void setPasswordInvalid(boolean invalidPassword) {
        editor = pref.edit();
        editor.putBoolean(INVALID_PASSWORD, invalidPassword);
        editor.apply();
        editor.commit();
    }

    public boolean isPasswordInvalid() {
        return pref.getBoolean(INVALID_PASSWORD, false);
    }

    public void setMaxFailedAttempt(int maxFailedAttempt) {
        editor = pref.edit();
        editor.putInt(MAX_FAILED_RE_ATTEMPT, maxFailedAttempt);
        editor.apply();
        editor.commit();
    }

    public int getMaxFailedAttempt() {
        return pref.getInt(MAX_FAILED_RE_ATTEMPT, 0);
    }

    public void setKeepAliveTime(int keepAliveTime) {
        editor = pref.edit();
        editor.putInt(KEEP_ALIVE, keepAliveTime);
        editor.apply();
        editor.commit();
    }

    public void setDeactivationDate(String deactivationDate) {
        editor = pref.edit();
        editor.putString(DEACTIVATION_TIME, deactivationDate);
        editor.apply();
        editor.commit();
    }

    public String getDeactivationDate() {
        return pref.getString(DEACTIVATION_TIME, "");
    }


    public void setDeactivationTimeStamp(long deactivationDate) {
        editor = pref.edit();
        editor.putLong(DEACTIVATION_TIME_STAMP, deactivationDate);
        editor.apply();
        editor.commit();
    }

    public long getDeactivationTimeStamp() {
        return pref.getLong(DEACTIVATION_TIME_STAMP, System.currentTimeMillis());
    }

    public void setDomainNames(String deactivationDate) {
        editor = pref.edit();
        editor.putString(DOMAIN_NAME, deactivationDate);
        editor.apply();
        editor.commit();
    }

    public String getDomainNames() {
        return pref.getString(DOMAIN_NAME, "app.madeenavpn.com");
    }

    public int getKeepAliveTime() {
        return pref.getInt(KEEP_ALIVE, 0);
    }

    public void setUserName(String userName) {
        editor = pref.edit();
        editor.putString(USER_NAME, userName);
        editor.apply();
        editor.commit();
    }

    public String getUserName() {
        return pref.getString(USER_NAME, "");
    }

    public void setPassword(String password) {
        editor = pref.edit();
        editor.putString(PASSWORD, password);
        editor.apply();
        editor.commit();
    }

    public String getPassword() {
        return pref.getString(PASSWORD, "");
    }

    public void setWrongVoucherAppliedCount(int count) {
        editor = pref.edit();
        editor.putInt(WRONG_VOUCHER_APPLIED_COUNT, count);
        editor.apply();
        editor.commit();
    }

    public int getWrongVoucherAppliedCount() {
        return pref.getInt(WRONG_VOUCHER_APPLIED_COUNT, 0);
    }

    public void setLastWrongAttemptTimestamp(long timeStamp) {
        editor = pref.edit();
        editor.putLong(LAST_WRONG_ATTEMPT_TIMESTAMP, timeStamp);
        editor.apply();
        editor.commit();
    }

    public Long getLastWrongAttemptTimestamp() {
        return pref.getLong(LAST_WRONG_ATTEMPT_TIMESTAMP, 0);
    }

    public void setLastTimeServerDownloadedTimestamp(long timeStamp) {
        editor = pref.edit();
        editor.putLong(LAST_TIME_SERVER_DOWNLOADED_TIMESTAMP, timeStamp);
        editor.apply();
        editor.commit();
    }

    public long getLastTimeServerDownloadedTimestamp() {
        return pref.getLong(LAST_TIME_SERVER_DOWNLOADED_TIMESTAMP, 0);
    }

    public void setIsCountryServerDownloadeOnce(boolean isDownloaded) {
        editor = pref.edit();
        editor.putBoolean(IS_COUNTRY_SERVER_DOWNLOADE_ONCE, isDownloaded);
        editor.apply();
        editor.commit();
    }

    public boolean getIsCountryServerDownloadeOnce() {
        return pref.getBoolean(IS_COUNTRY_SERVER_DOWNLOADE_ONCE, false);
    }

    public void setIsUiEnabled(boolean isUiEnabled) {
        editor = pref.edit();
        editor.putBoolean(IS_UI_ENABLED, isUiEnabled);
        editor.apply();
        editor.commit();
    }

    public boolean getIsUiEnabled() {
        return pref.getBoolean(IS_UI_ENABLED, false);
    }

    public void clearPreferenceData() {
        editor = pref.edit();
        editor.clear().apply();
        editor.commit();
    }

    public boolean getIsProvisiningDone() {
        return pref.getBoolean(IS_PROVISINING_DONE, false);
    }

    public void setIsProvisiningDone(boolean isProvisiningDone) {
        editor = pref.edit();
        editor.putBoolean(IS_PROVISINING_DONE, isProvisiningDone);
        editor.apply();
        editor.commit();
    }


//    public int getConnectionState(){
//        return pref.getInt(CONNECTION_STATE,-1);
//    }
//
//    public void setConnectionState(int connectionState){
//        editor = pref.edit();
//        editor.putInt(CONNECTION_STATE, connectionState);
//        editor.apply();
//    }

    public ConnectActivity.VPN_CONNECTION_STATE getConnectionState() {
        ConnectActivity.VPN_CONNECTION_STATE connectionState = ConnectActivity.VPN_CONNECTION_STATE.getEnum(pref.getInt(CONNECTION_STATE, -1));
        return connectionState;
    }

    public void setConnectionState(ConnectActivity.VPN_CONNECTION_STATE connectionState) {
        editor = pref.edit();
        editor.putInt(CONNECTION_STATE, connectionState.getId());
        editor.apply();
        editor.commit();
    }

    public String getConnectionWithServer() {
        return pref.getString(CONNECTION_WITH_SERVER, "");
    }

    public void setConnectionWithServer(String serverName) {
        editor = pref.edit();
        editor.putString(CONNECTION_WITH_SERVER, serverName);
        editor.apply();
        editor.commit();
    }


    public void setIsLogoutDone(boolean isLogOutDone) {
        editor = pref.edit();
        editor.putBoolean(IS_LOGOUT_DONE, isLogOutDone);
        editor.apply();
        editor.commit();
    }

    public boolean isLogOutDone() {
        return pref.getBoolean(IS_LOGOUT_DONE, false);
    }

    public void setRememberMe(boolean doRemember) {
        editor = pref.edit();
        editor.putBoolean(REMEMBER_ME, doRemember);
        editor.apply();
        editor.commit();
    }

    public boolean isRemeberMeOn() {
        return pref.getBoolean(REMEMBER_ME, false);
    }

}
