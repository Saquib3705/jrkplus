package com.jrk_plus.Tunnel;

/**
 * Created by ABHISHEK MAHATO on 5/25/2017.
 */

public interface SettingModification {
    public boolean isSettingsChanged(boolean isModified);
}
