package com.jrk_plus.Tunnel;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

import opt.packet.Packet;
import opt.packet.ReceivedPacket;
import opt.packet.SendPacket;
import opt.selector.OmInterface;
import opt.selector.OmSelector;

import static com.jrk_plus.Tunnel.BaseTunnel.tunnelInterface;
import static opt.log.OmLogger.logger;

/**
 * Created by ABHISHEK MAHATO on 4/24/2017.
 */

public class MultiHttpInterface extends OmInterface {
    public static AtomicLong connectedSocket = new AtomicLong(0);
    public static AtomicLong receivedCounter = new AtomicLong(0);
    private int nRecv = 0;
    private boolean connectionStatus = false;
    public static Config config;
//    public static OutputStream streamToClient;
    public static DatagramSocket client;
    Http http;

    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }

    public void setClientAddress(InetAddress clientAddress) {
        this.clientAddress = clientAddress;
    }

    InetAddress clientAddress;
    int clientPort;


    public MultiHttpInterface(boolean isSecured, int special) {
        super(isSecured, special);
        http = new Http(Http.HttpHeaderType.HTTP_HEADER_1);
    }

    public static void setDetails(Config conf, DatagramSocket clientSocket) {
        config = conf;
//        streamToClient = clientOutputStream;
        client = clientSocket;
    }

    @Override
    public void processData(ReceivedPacket rp) {
        byte[] reply;
        byte[] dataToReceive;
        logger.info("---------Received Data of length [" + rp.receivedLength + "] from [" + rp.remoteAddress + "]------");
        receivedCounter.incrementAndGet();
        nRecv++;
        logger.info("******** Stats ..Connected Count [" + MultiHttpInterface.connectedSocket.get() + "] and receivedCounter [" + MultiHttpInterface.receivedCounter.get() + "]");

//        logger.info(new String(rp.buffer));
        logger.info("SET HTTP MSG : " + http.setHttpMsg(rp));
        rp.reset();
        logger.info("UNWRAP : " + http.httpUnWrapClientSide(rp));
        reply = Arrays.copyOfRange(rp.buffer, rp.bufferOffset, rp.bufferOffset + rp.length());

//        logger.info(Proxy.printHexDump(reply, reply.length));

        try {
//            dataToReceive = HttpWrapUnwrap.httpUnwrapClientSide(reply);
//            streamToClient.write(reply, 0, reply.length);
//            streamToClient.flush();
            DatagramPacket packet = new DatagramPacket(reply, reply.length);
            packet.setAddress(clientAddress);
            packet.setPort(clientPort);

            client.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        if (nRecv == config.getClientMppc())
//            this.close();
    }

    @Override
    public void sendData(byte[] bytes, int i, int i1) {
        logger.info("SID [" + getId() + "] Sending Data Length [" + i1 + "]");
        SendPacket sendPacket = new SendPacket(8192);
        sendPacket.sendToIp = connectIp;
        sendPacket.sendToPort = connectPort;
        sendPacket.appendInEnd(bytes, i, i1);
        addDataForSending(sendPacket);
    }

    @Override
    public void sendData(SendPacket sp) {
        logger.info("SID [" + getId() + "] Sending Data Length [" + sp.length() + "]");
        addDataForSending(sp);
    }

    @Override
    public void receiveTimeout() {

    }

    @Override
    public void socketConnected() {
        logger.info("Connection estabished");

        String serverIp = config.getServerIp();
        int serverPort = config.getServerPort();

        byte[] firstPacket = Information.createFirstPacket();
        firstPacket = Information.addHeader(firstPacket, serverIp, serverPort,
                ConfigurationConstants.PROTO_UDP, ConfigurationConstants.ACTION_FORWARD);

        Packet httpPacket = new Packet(4096);
        Packet sendPacket = new Packet(firstPacket, 0, firstPacket.length);
        http.httpWrapClientSide(sendPacket, httpPacket);

//        firstPacket = HttpWrapUnwrap.httpWrapClientSide(firstPacket, firstPacket.length);
        this.sendData(httpPacket.buffer, httpPacket.bufferOffset, httpPacket.length());
        this.connectionStatus = true;
    }

    @Override
    public void close() {
        super.close();
        String bindIp = "0.0.0.0";
        String remoteIp = config.getRemoteIp();
        int remotePort = config.getRemotePort();
        SocketChannel socketChannel = null;
        try {
            socketChannel = SocketChannel.open();
            socketChannel.socket().bind(new InetSocketAddress(bindIp, 0));
        } catch (IOException e) {
            e.printStackTrace();
        }
        int j = getSpecialMeaning();
        tunnelInterface[j] = new MultiHttpInterface(false, j);
        tunnelInterface[j].setReceiveTimeout(60);
        tunnelInterface[j].setSocketCloseTimeout(3000);
        tunnelInterface[j].setConnectIpPort(remoteIp, remotePort);
        logger.info("Socket : [" + j + "] disconnected");
        logger.info("Reconnecting to [" + remoteIp + ":" + remotePort + "]");
        OmSelector.registerRequest(socketChannel, tunnelInterface[j]);

    }

    public boolean isConnected() {
        return this.connectionStatus;
    }

}
