package com.jrk_plus.Tunnel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.jrk_plus.Validity.ValidityUpdationActivity;
import com.jrk_plus.utilities.PrefManager;
import com.app.jrk_plus.R;


import static opt.log.OmLogger.logger;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {
    private FrameLayout frameLayoutSetting;
    private SettingModification settingModification;
    private static boolean isModified = false;
    private RelativeLayout validity_layout;
    private TextView validityText;
    private ProgressBar progressBar;

    public static boolean isModified() {
        return isModified;
    }

    public static void setIsModified(boolean isModified) {
        SettingsActivity.isModified = isModified;
    }

    @SuppressWarnings("ConstantConditions")
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        PrefManager prefManager = new PrefManager(this);
        Toolbar toolbarCustom = (Toolbar) this.findViewById(R.id.custom_toolbar);
        frameLayoutSetting = (FrameLayout) this.findViewById(R.id.frame_layout_setting_activity);
        validity_layout = (RelativeLayout)findViewById(R.id.validity_layout);
        validityText = (TextView)this.findViewById(R.id.day_left_text);
        progressBar = (ProgressBar)this.findViewById(R.id.waiting_expiry_date);
        progressBar.setVisibility(View.GONE);
        validity_layout.setOnClickListener(this);
        validityText.setText(prefManager.getDeactivationDate());
        setSupportActionBar(toolbarCustom);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        getSupportActionBar().setTitle("");
        toolbarCustom.setTitleTextColor(Color.WHITE);

        toolbarCustom.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        SettingsFragment settingsFragment = new SettingsFragment();
        transaction.replace(R.id.frame_layout_setting_activity,settingsFragment);
        transaction.commit();

    }

    @Override
    public void onClick(View view) {
        if(view == validity_layout){
            openValidityUpdationActivity();
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

        private static final String USERNAME = "pref_key_vpn_username";
        private static final String PASSWORD = "pref_key_password";
        private static final String TAG = SettingsFragment.class.getSimpleName();

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Preference preferenceUsername = findPreference(USERNAME);
            Preference preferencePassword = findPreference(PASSWORD);


            SharedPreferences preferencesCredential = getPreferenceManager().getSharedPreferences();

            preferenceUsername.setSummary(preferencesCredential.getString("pref_key_vpn_username",""));

            //preferencePassword.setSummary(preferencesCredential.getString(PASSWORD,""));
            if (!preferencesCredential.getString(PASSWORD,"").isEmpty()) {
                preferencePassword.setSummary("*****");
            }

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

            View view = super.onCreateView(inflater, container, savedInstanceState);

            /*assert view != null;
            view.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.home_screen_background));*/

            return view;
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals(USERNAME)) {
                Preference preferenceUsername = findPreference(key);
                preferenceUsername.setSummary(sharedPreferences.getString(key,""));
                isModified = true;
                 logger.info(TAG, "onSharedPreferenceChanged: UserNameChanged");
            } else if (key.equals(PASSWORD)) {
                Preference preferencePassword = findPreference(key);
                preferencePassword.setSummary("*****");
                isModified = true;
                 logger.info(TAG, "onSharedPreferenceChanged: PasswordChanged");
            }
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setHasOptionsMenu(true);
            addPreferencesFromResource(R.xml.app_settings_preference);
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }
    }

    @Override
    public void onBackPressed() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!preferences.getString("pref_key_vpn_username","").isEmpty() && !preferences.getString("pref_key_password","").isEmpty()) {
            super.onBackPressed();
        } else {
            Snackbar.make(frameLayoutSetting,"Username or Password Can't be Empty!", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void openValidityUpdationActivity(){
        Intent intent = new Intent(SettingsActivity.this, ValidityUpdationActivity.class);
        startActivity(intent);
    }

}
