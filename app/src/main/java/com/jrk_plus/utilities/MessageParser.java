package com.jrk_plus.utilities;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by bot on 08/01/2018.
 */

public class MessageParser {

    public JSONObject json_obj              = null;
    public boolean isJssonPresent           = false;
    public boolean error                    = true;
    public String result                    = "xxoxoxoxxo";
    public int MsgType                      = 555555555;
    public String mobileNumber              = "";
    public String key                       = "";
    public long validityInfo                = 0000000;
    public long errorCode                   = 0000000;

    public MessageParser(String message){
        try {
            json_obj = new JSONObject(message);
            isJssonPresent = true;
            switch (json_obj.getInt(MessageConstatns.MSG_TYPE)) {
                case MessageConstatns.REGISTER_RESPONSE :
                    parseAuthenticationResponse(json_obj);
                    break;

                case MessageConstatns.PING_RESPONSE :
                    parsePingResponse(json_obj);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseAuthenticationResponse(JSONObject jsonObject) throws JSONException {
        this.MsgType = json_obj.getInt(MessageConstatns.MSG_TYPE);
        JSONObject MsgContent = new JSONObject(json_obj.getString(MessageConstatns.MSG_CONTENT));
        this.error = MsgContent.getBoolean(MessageConstatns.ERROR);
        this.result = MsgContent.getString(MessageConstatns.RESULT);
        if(this.error){
            this.errorCode = MsgContent.getLong(MessageConstatns.ERROR_CODE);
        }else {
            this.mobileNumber = MsgContent.getString(MessageConstatns.MOBILE_NUMBER);
            this.key = MsgContent.getString(MessageConstatns.KEY);
        }
    }

    private void parsePingResponse(JSONObject jsonObject) throws JSONException{
        this.MsgType = jsonObject.getInt(MessageConstatns.MSG_TYPE);
        JSONObject MsgContent = new JSONObject(json_obj.getString(MessageConstatns.MSG_CONTENT));
        this.error = MsgContent.getBoolean(MessageConstatns.ERROR);
        if(this.error){
            this.errorCode = MsgContent.getLong(MessageConstatns.ERROR_CODE);
        }else{
            this.validityInfo = MsgContent.getLong(MessageConstatns.VALIDITY_INFO);
        }
    }
}
