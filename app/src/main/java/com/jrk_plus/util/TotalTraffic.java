package com.jrk_plus.util;

import android.content.Context;
import android.content.Intent;

import com.jrk_plus.utilities.MessageConstatns;

import java.util.ArrayList;
import java.util.List;

import de.blinkt.openvpn.core.OpenVPNManagement;
import de.blinkt.openvpn.core.OpenVPNService;

import static com.jrk_plus.Tunnel.ConfigurationConstants.CONNECT_ACTIVITY_BROADCAST_ACTION;

public class TotalTraffic {

    public static final String TRAFFIC_ACTION = "traffic_action";

    public static final String DOWNLOAD_ALL = "download_all";
    public static final String DOWNLOAD_SESSION = "download_session";
    public static final String UPLOAD_ALL = "upload_all";
    public static final String UPLOAD_SESSION = "upload_session";

    public static long inTotal;
    public static long outTotal;


    public static void calcTraffic(Context context, long in, long out, long diffIn, long diffOut) {
        List<String> totalTraffic = getTotalTraffic(context,diffIn, diffOut);

        Intent traffic = new Intent();
        traffic.setAction(CONNECT_ACTIVITY_BROADCAST_ACTION);
        traffic.putExtra(MessageConstatns.ID,4);

//        String inVoulume =  OpenVPNService.humanReadableByteCount(in, false);
        String inVoulume =  OpenVPNService.humanReadableByteCount(in, false);
        String inSpeed = OpenVPNService.humanReadableByteCount(diffIn / OpenVPNManagement.mBytecountInterval, true).concat("/s");
        String outVolume = OpenVPNService.humanReadableByteCount(out, false);
        String outSpeed = OpenVPNService.humanReadableByteCount(diffOut / OpenVPNManagement.mBytecountInterval, true).concat("/s");

        traffic.putExtra(DOWNLOAD_ALL, inSpeed);
        traffic.putExtra(DOWNLOAD_SESSION, inVoulume);
        traffic.putExtra(UPLOAD_ALL, outSpeed);
        traffic.putExtra(UPLOAD_SESSION, outVolume);

//        traffic.putExtra(DOWNLOAD_ALL, totalTraffic.get(0));
//        traffic.putExtra(DOWNLOAD_SESSION, OpenVPNService.humanReadableByteCount(in, false));
//        traffic.putExtra(UPLOAD_ALL, totalTraffic.get(1));
//        traffic.putExtra(UPLOAD_SESSION, OpenVPNService.humanReadableByteCount(out, false));

        context.sendBroadcast(traffic);
    }

    public static List<String> getTotalTraffic(Context context) {
        return getTotalTraffic(context,0, 0);
    }

    public static List<String> getTotalTraffic(Context context,long in, long out) {
        List<String> totalTraffic = new ArrayList<String>();

        if (inTotal == 0)
            inTotal = PropertiesService.getDownloaded();

        if (outTotal == 0)
            outTotal = PropertiesService.getUploaded();

        inTotal = inTotal + in;
        outTotal = outTotal + out;

        totalTraffic.add(OpenVPNService.humanReadableByteCount(inTotal, false));
        totalTraffic.add(OpenVPNService.humanReadableByteCount(outTotal, false));

        return totalTraffic;
    }

    public static void saveTotal() {
        if (inTotal != 0)
            PropertiesService.setDownloaded(inTotal);

        if (outTotal != 0)
            PropertiesService.setUploaded(outTotal);
    }

}
