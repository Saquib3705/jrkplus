package com.jrk_plus.NewDashboard;

import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.app.jrk_plus.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InitialisingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InitialisingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InitialisingFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private RelativeLayout initialisingContainer ,validityExpiredContainer;
    private Button voucherApplyButton;

    public InitialisingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InitialisingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InitialisingFragment newInstance(String param1, String param2) {
        InitialisingFragment fragment = new InitialisingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_initialising, container, false);
        setEventListner(view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        if(view == voucherApplyButton){
//            openValidityExpiredActivity();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void setEventListner(View view){
        initialisingContainer                   = view.findViewById(R.id.initializing_container);
        validityExpiredContainer                = view.findViewById(R.id.validity_expired_container);
        voucherApplyButton                      = view.findViewById(R.id.voucher_apply_button);

        voucherApplyButton.setOnClickListener(this);
    }

    public void gotValidationExpired(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initialisingContainer.setVisibility(View.GONE);
                validityExpiredContainer.setVisibility(View.VISIBLE);
            }
        });

    }

    private void openValidityExpiredActivity(){
//        Intent intent = new Intent(getActivity(), ValidityUpdationActivity.class);
//        startActivity(intent);
    }
}
