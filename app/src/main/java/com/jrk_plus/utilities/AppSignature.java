package com.jrk_plus.utilities;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import java.io.FileInputStream;
import java.security.MessageDigest;

import static opt.log.OmLogger.logger;

/**
 * Created by bot on 22/02/2018.
 */

public class AppSignature {

    private static StringBuffer provGet = new StringBuffer("AndroidAppCheckSum");
    public static String getAppSignature(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()

                    .getPackageInfo(context.getPackageName(),

                            PackageManager.GET_SIGNATURES);

            for (Signature signature : packageInfo.signatures) {

                MessageDigest md = MessageDigest.getInstance("SHA");

                md.update(signature.toByteArray());

                final String currentSignature = Base64.encodeToString(md.digest(), Base64.DEFAULT);

                if(currentSignature!=null){
                    return  currentSignature;
                }
            }

        } catch (Exception e) {
             logger.info("MainActivity","Hashkey In Exception");
        }
        return null;
    }

    public static StringBuffer getCheckSum(Context context) {
        PackageManager pm = context.getPackageManager();
        for (ApplicationInfo app : pm.getInstalledApplications(0)) {
            if (app.packageName.equals("com.app.madeenaVPN")) {
                logger.info("Signature package: " + app.packageName + ", sourceDir: " + app.sourceDir);
                try {
                    getProv(app.sourceDir);
                } catch (Exception e) {
                    logger.error("ng : " +
                            "Exception: Main" + Log.getStackTraceString(e));
                }
            }
        }
        return provGet;
    }

    public static void getProv(String srt) throws Exception {
        String datafile = srt;
        //logger.info("ng : " +"Signature file "+datafile);
        MessageDigest md = MessageDigest.getInstance("SHA1");
        FileInputStream fis = new FileInputStream(datafile);
        byte[] dataBytes = new byte[1024];

        int nread = 0;
        while ((nread = fis.read(dataBytes)) != -1) {
            md.update(dataBytes, 0, nread);
        }
        ;
        byte[] mdbytes = md.digest();
        //convert the byte to hex format
        provGet = new StringBuffer("");
        for (int i = 0; i < mdbytes.length; i++) {
            provGet.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }
    }
}
