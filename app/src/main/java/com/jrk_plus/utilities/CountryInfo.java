package com.jrk_plus.utilities;

/**
 * Created by bot on 25/01/2018.
 */

public class CountryInfo {
    String countryName;
    String shortCountryName;

    public CountryInfo(String countryName, String shortCountryName) {
        this.countryName = countryName;
        this.shortCountryName = shortCountryName;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getShortCountryName() {
        return shortCountryName;
    }
}
