/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jrk_plus.Provisioning;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 *
 * @author Netgear
 */
public class Constants {

    
     public static final String CONFIG_FILE_NAME = "VPNConfigurationFile.cfg";
     /*
     Config File Details
     */
     public static final String WORKER_THREADS_COUNT ="WorkerCount";
      public static final String COMMUNICATION_MODE ="CommunicationMode";
      public static final String UDP_BS2_LISTENER = "udpbs2listenerip";
      public static final String UPD_LISTENER_PORT = "udpbs2listenerport";
      
      
    public final static int LISTENING_NETWORK_TYPE_BS2_UDP = 1;
    public final static int MINIMUM_PACKET_LEN = 4;
    public static final String DATA_LOG_DATE_FORMAT = "MMM dd, HH:mm";

    public static final int MSG_TYPE_DIALER_REQUEST_BS2 = 10;
    public static final int REQUEST_TYPE_DIALER_BS2 = 11;
    public static final int ATTR_TYPE_TRANSACTION_ID = 12;
    public static final int ATTR_TYPE_DIALER_IP = 13;
    public static final int ATTR_TYPE_BS1_PACKET = 14;
    public static final int MSG_TYPE_DIALER_RESPONSE_BS2 = 20002;

//   CREATE TABLE IF NOT EXISTS `VPN`.`UserDetails` (
//  `USerDetailsID` INT NOT NULL AUTO_INCREMENT,
//  `UserName` VARCHAR(45) NULL,
//  `Password` VARCHAR(45) NULL,
//  `UserCreationTime` BIGINT(20) NULL,
//  `UserExipryTime` BIGINT(20) NULL,
//  `UserActivationTime` BIGINT(20) NULL,
//  `ServiceActivationTime` BIGINT(20) NULL,
//  `ServiceDeactivationTime` BIGINT(20) NULL,
//  `BatchNumber` INT(11) NULL,
//  `ResellerID` INT(11) NULL,
//  `MaxDevicesAllowed` INT(11) NULL,
//  `UserStatus` TINYINT(4) NULL,
//  `PinType` SMALLINT(6) NULL,
//  `Imei` VARCHAR(150) NULL,
//  PRIMARY KEY (`USerDetailsID`))
//ENGINE = InnoDB;
    
    
    //Database Feilds Name USERDETAILS
    /*
     Username is the key value in the map UaserDetailsMap.
     check if userexpirytime > curr_time and servicedeactivation > curr_time and userstatus is enabled ... if pin type is licensed no expiry
     is calulated only status is checked.
     */
    public static final String USERDETAILSTABLE ="UserDetails";
    public static final String PASSWORD = "Password";
    public static final String USERNAME = "UserName";
    public static final String USERCREATIONTIME = "UserCreationTime";
    public static final String USEREXIPRYTIME = "UserExipryTime";
    public static final String USERACTIVATIONTIME = "UserActivationTime";
    public static final String SERVICEACTIVATIONTIME = "ServiceActivationTime";
    public static final String SERVICEDEACTIVATIONTIME = "ServiceDeactivationTime";
    public static final String BATCHNUMBER = "BatchNumber";
    public static final String RESELLERID = "ResellerID";
    public static final String MAXDEVICESALLOWED = "MaxDevicesAllowed";
    public static final String USERSTATUS = "UserStatus";
    public static final String PINTYPE = "PinType";
    public static final String IMEI = "Imei";
    public static final String LASTUPDATEDTIME ="lastUpdatedTime";

//    CREATE TABLE IF NOT EXISTS `VPN`.`ServerDetails` (
//  `ServerDetailsID` INT NOT NULL AUTO_INCREMENT,
//  `ServerName` VARCHAR(45) NULL,
//  `ServerIP` BIGINT(20) NULL,
//  `ServerPortAndProtocol` VARCHAR(50) NULL,
//  `ServerStatus` TINYINT(6) NULL)
//ENGINE = InnoDB;
    
     //Database Feilds Name SERVERDETAILS
    /*
    Server Ip and port is ip and port of SIP proxy running on different protocol.
    if status is enabled send IP Port and Protocol.
    */
    public static final String SERVERDETAILSTABLE="ServerDetails";
    public static final String SERVERNAME = "ServerName";
    public static final String SERVERIP = "ServerIP";
    public static final String SERVERPORTANDPROTOCOL = "ServerPortAndProtocol";
    public static final String SERVERSTATUS = "ServerStatus";
    public static final String SERVERDETAILSID  = "ServerDetailsID";
    
    /*
    Properties Table
    */
    public static final String DNSLIST = "dnslist";
    
    /*
    CheckSum Details Table
    */
    public static final String CHECKSUMDETAILSTABLE= "CheckSumDetails";
    
    /*
    Database Queries
    */
     public static final String UPDATE_IMEI_USERDETAILS_TABLE = " update UserDetails set imei=? where UserName = ?";
     public static final String FETCH_ALL_RECORDS_MODIFIED_AFTER_LAST_MODF_ID = "select * from ModificationDetails where NextSequenceId > ?";
     public static final String FETCH_SINGLE_ROW_SERVER_DETAILS = "select * from ServerDetails where ServerDetailsID = ?";
     public static final String FETCH_SINGLE_ROW_USER_DETAILS = "select * from UserDetails where UserName = ?";
     public static final String FETCH_ALL_DETAILS_PROPERTIESTABLE = "Select * from PropertiesTable";
      public static final String FETCH_SINGLE_ROW_DETAILS_PROPERTIESTABLE = "Select * from PropertiesTable where key = ? ";
      public static final String UPDATE_USER_DETAILS_EXIPRY_DATE = "update UserDetails set ServiceDeactivationTime=? where UserName = ?";
      public static final String UPDATE_USER_DETAILS_ACTIVATION_DATE = " update UserDetails set ServiceActivationTime=? where UserName = ?";

     /*
     Database Atributes
     */
    public static final String ENABLED = "enabled";
    public static final String COUNT = "Count";
    public static final String TABLE_NAME = "TableName";
    public static final String PRIMARY_KEY = "PrimaryKey";
    public static final String ACTION = "Action";
    public static final String MODF_ID = "NextSequenceId";
    public static final String CURRENT_TIME = "CurrentTime";

    public static final int CONFIGURATION_REQUEST = 20001;
    public static final int CONFIGURATION_RESPONSE = 20002;


    public static String catchExceptionStackTrace(Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }
}
