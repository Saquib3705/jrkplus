package com.jrk_plus.Validity;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;



import com.jrk_plus.utilities.CountryInfo;
import com.app.jrk_plus.R;


import java.util.ArrayList;

import static opt.log.OmLogger.logger;

/**
 * Created by bot on 25/01/2018.
 */

public class CountryListAdapter extends RecyclerView.Adapter<CountryListAdapter.MyViewHolder>{

    private Context mContext;
    private CountryListAdapter.AdapterCallback mAdapterCallback;
    private ArrayList<CountryInfo> list = new ArrayList<>();

    public CountryListAdapter(Context mContext , ArrayList<CountryInfo> list , CountryListAdapter.AdapterCallback mListner) {
        this.mContext = mContext;
        this.list = list;
        this.mAdapterCallback = mListner;
    }

    @Override
    public CountryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.country_list_row, parent, false);

        return new CountryListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CountryListAdapter.MyViewHolder holder, int position) {
        CountryInfo countryInfo = list.get(position);
        configureRow(holder ,countryInfo.getCountryName(),countryInfo.getShortCountryName(),position);
    }

    @Override
    public int getItemCount() {
        return (list == null) ? 0 : list.size();
    }

    public  interface AdapterCallback {
        void onMethodCallback(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView countryFlag;
        private TextView countryName,positionText;
        private CheckBox favouriteCheckBox;
        private RelativeLayout countryRowClickable;

        public MyViewHolder(View view) {
            super(view);
            countryFlag = (ImageView)view.findViewById(R.id.country_flag);
            countryName =(TextView)view.findViewById(R.id.country_name);
            favouriteCheckBox = (CheckBox)view.findViewById(R.id.favourite_check_box);
            countryRowClickable = (RelativeLayout)view.findViewById(R.id.country_row_clickable);
            positionText = (TextView)view.findViewById(R.id.position_text);
            countryRowClickable.setOnClickListener(this);
        }

        public ImageView getCountryFlag() {
            return countryFlag;
        }

        public TextView getCountryName() {
            return countryName;
        }

        public CheckBox getFavouriteCheckBox() {
            return favouriteCheckBox;
        }

        public TextView getPositionText() {
            return positionText;
        }

        @Override
        public void onClick(View view) {
            if(view == countryRowClickable) {
                mAdapterCallback.onMethodCallback(Integer.parseInt(positionText.getText().toString()));
            }
        }
    }
    private void configureRow(CountryListAdapter.MyViewHolder holder , String countryName , String countryShortName, int position){
        //set values in row
        if(countryShortName.equals("DO"))
            countryShortName="dom";
         logger.info("ng","Short country name =="+countryShortName);
        holder.getCountryName().setText(countryName);
        holder.getCountryFlag()
                .setImageResource(
                        mContext.getResources().getIdentifier(countryShortName.toLowerCase(),
                                "drawable",
                                mContext.getPackageName()));
        holder.getPositionText().setText(Integer.toString(position));
    }

}
