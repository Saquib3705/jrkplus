package com.jrk_plus.Tunnel;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import static opt.log.OmLogger.logger;

//import com.github.terma.javaniotcpserver.TcpServer;
//import com.github.terma.javaniotcpserver.TcpServerConfig;

/**
 * Created by ABHISHEK MAHATO on 3/24/2017.
 */

public class ProxyUDP implements Runnable {

    int localPort;
    static boolean isFirstPacket = true;
    public static ServerSocket ss;

    static InetAddress clientAddress;
    static int clientPort;

    public ProxyUDP(int port) {
        this.localPort = port;
    }


    @Override
    public void run() {
        try {
            String host = "162.222.185.40";
            int remoteport = 220;
            // Print a start-up message
            System.out.println("Starting proxy for " + host + ":" + remoteport
                    + " on port " + localPort);
            // And start running the server
            runServer(host, remoteport, localPort); // never returns
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static String printHexDump(byte[] bytes, int len) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++) {
                if (i % 20 == 0) {
                    sb.append("\n");
                }
                sb.append(String.format("%02x ", bytes[i] & 0xff));
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error("Ex[printHexDump]: ", e);
        }
        return "failed to print hexdump";
    }

    public static byte[] addHeader(byte[] info, int length, String ip, int port, int protocol, int action) {
        try {
             logger.info("ng", "Stuffing SIP Proxy Header");
             logger.info("ng", "Original Length : " + length);
            byte[] data = null;
            int i = 0;

            int len = 0;
            if (isFirstPacket)
                len = length + 8;
            else
                len = length;

//             logger.info("ng", "PROTO_TCP : Adding two bytes of data");
            data = new byte[len];
//            data[i++] = (byte) ((len >> 8) & 0xFF);
//            data[i++] = (byte) (len & 0xFF);

            if (isFirstPacket) {
                String[] labels = ip.split("\\.");

                for (int j = 0; j < labels.length; j++)
                    data[i++] = (byte) Integer.parseInt(labels[j]);

                data[i++] = (byte) ((port >> 8) & 0xFF);
                data[i++] = (byte) (port & 0xFF);

                data[i++] = (byte) (protocol & 0xFF);
                data[i++] = (byte) (action & 0xFF);
                isFirstPacket = false;
            }

             logger.info("ng", "Value of i : " + i);

            if (action == ConfigurationConstants.ACTION_PROVISIONING_PROXY)
                 logger.info("ng", "Provisioning Proxy Request");
            else if (action == ConfigurationConstants.ACTION_BALANCE_PROXY)
                 logger.info("ng", "Balance Proxy Request");
            else if (action == ConfigurationConstants.ACTION_FORWARD)
                 logger.info("ng", "ACTION_FORWARD");
            System.arraycopy(info, 0, data, i, length);
             logger.info("ng", "Length of data copied: " + length + " bytes");
             logger.info("ng", "Final Length : " + data.length);
            return data;
        } catch (Exception e) {
            Log.e("ng", "Add Header Exception:" + Log.getStackTraceString(e));
        }
        return null;
    }

    public static void runServer(String host, int remoteport, int localport)
            throws IOException {
        // Create a ServerSocket to listen for connections with
        byte[] arr = new byte[4096];
        final DatagramPacket request = new DatagramPacket(arr, 4096);
        byte[] reply = new byte[4096];

        Socket server = null;
        final DatagramSocket client = new DatagramSocket(localport, InetAddress.getLocalHost());
//        ss = new ServerSocket(localport, 50, InetAddress.getLocalHost());

//        // Wait for a connection on the local port
//        client = ss.accept();
//        client.setTcpNoDelay(true);
//
//        final InputStream streamFromClient = client.getInputStream();
//        final OutputStream streamToClient = client.getOutputStream();

        try {
            server = new Socket(host, remoteport);
            server.setTcpNoDelay(true);
            isFirstPacket = true;
        } catch (IOException e) {
             logger.info("ng", Log.getStackTraceString(e));
        }

        // Get server streams.
        final InputStream streamFromServer = server.getInputStream();
        final OutputStream streamToServer = server.getOutputStream();

        while (true) {

            try {
//                client = ss.accept();
//                client.setTcpNoDelay(true);

//                final InputStream streamFromClient = client.getInputStream();
//                final OutputStream streamToClient = client.getOutputStream();
                // a thread to read the client's requests and pass them
                // to the server. A separate thread for asynchronous.
                Thread t = new Thread() {
                    public void run() {
                        int bytesRead;
                        while (true) {
                            try {
//                            while ((bytesRead = client.receive(request).read(request)) != -1) {
                                client.receive(request);
                                 logger.info("ng", "Data received from client : " + printHexDump(request.getData(), request.getLength()));
                                byte[] dataToSend = addHeader(request.getData(), request.getLength(), "88.150.226.226",
                                        1194, ConfigurationConstants.PROTO_UDP,
                                        ConfigurationConstants.ACTION_FORWARD);
                                streamToServer.write(dataToSend, 0, dataToSend.length);

                                clientAddress = request.getAddress();
                                clientPort = request.getPort();
//                                 logger.info("ng", "Data sent to server : " + printHexDump(dataToSend, dataToSend.length));
                                 logger.info("ng", "Bytes sent to server : " + dataToSend.length);

                                streamToServer.flush();
//                            }
                            } catch (IOException e) {
                                 logger.info("ng", Log.getStackTraceString(e));
                            }
                        }
                    }
                };
                // Start the client-to-server request thread running
                t.start();

                // Read the server's responses
                // and pass them back to the client.
                int bytesRead;
                try {
                    while ((bytesRead = streamFromServer.read(reply)) != -1) {
                         logger.info("ng", "Data received from server : " + printHexDump(reply, bytesRead));
                        DatagramPacket packet = new DatagramPacket(reply, bytesRead);
                        packet.setAddress(clientAddress);
                        packet.setPort(clientPort);

                        client.send(packet);
                    }
                } catch (IOException e) {
                     logger.info("ng", Log.getStackTraceString(e));
                }
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }

    public static void stopServer() {
        try {
            ss.close();
        } catch (IOException e) {
            Log.e("ng", "Proxy socket close error : " + Log.getStackTraceString(e));
        }
    }
}
