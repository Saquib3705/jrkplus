package com.jrk_plus.TransportLayer;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.jrk_plus.Tunnel.DnsResolverAPI;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.jrk_plus.DnsClass.DnsClientJava.T_A;
import static com.jrk_plus.TransportLayer.OkktpHttpRequest.setSNIHost;
import static opt.log.OmLogger.logger;

public class HttpRequestClass {

    private static final String TAG = HttpRequestClass.class.getName();
    private static int LOGIN_REQUEST = 1;
    private static int OTHER = 2;
    private static int PROFILE_UPDATE = 3;
    public Context context;
    String response = "";
    private HttpResponseEvents events;

    public HttpRequestClass(Context context) {
        this.context = context;
    }

    public void sendHttpsRequest(final String url, final int requestFor, final HttpResponseEvents events, final String object, final JSONObject jsonObject) {

        this.events = events;
        AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                // configure the SSLContext with a TrustManager
                TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                    }
                }
                };
                // Install the all-trusting trust manager
                try {
                    SSLContext sc = null;
                    sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new SecureRandom());

                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

                    // Create all-trusting host name verifier
                    HostnameVerifier allHostsValid = new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    };
                    // Install the all-trusting host verifier
                    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
                    String httpsURL = url;
//                    String httpsURL = "https://192.151.152.170:8443/AuthServerVPN-1.0.0/rest/verifyNumber/+918588085113/EMAWeVE9Mg3i2zIJBjNzkAEsZAwq1hzGZB5ZA043ZC0leGDAUyi6W9IU7oCjXYrF3vplWpUuzZC1c8I3GZCa1UKDk8LhQIp0LsY4cmIYPbPEsAZDZD";

                    //  String httpsURL = "https://212.84.160.112:9876/test";
                    URL myurl = null;

                    myurl = new URL(httpsURL);
                    HttpsURLConnection con = null;
                    con = (HttpsURLConnection) myurl.openConnection();
                    con.setConnectTimeout(5000);
                    con.setRequestMethod("POST");
                     logger.info("HttpsConnection", "Url == " + url);
                     logger.info("HttpsConnection", "myUrl == " + myurl);

                    con.setRequestProperty("User-Agent", "androidVPN");
                    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                    con.addRequestProperty("Content-Type", "application/json");

                    JSONObject obj_send = null;
                    obj_send = new JSONObject();
                    if (requestFor == LOGIN_REQUEST) {
                        try {
                            String urlParameters = jsonObject.toString();
                            // Send post request
                            con.setDoOutput(true);
                            DataOutputStream wr = null;
//                            Uri.Builder builder = new Uri.Builder()
//                                    .appendQueryParameter("body", urlParameters);
                            //  String query = builder.build().getEncodedQuery();
                            //  wr = new DataOutputStream(con.getOutputStream());
                            //  wr.writeBytes(query);
                            // wr.writeBytes(urlParameters);
                            con.setRequestProperty("Content-Length", Integer.toString(urlParameters.length()));
                            con.getOutputStream().write(urlParameters.getBytes("UTF8"));
                            wr.flush();
                            wr.close();
                        } catch (Exception ex) {
                             logger.info(TAG, "Inside second last exception");
                            ex.printStackTrace();
                        }
                    } else if (requestFor == OTHER) {

                        // Send post request
                        con.setDoOutput(true);
                        DataOutputStream wr = null;
                        wr = new DataOutputStream(con.getOutputStream());
                        wr.flush();
                        wr.close();
                    } else if (requestFor == PROFILE_UPDATE) {
                        //obj_send = object;
                        String urlParameters = object;
                        // Send post request
                        con.setDoOutput(true);
                        DataOutputStream wr = null;
                        Uri.Builder builder = new Uri.Builder()
                                .appendQueryParameter("m", urlParameters);
                        String query = builder.build().getEncodedQuery();
                        wr = new DataOutputStream(con.getOutputStream());
                        wr.writeBytes(query);
                        // wr.writeBytes(urlParameters);
                        wr.flush();
                        wr.close();
                    }
                    int responseCode = 0;
                    responseCode = con.getResponseCode();
                    System.out.println("\nSending 'POST' request to URL : " + httpsURL);
                    // System.out.println("Post parameters : " + urlParameters);
                    System.out.println("Response Code : " + responseCode);
                    InputStream ins = null;
                    ins = con.getInputStream();
                    InputStreamReader isr = new InputStreamReader(ins);
                    BufferedReader in = new BufferedReader(isr);
                    String inputLine;

                    while ((inputLine = in.readLine()) != null) {
                        System.out.println(inputLine);
                        response = response + inputLine;
                    }
                     logger.info(TAG, "The input line  =====" + response);
                     logger.info(TAG, "Response ====" + response);
                    //As i got the response from the Http server i am sending the response string to the activity
                    if (responseCode == 200) {
                        if (requestFor == LOGIN_REQUEST) {
                            //Fire callback for getting the response from http server
                            if (events != null)
                                events.httpResponseForLogin(response);

                        } else if (requestFor == OTHER) {
                            //Fire callback for getting the response from http server
                            if (events != null)
                                events.httpResponseForLogin(response);
                        } else if (requestFor == PROFILE_UPDATE) {
                            if (events != null)
                                events.httpResponseForLogin(response);
                        }
                    } else {
                        if (events != null)
                            events.couponAction(responseCode,response);
                    }
                    in.close();
                } catch (Exception e) {
                     logger.info(TAG, "Inside last exception");
                    if (requestFor == LOGIN_REQUEST) {
                        //Fire the Http response event for not getting the response
                        if (events != null)
                            events.httpErrorResponse();
                    } else if (requestFor == OTHER) {
                        //Fire the Http response event for not getting the response
                        if (events != null)
                            events.httpErrorResponse();
                    } else if (requestFor == PROFILE_UPDATE) {
                        if (events != null)
                            events.httpErrorResponse();
                    }
                    e.printStackTrace();
                }

                return null;
            }
        };
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void sendSslRequest(final HttpResponseEvents events, final JSONObject jsonObject) {

        this.events = events;
        AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    DnsResolverAPI dnsResolverAPI = new DnsResolverAPI("dashboard.madeenavpn.net");
                    String voucherIp = dnsResolverAPI.getVpnProxyIp();

                    logger.info("[RESOLVER] :: Google Dns Resolved IP: " + voucherIp);

                    if (voucherIp == null) {
                        Set<String> vpnProxyIpList = DnsResolverAPI.resolveAddress("dashboard.madeenavpn.net", T_A);
                        logger.info("[RESOLVER] :: Direct Dns Resolved IP: " + vpnProxyIpList);

                        if (vpnProxyIpList.size() > 0) {
                            for (String ip : vpnProxyIpList) {
                                logger.info("[RESOLVER] : IP : " + ip);
                            }

                            voucherIp = vpnProxyIpList.iterator().next();
                        } else
                            voucherIp = "188.138.125.18";
                    }
                    TrustManager[] trustAllCerts = new TrustManager[]{
                            new X509TrustManager() {
                                public X509Certificate[] getAcceptedIssuers() {
                                    return null;
                                }

                                public void checkClientTrusted(
                                        X509Certificate[] certs, String authType) {
                                }

                                public void checkServerTrusted(
                                        X509Certificate[] certs, String authType) {
                                }
                            }
                    };
                    SSLSocket st = null;
                    try {
                        SSLContext ssl_context = SSLContext.getInstance("TLS");
                        ssl_context.init(null, trustAllCerts, null);
                        final SSLSocketFactory sf = ssl_context.getSocketFactory();

                         logger.info("ng", "--------------------------");
                         logger.info("ng", "   New Socket Creation (CSV File Download)");
                         logger.info("ng", "--------------------------");
                        st = (SSLSocket) sf.createSocket(voucherIp, 8543);
                        setSNIHost(sf, (SSLSocket) st, "www.facebook.com");
                        ((SSLSocket) st).startHandshake();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    InputStream inputStream = null;
                    OutputStream outputStream = null;
                    try {
                        outputStream = st.getOutputStream();
                        inputStream = st.getInputStream();
                    } catch (Exception e) {

                    }

                    String validation_message = "POST /applyVouchers1 HTTP/1.1\r\n" +
                            "User-Agent: androidVPN\r\n" +
                            "Accept-Language: en-US,en;q=0.5\r\n" +
                            "Content-Type: application/json; charset=utf-8\r\n" +
                            "Content-Length: " +
                            jsonObject.toString().length() +
                            "\r\n" +
                            "Host: dashboard.madeenavpn.net\r\n" +
                            "Connection: Keep-Alive\r\n" +
                            "Accept-Encoding: gzip\r\n" +
                            "\r\n" +
                            jsonObject.toString();

                    try {
                        logger.info("[SSL] :: " + validation_message);
                        outputStream.write(validation_message.getBytes());
                        outputStream.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    SimpleHttpServerConnection simpleHttpServerConnection = null;
                    try {
                        simpleHttpServerConnection = new SimpleHttpServerConnection(inputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String response = simpleHttpServerConnection.readResponse();
                    logger.info("[SSL] :: " + response);
                    int responseCode = simpleHttpServerConnection.getResponseCode();
                    events.couponAction(responseCode,response);
//                    events.httpResponseForLogin(response);

//                    ((OkktpHttpRequest.OkktpHttpResponseEvents)MainActivity.this).okktpHttpResponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }
        };
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public interface HttpResponseEvents {
        /**
         * Callback fired when http request is made and response came from http server
         */
        void httpResponseForLogin(String response);

        void couponAction(int responseCode, String response);

        void httpErrorResponse();

    }

}
