package com.jrk_plus.fragments;

import android.animation.ObjectAnimator;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import com.jrk_plus.Configuration.ClientConfig;
import com.jrk_plus.NewDashboard.ConnectActivity;
import com.jrk_plus.Provisioning.ServerConfig;
import com.jrk_plus.Provisioning.ServerDetails;
import com.jrk_plus.Tunnel.Config;
import com.jrk_plus.Tunnel.Constants;
import com.jrk_plus.Tunnel.LaunchVPN;
import com.jrk_plus.utilities.CredentialCheck;
import com.jrk_plus.utilities.MessageConstatns;
import com.jrk_plus.utilities.NetworkUtil;
import com.jrk_plus.utilities.PrefManager;
import com.app.jrk_plus.R;
import com.ksmaze.android.preference.ListPreferenceMultiSelect;

import org.sshtunnel.SSHTunnelService;
import org.sshtunnel.db.Profile;
import org.sshtunnel.utils.Constraints;
import org.sshtunnel.utils.Utils;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.core.ConfigParser;
import de.blinkt.openvpn.core.OpenVPNService;
import de.blinkt.openvpn.core.ProfileManager;
import de.blinkt.openvpn.core.VpnStatus;
import jrk_plus.hu.blint.ssldroid.TcpProxy;

import static opt.log.OmLogger.logger;

public class ServerListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private String TAG = ServerListFragment.class.getSimpleName();
    private ServerListAdapterOLD mAdapter;
    private RecyclerView serverListContainer;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView noServerText;
    ArrayList<ServerDetails> serversDetailList = new ArrayList<>();
    PrefManager prefManager;
    public int selectedPosition = -1;
    public int imageType = 0;   //0-hide,1-connect,2-disconnect
    public int connectedPosition = -1, prevSelectedPosition = -1;
    AsyncTask<Object, Integer, Integer> workerAsyncTask = null;
    Config config = new Config();
    TcpProxy tp;
    protected OpenVPNService mService;
    boolean mBound = false;
    private Random randomGenerator;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout layoutConnectLayout;
    private ProgressBar progressBarConnect;
    private Button buttonConnect;
    private int progress = 0;
    private TextView connectingText;
    public static Context context;

    ConnectActivity activity;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (ConnectActivity) getActivity();
        Intent intent = new Intent(activity, OpenVPNService.class);
        intent.setAction(OpenVPNService.START_SERVICE);
        activity.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        prefManager = new PrefManager(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_server_list_fragment, container, false);
    }

    private void setEventListner(final View view) {
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        serverListContainer = view.findViewById(R.id.list_view_server_list);
        layoutConnectLayout = view.findViewById(R.id.progress_layout);
        progressBarConnect = view.findViewById(R.id.ProgressBar_connect);
        connectingText = view.findViewById(R.id.tv_connect_text);
        buttonConnect = view.findViewById(R.id.bt_connect);
        buttonConnect.setVisibility(View.GONE);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(false);

   /*     buttonConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((Button) v).getText().toString().equalsIgnoreCase("Connect")) {
                    if (!prefManager.isValidityExpired() && !prefManager.isPasswordInvalid()) {
                        Log.d(TAG, "onClick: " + prefManager.isValidityExpired());
                        layoutConnectLayout.setVisibility(View.VISIBLE);
                        showCancelButton();
                        Log.d("ServerListFragment", "Server Detail" + serversDetailList.get(mAdapter.getItem()) + "Name ==" + serversDetailList.get(mAdapter.getItem()).getName());
                        startConnection(serversDetailList.get(mAdapter.getItem()));
                    } else {
                        if (prefManager.isValidityExpired()) {
                            Toast.makeText(getActivity(), "Your validity expired", Toast.LENGTH_LONG).show();
                        } else if (prefManager.isPasswordInvalid()) {
                            Toast.makeText(getActivity(), "Password has changed,please logout and login again with changed password", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Reattempt exceeded specified number", Toast.LENGTH_LONG).show();
                        }
                    }
                } else if (((Button) v).getText().toString().equalsIgnoreCase("Cancel")) {

                    buttonConnect.setEnabled(false);
                    buttonConnect.setAlpha(0.2f);
                    progress = 0;

                    connectingText.setText("Disconnecting ,please wait...");
                    connectingText.setTextColor(getResources().getColor(R.color.red));
                    progressBarConnect.setProgress(progress);
                    startTimer();
                    ((ConnectActivity) getActivity()).cancelConnection();
                    buttonConnect.setText(getString(R.string.connect));
                    connectingText.setText("");
                    layoutConnectLayout.setVisibility(View.INVISIBLE);
                }
            }
        });*/
    }

    public void startTimer() {

        Timer buttonTimer = new Timer();
        buttonTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                if(activity!=null) {
                    activity.runOnUiThread(() -> {
                        buttonConnect.setEnabled(true);
                        buttonConnect.setAlpha(1.0f);
                        progress = 0;

//                        connectingText.setText("Initializing connection ....");
                        connectingText.setTextColor(getResources().getColor(R.color.black));
                        progressBarConnect.setProgress(progress);

                    });
                }
            }
        }, 2000);
    }

    private void showCancelButton() {
        buttonConnect.setText("Cancel");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Intent intent = new Intent(getActivity(), OpenVPNService.class);
        intent.setAction(OpenVPNService.START_SERVICE);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        noServerText = (TextView) view.findViewById(R.id.no_server_text);

        if (serversDetailList.size() > 0) {
            noServerText.setVisibility(View.GONE);
        } else {
            noServerText.setVisibility(View.VISIBLE);
        }


        logger.info("ServerList", "On view created");
        setEventListner(view);
        setRecyclerView();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        logger.info(TAG, "onDestroy: ");
        stopVPN();
        if (tp != null)
            tp.stop();
        if (mConnection != null) {
            getActivity().unbindService(mConnection);
            mConnection = null;
        }
    }

    public void connectionState(final VpnStatus.ConnectionStatus status) {
        if (prefManager.getConnectionState() == ConnectActivity.VPN_CONNECTION_STATE.CONNECTED || prefManager.getConnectionState() == ConnectActivity.VPN_CONNECTION_STATE.CONNECTING) {

//            Log.d("ConnectionStatus","Inside if case");
            //For Easy parentServer changing status


            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    logger.info("CheckStatus", "Status ===" + status);
                    logger.info("CheckStatus", "vpn current state ===" + VpnStatus.currentState);
                    if (VpnStatus.currentState.equalsIgnoreCase("CONNECTING")) {
                        progress = 0;
//                        progressBarConnect.setProgress(0);
                    } else if (VpnStatus.currentState.equalsIgnoreCase("TCP_CONNECT")) {
                        progress = 15;
//                        progressBarConnect.setProgress(15);
                    } else if (VpnStatus.currentState.equalsIgnoreCase("WAIT")) {
                        progress = 30;
//                        progressBarConnect.setProgress(30);
                    } else if (VpnStatus.currentState.equalsIgnoreCase("AUTH")) {
                        progress = 45;
//                        progressBarConnect.setProgress(45);
                    } else if (VpnStatus.currentState.equalsIgnoreCase("GET_CONFIG")) {
                        progress = 60;
//                        progressBarConnect.setProgress(60);
                    } else if (VpnStatus.currentState.equalsIgnoreCase("ASSIGN_IP")) {
                        progress = 75;
//                        progressBarConnect.setProgress(75);
                    } else if (VpnStatus.currentState.equalsIgnoreCase("ADD_ROUTES")) {
                        progress = 90;
//                        progressBarConnect.setProgress(90);
                    } else if (VpnStatus.currentState.equalsIgnoreCase("CONNECTED")) {
                        progress = 100;
//                        progressBarConnect.setProgress(100);
                    } else if (VpnStatus.currentState.equalsIgnoreCase("Initializing")) {


                    }


//                    progressBarConnect.setProgress(progress);

                    if (connectingText != null)
                        connectingText.setText("Jrk PLUS VPN " + (VpnStatus.currentState.equalsIgnoreCase("RECONNECTING") ? "INITIALIZING" : VpnStatus.currentState) + "...");
                    logger.info("ConnectActivity", "Vpn status===" + VpnStatus.currentState);
                    if (VpnStatus.currentState.equalsIgnoreCase("CONNECTED")) {
                        logger.info("ConnectActivity", "Changing to connected from status");
                        progress = 0;
                        progressBarConnect.setProgress(progress);
                        buttonConnect.setText(getString(R.string.connect));
                        connectingText.setText("");
                        layoutConnectLayout.setVisibility(View.INVISIBLE);
                        buttonConnect.setEnabled(true);
                        ((ConnectActivity) getActivity()).connected("XYZ");
                    }
                    ObjectAnimator animation = ObjectAnimator.ofInt(progressBarConnect, "progress", progress);
                    animation.setDuration(500); // 0.5 second
                    animation.setInterpolator(new LinearInterpolator());
                    animation.start();
                }
            });
        } else {
            Log.d("ConnectionStatus", "Inside else case");
//            ((ConnectActivity) getActivity()).disconenctConnection(false);
        }
    }

    private void connected() {
        /*ConnectionFragment fragment = (ConnectionFragment) getActivity().getSupportFragmentManager().findFragmentByTag("CF");
        if (fragment != null) {
            fragment.connectingLayout.setVisibility(View.GONE);
            fragment.connectedLayout.setVisibility(View.VISIBLE);
            fragment.sessionTimeText.start();
        }*/
    }

    public void setRecyclerView() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                ServerConfig serverConfig = com.jrk_plus.Provisioning.Information.fetchServerConfig(getContext());
                if (serverConfig != null) {
                    logger.info("ServerList", "ServerConfig not null");

                    if (ConnectActivity.networkStatus == NetworkUtil.TYPE_WIFI) {
                        ArrayList<ServerDetails> localServerDetail = new ArrayList<>();
                        for (int i = 0; i < serverConfig.servers.size(); i++) {
                            localServerDetail.add(serverConfig.servers.get(i));
//                               if(!serverConfig.servers.get(i).getName().toUpperCase().contains("SOCIAL")){
//                                   localServerDetail.add(serverConfig.servers.get(i));
//                               }
                        }
                        serversDetailList = localServerDetail;
                    } else {
                        serversDetailList = serverConfig.servers;
                    }
                    logger.info("ServerList", "Server Detail size ==" + serversDetailList.size());


                }
                if (serversDetailList.size() > 0) {
                    noServerText.setVisibility(View.GONE);
                } else {
                    noServerText.setVisibility(View.VISIBLE);
                }
            }
        });

        if (mAdapter == null) {
            mLayoutManager = new LinearLayoutManager(getActivity());
            serverListContainer.setLayoutManager(mLayoutManager);
            serverListContainer.setItemAnimator(new DefaultItemAnimator());
            mAdapter = new ServerListAdapterOLD(getActivity(), serversDetailList);
            serverListContainer.setAdapter(mAdapter);

        } else {
            mAdapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onRefresh() {
        setRecyclerView();
    }


    public class ServerListAdapterOLD extends RecyclerView.Adapter<ServerListAdapterOLD.MyViewHolder> {
        private Context mContext;
        private ArrayList<ServerDetails> list;
        int selectedPos = -1;
        private Map<Integer, Boolean> mSelectedItem = new LinkedHashMap<>();

        public ServerListAdapterOLD(Context mContext, ArrayList<ServerDetails> list) {
            this.mContext = mContext;
            this.list = list;
            mSelectedItem.clear();
            for (int i = 0; i < list.size(); i++) {
                mSelectedItem.put(i, false);
            }

        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.server_list_item, parent, false);


            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {

            configureRow(holder, list.get(position), position);
        }

        @Override
        public int getItemCount() {
            return (list == null) ? 0 : list.size();
        }

        public int getItem() {
            for (int i = 0; i < mSelectedItem.size(); i++) {
                if (mSelectedItem.get(i)) {
                    return i;
                }
            }
            return 0;
        }

        private void configureRow(final MyViewHolder holder, final ServerDetails server, final int position) {
            //set values in row
            holder.getTvServerName().setText(server.getName());
            holder.getPositionText().setText(Integer.toString(position));

            holder.btnConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*for (int i = 0; i < mSelectedItem.size(); i++) {
                        if (mSelectedItem.get(i))
                            mSelectedItem.put(i, false);
                    }
                    mSelectedItem.put(position, true);*/

                    if (holder.btnConnect.getText().toString().equalsIgnoreCase("Connect")) {

                        if (!prefManager.isValidityExpired() && !prefManager.isPasswordInvalid()) {
                            progress = 0;
                            selectedPos = position;
                            notifyDataSetChanged();
                            Log.d(TAG, "onClick: " + prefManager.isValidityExpired());
                            layoutConnectLayout.setVisibility(View.VISIBLE);
//                            showCancelButton();
//
                            Log.d("ServerListFragment", "Server Detail" + serversDetailList.get(position) + "Name ==" + serversDetailList.get(position).getName());
                            startConnection(serversDetailList.get(position));
                        } else {
                            if (prefManager.isValidityExpired()) {
                                Toast.makeText(getActivity(), "Your validity expired", Toast.LENGTH_LONG).show();
                            } else if (prefManager.isPasswordInvalid()) {
                                Toast.makeText(getActivity(), "Password has changed,please logout and login again with changed password", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Reattempt exceeded specified number", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {

                        selectedPos = -1;
                        notifyDataSetChanged();

                        progress = 0;

                        connectingText.setText("Disconnecting ,please wait...");
                        connectingText.setTextColor(getResources().getColor(R.color.red));
                        progressBarConnect.setProgress(progress);
                        startTimer();
                        ((ConnectActivity) getActivity()).cancelConnection();

                        connectingText.setText("");
                        layoutConnectLayout.setVisibility(View.INVISIBLE);
                    }
                }
            });

            if (selectedPos == position) {
               /* holder.btnConnect.setEnabled(false);
                holder.btnConnect.setAlpha(0.2f);*/
                holder.tvServerName.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                holder.btnConnect.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                holder.btnConnect.setText(getString(R.string.cancel));
                holder.clickableRow.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.connected_layout_bg));
                holder.btnConnect.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.connected_layout_bg));
            } else {
                holder.tvServerName.setTextColor(ContextCompat.getColor(getActivity(), R.color.server_name_text_color));
                holder.btnConnect.setTextColor(ContextCompat.getColor(getActivity(), R.color.connect_text_color));
                holder.btnConnect.setText(getString(R.string.connect));
                holder.clickableRow.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.connect_layout_bg));
                holder.btnConnect.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.connect_btn_bg));

            }

            holder.connectImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    slideOut(holder.connectImage);
                    if (!prefManager.isValidityExpired() && !prefManager.isPasswordInvalid()) {
                        startConnection(serversDetailList.get(position));
                    } else {
                        if (prefManager.isValidityExpired()) {
                            Toast.makeText(getActivity(), "Your validity expired", Toast.LENGTH_LONG).show();
                        } else if (prefManager.isPasswordInvalid()) {
                            Toast.makeText(getActivity(), "Password has changed,please logout and login again with changed password", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Reattempt exceeded specified number", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });

//            System.out.println("arrayList:1= " + mSelectedItem.get(position));



         /*   holder.clickableRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    System.out.println("arrayList:11= " + mSelectedItem.size());

                    for (int i = 0; i < mSelectedItem.size(); i++) {
                        if (mSelectedItem.get(i))
                            mSelectedItem.put(i, false);
                    }

                    System.out.println("arrayList:2= " + mSelectedItem.size() + "," + mSelectedItem.get(position));

                    mSelectedItem.put(position, true);
                    System.out.println("arrayList:3= " + mSelectedItem.size() + "," + mSelectedItem.get(position));

                    notifyDataSetChanged();
                }
            });*/


            holder.connectDisconnectSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        if (!prefManager.isValidityExpired() && !prefManager.isPasswordInvalid()) {
                            startConnection(serversDetailList.get(position));
                        } else {
                            if (prefManager.isValidityExpired()) {
                                Toast.makeText(getActivity(), "Your validity expired", Toast.LENGTH_LONG).show();
                            } else if (prefManager.isPasswordInvalid()) {
                                Toast.makeText(getActivity(), "Password has changed,please logout and login again with changed password", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Reattempt exceeded specified number", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
//                        disconnectConnection(false);
//                        ((ConnectActivity)getActivity()).disConencted(false);
                    }
                }
            });

           /* holder.disconnectImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    disconnectConnection(false);
                    ((ConnectActivity) getActivity()).disConencted(false);
                }
            });*/
//            holder.clickableRow.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    if(prevSelectedPosition == -1){
//                        prevSelectedPosition = position;
//                    }else{
//
//                        if(prevSelectedPosition != selectedPosition) {
//                            ServerDetails prevServer = serversDetailList.get(prevSelectedPosition);
//                            prevServer.setSelected(false);
//                            prevSelectedPosition = selectedPosition;
//                        }
//                    }
//                    selectedPosition = position;
//                    ServerDetails serverDetails = serversDetailList.get(position);
//
//                    if (serverDetails != null && serverDetails.isSelected) {
//                        serverDetails.setSelected(false);
//                    } else if(serverDetails != null){
//                        serverDetails.setSelected(true);
//                    }
//
//                    serverListContainer.post(new Runnable()
//                    {
//                        @Override
//                        public void run() {
//                            mAdapter.notifyDataSetChanged();
//                        }
//                    });
////                    mAdapter.notifyDataSetChanged();
//                }
//            });
            if (prefManager.getConnectionWithServer().equalsIgnoreCase(holder.getTvServerName().getText().toString()) && prefManager.getConnectionState() == ConnectActivity.VPN_CONNECTION_STATE.CONNECTED) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        holder.getConnectedText().setVisibility(View.VISIBLE);
//                       holder.clickedRowSideLine.setBackgroundColor(getResources().getColor(R.color.transparent));
                        holder.rowContent.setBackgroundColor(getResources().getColor(R.color.transparent));
                        holder.connectDisconnectSwitch.setChecked(true);
                    }
                });

            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        holder.clickedRowSideLine.setBackgroundColor(getResources().getColor(R.color.transparent));
                        holder.rowContent.setBackgroundColor(getResources().getColor(R.color.transparent));
                        holder.getConnectedText().setVisibility(View.GONE);
                        holder.connectDisconnectSwitch.setChecked(false);
                    }
                });


            }

            if (position == selectedPosition) {
                if (server.isSelected) {
                    if (prefManager.getConnectionWithServer().equalsIgnoreCase(holder.getTvServerName().getText().toString())) {

                        slideOut(holder.connectImage);
                        slideIn(holder.disconnectImage);
                    } else {

                        slideOut(holder.disconnectImage);
                        slideIn(holder.connectImage);
                    }
                } else {
//                    holder.clickedRowSideLine.setBackgroundColor(getResources().getColor(R.color.white));
//                    holder.rowContent.setBackgroundColor(getResources().getColor(R.color.white));
                    slideOut(holder.disconnectImage);
                    slideOut(holder.connectImage);
                }
            } else {
//                holder.clickedRowSideLine.setBackgroundColor(getResources().getColor(R.color.white));
//                holder.rowContent.setBackgroundColor(getResources().getColor(R.color.white));
                slideOut(holder.disconnectImage);
                slideOut(holder.connectImage);
            }
        }

        public void notifyAdapter() {
          /*  ServerDetails selecServer = serversDetailList.get(selectedPos);
            selecServer.setSelected(false);*/
            selectedPos = -1;
            mAdapter.notifyDataSetChanged();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private TextView tvServerName, positionText, connectedText;
            private RelativeLayout clickableRow, rowContent;
            private ImageView connectImage, disconnectImage;
            private boolean isServerSelected = false;
            private LinearLayout clickedRowSideLine;
            private Switch connectDisconnectSwitch;
            private ImageView ivServerList;
            private Button btnConnect;

            public MyViewHolder(View view) {
                super(view);
                btnConnect = view.findViewById(R.id.btn_connect);
                tvServerName = view.findViewById(R.id.tv_server_name);
                positionText = view.findViewById(R.id.position_text);
                connectImage = view.findViewById(R.id.connect_button);
                disconnectImage = view.findViewById(R.id.disconnect_button);
                clickableRow = view.findViewById(R.id.clickable_row);
                connectedText = view.findViewById(R.id.connected_text);
//            clickedRowSideLine          = view.findViewById(R.id.clicked_row_side_line);
                rowContent = view.findViewById(R.id.row_content);
                connectDisconnectSwitch = view.findViewById(R.id.connect_disconnect_switch);
                ivServerList = view.findViewById(R.id.image_server_list);

                /*radioServerList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lastCheckedPosition = getAdapterPosition();
                        notifyDataSetChanged();
                    }
                });*/

            }

            //
            public TextView getTvServerName() {
                return tvServerName;
            }

            public TextView getPositionText() {
                return positionText;
            }

            public TextView getConnectedText() {
                return connectedText;
            }

            @Override
            public void onClick(View view) {

            }

        }
    }

    public String onlineSSID(Context context, String ssid) {
        String ssids[] = ListPreferenceMultiSelect.parseStoredValue(ssid);
        if (ssids == null)
            return null;
        if (ssids.length < 1)
            return null;
        ConnectivityManager manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo == null)
            return null;
        if (!networkInfo.getTypeName().equals("WIFI")) {
            for (String item : ssids) {
                if (item.equals(Constraints.WIFI_AND_3G))
                    return item;
                if (item.equals(Constraints.ONLY_3G))
                    return item;
            }
            return null;
        }
        WifiManager wm = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wm.getConnectionInfo();
        if (wInfo == null)
            return null;
        String current = wInfo.getSSID();
        if (current == null || current.equals(""))
            return null;
        for (String item : ssids) {
            if (item.equals(Constraints.WIFI_AND_3G))
                return item;
            if (item.equals(Constraints.ONLY_WIFI))
                return item;
            if (item.equals(current))
                return item;
        }
        return null;
    }

    public void onReceive(final Context context, final String host, final String pass, final String user, final int port) {

        Handler handler = new Handler(context.getMainLooper());

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences settings = PreferenceManager
                        .getDefaultSharedPreferences(context);

                if (SSHTunnelService.isConnecting
                        || SSHTunnelService.isStopping)
                    return;

                // only switching profiles when needed
                ConnectivityManager manager = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = manager.getActiveNetworkInfo();
                if (networkInfo == null) {
                    if (Utils.isWorked()) {
                        Log.d(TAG, "onConnectionLost");
                        context.stopService(new Intent(context, SSHTunnelService.class));
                        return;
                    }
                }

                //ProfileFactory.context = context;
               /* Context context = SSHTunnelContext.getAppContext();
                Profile profilenew = createprofile( host, pass, user);
                ProfileFactory p = new ProfileFactory(context);
                p.createProfilePactory();
              //  ProfileFactory p1 = new ProfileFactory();
                ProfileFactory.profile = profilenew;
                ProfileFactory.saveToPreference();
               // ProfileFactory.initProfile();
               // ProfileFactory.saveToDao(profilenew);

                // Save current settings first
                ProfileFactory.getProfile();
                ProfileFactory.loadFromPreference();

                String curSSID = null;
                List<Profile> profileList = ProfileFactory.loadAllProfilesFromDao();
                int profileId = -1;

                if (profileList == null)
                    return;

                // Test on each profile
                for (Profile profile : profileList) {
                    curSSID = onlineSSID(context, profile.getSsid());
                    if (profile.isAutoConnect() && curSSID != null) {
                        // Then switch profile values
                        profileId = profile.getId();
                        break;
                    }
                }

                //curSSID = profile.getSsid();
*/
//                if (curSSID != null && profileId != -1) {
//                    if (!Utils.isWorked()) {

                SharedPreferences.Editor ed = settings.edit();
                //  ed.putString("lastSSID", curSSID);
                ed.commit();

                // Utils.notifyConnect();
                Profile profilenew = createprofile(host, pass, user, port);
                SSHTunnelService.profile = profilenew;
                Intent it = new Intent(context, SSHTunnelService.class);
                Bundle bundle = new Bundle();
                bundle.putInt(Constraints.ID, profilenew.getId());
                it.putExtras(bundle);
                context.startService(it);
                SSHTunnelService.isConnecting = true;
//                    }
//                }
            }
        }, 2000);
    }

    public Profile createprofile(String host, String pass, String user, int port) {
        Profile p = new Profile("current");
        p.setHost(host);
        p.setUser(user);
        p.setPassword(pass);
        p.setSocks(true);
        p.setSsid("wifi");
        p.setId(100);
        p.setPort(port);
        return p;
    }


    /*public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvServerName,positionText,connectedText;
        private RelativeLayout clickableRow,rowContent;
        private ImageView connectImage,disconnectImage;
        private boolean isServerSelected = false;
        private LinearLayout clickedRowSideLine;
        private Switch connectDisconnectSwitch;
        private CheckBox radioServerList;

        public MyViewHolder(View view) {
            super(view);
            tvServerName                = view.findViewById(R.id.tv_server_name);
            positionText                = view.findViewById(R.id.position_text);
            connectImage                = view.findViewById(R.id.connect_button);
            disconnectImage             = view.findViewById(R.id.disconnect_button);
            clickableRow                = view.findViewById(R.id.clickable_row);
            connectedText               = view.findViewById(R.id.connected_text);
//            clickedRowSideLine          = view.findViewById(R.id.clicked_row_side_line);
            rowContent                  = view.findViewById(R.id.row_content);
            connectDisconnectSwitch     = view.findViewById(R.id.connect_disconnect_switch);
            radioServerList = view.findViewById(R.id.checkbox_server_list);

            radioServerList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
//
        public TextView getTvServerName() {
            return tvServerName;
        }

        public TextView getPositionText() {
            return positionText;
        }

        public TextView getConnectedText() {
            return connectedText;
        }

        @Override
        public void onClick(View view) {

        }

    }*/

    public void slideIn(View view) {
//
//        TranslateAnimation animate = new TranslateAnimation(
//                view.getWidth(),                 // fromXDelta
//                0,                 // toXDelta
//                0,  // fromYDelta
//                0);                // toYDelta
//        animate.setDuration(500);
//        animate.setFillAfter(true);
//        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);
    }

    // slide the view from its current position to below itself
    public void slideOut(View view) {

//        TranslateAnimation animate = new TranslateAnimation(
//                0,                 // fromXDelta
//                view.getWidth(),                 // toXDelta
//                0,                 // fromYDelta
//                0); // toYDelta
//        animate.setDuration(500);
//        animate.setFillAfter(true);
//        view.startAnimation(animate);
        view.setVisibility(View.INVISIBLE);
    }

    public void startConnection(ServerDetails serverDetails) {

//        System.out.println("serverName:-" + serverDetails.getName());

        if (serverDetails.getName().contains("1234567890")) {

            //change by saquib
            String host = "85.25.43.72";
            String pass = "h8Triag0#2kT3n2liFe}=Zm";
            String user = "root";
            onReceive(getContext(), host, pass, user, 22);

        } else {

            disconnectConnection(true);  //First disconnect existing connection if it exist
//        ((ConnectActivity)getActivity()).disconenctConnection();
            if (serverDetails.getHostaddress() != null) {
                String host = serverDetails.getHostaddress();//"85.25.43.72";
                String pass = serverDetails.getHostpassword().trim();//"h8Triag0#2kT3n2liFe}=Zm";
                String user = serverDetails.getHostuser();//"root";
                int port = serverDetails.getHostport();
                logger.info("HomeFragment serverDetails : ", host, " : ", pass, " :", user, ": ", port);
                onReceive(getContext(), host, pass, user, port);
            }
            prefManager.setConnectionWithServer(serverDetails.getName());
            SelectedServer selectedServer = new SelectedServer(serverDetails.getIp(), serverDetails.getPort(), serverDetails.getProtocol(), serverDetails.getSniList(), serverDetails.getCertificate());

            Log.d(TAG, "startConnection: ServerDetails:- " + serverDetails.getIp() + " port:- " + serverDetails.getPort() +
                    " sniList:- " + serverDetails.getSniList() + " Protocol:- " + serverDetails.getProtocol());

            createWorkerAsyncTask(selectedServer, getActivity(), CredentialCheck.RequestType.SELECTED_SERVER);
            prefManager.setConnectionState(ConnectActivity.VPN_CONNECTION_STATE.CONNECTING);
            ((ConnectActivity) getActivity()).connecting(serverDetails.getName(), 0);
        }
    }

    private void stopVPN() {
        logger.info("HomeFragment", "Stopping Vpn");
        try {
            ProfileManager.setConntectedVpnProfileDisconnected(getActivity());
            if (mService.getManagement() != null) {
                mService.getManagement().stopVPN(false);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void disconnectConnection(final boolean isConnecting) {
//        System.out.println("Saquib:- onDisconnetConnection-" + isConnecting);
        if (workerAsyncTask != null)
            workerAsyncTask.cancel(true);

        if (prevSelectedPosition != -1) {
            ServerDetails prevServer = serversDetailList.get(prevSelectedPosition);
            prevServer.setSelected(false);
        }
        if (selectedPosition != -1) {
            ServerDetails selecServer = serversDetailList.get(selectedPosition);
            selecServer.setSelected(false);
        }
        prevSelectedPosition = -1;
        selectedPosition = -1;
        prefManager.setConnectionState(ConnectActivity.VPN_CONNECTION_STATE.NOT_CONNECTED);
        prefManager.setConnectionWithServer("");
        selectedPosition = -1;
        imageType = 0;
        serverListContainer.post(new Runnable() {
            @Override
            public void run() {
                //change by saquib
                if (isConnecting)
                    mAdapter.notifyDataSetChanged();
                else {
                    activity.setToolBarVisibility(true);
                    mAdapter.notifyAdapter();
                }
            }
        });
//        mAdapter.notifyDataSetChanged();
        stopVPN();
        if (tp != null)
            tp.stop();

        if (isConnecting)
            prefManager.setConnectionState(ConnectActivity.VPN_CONNECTION_STATE.CONNECTING);

    }

    public void connectionConnected() {
        logger.info("TESTINGG onConnectionConnected");
        if (prevSelectedPosition != -1) {
            ServerDetails prevServer = serversDetailList.get(prevSelectedPosition);
            prevServer.setSelected(false);
        }
        if (selectedPosition != -1) {
            ServerDetails selecServer = serversDetailList.get(selectedPosition);
            selecServer.setSelected(false);
        }
        prevSelectedPosition = -1;
        selectedPosition = -1;
        serverListContainer.post(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
//        mAdapter.notifyDataSetChanged();

    }

    public void executeTask(SelectedServer selectedServer, Context ctx,
                            CredentialCheck.RequestType requestType) {

        if (requestType == CredentialCheck.RequestType.SELECTED_SERVER) {
            Config config = new Config();
            config.setLocalIp("0.0.0.0");
            randomGenerator = new Random();
            int low = 1;
            int high = 20;
            int result = randomGenerator.nextInt(high - low) + low;

            config.setLocalPort(1195 + result);

            config.setClientChildCount(1);
            config.setClientMppc(20000);
            config.setRemoteIp(selectedServer.ip);
            config.setRemotePort(selectedServer.port);
            config.setSni((String) selectedServer.sniList.get(0));

            String tunnelName = "testVpn";
            String keyFile = "";
            String keyPass = "";
            if (!(selectedServer.protocol == MessageConstatns.DIRECT_UDP || selectedServer.protocol == MessageConstatns.DIRECT_TCP)) {
                logger.info(TAG, "InLocalTunnel");
                if (tp != null) {
                    logger.info("[TUNNEL] :: Stopping previous tunnel...");
                    tp.stop();
                }
                logger.info("[TUNNEL] :: About to start tunnel...");
                logger.info("[TUNNEL] : " + config.getRemoteIp());
                logger.info("[TUNNEL] : " + config.getRemotePort());
                logger.info("[TUNNEL] : " + config.getSni());
                tp = new TcpProxy(tunnelName, config.getLocalPort(), config.getRemoteIp(),
                        config.getRemotePort(), keyFile, keyPass, config.getSni(), ctx);
                try {
                    tp.serve();
                } catch (Exception e) {
                    logger.debug("HomeActivity", "Exception ===" + Log.getStackTraceString(e));
                }
            }
            configureAndStartVpn(selectedServer, config, ctx, selectedServer.cert);
        }
    }

    private void createWorkerAsyncTask(final SelectedServer selectedServer, final Context ctx, final CredentialCheck.RequestType requestType) {

        if (workerAsyncTask == null) {
            workerAsyncTask = new AsyncTask<Object, Integer, Integer>() {

                @Override
                protected void onCancelled(Integer integer) {
                    super.onCancelled(integer);
                    Log.d("AsynkTask", "Oncancelled ==" + integer);
                }

                @Override
                protected void onCancelled() {
                    super.onCancelled();
                    Log.e("AsynkTask", "Oncancelled ==");
                }

                @Override
                protected Integer doInBackground(Object[] objects) {
                    logger.debug("WorkerAsynk", "Thread Start Running");
                    executeTask(selectedServer, ctx, requestType);
                    return null;
                }

            };
            workerAsyncTask.execute();
        } else {
            workerAsyncTask.cancel(true);
            logger.debug("WorkerAsynk", "Thread isCancelled==" + workerAsyncTask.isCancelled());
            workerAsyncTask = null;
            createWorkerAsyncTask(selectedServer, ctx, requestType);

        }
    }

    private void configureAndStartVpn(SelectedServer selectedServer, Config config, Context ctx, String cert) {

        logger.info("[TUNNEL] :: VPN Configure");
        try {
            if (selectedServer.protocol == MessageConstatns.DIRECT_TCP) {
                ClientConfig clientConfig = new ClientConfig();
                clientConfig.setClientName("client");
                clientConfig.setProtocol("tcp");
                clientConfig.setLocalPort(selectedServer.port);
//                clientConfig.setExcludedRoutes("88.150.226.228");
                clientConfig.setCipherMethod("AES-256-CBC");
                clientConfig.setCompression(true);
                clientConfig.setReplay(true);
                clientConfig.setVerbLevel(3);
                clientConfig.setIp(selectedServer.ip);
                clientConfig.setSubnet("255.255.255.0");
//                clientConfig.setLocalPort(config.getRemotePort());
                clientConfig.setExcludedRoutes(config.getRemoteIp());
                clientConfig.setCipherMethod("AES-128-CBC");
                clientConfig.setCompression(false);
                clientConfig.setReplay(false);

                StringBuilder sb = clientConfig.createConfig(ctx, cert);
//            logger.info(sb.toString());
                Log.d(TAG, "configureAndStartVpn: InTcp");
                String retVal = sb.toString();
                logger.debug("HomeActivity", "ng == " + retVal);

                if (retVal != null && retVal.trim().length() > 0) {

                    byte[] buffer = retVal.getBytes();

                    VpnProfile vp = saveProfile(buffer, ctx);

                    if (vp != null) {
                        Log.d(TAG, "configureAndStartVpn: VpProfile is not null!");
                        startVPN(vp, ctx);
                    }
                } else {
                    startVPN(ctx);
                }
            } else if (selectedServer.protocol == MessageConstatns.DIRECT_UDP) {
                ClientConfig clientConfig = new ClientConfig();
                clientConfig.setClientName("client");
                clientConfig.setProtocol("udp");
                clientConfig.setLocalPort(selectedServer.port);
                clientConfig.setCipherMethod("AES-256-CBC");
                clientConfig.setCompression(true);
                clientConfig.setReplay(true);
                clientConfig.setVerbLevel(3);
                clientConfig.setSubnet("255.255.255.0");
                clientConfig.setIp(selectedServer.ip);
                clientConfig.setExcludedRoutes(config.getRemoteIp());
                clientConfig.setCipherMethod("AES-128-CBC");
                clientConfig.setCompression(false);
                clientConfig.setReplay(false);

                StringBuilder sb = clientConfig.createConfig(ctx, cert);
//            logger.info(sb.toString());
                String retVal = sb.toString();
                logger.debug("HomeActivity", "ng ==" + retVal);

                if (retVal != null && retVal.trim().length() > 0) {

                    byte[] buffer = retVal.getBytes();

                    VpnProfile vp = saveProfile(buffer, ctx);

                    if (vp != null) {
                        Log.d(TAG, "configureAndStartVpn: VpProfile is not null!");
                        startVPN(vp, ctx);
                    }
                } else {
                    startVPN(ctx);
                }
            } else {

                ClientConfig clientConfig = new ClientConfig();
//                clientConfig.setClientName("client");
//                clientConfig.setProtocol("tcp");
//                clientConfig.setLocalPort(1195);
//                clientConfig.setCipherMethod("AES-256-CBC");
//                clientConfig.setSubnet("255.255.255.255");
//                clientConfig.setCompression(true);
//                clientConfig.setReplay(true);
//                clientConfig.setVerbLevel(3);
                clientConfig.setIp("127.0.0.1");
                clientConfig.setLocalPort(config.getLocalPort());
//                clientConfig.setExcludedRoutes(config.getRemoteIp());
//                clientConfig.setCipherMethod("AES-128-CBC");
//                clientConfig.setCompression(false);
//                clientConfig.setReplay(false);

                StringBuilder sb = new StringBuilder();//= clientConfig.createConfig(ctx,cert);
                sb.append("remote " + clientConfig.getIp() + " " + clientConfig.getLocalPort() + "\n");
                sb.append(cert);
//            logger.info(sb.toString());
                String retVal = sb.toString();
                logger.debug("HomeActivity", "ng ==" + retVal);

                if (retVal != null && retVal.trim().length() > 0) {

                    byte[] buffer = retVal.getBytes();

                    VpnProfile vp = saveProfile(buffer, ctx);

                    if (vp != null) {
                        Log.d(TAG, "configureAndStartVpn: VpProfile is not null!");
                        startVPN(vp, ctx);
                    }
                } else {
                    startVPN(ctx);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void startVPN(VpnProfile vp, Context ctx) {
        Intent intent = new Intent(ctx, LaunchVPN.class);
        intent.putExtra(LaunchVPN.EXTRA_KEY, vp.getUUID().toString());
        intent.setAction(Intent.ACTION_MAIN);
        ctx.startActivity(intent);
    }

    private void startVPN(Context ctx) {

        ProfileManager pm = ProfileManager.getInstance(ctx);
        VpnProfile profile = pm.getProfileByName(Constants.VPN_PROFILE_NAME);

        if (profile == null) {
            int duration = Toast.LENGTH_LONG;
            return;
        }

        Intent intent = new Intent(ctx, LaunchVPN.class);
        intent.putExtra(LaunchVPN.EXTRA_KEY, profile.getUUID().toString());
        intent.setAction(Intent.ACTION_MAIN);
        ctx.startActivity(intent);
    }

    private VpnProfile saveProfile(byte[] data, Context ctx) {

        ConfigParser cp = new ConfigParser();
        try {

            InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(data));

            cp.parseConfig(isr);
            VpnProfile vp = cp.convertProfile();

            ProfileManager vpl = ProfileManager.getInstance(ctx);

            vp.mName = Constants.VPN_PROFILE_NAME;
            vpl.addProfile(vp);
            vpl.saveProfile(ctx, vp);
            vpl.saveProfileList(ctx);

            return vp;
        } catch (Exception e) {
            return null;
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            OpenVPNService.LocalBinder binder = (OpenVPNService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            mBound = false;
        }
    };

    public void startQuickConnection() {
        if (serversDetailList.size() > 0) {
            randomGenerator = new Random();
            int index = randomGenerator.nextInt(serversDetailList.size());
            startConnection(serversDetailList.get(index));
        } else {
            Toast.makeText(getActivity(), "Server list is empty", Toast.LENGTH_LONG).show();
        }
    }

    public void gotProvisiongResponse() {
        Log.e("ServerList", "Setting recycle view");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (serversDetailList.size() > 0) {
                    noServerText.setVisibility(View.GONE);
                } else {
                    noServerText.setVisibility(View.VISIBLE);
                }
                setRecyclerView();
                serverListContainer.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
//                mAdapter.notifyDataSetChanged();
            }
        });
    }

}
